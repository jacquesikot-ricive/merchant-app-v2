import "react-native-gesture-handler";
import "expo-dev-client";
import { StatusBar } from "expo-status-bar";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Provider } from "react-redux";
import Toast from "react-native-toast-message";
import { QueryClient, QueryClientProvider } from "react-query";
import {
  createClient,
  AnalyticsProvider,
} from "@segment/analytics-react-native";

import useCachedResources from "./src/hooks/useCachedResources";
import Navigation from "./src/navigation";
import { ThemeProvider } from "@shopify/restyle";
import theme from "./src/components/Themed";
import store from "./src/redux/store";
import OfflineNotice from "./src/components/OfflineNotice";
import { BottomSheetModalProvider } from "@gorhom/bottom-sheet";

export default function App(): JSX.Element | null {
  const isLoadingComplete = useCachedResources();
  const queryClient = new QueryClient();

  const segmentClient = createClient({
    writeKey: "v0pZ2LWxbySolxf5gilXdI7wzKsC5Ps3",
    trackAppLifecycleEvents: true,
  });

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <AnalyticsProvider client={segmentClient}>
        <Provider store={store}>
          <QueryClientProvider client={queryClient}>
            <ThemeProvider theme={theme}>
              <SafeAreaProvider>
                <BottomSheetModalProvider>
                  <OfflineNotice />
                  <Navigation />
                  <StatusBar style="dark" />
                  <Toast />
                </BottomSheetModalProvider>
              </SafeAreaProvider>
            </ThemeProvider>
          </QueryClientProvider>
        </Provider>
      </AnalyticsProvider>
    );
  }
}

import axios from 'axios';

const client = 'http://dev-api-ricive.us-east-1.elasticbeanstalk.com/api/';

export interface SignInProps {
  username: string;
  password: string;
}

export type UserAuthRole = 'SUPERADMIN' | 'ADMIN' | 'TEAM' | 'DELIVERY';
export type BusinessType = 'LAUNDRY' | 'OTHER';

export interface SignUpProps {
  // Auth data
  role?: UserAuthRole;
  password?: string;
  email: string;
  username?: string;
  // Profile data
  first_name: string;
  last_name: string;
  business?: string;
}

export interface ForgotPasswordProps {
  email: string;
}

const signin = async (auth: SignInProps) => {
  const res = await fetch(
    'http://dev-api-ricive.us-east-1.elasticbeanstalk.com/api/user/signin',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(auth),
    }
  );

  const response = await res.json();

  return response;
};

const signup = async (auth: SignUpProps) => {
  const res = await fetch(
    'http://dev-api-ricive.us-east-1.elasticbeanstalk.com/api/user/signup',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(auth),
    }
  );

  const response = await res.json();

  return response;
};


const forgotPassword = async (auth: ForgotPasswordProps) => {
  const res = await fetch(
    'http://dev.riciveapi.com/api/user/forgot-password',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(auth),
    }
  );
  const response = await res.json();

  return response;
}


export default {
  signin,
  signup,
  forgotPassword
};

import supabase from '../../../supabase';
import db from '../db';

export interface LoginMerchatnAdminProps {
  email: string;
  password: string;
}

const loginMerchantAdmin = async (data: LoginMerchatnAdminProps) => {
  const signInResponse = await supabase.auth.signIn({
    email: data.email,
    password: data.password,
  });

  if (signInResponse.user === null) {
    return {
      error: true,
      message: signInResponse.error?.message,
    };
  }

  if (signInResponse.error) {
    throw signInResponse.error.message;
  }

  const profile = await supabase
    .from(db.tables.merchant_user)
    .select()
    .eq('auth_id', signInResponse.user?.id);

  if (profile.error) {
    throw profile.error.message;
  }

  if (profile.body) return profile.body[0];
};

export default {
  loginMerchantAdmin,
};

import supabase from '../../../supabase';
import db from '../db';

export interface RegisterMerchantProps {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  storeName: string;
  address: string;
  password: string;
}

const registerMerchant = async (data: RegisterMerchantProps) => {
  const merchant = await supabase.auth.signUp({
    email: data.email,
    password: data.password,
  });

  if (merchant.error) {
    throw merchant.error.message;
  }

  if (!merchant.error) {
    const addMerchantProfile = await supabase
      .from(db.tables.merchant_user)
      .insert([
        {
          first_name: data.firstName,
          last_name: data.lastName,
          email: data.email,
          phone: data.phone,
          store_name: data.storeName,
          last_sign_in: merchant.user?.last_sign_in_at,
          auth_id: merchant.user?.id,
          updated_at: new Date().toISOString(),
          role: 'SUPER_ADMIN',
        },
      ]);

    await supabase.from(db.tables.address).insert([
      {
        //   address_area: 1,
        updated_at: new Date().toISOString(),
        address_type: 'hybrid',
        user_id: addMerchantProfile.body && addMerchantProfile.body[0].id,
        address_name: data.address,
        //   landmark: 'some landmark',
      },
    ]);
  }
};

export default {
  registerMerchant,
};

import supabase from '../../../supabase';
import db from '../db';

interface NewCustomerProps {
  name: string;
  phone: string;
  email?: string;
  merchantId: number;
  address?: string;
}

const getMerchantCustomers = async (merchantId: number) => {
  const res = await supabase
    .from(db.tables.merchant_customer)
    .select()
    .eq('merchant_id', merchantId);

  if (res.error) {
    throw res.error;
  }

  if (res.body) {
    return res.body;
  }
};

const createCustomer = async (data: NewCustomerProps) => {
  const res = await supabase.from(db.tables.merchant_customer).insert([
    {
      merchant_id: data.merchantId,
      name: data.name,
      email: data.email,
      phone: data.phone,
      updated_at: new Date().toISOString(),
    },
  ]);

  if (!res.error) {
    const res2 = await supabase
      .from(db.tables.merchant_customer_address)
      .insert([
        {
          address_name: data.address,
          updated_at: new Date().toISOString(),
          address_type: 'hybrid',
          user_id: res.body[0].id,
        },
      ]);

    if (res2.error) {
      throw res2.error;
    }

    return res.body[0];
  }

  if (res.error) {
    throw res.error;
  }
};

const getCustomerAddress = async (customerId: number) => {
  const res = await supabase
    .from(db.tables.merchant_customer_address)
    .select()
    .eq('user_id', customerId);

  if (res.error) {
    throw res.error;
  }

  if (res.body) {
    return res.body[0];
  }
};

interface NewCustomerAddressProps {
  address_name: string;
  user_id: number;
}

const addCustomerAddress = async (data: NewCustomerAddressProps) => {
  const res = await supabase.from(db.tables.merchant_customer_address).insert([
    {
      address_name: data.address_name,
      updated_at: new Date().toISOString(),
      address_type: 'hybrid',
      user_id: data.user_id,
    },
  ]);

  if (res.error) {
    throw res.error;
  }

  if (res.body) {
    return res.body[0];
  }
};

const getCustomerById = async (customer_id: number) => {
  const res1 = await supabase
    .from(db.tables.merchant_customer)
    .select()
    .eq('id', customer_id);

  if (res1.error) {
    throw res1.error;
  }

  const res2 = await supabase
    .from(db.tables.merchant_customer_address)
    .select()
    .eq('user_id', customer_id);

  if (res2.error) {
    throw res2.error;
  }

  return {
    ...res1.body[0],
    address: res2.body[0].address_name,
  };
};

export default {
  getMerchantCustomers,
  createCustomer,
  getCustomerAddress,
  addCustomerAddress,
  getCustomerById,
};

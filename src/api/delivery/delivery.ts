import supabase from '../../../supabase';
import db from '../db';

interface CreateDeliveryProps {
  order: number;
  merchant: number;
  user: number;
  fulfilDate: string;
  fulfilTime: string;
  type: 'delivery' | 'pickup';
}

const getMerchantDeliveries = async (id: number) => {
  const res = await supabase
    .from(db.tables.merchant_delivery)
    .select(
      `*, order(*, delivery_address(*), pick_up_address(*)), merchant(*), user(*)`
    )
    .eq('merchant', id);

  if (res.error) throw res.error;

  if (res) {
    return res.body;
  }
};

const createMerchantDelivery = async (data: CreateDeliveryProps) => {
  const res = await supabase.from(db.tables.merchant_delivery).insert([
    {
      order: data.order,
      merchant: data.merchant,
      user: data.user,
      updated_at: new Date().toISOString(),
      type: data.type,
      fulfil_date: data.fulfilDate,
      fulfil_time: data.fulfilTime,
      delete_key: data.order,
    },
  ]);

  if (res.error) throw res.error;
};

export default {
  getMerchantDeliveries,
  createMerchantDelivery,
};

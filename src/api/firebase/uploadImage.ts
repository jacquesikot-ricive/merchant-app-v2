// import {
//   ref as FBRef,
//   uploadBytesResumable,
//   getStorage,
//   getDownloadURL,
// } from 'firebase/storage';
// // import firebaseInit from './config';

// const storage = getStorage();

// const uploadImage = async (uri: string | undefined) => {
//   if (uri === undefined) return '0';
//   if (uri === '0') return '0';
//   // Implement a new Blob promise with XMLHTTPRequest
//   const blob: Blob = await new Promise((resolve, reject) => {
//     const xhr = new XMLHttpRequest();
//     xhr.onload = () => {
//       resolve(xhr.response);
//     };
//     xhr.onerror = () => {
//       reject(new TypeError('Network request failed'));
//     };
//     xhr.responseType = 'blob';
//     xhr.open('GET', uri, true);
//     xhr.send(null);
//   });

//   // Create a ref in Firebase (I'm using a random number)
//   const ref = FBRef(storage, `ricivemerchantlogo/${Math.random()}`);

//   // Upload blob to Firebase
//   const uploadTask: any = uploadBytesResumable(ref, blob).then((snapshot) => {
//     console.log('Uploaded a blob or file!:', snapshot.metadata.fullPath);
//   });

//   // Create a download URL
//   uploadTask.on(
//     'state_changed',
//     (snapshot: any) => {
//       // Observe state change events such as progress, pause, and resume
//       // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
//       const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
//       console.log('Upload is ' + progress + '% done');
//       switch (snapshot.state) {
//         case 'paused':
//           console.log('Upload is paused');
//           break;
//         case 'running':
//           console.log('Upload is running');
//           break;
//       }
//     },
//     (error: any) => {
//       // Handle unsuccessful uploads
//     },
//     () => {
//       // Handle successful uploads on complete
//       // For instance, get the download URL: https://firebasestorage.googleapis.com/...
//       getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
//         console.log('File available at', downloadURL);
//         // Return the URL
//         return downloadURL;
//       });
//     }
//   );
// };

// export default uploadImage;

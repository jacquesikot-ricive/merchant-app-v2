import supabase from '../../../supabase';
import db from '../db';

const getBalance = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_balance)
    .select()
    .eq('merchant', merchant_id);

  if (res.error) {
    throw res.error;
  }

  return res.body[0];
};

interface CreateAccountData {
  pin: string;
  merchant_id: number;
}

const createAccount = async (data: CreateAccountData) => {
  const res = await supabase.from(db.tables.merchant_balance).insert([
    {
      updated_at: new Date().toISOString(),
      merchant: data.merchant_id,
      pin: data.pin,
    },
  ]);

  if (res.error) {
    throw res.error;
  }
};

export default {
  getBalance,
  createAccount,
};

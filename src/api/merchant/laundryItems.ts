import supabase from '../../../supabase';
import db from '../db';

export interface PriceItem {
  id: number; // Id of the service type for that laundry item
  amount: string; // how much the laundry item costs with this particular service type
}

export interface AddLaundryItemProps {
  merchantId: string;
  name: string;
  description: string;
  // Each laundry item has a different price for each service type the merchant offers
  price: PriceItem[];
}

const getLaundryItems = async (service_type: number) => {
  const res = await supabase
    .from(db.tables.laundry_item_price)
    .select(`*, item(*)`)
    .eq('service_type', service_type);

  if (res.error) {
    throw res.error;
  }

  if (res.body) {
    return res.body;
  }
};

const getLaundryItemsByMerchant = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.laundry_item)
    .select()
    .eq('merchant_id', merchant_id);

  if (res.error) {
    throw res.error;
  }

  return res.body;
};

/*
Ideally to implement this, you will need to fetch service types first,
then for each service type, return a price input collecting the amount
a laundry item will cost for that service type, for example, a merchant may have 2 service types
Wash & Iron, Wash & Fold. Each laundry item will have two prices, one for each service type.

NOTE: If a user does not enter a price for a particular service type, it is assumed that the 
item is not available for that service type, for example, using the same 2 services above, if
a laundry item, like a suit does not offer wash & fold services, the user will enter only price
for wash & iron and we would send only that in the PriceItem array.

SAMPLE DATA
 {
    name: 'Jacket',
    description: 'Heavy Jacket',
    price: [
      {
        id: 13,
        amount: '1000',
      },
      {
        id: 15,
        amount: '1700',
      },
    ],
  },
*/
const addLaundryItem = async (data: AddLaundryItemProps) => {
  const laundryItem = await supabase.from(db.tables.laundry_item).insert([
    {
      merchant_id: data.merchantId,
      name: data.name,
      description: data.description,
      updated_at: new Date().toISOString(),
    },
  ]);

  if (laundryItem.body) {
    data.price.map(async (priceItem: any) => {
      await supabase.from(db.tables.laundry_item_price).insert([
        {
          price: priceItem.amount,
          service_type: priceItem.id,
          item: laundryItem.body[0].id,
          merchant: data.merchantId,
        },
      ]);
    });
  }
};

export default { getLaundryItems, addLaundryItem, getLaundryItemsByMerchant };

import supabase from '../../../supabase';
import db from '../db';

export interface AddServiceProps {
  name: string;
  merchantId: number; // ID from - const merchantId = useAppSelector(state => state.login.id)
  description: string;
}

const getServiceType = async (merchantId: number) => {
  const res = await supabase
    .from(db.tables.laundry_service_type)
    .select()
    .eq('merchant_id', merchantId);

  if (res.error) {
    throw res.error;
  }

  if (res.body) {
    return res.body;
  }
};

const addServiceType = async (data: AddServiceProps) => {
  const res = await supabase.from(db.tables.laundry_service_type).insert([
    {
      updated_at: new Date().toISOString(),
      name: data.name,
      merchant_id: data.merchantId,
      description: data.description,
    },
  ]);

  if (res.error) {
    throw res.error;
  }
};

export default {
  getServiceType,
  addServiceType,
};

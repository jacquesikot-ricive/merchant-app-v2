import supabase from '../../../supabase';
import db from '../db';

interface NewStoreFront {
  domain: string;
  merchant: number;
  primary_color?: string;
  secondary_color?: string;
  tertiary_color?: string;
  logo_url?: string;
  tagline: string;
}

const createStoreFont = async (data: NewStoreFront) => {
  const res = await supabase.from(db.tables.storefront).insert([
    {
      updated_at: new Date().toISOString(),
      domain: data.domain,
      merchant: data.merchant,
      primary_color: data.primary_color,
      secondary_color: data.secondary_color,
      tertiary_color: data.tertiary_color,
      logo_url: data.logo_url,
      tagline: data.tagline,
    },
  ]);
  if (res.error) {
    throw res.error;
  }
};
const getStoreFront = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.storefront)
    .select()
    .eq('merchant', merchant_id);

  if (res.error) {
    throw res.error;
  }
  return res.body[0];
};

interface UpdateStoreFrontProps {
  domain: string;
  tagline: string;
  id: number;
}

const updateStoreFront = async (data: UpdateStoreFrontProps) => {
  const res = await supabase
    .from(db.tables.storefront)
    .update({
      updated_at: new Date().toISOString(),
      domain: data.domain,
      tagline: data.tagline,
    })
    .match({ id: data.id });

  if (res.error) {
    throw res.error;
  }
};

export default {
  createStoreFont,
  getStoreFront,
  updateStoreFront,
};

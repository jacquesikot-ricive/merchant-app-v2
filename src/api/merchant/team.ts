import supabase from '../../../supabase';
import db from '../db';
import sendSms from '../riciveApi/sendSms';

export interface NewTeamProps {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  role: 'Admin' | 'Sorter' | 'Rider';
  merchant: number;
}

export interface LoginTeamProps {
  phone: string;
  passcode: string;
}

const addTeamMember = async (data: NewTeamProps) => {
  const passcode = Math.floor(1000 + Math.random() * 9000);

  await supabase.from(db.tables.merchant_team).insert([
    {
      first_name: data.firstName,
      last_name: data.lastName,
      email: data.email,
      phone: data.phone,
      role: data.role,
      merchant: data.merchant,
      updated_at: new Date().toISOString(),
      passcode,
    },
  ]);

  await sendSms({
    message: `Your team mate has invited you to join them on Ricive. Login with ${data.email} and password: ${passcode}`,
    phone: `+234${data.phone.substring(1)}`,
  });
};

const loginTeamMember = async (data: LoginTeamProps) => {
  const phoneNumber = `+234${data.phone.substring(1)}`;
  const res = await supabase
    .from(db.tables.merchant_team)
    .select()
    .eq('phone', phoneNumber)
    .eq('passcode', data.passcode);

  if (!res.error) {
    return res.body[0];
  } else {
    return {
      error: {
        msg: 'Phone or passcode is not correct',
      },
    };
  }
};

const getTeamMembers = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_team)
    .select()
    .eq('merchant', merchant_id);

  if (!res.error) {
    return res.body;
  }

  return [];
};

export default {
  addTeamMember,
  loginTeamMember,
  getTeamMembers,
};

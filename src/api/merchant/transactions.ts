import supabase from '../../../supabase';
import db from '../db';

const getTransactions = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_transaction)
    .select(`*, invoice(*), user(*), merchant(*)`)
    .eq('merchant', merchant_id);

  if (res.error) {
    throw res.error;
  }

  return res.body.sort((a: any, b: any) => {
    return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
  });
};

interface NewTransactionPinProps {
  merchant: number;
  pin: string;
}

const addTransactionPin = async (data: NewTransactionPinProps) => {
  const res = await supabase.from(db.tables.merchant_pin).insert([
    {
      updated_at: new Date().toISOString(),
      merchant: data.merchant,
      pin: data.pin,
    },
  ]);

  if (res.error) {
    throw res.error;
  }
};

export default {
  getTransactions,
  addTransactionPin,
};

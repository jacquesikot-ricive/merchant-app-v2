import supabase from '../../../supabase';
import db from '../db';

interface NewExpoToken {
  token: string;
  merchant_id: number;
}

const saveExpoToken = async (data: NewExpoToken) => {
  const res = await supabase.from(db.tables.merchant_expo_push_token).insert([
    {
      updated_at: new Date().toISOString(),
      token: data.token,
      merchant: data.merchant_id,
      is_active: true,
    },
  ]);

  if (res.error) {
    throw res.error.message;
  }

  return res.body[0];
};

const removeToken = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_expo_push_token)
    .delete()
    .match({
      merchant: merchant_id,
    });

  if (res.error) {
    throw res.error.message;
  }
};

export default {
  saveExpoToken,
  removeToken,
};

import supabase from '../../../supabase';
import db from '../db';

const getNotifications = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_notification)
    .select()
    .eq('merchant', merchant_id);
  if (res.error) {
    throw res.error.message;
  }

  return res.body;
};

export default {
  getNotifications,
};

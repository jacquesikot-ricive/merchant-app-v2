import db from '../db';
import supabase from '../../../supabase';
import customer from '../customer/customer';
import sendSms from '../riciveApi/sendSms';
import getPaymentLink from '../riciveApi/getPaymentLink';
import numberWithCommas from '../../utils/numbersWithComma';

interface InvoiceItem {
  invoiceId: number;
  itemId: number;
  quantity: number;
  total: string;
  item: {
    id: number;
  };
}

interface CreateInvoiceProps {
  userId: number;
  orderId?: number;
  invoiceTotal: string;
  deliveryFee?: string;
  deliveryFeeType?: string;
  merchantId: number;
  invoiceItems?: InvoiceItem[];
  storeName: string;
  customerPhone: string;
}

const createInvoice = async (data: CreateInvoiceProps) => {
  const invoice = await supabase.from(db.tables.laundry_invoice).insert([
    {
      user_id: data.userId,
      updated_at: new Date().toISOString(),
      order_id: data.orderId ? data.orderId : undefined,
      total: data.invoiceTotal,
      is_paid: false,
      delivery_fee: data.deliveryFee ? data.deliveryFee : undefined,
      merchant_id: data.merchantId,
      delivery_fee_type: data.deliveryFeeType
        ? data.deliveryFeeType
        : undefined,
    },
  ]);

  const paymentLink = await getPaymentLink({
    order_id: data.orderId || 0,
    amount: data.invoiceTotal,
    invoice_id: invoice.body && invoice.body[0].id,
    merchant_id: data.merchantId,
    user_id: data.userId,
  });

  const sendSMS = await sendSms({
    phone: `+234${data.customerPhone.substring(1)}`,
    message: `Your invoice from ${
      data.storeName
    } is ready for payment. The total is NGN${numberWithCommas(
      data.invoiceTotal
    )} and can be paid through this link: ${
      paymentLink.data.data.authorization_url
    }`,
  });

  if (invoice.error) {
    throw invoice.error;
  } else {
    data.invoiceItems &&
      data.invoiceItems.length > 0 &&
      data.invoiceItems.map(async (i: InvoiceItem) => {
        const res = await supabase.from(db.tables.invoice_item).insert([
          {
            invoice_id: invoice.body && invoice.body[0].id,
            item_id: i.item.id,
            quantity: i.quantity,
            total: i.total,
            merchant: data.merchantId,
          },
        ]);

        if (res.error) {
          throw res.error;
        }
      });
  }
};

const getOrderInvoice = async (orderId: number) => {
  const res = await supabase
    .from(db.tables.laundry_invoice)
    .select(`*, order_id(*, service_type(*)), user_id(*)`)
    .eq('order_id', orderId);

  return res.body;
};

const getInvoiceItems = async (invoiceId: number) => {
  const res = await supabase
    .from(db.tables.invoice_item)
    .select(`*, item_id(*)`)
    .eq('invoice_id', invoiceId);

  return res.body;
};

const getInvoiceItemsByMerchant = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.invoice_item)
    .select(`*, item_id(*)`)
    .eq('merchant', merchant_id);

  if (res.error) {
    throw res.error;
  }

  return res.body;
};

const updatePayWithCash = async (invoice_id: number, total: string) => {
  await supabase
    .from(db.tables.laundry_invoice)
    .update({
      is_cash: true,
      is_paid: true,
      total,
    })
    .match({ id: invoice_id });
};

const getInvoiceById = async (invoice_id: number) => {
  const res = await supabase
    .from(db.tables.laundry_invoice)
    .select(`*, merchant_id(*), order_id(*), user_id(*)`)
    .eq('id', invoice_id);

  if (res.error) {
    throw res.error;
  }

  return res.body[0];
};

const getAllInvoices = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.laundry_invoice)
    .select(`*, order_id(*, service_type(*)), user_id(*)`)
    .eq('merchant_id', merchant_id);

  if (res.error) {
    throw res.error;
  }

  return res.body;
};

const getInvoiceByCustomer = async (customer_id: number) => {
  const res = await supabase
    .from(db.tables.laundry_invoice)
    .select(`*, order_id(*, service_type(*)), user_id(*)`)
    .eq('user_id', customer_id);

  if (res.error) {
    throw res.error;
  }

  return res.body;
};

export default {
  createInvoice,
  getOrderInvoice,
  getInvoiceItems,
  updatePayWithCash,
  getInvoiceById,
  getAllInvoices,
  getInvoiceItemsByMerchant,
  getInvoiceByCustomer,
};

import supabase from '../../../supabase';
import db from '../db';

// order_status:
//     | 'CREATED',
//     | 'SCHEDULED'
//     | 'PICKED_UP'
//     | 'CLEANING'
//     | 'DELIVERY'
//     | 'COMPLETED';

interface AcceptOrderProps {
  order: number;
  merchant: number;
  user: number;
}

interface AcceptPickupOrderProps {
  order: number;
  merchant: number;
  user: number;
  fulfilDate: string;
  fulfilTime: string;
}

interface UpdateOrderStatusProps {
  order: number;
  status: string;
}

const getOrders = async (merchant_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_order)
    .select('*, service_type(*), user(*)')
    .match({ merchant: merchant_id })
    .order('pick_up_date', { ascending: false });

  if (res.error) {
    throw res.error;
  }

  if (res.body) {
    return res.body;
  }
};

const acceptOrder = async ({ order, merchant, user }: AcceptOrderProps) => {
  const [a, b] = await Promise.all([
    await supabase
      .from(db.tables.merchant_order)
      .update({
        is_accepted: true,
        order_status: 'SCHEDULED',
      })
      .match({ id: order }),

    await supabase.from(db.tables.merchant_delivery).insert([
      {
        order,
        merchant,
        user,
        updated_at: new Date().toISOString(),
        type: 'pickup',
      },
    ]),
  ]);
};

const acceptPickupOrder = async ({
  order,
  merchant,
  user,
  fulfilDate,
  fulfilTime,
}: AcceptPickupOrderProps) => {
  await supabase
    .from(db.tables.merchant_order)
    .update({
      is_accepted: true,
      order_status: 'SCHEDULED',
    })
    .match({ id: order });

  await supabase.from(db.tables.merchant_delivery).insert([
    {
      order,
      merchant,
      user,
      updated_at: new Date().toISOString(),
      type: 'pickup',
      fulfil_date: fulfilDate,
      fulfil_time: fulfilTime,
    },
  ]);

  await supabase
    .from(db.tables.merchant_order)
    .update({
      pick_up_date: fulfilDate,
      pick_up_time: fulfilTime,
    })
    .match({ id: order });
};

const updateStatus = async ({ order, status }: UpdateOrderStatusProps) => {
  if (status === 'COMPLETED') {
    await supabase
      .from(db.tables.merchant_order)
      .update({
        order_status: status,
        is_complete: true,
      })
      .match({ id: order });
  } else {
    await supabase
      .from(db.tables.merchant_order)
      .update({
        order_status: status,
      })
      .match({ id: order });
  }
};

const getOrder = async (order_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_order)
    .select(
      '*, user(*), pick_up_address(*), delivery_address(*), service_type(*)'
    )
    .eq('id', order_id);

  if (res.body) {
    return res.body[0];
  }
};

export interface NewOrderProps {
  service_type: number;
  pick_up_date?: string;
  pick_up_time?: string;
  pick_up_address?: number;
  delivery_address?: number;
  order_status: string;
  is_accepted: boolean;
  user: number;
  merchant: number;
  is_delivery: boolean;
  is_pickup: boolean;
  is_complete: boolean;
  is_subscription: boolean;
}

const createOrder = async (data: NewOrderProps) => {
  const res = await supabase.from(db.tables.merchant_order).insert([
    {
      delivery_address: data.delivery_address,
      is_accepted: data.is_accepted,
      is_delivery: data.is_delivery,
      is_pickup: data.is_pickup,
      merchant: data.merchant,
      order_status: data.order_status,
      pick_up_date: data.pick_up_date,
      pick_up_address: data.pick_up_address,
      pick_up_time: data.pick_up_time,
      service_type: data.service_type,
      user: data.user,
      is_complete: data.is_complete,
      is_subscription: data.is_subscription,
      updated_at: new Date().toISOString(),
    },
  ]);

  if (res.body) {
    if (data.is_pickup) {
      await supabase.from(db.tables.merchant_delivery).insert([
        {
          order: res.body[0].id,
          merchant: data.merchant,
          user: data.user,
          updated_at: new Date().toISOString(),
          type: 'pickup',
          fulfil_date: data.pick_up_date,
          fulfil_time: data.pick_up_time,
        },
      ]);
    }
  }

  if (res.error) {
    throw res.error;
  }
};

const getOrderbyCustomer = async (customer_id: number) => {
  const res = await supabase
    .from(db.tables.merchant_order)
    .select(
      '*, user(*), pick_up_address(*), delivery_address(*), service_type(*)'
    )
    .eq('user', customer_id);

  if (res.error) {
    throw res.error;
  }

  return res.body;
};

const declineOrder = async (
  order_id: number,
  number: string,
  message: string
) => {
  const res1 = await supabase
    .from(db.tables.merchant_delivery)
    .delete()
    .match({ delete_key: order_id });

  if (res1.error) {
    throw res1.error;
  }

  const res2 = await supabase
    .from(db.tables.merchant_order)
    .delete()
    .eq('id', order_id);

  if (res2.error) {
    throw res2.error;
  }
};

export default {
  getOrders,
  acceptOrder,
  acceptPickupOrder,
  updateStatus,
  getOrder,
  createOrder,
  getOrderbyCustomer,
  declineOrder,
};

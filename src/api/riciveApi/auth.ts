import client from './client';

export interface SignInProps {
  email: string;
  password: string;
}

export type UserAuthRole = 'SUPERADMIN' | 'ADMIN' | 'TEAM' | 'DELIVERY';
export type BusinessType = 'LAUNDRY' | 'OTHER';

export interface SignUpProps {
  // Auth data
  role?: UserAuthRole;
  password?: string;
  email: string;
  // Profile data
  first_name: string;
  last_name: string;
  referral_code?: string;
}

interface ValidateLoginProps {
  refresh_token: string;
  user_id: string;
}

export interface ForgotPasswordProps {
  email: string;
}

const signin = async (auth: SignInProps) => {
  const res = await client.post('/user/signin', auth);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const signup = async (auth: SignUpProps) => {
  console.log(auth);

  const res = await client.post('/user/signup', auth);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const validateLogin = async ({ refresh_token, user_id }: ValidateLoginProps) =>
  await client.post('/user/validate', {
    refresh_token,
    user_id,
  });

const forgotPassword = async (auth: ForgotPasswordProps) => {
  const res = await client.post('/user/forgot-password', auth);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteAccount = async () => {
  const res = await client.post('/user/delete');

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  signin,
  signup,
  validateLogin,
  forgotPassword,
  deleteAccount,
};

import client from './client';

const getBusinessApi = async () => {
  const res = await client.get('/business');

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface BusinessProfileProps {
  id: string;
  business_name: string;
  currency: string;
  state: string;
  country: string;
  city: string;
  address: string;
  phone: string;
  type: 'LAUNDRY' | 'ORDER';
  district?: string;
}

const getBusinesses = async (auth_id: string) => {
  const res = await client.get(`/business/${auth_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  const returnedData = res.data.data.map((d: any) => {
    return {
      ...d.business_businessTouser_business,
    };
  });

  return returnedData;
};

const updateBusiness = async (data: BusinessProfileProps) => {
  const res = await client.patch('/business', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const createBusiness = async (business_name?: string) => {
  const res = await client.post('/business', {
    business_name,
  });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface LoginBusinessProps {
  business_id: string;
  auth_id: string;
}

const loginBusiness = async (data: LoginBusinessProps) => {
  const res = await client.post('/business/signin', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getBusinessApi,
  updateBusiness,
  getBusinesses,
  createBusiness,
  loginBusiness,
};

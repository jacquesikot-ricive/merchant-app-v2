import client from './client';

const getCategories = async (businessId: string) => {
  const res = await client.get(`/category/${businessId}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface NewCategory {
  name: string;
}

const createCategory = async (data: NewCategory) => {
  const res = await client.post('/category', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface EditCategory {
  name: string;
  id: string;
}

const editCategory = async (data: EditCategory) => {
  const res = await client.patch('/category', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteCategory = async (id: string) => {
  const res = await client.delete(`/category/${id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getCategories,
  createCategory,
  editCategory,
  deleteCategory,
};

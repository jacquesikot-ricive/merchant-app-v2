import axios from 'axios';

import storageKeys from '../../constants/storageKeys';
import storage from '../../utils/storage';

export const BASE_URL = 'https://riciveapi.com/api';
// export const BASE_URL = 'http://localhost:3000/api';
// export const BASE_URL = 'https://dev.riciveapi.com/api';

const client = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});

client.interceptors.request.use(
  async (config) => {
    const user = await storage.getData(storageKeys.USER);

    if (user && config.headers) {
      config.headers['x-access-token'] = user.access_token;
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

client.interceptors.response.use(undefined, async (err) => {
  const originaConfig = err.config;
  if (originaConfig.url !== 'user/signin' || ('user/signup' && err.response)) {
    // Access token was expired
    if (err.response.status === 401 && !originaConfig._retry) {
      originaConfig._retry = true;
      try {
        const user = await storage.getData(storageKeys.USER);
        const rs = await client.post('/access-token', {
          refresh_token: user.refresh_token,
          type: 'USER',
          auth_id: user.auth.id,
        });
        const newAccessToken = rs.data.data.access_token;
        const newRefreshToken = rs.data.data.refresh_token;
        const newUser = {
          ...user,
          refresh_token: newRefreshToken,
          access_token: newAccessToken,
        };
        await storage.removeData(storageKeys.USER);
        await storage.storeData(storageKeys.USER, newUser);
        return client(originaConfig);
      } catch (_error) {
        return Promise.reject(_error);
      }
    }
  }
  return Promise.reject(err);
});

export default client;

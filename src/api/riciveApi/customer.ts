import client from './client';

const getCustomers = async () => {
  const res = await client.get('/customer');

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface NewCustomer {
  first_name: string;
  last_name?: string;
  email?: string;
  phone: string;
  address?: string;
}

const createCustomer = async (data: NewCustomer) => {
  const res = await client.post('/customer', data);

  if (res.data.status === 'failed') {
    console.log(res.data);
    throw new Error(res.data.message);
  }

  return res.data;
};

interface UpdateCustomer {
  first_name: string;
  last_name: string;
  email?: string;
  phone: string;
  address?: string;
  id: string;
}

const updateCustomer = async (data: UpdateCustomer) => {
  console.log(data);
  const res = await client.patch('/customer/update', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteCustomer = async (id: string) => {
  const res = await client.patch('/customer/delete', { id });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getSingleCustomer = async (id: string) => {
  const res = await client.post(`/customer/${id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getCustomerOrders = async (customer_id: string) => {
  const res = await client.get(`/customer/order/${customer_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getCustomerInvoices = async (customer_id: string) => {
  const res = await client.get(`/customer/order/${customer_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getCustomers,
  updateCustomer,
  deleteCustomer,
  createCustomer,
  getSingleCustomer,
  getCustomerInvoices,
  getCustomerOrders,
};

import axios from 'axios';

import { baseUrl } from '.';

interface PaymentLinkProps {
  order_id: number;
  user_id: number;
  merchant_id: number;
  invoice_id: number;
  amount: string;
}

const getPaymentLink = async (data: PaymentLinkProps) => {
  const res = await axios.post(`${baseUrl}/payment/init`, {
    order_id: data.order_id,
    user_id: data.user_id,
    merchant_id: data.merchant_id,
    invoice_id: data.invoice_id,
    amount: data.amount,
  });

  return res.data;
};

export default getPaymentLink;

import client from './client';
import FileSystem from 'expo-file-system';

interface InvoiceItem {
  item_id: string;
  quantity: string;
  total: string;
}

interface NewInvoice {
  customer: string;
  total: string;
  delivery_fee?: string;
  delivery_type?: string;
  order?: string;
  is_cash: boolean;
  note?: string;
  sendSMS?: boolean;
  invoice_items?: InvoiceItem[];
  is_paid?: boolean;
  amount_collected?: string;
  business_id?: string;
  discount?: string;
  deposit?: string;
  tax?: string;
  address?: string;
}

const createInvoice = async (data: NewInvoice) => {
  const res = await client.post('/invoice', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getOneInvoice = async (id: string) => {
  const res = await client.get(`/invoice/${id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface FetchInvoice {
  order?: string;
  service?: string;
}

const getInvoices = async (data: FetchInvoice) => {
  const res = await client.get(
    `/invoice${data.order ? `/?order=${data.order}` : ''}${
      data.service ? `&service=${data.service}` : ''
    }`
  );

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getInvoicesByCustomer = async (customer: string) => {
  const res = await client.get(`/invoice/?customer=${customer}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface UpdateInvoice {
  id: string;
  customer: string;
  total: string;
  delivery_fee?: string;
  delivery_type?: string;
  order?: string;
  is_cash: boolean;
  note?: string;
  is_paid: boolean;
  amount_collected: string;

  discount?: string;
  deposit?: string;
  tax?: string;
  address?: string;
  invoice_items?: InvoiceItem[];
}

const updateInvoice = async (data: UpdateInvoice) => {
  const res = await client.patch(`/invoice/update`, data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const sendReminder = async (invoice_id: string) => {
  const res = await client.post(`/invoice/${invoice_id}/remind`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteInvoice = async (invoice_id: string) => {
  const res = await client.delete(`/invoice/${invoice_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  createInvoice,
  getOneInvoice,
  getInvoices,
  getInvoicesByCustomer,
  updateInvoice,
  sendReminder,
  deleteInvoice,
};

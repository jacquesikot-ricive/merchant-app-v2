import client from './client';

const getData = async (business: string) => {
  const res = await client.get(`/metrics/${business}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getData,
};

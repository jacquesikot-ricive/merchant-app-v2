import client from './client';

const getNotifications = async (user_id: string) => {
  const res = await client.get(`/notification/${user_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface NewToken {
  token: string;
  user: string;
}

const newExpoToken = async (data: NewToken) => {
  const res = await client.post(`/notification/token`, data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteExpoToken = async (token_id: string) => {
  const res = await client.patch(`/notification/token/delete`, {
    id: token_id,
  });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface UpdateNotification {
  id: string;
  is_delivered: boolean;
  is_opened: boolean;
}

const updateNotification = async (data: UpdateNotification) => {
  const res = await client.patch(`/notification/update`, data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getNotifications,
  newExpoToken,
  deleteExpoToken,
  updateNotification,
};

import client from './client';

export type ORDER_STATUS =
  | 'CREATED'
  | 'ACCEPTED'
  | 'SCHEDULED'
  | 'IN STORE'
  | 'PROCESSING'
  | 'DELIVERY'
  | 'COMPLETED';

interface NewOrder {
  customer?: string;
  status: ORDER_STATUS;
  customer_name: string;
  delivery_address?: string;
  pickup_address?: string;
  is_accepted: boolean;
  phone: string;
  pickup_date: string;
  pickup_time?: string;
  business: string;
  is_delivery: boolean;
  is_pickup: boolean;
}

const createOrder = async (data: NewOrder) => {
  const res = await client.post('/order', {
    ...data,
    is_admin: true,
  });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getOrders = async (customer?: string) => {
  const res = await client.get(
    `/order${customer ? `?customer=${customer}` : ''}`
  );

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getOrder = async (id: string) => {
  const res = await client.get(`/order/${id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface UpdateOrderStatus {
  id: string;
  status: ORDER_STATUS;
  is_complete: boolean;
}

const updateOrderStatus = async (data: UpdateOrderStatus) => {
  const res = await client.patch(`/order/update`, data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface AcceptOrderProps {
  order_id: string;
  pickup_date?: string;
  pickup_time?: string;
  is_pickup: boolean;
}
const acceptOrder = async (data: AcceptOrderProps) => {
  const res = await client.patch(`/order/accept`, data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface DeclineOrderProps {
  order_id: string;
  reason: string;
}

const declineOrder = async (data: DeclineOrderProps) => {
  const res = await client.patch(`/order/decline`, data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteOrder = async (id: string) => {
  const res = await client.patch(`/order/delete`, {
    id,
  });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  createOrder,
  getOrders,
  getOrder,
  updateOrderStatus,
  acceptOrder,
  declineOrder,
  deleteOrder,
};

import client from "./client";

const getProducts = async (
  business_id: string,
  type?: "web" | "mobile",
  service?: string
) => {
  const res = await client.get(
    `/products/?business_id=${business_id}&type=${type ? type : "mobile"}${
      service && service.length > 0 ? `&service_id=${service}` : ""
    }`
  );

  if (res.data.status === "failed") {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface ProductVariantValues {
  value: string;
  price: string;
}

interface ProductVariant {
  name: string;
  values: ProductVariantValues[];
}

interface Product {
  name: string;
  description?: string;
  image?: string;
  business: string;
  price: string;
  price_type: string;
  low_stock_level: string;
  low_stock_alert?: boolean;
  allow_booking?: boolean;
  allow_preorder?: boolean;
  productPrice?: Array<ProductPrice>;
  is_published?: boolean;
  cost_price?: string;
  variants?: ProductVariant[];
  creation_time?: string;
  has_creation_time?: boolean;
  expiry_date?: Date | string;
  stock_quantity?: string;
  category?: string[];
}
interface ProductPrice {
  service: string;
  amount: string;
}

const addProduct = async (data: Product) => {
  const res = await client.post("/products", data);

  if (res.data.status === "failed") {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface ProductPriceData {
  service: string;
  amount: string;
  id: string;
}

interface ProductUpdate {
  name: string;
  price?: string;
  price_type: string;
  low_stock_level: string;
  low_stock_alert?: boolean;
  allow_booking?: boolean;
  allow_preorder?: boolean;
  id: string;
  description?: string;
  image?: string | null;
  business: string;
  productPrice?: Array<ProductPriceData>;
  is_published?: boolean;
  cost_price?: string;
  creation_time?: string;
  variants?: ProductVariant[];
  has_creation_time?: boolean;
  expiry_date?: Date | string;
  stock_quantity?: string;
  category?: string[];
}

const updateProduct = async (data: ProductUpdate) => {
  const res = await client.patch("/products/update", data);

  if (res.data.status === "failed") {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteProduct = async (id: string) => {
  const res = await client.patch("/products/delete", { id });

  if (res.data.status === "failed") {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getProducts,
  addProduct,
  updateProduct,
  deleteProduct,
};

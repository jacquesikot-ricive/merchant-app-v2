import client from './client';

export interface UpdateProfileProps {
  first_name: string;
  last_name: string;
  phone?: string;
  id: string;
}

export interface UpdatePasswordProps {
  oldPassword: string;
  newPassword: string;
  id: string;
}

const updateProfile = async (data: UpdateProfileProps) => {
  const res = await client.patch('/update/profile', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const updatePassword = async (data: UpdatePasswordProps) => {
  const res = await client.patch('/update/password', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  updateProfile,
  updatePassword,
};

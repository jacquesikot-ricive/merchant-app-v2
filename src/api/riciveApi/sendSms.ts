import axios from 'axios';
import { baseUrl } from '.';

interface SendSmsProps {
  phone: string;
  message: string;
}

const sendSms = async (data: SendSmsProps) => {
  const res = await axios.post(`${baseUrl}/sms`, {
    phone: data.phone,
    message: data.message,
  });

  return res.data;
};

export default sendSms;

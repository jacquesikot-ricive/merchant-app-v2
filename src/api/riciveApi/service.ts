import client from './client';

const getServices = async (id: string) => {
  const res = await client.get(`/service/?business_id=${id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface NewService {
  name: string;
  description?: string;
  image?: string;
  business: string;
  id?: string;
}

const createService = async (props: NewService) => {
  const res = await client.post('/service', props);
  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const updateService = async (props: NewService) => {
  const res = await client.patch('/service/update', props);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteService = async (id: string, type: 'SOFT' | 'HARD') => {
  const res = await client.patch('/service/delete', {
    id,
    type,
  });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getServices,
  createService,
  updateService,
  deleteService,
};

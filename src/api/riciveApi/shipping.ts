import client from './client';

interface UpdateTeamProps {
  id?: string;
  name: string;
  cost: string;
  message?: string;
}

const createShipping = async (data: UpdateTeamProps) => {
  const res = await client.post('/ship', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const updateShipping = async (data: UpdateTeamProps) => {
  const res = await client.patch('/ship', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getShipping = async (business_id: string) => {
  const res = await client.get(`/ship/business/${business_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteShipping = async (shipping_id: string) => {
  const res = await client.delete(`/ship/${shipping_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  createShipping,
  updateShipping,
  getShipping,
  deleteShipping,
};

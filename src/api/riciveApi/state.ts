import client from './client';

const getData = async () => {
  const res = await client.get(`/states`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getData,
};

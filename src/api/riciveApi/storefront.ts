import client from './client';

const getStorefront = async (business_id: string) => {
  const res = await client.get(`/storefront/?business_id=${business_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface UpdateStorefrontProps {
  domain: string;
  tagline?: string;
  logo?: string | null;
  facebook_url?: string | null;
  twitter_url?: string | null;
  instagram_url?: string | null;
  id: string;
  color?: string | null;
}

const updateStorefront = async (data: UpdateStorefrontProps) => {
  const res = await client.patch('/storefront/update', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface CreateStorefrontProps {
  domain: string;
  tagline?: string;
  logo?: string;
  facebook_url?: string;
  twitter_url?: string;
  instagram_url?: string;
  color?: string;
}

const createStorefront = async (data: CreateStorefrontProps) => {
  const res = await client.post('/storefront', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getStorefront,
  updateStorefront,
  createStorefront,
};

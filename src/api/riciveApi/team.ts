import client from './client';

interface NewTeamProps {
  email: string;
  first_name: string;
  last_name: string;
  business: string;
  role: string;
  permissions?: any;
}

const addTeamMember = async (data: NewTeamProps) => {
  console.log(data);
  const res = await client.post('/team', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const getTeamsMembers = async (business_id: string) => {
  const res = await client.get(`/team/?business_id=${business_id}`);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const deleteTeam = async (auth_id: string) => {
  const res = await client.patch(`/team/delete`, {
    auth_id,
  });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface UpdateTeamProps {
  role: string;
  profile_id: string;
  first_name: string;
  last_name: string;
  permissions: any;
}

const updateTeam = async (data: UpdateTeamProps) => {
  const res = await client.patch('/team', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  addTeamMember,
  getTeamsMembers,
  deleteTeam,
  updateTeam,
};

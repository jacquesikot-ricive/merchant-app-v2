import client from './client';

const getTransaction = async () => {
  const res = await client.get('/transaction');

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

const updateTransactionPin = async (pin: string) => {
  const res = await client.patch('/transaction/pin', {
    pin,
  });

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getTransaction,
  updateTransactionPin,
};

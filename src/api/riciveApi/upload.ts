import client from './client';

interface UploadData {
  file: string;
}

const upload = async (data: UploadData) => {
  const formData = new FormData();

  const file = {
    uri: data.file,
    name: 'logo.jpeg',
    type: 'image/jpeg',
  };

  if (data.file) formData.append('file', file as any);

  const res = await client.post(`/upload`, formData);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  upload,
};

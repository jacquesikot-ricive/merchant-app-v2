import client from './client';

const getWallet = async () => {
  const res = await client.get('/wallet');

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

interface WithdrawData {
  account_number: string;
  amount: string;
  bank_code: string;
  account_name: string;
  pin: string;
  reason: string;
}

const withdraw = async (data: WithdrawData) => {
  const res = await client.post('/wallet/withdraw', data);

  if (res.data.status === 'failed') {
    throw new Error(res.data.message);
  }

  return res.data;
};

export default {
  getWallet,
  withdraw,
};

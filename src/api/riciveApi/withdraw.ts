import axios from 'axios';
import { baseUrl } from './index';

interface WithdrawProps {
  name: string;
  account_number: string;
  bank_code: string;
  amount: string;
  merchant: number;
}

const withdrawFunds = async (data: WithdrawProps) => {
  console.log(data);

  const res = await axios.post(
    `${baseUrl}/payment/withdraw`,
    {
      name: data.name,
      account_number: data.account_number,
      bank_code: data.bank_code,
      amount: data.amount,
      merchant: data.merchant,
    },
    {
      headers: {
        'Content-Type': 'application/json',
      },
    }
  );

  console.log(res);

  return res.data;
};

export default {
  withdrawFunds,
};

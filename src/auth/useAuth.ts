import { useAnalytics } from '@segment/analytics-react-native';

import storage from '../utils/storage';
import storageKeys from '../constants/storageKeys';
import { setUser, setIsLoggedIn, UserSignInResponse } from '../redux/login';
import { useAppDispatch } from '../redux/hooks';
import supabase from '../../supabase';
import { SignUpProps } from '../api/riciveApi/auth';

const useAuth = () => {
  const dispatch = useAppDispatch();
  const { identify } = useAnalytics();
  // data here is response from signin api endpoint
  const login = async (data: UserSignInResponse) => {
    await storage.storeData(storageKeys.USER, data);
    dispatch(setUser(data));
    dispatch(setIsLoggedIn(true));
    identify(data.profile.id, {
      username: data.profile.first_name,
      email: data.profile.email,
    });
  };

  const logout = async () => {
    await supabase.auth.signOut();
    await storage.removeData(storageKeys.USER);
    dispatch(
      setUser({
        auth: {},
        business: {},
        profile: {},
        refresh_token: '',
        access_token: '',
        permissions: {},
      })
    );
    dispatch(setIsLoggedIn(false));
  };

  return {
    login,
    logout,
  };
};

export default useAuth;

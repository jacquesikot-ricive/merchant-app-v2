import React, { FC, useState } from 'react';
import { StyleSheet, TouchableOpacity, Modal, Platform } from 'react-native';
import { Picker as RNPicker, PickerProps } from '@react-native-picker/picker';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from '../components/Themed';

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    height: 79,
    justifyContent: 'space-between',
  },
  box: {
    height: 56,
    borderRadius: 8,
    // backgroundColor: theme.colors.white,
    borderWidth: 1,
    borderColor: theme.colors.border1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 22,
    alignItems: 'center',
  },
  picker: {
    backgroundColor: theme.colors.light,
    width: '100%',
    height: '40%',
    position: 'absolute',
    bottom: 0,
  },
  pickerContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    width: '100%',
    height: '100%',
    position: 'absolute',
    bottom: 0,
  },
  error: {
    position: 'absolute',
    top: 87,
    right: 1,
  },
  label: {
    flexDirection: 'row',
  },
});

interface DataProps {
  label: string;
  value: string;
  id?: number;
}

interface Props extends PickerProps {
  label: string;
  placeholder: string;
  value: string;
  setValue: (state: string) => void;
  data: DataProps[];
  error?: string;
  optional?: boolean;
  setSelectedId?: (state: any) => void;
}

const AboutBusinessPicker: FC<Props> = ({
  label,
  placeholder,
  value,
  setValue,
  data,
  error,
  optional,
  setSelectedId,
}) => {
  const [visible, setVisible] = useState<boolean>(false);

  if (Platform.OS === 'android') {
    return (
      <Box style={styles.container}>
        {optional ? (
          <Box style={styles.label}>
            <Text variant="DetailsR" color="text1" mb="m">
              {label}
            </Text>
          </Box>
        ) : (
          <Box style={styles.label}>
            <Text variant="DetailsR" color="text1" mb="m">
              {label}
            </Text>
            <Text color="error">*</Text>
          </Box>
        )}

        <Box
          style={
            Platform.OS === 'android' && {
              width: wp(90),
              height: 58,
              borderRadius: 8,
              borderWidth: 1,
              borderColor: theme.colors.border1,
              justifyContent: 'space-between',
              paddingHorizontal: 20,
            }
          }
        >
          <RNPicker
            selectedValue={value}
            onValueChange={(itemValue: string, itemIndex: number) => {
              setValue(itemValue);
              setVisible(!visible);
              setSelectedId && setSelectedId(data[itemIndex].id);
            }}
          >
            {/* eslint-disable-next-line prettier/prettier */}
            {data.map((d) => (
              <RNPicker.Item label={d.label} value={d.value} key={d.value} />
            ))}
          </RNPicker>
        </Box>
      </Box>
    );
  }

  return (
    <>
      <Box style={styles.container}>
        {optional ? (
          <Box style={styles.label}>
            <Text variant="DetailsR" color="text1" mb="m">
              {label}
            </Text>
          </Box>
        ) : (
          <Box style={styles.label}>
            <Text variant="DetailsR" color="text1" mb="m">
              {label}
            </Text>
            <Text color="error">*</Text>
          </Box>
        )}

        <TouchableOpacity
          style={styles.box}
          activeOpacity={0.7}
          onPress={() => setVisible(!visible)}
        >
          <Text variant="DetailsR" color="text5">
            {value && value.length > 0 ? value : placeholder}
          </Text>
          <Icon name="chevron-down" color={theme.colors.text2} size={20} />
        </TouchableOpacity>

        {error && (
          <Box style={styles.error}>
            <Text variant="DetailsR" color="error">
              {error}
            </Text>
          </Box>
        )}
      </Box>

      <Modal
        animationType="fade"
        transparent
        visible={visible}
        onRequestClose={() => {
          setVisible(!visible);
        }}
      >
        <Box style={styles.pickerContainer}>
          <Box style={styles.picker}>
            <TouchableOpacity
              style={{ padding: 20 }}
              onPress={() => setVisible(!visible)}
            >
              <Text variant="DetailsR" color="primary1">
                Done
              </Text>
            </TouchableOpacity>
            <RNPicker
              selectedValue={value}
              onValueChange={(itemValue: string, itemIndex: number) => {
                setValue(itemValue);
                setVisible(!visible);
                setSelectedId && setSelectedId(data[itemIndex].id);
              }}
            >
              {/* eslint-disable-next-line prettier/prettier */}
              {data.map((d) => (
                <RNPicker.Item label={d.label} value={d.value} key={d.value} />
              ))}
            </RNPicker>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default AboutBusinessPicker;

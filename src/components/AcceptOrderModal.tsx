import React, { FC, useRef, useState } from 'react';
import { StyleSheet, ScrollView } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { useAnalytics } from '@segment/analytics-react-native';

import theme, { Box, Text } from './Themed';
import Button from './Button';
import TextInput from './TextInput';
import { useAppSelector } from '../redux/hooks';
import ProductAppModal from './ProductAppModal';
import Picker from './Picker';
import OrderAppModal from './OrderAppModal';
import DatePicker from './DatePicker';

const styles = StyleSheet.create({
  content: {
    marginTop: 30,
    // marginHorizontal: 20,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  picker: {
    // width: 10,
    marginRight: 5,
  },
  button: {
    marginTop: 52,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  image: {
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 40,
    borderColor: theme.colors.border1,
  },
});

const timeData = [
  {
    label: '',
    value: '',
  },
  {
    label: '7:00am - 8:00am',
    value: '7:00am - 8:00am',
  },
  {
    label: '8:00am - 9:00am',
    value: '8:00am - 9:00am',
  },
  {
    label: '9:00am - 10:00am',
    value: '9:00am - 10:00am',
  },
  {
    label: '10:00am - 11:00am',
    value: '10:00am - 11:00am',
  },
  {
    label: '11:00am - 12:00pm',
    value: '11:00am - 12:00pm',
  },
  {
    label: '12:00pm - 1:00pm',
    value: '12:00pm - 1:00pm',
  },
  {
    label: '1:00pm - 2:00pm',
    value: '1:00pm - 2:00pm',
  },
  {
    label: '2:00pm - 3:00pm',
    value: '2:00pm - 3:00pm',
  },
  {
    label: '3:00pm - 4:00pm',
    value: '3:00pm - 4:00pm',
  },
  {
    label: '4:00pm - 5:00pm',
    value: '5:00pm - 5:00pm',
  },
];

const dateData = [
  {
    label: '',
    value: '',
  },
  {
    label: '6/22/2022',
    value: '6/22/2022',
  },
];

interface Props {
  visible: boolean;
  setVisible: (visible: boolean) => void;
}

const AcceptOrderModal: FC<Props> = ({ visible, setVisible }) => {
  const [categoryName, setCategoryName] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [selectedDate, setSelectedDate] = useState<Date>(new Date());
  const [selectedTime, setSelectedTime] = useState<string>('');
  const merchantId = useAppSelector((state) => state.login.user.business.id);
  const { track } = useAnalytics();

  return (
    <OrderAppModal
      setVisible={setVisible}
      visible={visible}
      heightValue={458}
      width={390}
      content={
        <>
          <Box style={styles.content}>
            <Box
              style={{
                marginLeft: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <Text variant="Body2M" color="text1">
                Confirm Schedule
              </Text>
            </Box>
            <Box
              style={{
                borderBottomColor: theme.colors.border1,
                borderBottomWidth: 1,
                paddingTop: 5,
                paddingBottom: 18,
                width: wp(100),
                alignItems: 'center',
              }}
            />
            <Box style={{ alignItems: 'center', width: wp(100) }}>
              <Box mt="xl">
                <Picker
                  label={'Schedule time'}
                  placeholder={''}
                  value={selectedTime}
                  setValue={setSelectedTime}
                  data={timeData}
                />
              </Box>
              <Box mt="xl">
                <DatePicker
                  label={'Schedule date'}
                  date={selectedDate}
                  setDate={setSelectedDate}
                />
              </Box>
            </Box>
          </Box>

          <Box style={styles.button}>
            <Button
              disabled={selectedTime.length < 0 ? true : false}
              loading={loading}
              type="primary"
              label="Accept Order"
              onPress={() => true}
              width={wp(90)}
            />
          </Box>
        </>
      }
    />
  );
};

export default AcceptOrderModal;

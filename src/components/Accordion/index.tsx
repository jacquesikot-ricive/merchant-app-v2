import React, { FC, useState } from 'react';
import {
  View,
  Animated,
  useWindowDimensions,
  StyleSheet,
  UIManager,
  LayoutAnimation,
  Platform,
  TouchableOpacity,
} from 'react-native';
import { Feather } from '@expo/vector-icons';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box } from '../Themed';
import TextInput from '../TextInput';
import PickupInformationCard from '../PickupInformationText';
import PickupInformationText from '../PickupInformationText';
import NewOrderCard from '../NewOrderCard';
import UpcomingOrderInvoiceCard from '../UpcomigOrderInvoiceCard';
import PickupCard from '../PickupCard';

const styles = StyleSheet.create({
  container: {
    padding: 22,
    marginTop: 20,
    width: '100%',
    backgroundColor: theme.colors.white,
    // borderWidth: 1.0,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: theme.colors.border1,
    borderBottomColor: theme.colors.border1,
  },
  heading: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  hidden: {
    height: 0,
  },
  list: {
    overflow: 'hidden',
  },
  sectionTitle: {
    fontSize: 16,
    height: 30,
    marginLeft: '5%',
  },
});

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

interface Props {
  timestamp: string;
  address: string;
  isBooking?: boolean;
  pickup?: boolean;
}

const Accordion: FC<Props> = ({ timestamp, address, isBooking, pickup }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const toggleOpen = () => {
    setIsOpen((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  return (
    <Box style={styles.container}>
      <TouchableOpacity
        onPress={toggleOpen}
        style={styles.heading}
        activeOpacity={0.6}
      >
        <PickupInformationText />
        <Feather
          name={isOpen ? 'chevron-up' : 'chevron-down'}
          size={18}
          color="black"
        />
      </TouchableOpacity>
      <Box style={[styles.list, !isOpen ? styles.hidden : undefined]}>
        <PickupCard
          address={address}
          timestamp={timestamp}
          isBooking={isBooking}
          isPickup={pickup}
        />
      </Box>
    </Box>
  );
};

export default Accordion;

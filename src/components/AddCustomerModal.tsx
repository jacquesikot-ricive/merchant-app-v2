import React, { FC, useState, useRef, useEffect } from 'react';
import { StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';
import NigerianPhone from 'validate_nigerian_phone';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import Button from './Button';
import TextInput from './TextInput';
import ListEmpty from './ListEmpty';
import { useAppSelector } from '../redux/hooks';
import customerApi from '../api/riciveApi/customer';
import { useAnalytics } from '@segment/analytics-react-native';
import HeaderTextInput from './HeaderTextInput';
import { returnProfileBgColor } from './TeamList.tsx';
import PhoneTextInput from './PhoneTextInput';

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    alignItems: 'center',
  },
  list: {
    marginTop: 10,
    height: hp(55),
  },
  customerCard: {
    borderBottomWidth: 1,
    borderColor: 'rgba(247, 247, 247, 1)',
    width: wp(100),
    height: 60,
    paddingHorizontal: 25,
    paddingVertical: 5,
    marginBottom: 10,
    flexDirection: 'row',
  },
  title: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    justifyContent: 'space-between',
    width: wp(90),
    marginTop: 10,
  },
  newCustomer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp(90),
    justifyContent: 'flex-end',
    marginBottom: 10,
  },
});

interface Props {
  allowDelivery?: boolean;
  visible: boolean;
  setVisible: (state: boolean) => void;
  customers: any[];
  setActiveCustomer: (state: any) => void;
}

const AddCustomerModal: FC<Props> = ({
  allowDelivery,
  visible,
  setVisible,
  customers,
  setActiveCustomer,
}: Props) => {
  const { track } = useAnalytics();
  const [phone, setPhone] = useState<string>('');
  const [name, setName] = useState<string>('');
  const [address, setAddress] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [searchResult, setSearchResult] = useState<any[]>(customers);
  const [customerInput, setCustomerInput] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);

  useEffect(() => {
    setSearchResult(customers);

    console.log(customers[0]);
  }, [customers]);

  // Form input refs
  const nameRef = useRef<any>();
  const addressRef = useRef<any>();
  // const phoneRef = useRef<any>();

  const handleChangeNumber = (e: string) => {
    setPhone(e);

    // Filter by phone number or name
    const result = customers.filter(
      (c) =>
        (c.phone &&
          c.phone.trim().toLowerCase().includes(e.trim().toLowerCase())) ||
        (c.first_name &&
          c.first_name.trim().toLowerCase().includes(e.trim().toLowerCase())) ||
        (c.last_name &&
          c.last_name.trim().toLowerCase().includes(e.trim().toLowerCase()))
    );
    setSearchResult([...result]);
  };

  const handleSelectCustomer = (item: any) => {
    setActiveCustomer(item);
    setVisible(false);
  };

  const handleCreateCustomer = async () => {
    // Validate Form
    if (phone.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Phone Number',
        text2: 'Phone number must be present',
      });
    }

    if (name.length < 1) {
      setLoading(false);
      return Toast.show({
        type: 'error',
        text1: 'Customer Name',
        text2: 'Customer must have a name present',
        visibilityTime: 3000,
      });
    }

    try {
      setLoading(true);
      const newCustomer = await customerApi.createCustomer({
        phone,
        first_name: name.split(' ')[0],
        last_name: name.split(' ')[1],
        address: address.length > 0 ? address : undefined,
        email: email && email.length > 0 ? email : undefined,
      });
      if (newCustomer) {
        track('New Customer From Order Screen', {
          business_id,
        });
        setActiveCustomer({
          first_name: newCustomer.data.first_name,
          last_name: newCustomer.data.last_name,
          phone: newCustomer.data.phone,
          address:
            newCustomer.data.address && newCustomer.data.address.length > 0
              ? newCustomer.data.address
              : '',
          id: newCustomer.data.id,
        });
      }
      setVisible(false);
      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      console.log(
        'Error creating customer:',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          'Sorry we cannot create your customer now, please try again later...',
      });
    }
  };

  return (
    <AppModal
      heightValue={hp(95)}
      width={wp(100)}
      visible={visible}
      setVisible={setVisible}
      content={
        <Box style={styles.container}>
          {/* Title */}
          <Box style={styles.title}>
            <Text variant={'Body2M'} color="text1">
              Select Customer
            </Text>
          </Box>

          <TouchableOpacity
            onPress={() => {
              // phoneRef.current.clear();
              setCustomerInput(!customerInput);
            }}
            style={styles.newCustomer}
          >
            <Box
              style={
                !customerInput && {
                  width: 17,
                  height: 17,
                  borderRadius: 9,
                  borderWidth: 1,
                  borderColor: theme.colors.primary1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }
              }
            >
              <Icon
                name={customerInput ? 'x' : 'plus'}
                color={
                  customerInput ? theme.colors.error : theme.colors.primary1
                }
                size={14}
              />
            </Box>
            <Text variant={'DetailsM'} color="primary1" ml="m">
              {customerInput ? 'Cancel' : 'Add New Customer'}
            </Text>
          </TouchableOpacity>

          {/* Number Input */}

          {!customerInput && (
            <>
              <HeaderTextInput
                type="search"
                keyboardType={'default'}
                placeholder={'Search by phone or name'}
                onChangeText={(e) => handleChangeNumber(e)}
                width={wp(90)}
              />

              <Box
                style={{
                  width: wp(100),
                  height: 1,
                  backgroundColor: 'background: rgba(196, 196, 196, 0.2)',
                  alignSelf: 'center',
                  marginTop: 5,
                }}
              />
            </>
          )}

          {customerInput && (
            // <TextInput
            //   type="input"
            //   autoCompleteType="off"
            //   autoCorrect={false}
            //   keyboardType={customerInput ? 'phone-pad' : 'default'}
            //   placeholder={'Customer Phone'}
            //   onChangeText={(e) => handleChangeNumber(e)}
            //   width={wp(90)}
            // />
            <PhoneTextInput
              onChangeFormattedText={(t: string) => setPhone(t)}
            />
          )}

          {customerInput ? (
            // Add customer form
            <>
              <Box style={{ height: 15 }} />

              <TextInput
                inputRef={nameRef}
                type="input"
                autoCompleteType="name"
                autoCorrect={false}
                keyboardType="default"
                autoCapitalize="words"
                placeholder="Customer Name"
                onChangeText={(e) => setName(e)}
                width={wp(90)}
              />

              <Box style={{ height: 20 }} />

              <TextInput
                inputRef={nameRef}
                type="input"
                autoCompleteType="email"
                autoCorrect={false}
                keyboardType="email-address"
                placeholder="Customer Email (Optional)"
                onChangeText={(e) => setEmail(e)}
                width={wp(90)}
              />

              <Box style={{ height: 20 }} />

              <TextInput
                inputRef={addressRef}
                type="input"
                autoCompleteType="off"
                autoCorrect={false}
                keyboardType="default"
                autoCapitalize="words"
                placeholder="Customer Address (Optional)"
                onChangeText={(e) => setAddress(e)}
                width={wp(90)}
              />

              <Box style={{ height: 10 }} />

              <Box mt="m">
                <Button
                  type="primary"
                  label="Add Customer"
                  onPress={handleCreateCustomer}
                  width={wp(90)}
                  loading={loading}
                />
              </Box>
            </>
          ) : (
            // Customers List
            <Box style={styles.list}>
              <FlatList
                data={searchResult}
                keyExtractor={(item: any) => item.id.toString()}
                ListEmptyComponent={() => (
                  <Box style={{ marginTop: -100 }}>
                    <ListEmpty
                      topText="No Customers found"
                      bottomText="Try checking the number again or creating a new customer"
                      button
                      buttonText="Add Customer"
                      onPressButton={() => setCustomerInput(true)}
                      noImage
                    />
                  </Box>
                )}
                renderItem={({ item, index }) => (
                  <TouchableOpacity
                    onPress={() => handleSelectCustomer(item)}
                    style={styles.customerCard}
                  >
                    <Box
                      style={{
                        width: 44,
                        height: 44,
                        borderRadius: 22,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: returnProfileBgColor(Math.random()),
                      }}
                    >
                      <Text variant={'DetailsM'} color="text5">
                        {`${
                          item.first_name && item.first_name.trim().charAt(0)
                        }${
                          item.last_name ? item.last_name.trim().charAt(0) : ''
                        }`}
                      </Text>
                    </Box>

                    <Box ml="l">
                      <Text variant={'DetailsM'}>
                        {`${item.first_name} ${
                          item.last_name ? item.last_name : ''
                        }`}
                      </Text>
                      <Text variant={'DetailsR'} color="text5">
                        {item.phone}
                      </Text>
                    </Box>
                  </TouchableOpacity>
                )}
              />
            </Box>
          )}
        </Box>
      }
    />
  );
};

export default AddCustomerModal;

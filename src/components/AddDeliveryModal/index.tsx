import React, { FC, useState } from 'react';
import { StyleSheet, Modal, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../Themed';
import Button from '../Button';
import TextInput from '../TextInput';
import Checkbox from '../Checkbox';
import CurrencyTextInput from '../CurrencyTextInput';
import useCurrency from '../../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  modal: {
    backgroundColor: theme.colors.white,
    height: 450,
    borderRadius: 8,
    width: wp(90),
  },
  close: {
    position: 'absolute',
    width: 28,
    height: 28,
    backgroundColor: theme.colors.light,
    borderRadius: 14,
    justifyContent: 'center',
    alignItems: 'center',
    top: 20,
    right: 20,
  },
  content: {
    marginTop: 50,
    marginHorizontal: 20,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  button: {
    marginTop: 20,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginRight: 20,
  },
});

interface Props {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  setFee: (fee: any) => void;
}

const AddDeliveryModal: FC<Props> = ({ visible, setVisible, setFee }) => {
  const { returnCurrency } = useCurrency();
  const [amount, setAmount] = useState<string>();
  const [type, setType] = useState<string>();
  const handleSave = () => {
    setFee({
      amount: amount || '',
      type: type || '',
    });
    setVisible(false);
  };
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => {
        setVisible(!visible);
      }}
    >
      <Box style={styles.container}>
        <Box
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 130,
          }}
        >
          <Box style={styles.modal}>
            <TouchableOpacity
              onPress={() => setVisible(false)}
              style={styles.close}
            >
              <Icon name="x" color={theme.colors.black} size={14} />
            </TouchableOpacity>
            <Box style={styles.content}>
              <Text variant={'Body2M'}>Add delivery fee</Text>

              <Text variant={'DetailsR'} color="text7" mt="m" mb="xxl">
                Enter a custom delivery fee you would want to charge this
                customer
              </Text>

              {/* <TextInput
                placeholder="Enter amount"
                type={'input'}
                width={wp(80)}
                onChangeText={(e) => setAmount(e)}
                keyboardType="number-pad"
              /> */}

              <CurrencyTextInput
                value={amount}
                onChangeValue={(t: string) => setAmount(t.toString())}
                placeholder={`Delivery Fee in ${returnCurrency().name}`}
              />

              <Text mt="xl" mb="s" variant={'DetailsR'}>
                Select fee option
              </Text>

              <TouchableOpacity
                style={styles.checkbox}
                onPress={() => setType('Pick up')}
              >
                <Box>
                  <Checkbox checked={type === 'Pick up'} />
                </Box>
                <Text ml="m" variant={'DetailsR'} color="text7">
                  Pick up
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.checkbox}
                onPress={() => setType('Delivery')}
              >
                <Box>
                  <Checkbox checked={type === 'Delivery'} />
                </Box>
                <Text variant={'DetailsR'} ml="m" color="text7">
                  Delivery
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.checkbox}
                onPress={() => setType('Pick up & Delivery')}
              >
                <Box>
                  <Checkbox checked={type === 'Pick up & Delivery'} />
                </Box>
                <Text variant={'DetailsR'} ml="m" color="text7">
                  Pick up & Delivery
                </Text>
              </TouchableOpacity>
            </Box>
            <Box style={styles.button}>
              <Button
                type="primary"
                label="Save"
                onPress={handleSave}
                width={162}
              />
            </Box>
          </Box>
        </Box>
      </Box>
    </Modal>
  );
};

export default AddDeliveryModal;

import React, { FC, useState } from 'react';
import {
  StyleSheet,
  Modal,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import HeaderTextInput from './HeaderTextInput';
import useCurrency from '../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    width: '100%',
    height: '100%',
  },
  modal: {
    backgroundColor: theme.colors.white,
    height: hp(73),
    marginTop: -30,
    borderRadius: 8,
    width: wp(90),
  },
  close: {
    position: 'absolute',
    width: 28,
    height: 28,
    backgroundColor: theme.colors.light,
    borderRadius: 14,
    justifyContent: 'center',
    alignItems: 'center',
    top: 20,
    right: 20,
  },
  content: {
    marginTop: 50,
    marginHorizontal: 20,
    width: wp(80),
    alignItems: 'center',
  },
  listItem: {
    width: wp(80),
    alignSelf: 'center',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: theme.colors.border,
    justifyContent: 'center',
    alignItems: 'center',
    height: 58,
    marginBottom: 10,
  },
  list: {
    marginTop: 10,
    height: hp(70) * 0.67,
  },
});

interface Props {
  data: any[];
  items: any[];
  setItem: (item: any) => void;
  visible: boolean;
  setVisible: (value: boolean) => void;
}

const AddInvoiceItemModal: FC<Props> = ({
  visible,
  setVisible,
  items,
  setItem,
  data,
}) => {
  const [searchResult, setSearchResult] = useState<any>(data);
  const { returnCurrency } = useCurrency();
  const handleSearch = (e: string) => {
    const result = data.filter(
      (d: any) => d.name && d.price.trim().includes(e.trim())
    );
    setSearchResult([...result]);
  };

  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => {
        setVisible(!visible);
      }}
    >
      <Box style={styles.container}>
        <Box
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 130,
          }}
        >
          <Box style={styles.modal}>
            <TouchableOpacity
              onPress={() => setVisible(false)}
              style={styles.close}
            >
              <Icon name="x" color={theme.colors.black} size={14} />
            </TouchableOpacity>
            <Box style={styles.content}>
              <Text variant={'Body2M'}>Add Items</Text>

              <Text variant={'DetailsR'} color="text7" mt="m" mb="xl">
                Add laundry items to customer's invoice
              </Text>

              <HeaderTextInput
                type="search"
                placeholder="Search laundry items"
                width={wp(80)}
                onChangeText={handleSearch}
              />

              <Box style={styles.list}>
                <FlatList
                  data={searchResult}
                  keyExtractor={(item: any) => item.id.toString()}
                  showsVerticalScrollIndicator={false}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() => {
                        if (items.find((i: any) => i.id === item.id)) {
                          return Alert.alert('Product is already in invoice');
                        }
                        const value = {
                          ...item,
                          quantity: 1,
                          total: item.price,
                          item_id: item.id,
                        };
                        setItem([...items, value]);
                        return setVisible(false);
                      }}
                      activeOpacity={0.5}
                      style={styles.listItem}
                    >
                      <Text variant={'Body2R'} color="text2">{`${
                        item.product_productToproduct_price.name
                      } - ${returnCurrency().code} ${item.amount}`}</Text>
                    </TouchableOpacity>
                  )}
                />
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </Modal>
  );
};

export default AddInvoiceItemModal;

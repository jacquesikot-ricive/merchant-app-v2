import React, { useEffect } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import CurrencyTextInput from './CurrencyTextInput';
import useCurrency from '../hooks/useCurrency';
import TextInput from './TextInput';
import Button from './Button';
import PercentageInput from './PercentageInput';

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    width: wp(90),
    alignSelf: 'center',
  },
});

interface Props {
  show: boolean;
  setShow: (show: boolean) => void;
  setDispatchFee: (dispatchFee: string) => void;
  dispatchFee: string;
  address: string;
  setAddress: (address: string) => void;
  discount?: {
    amount: string;
    percentage: string;
  };
  setDiscount: (
    discount: { amount: string; percentage: string } | undefined
  ) => void;
  deposit: string;
  setDeposit: (deposit: string) => void;
  tax?: {
    amount: string;
    percentage: string;
  };
  setTax: (tax: { amount: string; percentage: string } | undefined) => void;
  total: string;
}
const AdditionalPaymentModal = ({
  show,
  setShow,
  setDispatchFee,
  dispatchFee,
  address,
  setAddress,
  discount,
  setDeposit,
  setDiscount,
  deposit,
  tax,
  setTax,
  total,
}: Props) => {
  const { returnCurrency } = useCurrency();

  return (
    <AppModal
      setVisible={setShow}
      visible={show}
      heightValue={hp(90)}
      width={wp(100)}
      content={
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.container}
          contentContainerStyle={{ paddingBottom: 500 }}
        >
          <Text variant={'Body2M'} color="text1">
            Additional Payment Information
          </Text>

          <Text variant={'SmallerTextR'} color="text7" mb="l">
            Add dispatch information, fee, any discount or necessary tax.
          </Text>

          <Text variant={'DetailsR'} color="text1" mb="m">
            Dispatch Fee
          </Text>

          <CurrencyTextInput
            onChangeValue={setDispatchFee}
            value={dispatchFee}
            width={wp(90)}
            placeholder={`Enter Amount In ${returnCurrency().name}`}
          />

          <Box style={{ height: 15 }} />

          <Text variant={'DetailsR'} color="text1" mb="m">
            Customer Address
          </Text>

          <TextInput
            type="input"
            onChangeText={setAddress}
            value={address}
            autoCapitalize="words"
            keyboardType="default"
            autoCompleteType="street-address"
            placeholder="Enter Customer Address"
          />

          <Box style={{ height: 15 }} />

          <Text variant={'DetailsR'} color="text1" mb="m">
            Discount Details
          </Text>

          <PercentageInput
            value={discount}
            setValue={setDiscount}
            total={total}
            placeholder={'Enter Discount'}
          />

          <Box style={{ height: 15 }} />

          <Text variant={'DetailsR'} color="text1" mb="m">
            Deposit Made By Customer
          </Text>

          <CurrencyTextInput
            onChangeValue={setDeposit}
            value={deposit}
            width={wp(90)}
            placeholder={`Enter Amount In ${returnCurrency().name}`}
          />

          <Box style={{ height: 15 }} />

          <Text variant={'DetailsR'} color="text1" mb="m">
            Tax Deducted
          </Text>

          <PercentageInput
            value={tax}
            setValue={setTax}
            total={total}
            placeholder="Enter Tax"
          />

          <Box style={{ height: 25 }} />

          <Button type="primary" label="Save" onPress={() => setShow(false)} />
        </ScrollView>
      }
    />
  );
};

export default AdditionalPaymentModal;

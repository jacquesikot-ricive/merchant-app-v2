import React, { FC, ReactNode, useState } from 'react';
import {
  StyleSheet,
  UIManager,
  LayoutAnimation,
  Platform,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    justifyContent: 'center',
    marginTop: 20,
    backgroundColor: 'rgba(247, 247, 247, 0.5)',
    borderColor: theme.colors.border1,
    borderRadius: 8,
    paddingHorizontal: 18,
  },
  heading: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  hidden: {
    height: 0,
  },
  list: {
    overflow: 'hidden',
  },
  sectionTitle: {
    fontSize: 16,
    height: 30,
    marginLeft: '5%',
  },
});

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

interface Props {
  content: ReactNode;
  title: string;
  containerStyle?: ViewStyle;
  titleVariant?: any;
  titleColor?: any;
  startOpen?: boolean;
  height?: number;
  fullWidth?: boolean;
}

const AppAccordion: FC<Props> = ({
  content,
  title,
  containerStyle,
  titleVariant,
  titleColor,
  startOpen,
  height,
  fullWidth,
}) => {
  const [isOpen, setIsOpen] = useState(startOpen ? true : false);

  const toggleOpen = () => {
    setIsOpen(!isOpen);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  return (
    <Box style={containerStyle ? containerStyle : [styles.container]}>
      <TouchableOpacity
        onPress={toggleOpen}
        style={[
          styles.heading,
          {
            width: fullWidth ? wp(90) : undefined,
            alignSelf: 'center',
          },
        ]}
        activeOpacity={0.6}
      >
        <Text
          variant={titleVariant || 'DetailsM'}
          color={titleColor || 'text1'}
        >
          {title}
        </Text>

        <Icon
          name={isOpen ? 'chevron-up' : 'chevron-right'}
          size={18}
          color={theme.colors.text1}
        />
      </TouchableOpacity>
      <Box style={[styles.list, !isOpen && styles.hidden]}>{content}</Box>
    </Box>
  );
};

export default AppAccordion;

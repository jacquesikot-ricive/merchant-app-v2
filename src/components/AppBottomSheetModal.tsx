import {
  BottomSheetBackdrop,
  BottomSheetBackdropProps,
  BottomSheetModal,
  BottomSheetScrollView,
} from "@gorhom/bottom-sheet";
import { useNavigation } from "@react-navigation/native";
import React, { RefObject, useMemo, VFC } from "react";
import Button from "./Button";
import theme, { Box } from "./Themed";
import { Ionicons as Icon } from "@expo/vector-icons";
import { StyleSheet, TouchableOpacity } from "react-native";

const styles = StyleSheet.create({
  cancel: { position: "absolute", top: 0, right: 15 },
});
type EaseSuccessProps = {
  bottomSheetModalRef: RefObject<BottomSheetModal>;
  onProceed: () => void;
  handleClose: () => void;
  children: JSX.Element | JSX.Element[];
  hasBottomButton?: boolean;
  label?: string;
  buttonType?: "primary" | "secondary" | "transparent";
};
const renderBackdrop = (props: BottomSheetBackdropProps) => (
  <BottomSheetBackdrop {...props} opacity={0.7} pressBehavior="close" />
);

const AppBottomSheet: VFC<EaseSuccessProps> = (props) => {
  const snapPoints = useMemo(() => ["65%", "65%"], []);

  const handleClose = () => {
    props.handleClose();
  };

  return (
    <Box>
      <BottomSheetModal
        backdropComponent={renderBackdrop}
        enableDismissOnClose
        ref={props.bottomSheetModalRef}
        snapPoints={snapPoints}
      >
        <BottomSheetScrollView
          contentContainerStyle={{
            flex: 1,
          }}
        >
          <TouchableOpacity style={styles.cancel} onPress={handleClose}>
            <Icon name={"close"} size={25} color={theme.colors.black} />
          </TouchableOpacity>
          {props.children}
        </BottomSheetScrollView>
        {props.hasBottomButton ? (
          <Box justifyContent="center" alignSelf="center">
            <Button
              type={props.buttonType ? props.buttonType : "primary"}
              label={props.label ? props.label : ""}
              onPress={props.onProceed}
            />
          </Box>
        ) : null}
        <Box marginTop="l" />
      </BottomSheetModal>
    </Box>
  );
};

export default AppBottomSheet;

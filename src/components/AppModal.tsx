import React, { FC, ReactNode, useState } from 'react';
import { StyleSheet, Modal, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },
  modal: {
    backgroundColor: theme.colors.white,
    height: 450,
    borderRadius: 20,
    width: wp(90),
  },
  close: {
    position: 'absolute',
    width: 28,
    height: 28,
    backgroundColor: theme.colors.light,
    borderRadius: 14,
    justifyContent: 'center',
    alignItems: 'center',
    top: 20,
    right: 20,
    zIndex: 1,
  },
  content: {
    marginTop: 50,
    marginHorizontal: 20,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  button: {
    marginTop: 20,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginRight: 20,
  },
});

interface Props {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  content: ReactNode;
  heightValue: number;
  top?: number;
  width?: number;
}

const AppModal: FC<Props> = ({
  visible,
  setVisible,
  content,
  heightValue,
  top,
  width,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent
      visible={visible}
      onRequestClose={() => {
        setVisible(!visible);
      }}
    >
      <Box style={styles.container}>
        <Box
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 130,
          }}
        >
          <Box
            style={[
              styles.modal,
              {
                height: heightValue ? heightValue : 450,
                marginTop: top,
                width: width ? width : wp(90),
              },
            ]}
          >
            <TouchableOpacity
              onPress={() => {
                setVisible(false);
              }}
              style={styles.close}
            >
              <Icon name="x" color={theme.colors.black} size={14} />
            </TouchableOpacity>

            {content}
          </Box>
        </Box>
      </Box>
    </Modal>
  );
};

export default AppModal;

import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    zIndex: 4,
    position: 'absolute',
    right: 20,
    top: 90,
    borderRadius: 8,
    backgroundColor: theme.colors.white,
    shadowColor: theme.colors.text3,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 6,
    elevation: 1,
  },
});

interface Props {
  onEdit?: () => void;
  onDelete?: () => Promise<void>;
  editLabel?: string;
  deleteLabel?: string;
}

const BurgerOptions = ({ onDelete, onEdit, editLabel, deleteLabel }: Props) => {
  return (
    <Box style={styles.container}>
      {onEdit && (
        <TouchableOpacity
          style={{
            padding: 5,
            flexDirection: 'row',
            alignItems: 'center',
          }}
          onPress={onEdit}
        >
          <Icon name="edit" color={theme.colors.text5} size={14} />
          <Text ml="m" variant={'DetailsM'} color="text5">
            {editLabel}
          </Text>
        </TouchableOpacity>
      )}

      {onDelete && (
        <>
          <Box
            style={{
              width: '100%',
              height: 1,
              backgroundColor: theme.colors.border4,
            }}
          />

          <TouchableOpacity
            style={{
              padding: 5,
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={onDelete}
          >
            <Icon name="trash" color={theme.colors.error} size={14} />
            <Text ml="m" variant={'DetailsM'} color="error">
              {deleteLabel}
            </Text>
          </TouchableOpacity>
        </>
      )}
    </Box>
  );
};

export default BurgerOptions;

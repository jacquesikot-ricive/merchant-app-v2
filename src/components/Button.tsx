import React, { FC } from 'react';
import { ActivityIndicator, StyleSheet, TouchableOpacity } from 'react-native';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
  },
});

interface Props {
  type: 'secondary' | 'primary' | 'transparent';
  label: string;
  onPress: () => void;
  width?: number | string;
  loading?: boolean;
  disabled?: boolean;
  height?: number;
}

const Button: FC<Props> = ({
  type,
  label,
  onPress,
  width,
  loading,
  disabled,
  height,
}) => {
  const returnBgColor = () => {
    if (type === 'primary' && !disabled) return theme.colors.primary1;
    if (type === 'primary' && disabled) return theme.colors.border1;
    if (type === 'secondary') return theme.colors.bg5;
  };

  return (
    <TouchableOpacity
      activeOpacity={disabled ? 1 : 0.6}
      style={[
        styles.container,
        {
          backgroundColor: returnBgColor(),
          width: width || theme.layout.screenWidth,
          borderColor:
            type === 'transparent' ? theme.colors.primary1 : returnBgColor(),
          borderWidth: 1,
          height: height ? height : 56,
        },
      ]}
      onPress={!disabled ? onPress : () => true}
    >
      {loading ? (
        <ActivityIndicator
          color={type === 'primary' ? theme.colors.white : theme.colors.border}
        />
      ) : (
        <Text
          variant="Body2M"
          color={
            type === 'primary'
              ? 'white'
              : type === 'transparent'
              ? 'primary1'
              : 'text2'
          }
        >
          {label}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export default Button;

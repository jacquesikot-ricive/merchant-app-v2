import React, { FC, useState } from 'react';
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  LayoutAnimation,
  Platform,
  UIManager,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import EditIcon from '../svg/EditIcon';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    height: 73,
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.light3,
    backgroundColor: theme.colors.bg1,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
  },
});

// export const categoryData = [
//     {
//         id: '1',
//         categoryName: 'Beverages',
//         productCount: '0'
//     },
//     {
//         id: '2',
//         categoryName: 'Cleaning supplies',
//         productCount: '4'
//     },
// ]

interface Props {
  categoryName: string;
  productCount: string;
  handleEdit: any;
}

const CategoryCard: FC<Props> = ({
  categoryName,
  productCount,
  handleEdit,
}) => {
  return (
    <Box style={styles.container}>
      <Box>
        <Text variant="DetailsM" color="text1">
          {categoryName}
        </Text>
        <Text variant="SmallerTextR" color="text7">
          {productCount} products
        </Text>
      </Box>

      <TouchableOpacity
        style={{
          position: 'absolute',
          right: 20,
          top: 20,
        }}
        onPress={handleEdit}
      >
        <EditIcon />
      </TouchableOpacity>
    </Box>
  );
};

export default CategoryCard;

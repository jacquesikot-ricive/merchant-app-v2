import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box } from '../Themed';

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    borderWidth: 1,
    // borderColor: theme.colors.primary1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
});

interface Props {
  width?: string | number;
  checked?: boolean;
}

const Checkbox: FC<Props> = ({ width, checked }) => {
  const widthValue = width || 20;
  const heightValue = width || 20;

  return (
    <Box
      style={[
        styles.container,
        {
          width: widthValue,
          height: heightValue,
          borderColor: checked ? theme.colors.primary1 : theme.colors.text7,
          backgroundColor: checked ? theme.colors.primary1 : theme.colors.white,
        },
      ]}
    >
      {checked && <Icon name="check" color={theme.colors.white} size={16} />}
    </Box>
  );
};

export default Checkbox;

import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: wp(90),
    marginTop: 80,
    flexWrap: 'wrap',
    // padding: 20,
    marginLeft: 35,
  },
});

interface Props {
  setVisible: (state: boolean) => void;
  visible: boolean;
  setColor: (state: string) => void;
}

const basicColors = [
  '#000000',
  '#C0C0C0',
  '#808080',
  '#FFFFFF',
  '#800000',
  '#FF0000',
  '#800080',
  '#FF00FF',
  '#008000',
  '#00FF00',
  '#808000',
  '#FFFF00',
  '#000080',
  '#0000FF',
  '#008080',
  '#00FFFF',
];

const ColorPicker = ({ setVisible, visible, setColor }: Props) => {
  return (
    <AppModal
      heightValue={hp(60)}
      setVisible={setVisible}
      visible={visible}
      content={
        <Box style={styles.container}>
          {basicColors.map((c, index) => (
            <TouchableOpacity
              key={index.toString()}
              onPress={() => {
                setColor(c);
                setVisible(false);
              }}
            >
              <Box
                style={{
                  backgroundColor: c,
                  width: 60,
                  height: 60,
                  borderWidth: 1,
                  borderColor: theme.colors.border,
                  borderRadius: 35,
                  marginBottom: 10,
                  marginRight: 15,
                }}
              />
            </TouchableOpacity>
          ))}
        </Box>
      }
    />
  );
};

export default ColorPicker;

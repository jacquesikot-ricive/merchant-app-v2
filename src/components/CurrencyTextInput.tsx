import React from 'react';
import { StyleSheet } from 'react-native';
import CurrencyInput from 'react-native-currency-input';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import useCurrency from '../hooks/useCurrency';
import theme from './Themed';

const styles = StyleSheet.create({
  container: {},
});

interface Props {
  onChangeValue: any;
  value: any;
  width?: number;
  height?: number;
  placeholder?: string;
}
const CurrencyTextInput = ({
  onChangeValue,
  value,
  width,
  height,
  placeholder,
}: Props) => {
  const { returnCurrency } = useCurrency();
  return (
    <CurrencyInput
      value={value}
      onChangeValue={(e) => onChangeValue(e ? e : '')}
      prefix={returnCurrency().code + ' '}
      separator="."
      delimiter=","
      precision={2}
      minValue={0}
      placeholder={placeholder}
      placeholderTextColor={theme.colors.text5}
      style={{
        width: width ? width : wp(80),
        alignSelf: 'center',
        borderWidth: 1,
        borderColor: theme.colors.border1,
        borderRadius: 6,
        backgroundColor: theme.colors.white,
        height: height ? height : 58,

        paddingHorizontal: 15,
        fontFamily: 'basier-regular',
        fontSize: 14,
      }}
    />
  );
};

export default CurrencyTextInput;

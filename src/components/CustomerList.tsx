import React, { useState, useCallback } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import * as Linking from 'expo-linking';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import DeleteIcon from '../../svgs/DeleteIcon';
import EditIcon from '../../svgs/EditIcon';
import ItemMenuIcon from '../svg/ItemMenuIcon';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: theme.colors.bg5,
    paddingBottom: 10,
  },
  list: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  },
  textSpacing: {
    paddingTop: 4,
    paddingBottom: 4,
  },
  flex: {
    flexDirection: 'row',
  },
  rightSpacing: {
    paddingRight: 10,
  },
  border: {
    paddingRight: 10,
    fontSize: 20,
  },
  imageContainer: {
    width: 48,
    height: 48,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
});

interface Props {
  colorMax: number;
  id: number;
  name: string;
  phone: string;
  email: string;
  image?: any;
  onPress: any;
  handleDelete: () => void;
  handleEdit: () => void;
}

const CustomerList: React.FC<Props> = ({
  id,
  name,
  image,
  email,
  phone,
  onPress,
  handleDelete,
  handleEdit,
  colorMax,
}) => {
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const returnInitials = () => {
    const fullName = name.split(' ');
    const firstName = fullName[0];
    const lastName = fullName[fullName.length - 1];

    return `${firstName.charAt(0).toUpperCase()}${
      lastName.length > 0 ? lastName.charAt(0).toUpperCase() : ''
    }`;
  };

  const returnProfileBgColor = (max: number) => {
    const colors = [
      'rgba(252, 171, 154, 0.3)',
      'rgba(127, 241, 252, 0.2)',
      'rgba(193, 250, 179, 0.2)',
      'rgba(217, 217, 271, 0.2)',
      'rgba(251, 173, 49, 0.2)',
      'rgba(253, 143, 50, 0.2)',
      'rgba(48, 79, 254, 0.24)',
    ];

    const colorIndex = Math.floor(Math.random() * (max - 0 + 1) + 0);

    return colors[colorIndex];
  };

  const [profileBgColor, setProfileBgColor] = useState<string>(
    returnProfileBgColor(colorMax)
  );

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.7}
      style={styles.container}
    >
      {/* <TouchableOpacity
        onPress={() => Linking.openURL(`tel:${phone}`)}
        style={{
          position: 'absolute',
          right: 40,
          top: hp(4.5),
          width: 30,
          height: 30,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: theme.colors.light,
          borderRadius: 15,
        }}
      >
        <Icon name="phone-call" color={theme.colors.text7} size={15} />
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => Linking.openURL(`sms:${phone}`)}
        style={{
          position: 'absolute',
          right: 90,
          top: hp(4.5),
          width: 30,
          height: 30,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: theme.colors.light,
          borderRadius: 15,
        }}
      >
        <Icon name="message-circle" color={theme.colors.text7} size={15} />
      </TouchableOpacity> */}
      <Box style={styles.list}>
        <Box>
          {image ? (
            <Image
              source={image}
              style={{ width: 48, height: 48, marginRight: 10 }}
            />
          ) : (
            <Box
              style={[
                styles.imageContainer,
                {
                  backgroundColor: profileBgColor,
                },
              ]}
            >
              <Text variant={'Body1R'}>{returnInitials()}</Text>
            </Box>
          )}
        </Box>
        <Box>
          <Text
            numberOfLines={2}
            variant="DetailsM"
            color="text6"
            style={{
              width: widthPercentageToDP(40),
            }}
          >
            {name}
          </Text>
          {/* {email && (
            <Box style={styles.textSpacing}>
              <Text
                variant="SmallerTextR"
                color="text9"
                style={{ width: '90%' }}
                numberOfLines={1}
              >
                {email}
              </Text>
            </Box>
          )} */}
          <Text
            variant="SmallerTextR"
            color="text5"
            style={{ letterSpacing: 0.7 }}
          >
            {phone}
          </Text>
        </Box>
      </Box>

      <Box flex={1} />

      {/* <TouchableOpacity
        style={{
          padding: 5,
          position: 'absolute',
          right: 0,
          top: 10,
        }}
        onPress={() => {
          setShowMenu(!showMenu);
        }}
      >
        <ItemMenuIcon />
      </TouchableOpacity> */}

      <Box flexDirection={'row'} alignItems={'center'} mt="xxl">
        {/* <TouchableOpacity style={{ marginRight: 17 }} onPress={handleEdit}>
          <EditIcon />
        </TouchableOpacity>
        <TouchableOpacity onPress={handleDelete}>
          <DeleteIcon />
        </TouchableOpacity> */}
        <TouchableOpacity
          style={{
            padding: 5,
            marginBottom: 15,
          }}
          onPress={onPress}
        >
          <Icon name={'chevron-right'} color={theme.colors.text5} size={20} />
        </TouchableOpacity>
      </Box>
    </TouchableOpacity>
  );
};

export default CustomerList;

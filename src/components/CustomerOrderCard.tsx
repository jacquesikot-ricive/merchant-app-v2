import { Feather } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import useCurrency from '../hooks/useCurrency';
import numberWithCommas from '../utils/numbersWithComma';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    borderBottomWidth: 1,
    borderColor: theme.colors.border4,
    flexDirection: 'row',
    // justifyContent: 'space-between',
    paddingHorizontal: wp(5),
    paddingVertical: 15,
  },
});

interface Props {
  id: string;
  date: string;
  amount: string;
  onPress: () => void;
}
const CustomerOrderCard = ({ id, date, amount, onPress }: Props) => {
  const { returnCurrency } = useCurrency();
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
      activeOpacity={0.7}
    >
      <Box>
        <Text variant={'DetailsR'} color="text1">
          {'Order #' + id}
        </Text>
        <Text variant={'SmallerTextR'} color="text7">
          {date}
        </Text>
      </Box>

      <Box flex={1} />

      <Text variant={'DetailsR'} color="text1" style={{ textAlign: 'right' }}>
        {returnCurrency().code + ' ' + numberWithCommas(amount)}
      </Text>

      <Box
        style={{
          justifyContent: 'center',
          marginLeft: 20,
        }}
      >
        <Feather name="chevron-right" size={14} color={theme.colors.text5} />
      </Box>
    </TouchableOpacity>
  );
};

export default CustomerOrderCard;

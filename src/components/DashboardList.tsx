import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import CustomerListIcon from '../../svgs/CustomerListIcon';

import theme, { Box, Text } from './Themed';
import CircleCheckbox from '../../svgs/CircleCheckbox';
import CheckedIcon from '../../svgs/CheckedIcon';
import laundryItems from '../api/merchant/laundryItems';
import { useAppSelector } from '../redux/hooks';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginRight: 26,
  },
  listSelected: {
    marginTop: 20,
    backgroundColor: theme.colors.bg10,
    height: 130,
    width: 256,
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderRadius: 8,
  },
  list: {
    marginTop: 20,
    backgroundColor: theme.colors.white,
    height: 130,
    width: 256,
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderRadius: 8,
  },
  baseText: {
    marginTop: 8,
  },
});

interface Props {
  title: string;
  description: string;
  selected: boolean;
}

export const data = [
  {
    id: 1,
    title: 'Add products and services',
    description: 'Dont start empty, begin adding products into your database.',
  },
  {
    id: 2,
    title: 'Setup your online store',
    description: 'Dont start empty, begin adding products into database.',
  },
  {
    id: 3,
    title: 'Setup payments and account',
    description:
      'Add you local bank account and start collecting payments with Ricive',
  },
  {
    id: 4,
    title: 'Complete your first order',
    description:
      'Complete your first order to get fully integrated to the system',
  },
];

const DashboardList: React.FC<Props> = ({ title, description, selected }) => {
  return (
    <TouchableOpacity style={styles.container} activeOpacity={1}>
      {selected ? (
        <Box style={styles.listSelected}>
          <Box>
            <CheckedIcon />
          </Box>
          <Text variant="SmallerTextM" color="text8" mt="s">
            {title}
          </Text>
          <Box style={styles.baseText}>
            <Text variant="SmallerTextR" color="text2">
              {description}
            </Text>
          </Box>
        </Box>
      ) : (
        <Box style={styles.list}>
          <Box>
            <CircleCheckbox />
          </Box>
          <Text variant="SmallerTextM" color="text8" mt="s">
            {title}
          </Text>
          <Box style={styles.baseText}>
            <Text variant="SmallerTextR" color="text2">
              {description}
            </Text>
          </Box>
        </Box>
      )}
    </TouchableOpacity>
  );
};

export default DashboardList;

import React, { useState, useRef, useEffect } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Animated,
  ActivityIndicator,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../components/Themed';
import DashboardList, { data } from './DashboardList';
import { useAppSelector } from '../redux/hooks';

const styles = StyleSheet.create({
  container: {},
  slider: {
    width: wp(100),
    alignItems: 'center',
    // paddingRight: 20,
  },
  bottom: {
    width: wp(90),
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 15,
  },
  dashboardSetup: {
    marginTop: 15,
    justifyContent: 'flex-start',
    // paddingHorizontal: 15,
    paddingLeft: 20,
  },
});

interface Props {
  navigation: undefined;
  productsDone: boolean;
  storeDone: boolean;
  paymentDone: boolean;
  orderDone: boolean;
  loading: boolean;
}

const DashboardScrollView = ({
  navigation,
  productsDone,
  storeDone,
  orderDone,
  paymentDone,
  loading,
}: Props): JSX.Element => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const viewableItemsChanged = useRef(({ viewableItems }: any) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;
  const slidesRef = useRef<any>();

  const merchant_id = useAppSelector((state) => state.login.id);

  const getCompletion = () => {
    const product = productsDone ? 1 : 0;
    const store = storeDone ? 1 : 0;
    const payment = paymentDone ? 1 : 0;
    const order = orderDone ? 1 : 0;

    const sum = product + store + payment + order;

    return `${((sum / 4) * 100).toString()}%`;
  };

  const returnSelected = (title: string) => {
    if (title === data[0].title) return productsDone;
    if (title === data[1].title) return storeDone;
    if (title === data[2].title) return paymentDone;
    if (title === data[3].title) return orderDone;
    return false;
  };

  return (
    <SafeAreaView style={styles.container}>
      <Box style={styles.dashboardSetup}>
        <Text variant="DetailsM" color="primary5">
          COMPLETE YOUR SETUP
        </Text>
        <Text variant="SmallerTextR" color="text7">
          {`${getCompletion()} complete - nice work!`}
        </Text>
      </Box>
      <Box style={styles.slider}>
        <Animated.FlatList
          ListHeaderComponent={<Box style={{ width: 20 }} />}
          data={data}
          renderItem={({ item }: any) => {
            return (
              <DashboardList
                title={item.title}
                description={item.description}
                selected={returnSelected(item.title)}
              />
            );
          }}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          bounces={false}
          keyExtractor={(item: any) => item.id.toString()}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
            {
              useNativeDriver: true,
            }
          )}
          scrollEventThrottle={32}
          onViewableItemsChanged={viewableItemsChanged}
          viewabilityConfig={viewConfig}
          ref={slidesRef}
        />
      </Box>
    </SafeAreaView>
  );
};

export default DashboardScrollView;

import React, { FC, useState } from "react";
import { StyleSheet, TouchableOpacity, Modal, Platform } from "react-native";
import DateTimePicker, { Event } from "@react-native-community/datetimepicker";
import { Feather as Icon } from "@expo/vector-icons";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

import theme, { Box, Text } from "./Themed";

const styles = StyleSheet.create({
  container: {
    height: 79,
    justifyContent: "space-between",
  },
  box: {
    height: 55,
    borderRadius: 8,
    backgroundColor: theme.colors.white,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 22,
    alignItems: "center",
    borderColor: theme.colors.border1,
    borderWidth: 1,
  },
  picker: {
    backgroundColor: theme.colors.white,
    opacity: Platform.OS === "android" ? 0 : 1,
    width: "100%",
    height: "40%",
    position: "absolute",
    bottom: 0,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  pickerContainer: {
    backgroundColor: "rgba(0, 0, 0, 0.2)",
    opacity: Platform.OS === "android" ? 0 : 1,
    width: "100%",
    height: "100%",
  },
  error: {
    position: "absolute",
    top: 55,
    right: 1,
  },
  cancel: {
    width: 60,
    height: 30,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-end",
    backgroundColor: theme.colors.bg4,
  },
});

interface Props {
  label: string;
  date: Date;
  setDate: (date: any) => void;
  error?: string;
  width?: number;
  placeholder?: string;
}

const DatePicker: FC<Props> = ({
  label,
  date,
  setDate,
  error,
  width,
  placeholder,
}) => {
  const [visible, setVisible] = useState<boolean>(false);
  const onChangeIOS = (event: Event, selectedDate: any) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
  };

  const onChangeAndroid = (event: Event, selectedDate: any) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
    setVisible(false);
  };

  return (
    <Box style={[styles.container, { width: width || wp(90) }]}>
      <Text variant="DetailsR" color="text1" mb="m">
        {label}
      </Text>

      <TouchableOpacity onPress={() => setVisible(!visible)} style={styles.box}>
        <Text variant="DetailsR" color="text5">
          {date !== new Date()
            ? `${
                date.getMonth() + 1
              } / ${date.getDate()} / ${date.getFullYear()}`
            : "mm / dd / yy"}
        </Text>
        <Icon name="calendar" size={24} color={theme.colors.text7} />

        {error && (
          <Box style={styles.error}>
            <Text variant="DetailsR" color="error">
              {error}
            </Text>
          </Box>
        )}
      </TouchableOpacity>

      <Modal
        animationType="slide"
        transparent
        visible={visible}
        onRequestClose={() => {
          setVisible(!visible);
        }}
      >
        <Box style={styles.pickerContainer}>
          <Box style={styles.picker}>
            <TouchableOpacity
              activeOpacity={0.7}
              style={{
                padding: 20,
              }}
              onPress={() => setVisible(!visible)}
            >
              <Box style={styles.cancel}>
                <Text variant="Body2B" color="success">
                  Done
                </Text>
              </Box>
            </TouchableOpacity>
            <DateTimePicker
              testID="dateTimePicker"
              value={date}
              mode="date"
              display="spinner"
              onChange={Platform.OS === "ios" ? onChangeIOS : onChangeAndroid}
              style={{
                backgroundColor: theme.colors.white,
              }}
              textColor={theme.colors.text5}
            />
          </Box>
        </Box>
      </Modal>
    </Box>
  );
};

export default DatePicker;

import React from 'react';
import { StyleSheet } from 'react-native';
import DatePicker from 'react-native-date-picker';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import { TouchableOpacity } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  container: {
    width: wp(80),
  },
  textBox: {
    borderWidth: 1,
    borderColor: theme.colors.border1,
    borderRadius: 6,
    height: 58,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 10,
  },
});

interface Props {
  label: string;
  setDate: any;
  date: any;
  show: boolean;
  setShow: any;
}

const DateTimePicker = ({ label, setDate, show, setShow, date }: Props) => {
  return (
    <Box style={styles.container}>
      <Text
        variant={'DetailsR'}
        color="text1"
        mb="m"
        style={{ textAlign: 'left', width: '100%' }}
      >
        {label}
      </Text>

      <TouchableOpacity
        onPress={() => {
          setShow(true);
        }}
        activeOpacity={0.4}
        style={styles.textBox}
      >
        <Icon name="calendar" color={theme.colors.border} size={24} />

        <Text variant={'DetailsR'} color={'text5'} ml="m">
          {date
            ? new Date(date).toDateString() +
              ' ' +
              'at' +
              ' ' +
              new Date(date).toTimeString().slice(0, -18)
            : 'Select Date & Time'}
        </Text>

        <Box style={{ position: 'absolute', right: 10 }}>
          <Icon name="chevron-down" color={theme.colors.border} size={24} />
        </Box>
      </TouchableOpacity>

      <DatePicker
        date={new Date()}
        onConfirm={(date) => {
          setDate(date);
        }}
        modal
        open={show}
        onCancel={() => setShow(false)}
      />
    </Box>
  );
};

export default DateTimePicker;

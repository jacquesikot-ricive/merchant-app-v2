import React, { useState } from 'react';
import { Keyboard, StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import TextInput from './TextInput';
import Button from './Button';
import orderApi from '../api/riciveApi/order';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingVertical: 60,
  },
});

interface Props {
  order_id: string;
  refreshData: () => Promise<void>;
  visible: boolean;
  setVisible: (state: boolean) => void;
}

const DeclineOrderModal = ({
  order_id,
  refreshData,
  visible,
  setVisible,
}: Props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [reason, setReason] = useState<string>();

  const handleDecline = async () => {
    try {
      setLoading(true);
      await orderApi.declineOrder({
        order_id,
        reason: reason as string,
      });
      await refreshData();
      setLoading(false);
      setVisible(false);
      Toast.show({
        type: 'success',
        text1: 'Order Declined',
        text2: `Order #${order_id
          .substring(0, 5)
          .toUpperCase()} has been declined`,
      });
    } catch (error) {
      setLoading(false);
      console.log(error);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Sorry we cannot decline this order now, try again later..',
      });
    }
  };

  return (
    <AppModal
      heightValue={hp(40)}
      visible={visible}
      setVisible={setVisible}
      content={
        <Box style={styles.container}>
          <Text variant={'Body1M'}>Why are you declining the order?</Text>

          <Text variant={'DetailsR'} color="text5">
            This message will be sent to your customer
          </Text>

          <Box mt="xxl" mb="xxl">
            <TextInput
              placeholder="Decline message"
              type="input"
              multiline
              width={wp(80)}
              height={hp(10)}
              onKeyPress={(e) => {
                if (e.nativeEvent.key === 'Enter') {
                  Keyboard.dismiss();
                }
              }}
              onChangeText={(e) => setReason(e)}
            />
          </Box>

          <Button
            label="Decline Order"
            disabled={reason && reason?.length > 0 ? false : true}
            onPress={handleDecline}
            type="primary"
            width={wp(80)}
            loading={loading}
          />
        </Box>
      }
    />
  );
};

export default DeclineOrderModal;

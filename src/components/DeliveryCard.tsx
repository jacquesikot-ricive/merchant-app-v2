import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from '../components/Themed';

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    height: 180,
    paddingVertical: 19,
    backgroundColor: theme.colors.bg4,
    borderRadius: 10,
    marginBottom: 15,
  },
});

interface Props {
  serviceType: string;
  orderId: string;
  address: string | null;
  onPress: () => void;
  time: string;
}

const DeliveryCard: React.FC<Props> = ({
  serviceType,
  orderId,
  address,
  onPress,
  time,
}) => {
  return (
    <Box style={styles.container}>
      <Box style={{ paddingHorizontal: 15, marginBottom: 36 }}>
        <Box style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Box>
            <Text variant="Body1M" color="text6">
              Order {`#${orderId}`}
            </Text>
          </Box>
        </Box>
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Box>
            <Box style={{ marginTop: 14, flexDirection: 'row' }}>
              <Text variant="DetailsR" color="text5">
                Order Type:
              </Text>
              <Text ml="s" variant="DetailsR" color="text8">
                {`${serviceType} at ${time}`}
              </Text>
            </Box>
            <Box style={{ marginTop: 14 }}>
              <Text variant="DetailsR" color="text5">
                Address
              </Text>
              <Text variant="Body2R" color="text6">
                {address}
              </Text>
            </Box>
          </Box>
        </Box>
      </Box>
      <TouchableOpacity
        onPress={onPress}
        style={{
          position: 'absolute',
          right: 15,
          top: '50%',
        }}
      >
        <Icon name="chevron-right" color={theme.colors.text7} size={24} />
      </TouchableOpacity>
    </Box>
  );
};

export default DeliveryCard;

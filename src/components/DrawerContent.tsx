import React, { FC, useContext } from 'react';
import { StyleSheet, Alert, TouchableOpacity } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { AntDesign as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import HomeNavIcon from '../svg/homeTab/HomeIcon';
import TeamsNavIcon from '../svg/drawerNav/TeamsNavIcon';
import { useAppSelector } from '../redux/hooks';
import ServicesNavIcon from '../svg/drawerNav/ServicesNavIcon';
import ProductsNavIcon from '../svg/drawerNav/ProductsNavIcon';
import WalletNavIcon from '../svg/drawerNav/WalletNavIcon';
import StoreFrontNavIcon from '../svg/drawerNav/StoreFrontNavIcon';
import ProfileCard from './ProfileCard';
import useAuth from '../auth/useAuth';
import promptMessage from '../utils/prompt';
import { navigate } from '../navigation/navRef';
import LogoutIcon from '../svg/LogoutIcon';
import SupportIcon from '../svg/drawerNav/SupportIcon';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  content: {
    width: wp(62),
    marginTop: 60,
  },
  line: {
    width: '100%',
    height: 1,
    backgroundColor: theme.colors.text3,
    marginTop: hp(8),
    marginBottom: hp(2),
  },
  line2: {
    width: '100%',
    height: 1,
    backgroundColor: theme.colors.text3,
    marginTop: hp(5),
    marginBottom: hp(2),
  },
  logout: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 50,
    marginLeft: 30,
  },
});

const DrawerContent: FC = (props: any) => {
  const user = useAppSelector((state) => state.login.user);
  const role = user.profile.role;
  const { logout } = useAuth();
  return (
    <Box style={styles.container}>
      <DrawerContentScrollView bounces={false}>
        <Box style={styles.content}>
          <Box>
            <ProfileCard
              name={`${user.profile.first_name} ${user.profile.last_name}`}
              email={user.profile.email}
              onPress={() => {
                props.navigation.navigate('ProfileNav');
              }}
            />
          </Box>

          <DrawerItem
            style={{
              marginVertical: 5,
              marginTop: 40,
            }}
            labelStyle={{
              fontSize: 16,
              fontFamily: 'basier-regular',
              lineHeight: 24,
              color: theme.colors.text1,
              marginLeft: -15,
            }}
            icon={() => <HomeNavIcon color={theme.colors.text1} />}
            label="Dashboard"
            onPress={() => {
              props.navigation.navigate('HomeNav');
            }}
          />

          {role === 'SUPERADMIN' && (
            <>
              {/* <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <ProductsNavIcon color={theme.colors.text1} />}
                label="Products"
                onPress={() => {
                  props.navigation.navigate('ProductsNav');
                }}
              />
              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <ServicesNavIcon color={theme.colors.text1} />}
                label="Services"
                onPress={() => {
                  props.navigation.navigate('ServicesNav');
                }}
              /> */}
              <DrawerItem
                style={{
                  marginVertical: 10,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => (
                  <Icon size={24} name="team" color={theme.colors.text1} />
                )}
                label="Teams"
                onPress={() => {
                  props.navigation.navigate('TeamsNav');
                }}
              />
              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <WalletNavIcon color={theme.colors.text1} />}
                label="Wallet"
                onPress={() => {
                  props.navigation.navigate('WalletNav');
                }}
              />
              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <StoreFrontNavIcon color={theme.colors.text1} />}
                label="Website"
                onPress={() => {
                  props.navigation.navigate('StoreFrontNav');
                }}
              />
              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <SupportIcon color={theme.colors.text1} />}
                label="Support"
                onPress={() => {
                  props.navigation.navigate('SupportNav');
                }}
              />
            </>
          )}

          {role === 'ADMIN' && (
            <>
              {/* <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <ProductsNavIcon color={theme.colors.text1} />}
                label="Products"
                onPress={() => {
                  props.navigation.navigate('ProductsNav');
                }}
              /> */}
              {/* <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <ServicesNavIcon color={theme.colors.text1} />}
                label="Services"
                onPress={() => {
                  props.navigation.navigate('ServicesNav');
                }}
              /> */}
              <DrawerItem
                style={{
                  marginVertical: 10,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <TeamsNavIcon color={theme.colors.text1} />}
                label="Teams"
                onPress={() => {
                  props.navigation.navigate('TeamsNav');
                }}
              />

              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <WalletNavIcon color={theme.colors.text1} />}
                label="Wallet"
                onPress={() => {
                  props.navigation.navigate('WalletNav');
                }}
              />

              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <StoreFrontNavIcon color={theme.colors.text1} />}
                label="Website"
                onPress={() => {
                  props.navigation.navigate('StoreFrontNav');
                }}
              />

              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <SupportIcon color={theme.colors.text1} />}
                label="Support"
                onPress={() => {
                  props.navigation.navigate('SupportNav');
                }}
              />
            </>
          )}

          {role === 'TEAM' && (
            <>
              <DrawerItem
                style={{
                  marginVertical: 5,
                }}
                labelStyle={{
                  fontSize: 16,
                  fontFamily: 'basier-regular',
                  lineHeight: 24,
                  color: theme.colors.text1,
                  marginLeft: -15,
                }}
                icon={() => <SupportIcon color={theme.colors.text1} />}
                label="Support"
                onPress={() => {
                  props.navigation.navigate('SupportNav');
                }}
              />
            </>
          )}
        </Box>
      </DrawerContentScrollView>
    </Box>
  );
};

export default DrawerContent;

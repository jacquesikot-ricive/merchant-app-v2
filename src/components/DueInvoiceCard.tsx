import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import numberWithCommas from '../utils/numbersWithComma';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import useCurrency from '../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {},
});

interface Props {
  invoice: any;
  onPress: () => void;
  customerDetails?: boolean;
}

const DueInvoiceCard: FC<Props> = ({ invoice, onPress, customerDetails }) => {
  const { returnCurrency } = useCurrency();
  return (
    <Box
      style={{
        width: wp(90),
        borderWidth: 1,
        borderColor: theme.colors.border1,
        borderRadius: 6,
        paddingHorizontal: 20,
        paddingVertical: 10,
      }}
    >
      <Box justifyContent={'space-between'}>
        <Text variant={'Body2M'}>{`Invoice #${invoice.id}`}</Text>
        <Text variant={'DetailsR'} color="text7">
          {new Date(invoice.created_at).toDateString()}
        </Text>

        {customerDetails && (
          <Text variant={'DetailsM'} color="text7">
            {invoice.user_id.name}
          </Text>
        )}
        <Box
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 5,
          }}
        >
          <Text mr="m" variant={'DetailsR'} color="text7">
            Status:
          </Text>

          <Box
            style={{
              height: 20,
              paddingHorizontal: 10,
              borderRadius: 15,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: !invoice.is_paid
                ? theme.colors.errorLight
                : theme.colors.secondary2,
            }}
          >
            <Text
              variant={'SmallerTextM'}
              color={!invoice.is_paid ? 'error' : 'primary1'}
            >
              {!invoice.is_paid ? 'UNPAID' : 'PAID'}
            </Text>
          </Box>
        </Box>
      </Box>

      <Box style={{ position: 'absolute', right: 20, bottom: 5 }}>
        <Text variant={'Body2M'} color="text7">{`${
          returnCurrency().code
        } ${numberWithCommas(invoice.total)}`}</Text>
      </Box>

      <TouchableOpacity
        onPress={onPress}
        style={{
          position: 'absolute',
          right: 20,
          top: 20,
          padding: 10,
        }}
      ></TouchableOpacity>
    </Box>
  );
};

export default DueInvoiceCard;

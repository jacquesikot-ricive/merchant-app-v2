import React, { FC, useRef, useState } from 'react';
import { StyleSheet, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import TeamAppModal from './TeamAppModal';
import TeamDeleteIcon from '../svg/TeamDeleteIcon';
import TeamEditIcon from '../svg/TeamEditIcon';
import Picker from './Picker';
import Button from './Button';

const styles = StyleSheet.create({
  content: {
    marginTop: 30,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  picker: {
    // width: 10,
    marginRight: 5,
  },
  button: {
    marginTop: 112,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  image: {
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 40,
    borderColor: theme.colors.border1,
  },
  modalTitle: {
    marginLeft: 150,
    marginTop: -10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalContent: {
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 1,
    paddingTop: 17,
    paddingBottom: 18,
    width: wp(100),
    alignItems: 'center',
  },
  modalTextInput: {
    paddingHorizontal: 20,
    marginTop: 18,
  },
  imageContainer: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: theme.colors.secondary3,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
});

interface Props {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  isEdit: boolean;
  loading: boolean;
  data?: {
    id: string;
    image: any;
    email: string;
    first_name: string;
    last_name: string;
    role: any;
  };
  name?: any;
  colorNum?: any;
  EditPress: () => void;
  value: any;
  setValue: any;
  roleData: any;
}

const EditTeam: FC<Props> = ({
  visible,
  setVisible,
  isEdit,
  loading,
  data,
  colorNum,
  name,
  EditPress,
  value,
  setValue,
  roleData,
}) => {
  const returnInitials = () => {
    return (
      <Box
        style={{
          flexDirection: 'row',
          backgroundColor: 'rgba(252, 171, 154, 0.3)',
          width: 60,
          height: 60,
          borderRadius: 50,
          paddingTop: 15,
          paddingLeft: 18,
        }}
      >
        <Text variant={'Body1SB'} color="text5">
          {data?.first_name[0]}
        </Text>
        <Text variant={'Body1SB'} color="text5">
          {data?.last_name[0]}
        </Text>
      </Box>
    );

    // return `${data?.first_name.charAt(0)} ${data?.last_name?.charAt(0)}`;
  };

  const returnProfileBgColor = (max: number) => {
    const colors = [
      'rgba(252, 171, 154, 0.3)',
      'rgba(127, 241, 252, 0.2)',
      'rgba(193, 250, 179, 0.2)',
      'rgba(217, 217, 271, 0.2)',
      'rgba(251, 173, 49, 0.2)',
      'rgba(253, 143, 50, 0.2)',
      'rgba(48, 79, 254, 0.24)',
    ];

    const colorIndex = Math.floor(Math.random() * (max - 0 + 1) + 0);

    return colors[colorIndex];
  };

  // const roleData = [
  //   {
  //     label: '',
  //     value: ''
  //   },
  //   {
  //     label: 'Admin',
  //     value: 'Admin'
  //   },
  //   {
  //     label: 'Staff',
  //     value: 'Staff'
  //   },
  //   {
  //     label: 'Dispatch',
  //     value: 'Dispatch'
  //   },
  // ]

  return (
    <TeamAppModal
      setVisible={setVisible}
      visible={visible}
      heightValue={478}
      width={390}
      content={
        <>
          <Box style={styles.content}>
            <Text variant="Body2B" color="text1" ml="xl" mt="l">
              Edit Team Member
            </Text>
            <Box
              style={{
                borderBottomWidth: 1,
                borderBottomColor: theme.colors.border1,
                width: '100%',
                marginTop: 20,
              }}
            />
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 20,
                paddingHorizontal: 20,
              }}
            >
              <Box>
                {data?.image ? (
                  <Image
                    source={data?.image}
                    style={{ width: 60, height: 60, marginRight: 20 }}
                  />
                ) : (
                  <Box
                    style={[
                      styles.imageContainer,
                      { backgroundColor: returnProfileBgColor(colorNum) },
                    ]}
                  >
                    <Text variant={'Body1SB'}>{returnInitials()}</Text>
                  </Box>
                )}
              </Box>
              <Box>
                <Box style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text variant="Body2M" color="text5" mr="s">
                    {data?.first_name}
                  </Text>
                  <Text variant="Body2M" color="text5">
                    {data?.last_name}
                  </Text>
                </Box>
                <Box>
                  <Text variant="SmallerTextR" color="text7">
                    {data?.email}
                  </Text>
                </Box>
              </Box>
            </Box>
            <Box
              style={{
                borderBottomWidth: 1,
                borderBottomColor: theme.colors.border1,
                width: '100%',
                marginTop: 20,
              }}
            />
            <Box
              style={{ alignItems: 'center', marginTop: 20, marginBottom: 20 }}
            >
              <Picker
                label="Select role"
                placeholder={''}
                value={value}
                setValue={setValue}
                data={roleData}
              />
            </Box>
            <Box
              style={{
                borderBottomWidth: 1,
                borderBottomColor: theme.colors.border1,
                width: '100%',
                marginTop: 20,
              }}
            />
            <Box style={{ alignItems: 'center', marginTop: 20 }}>
              <Button type="primary" label="Save Changes" onPress={EditPress} />
            </Box>
          </Box>
        </>
      }
    />
  );
};

export default EditTeam;

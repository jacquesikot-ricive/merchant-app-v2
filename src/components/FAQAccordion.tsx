import React, { useState } from 'react';
import { LayoutAnimation, TouchableOpacity, StyleSheet } from 'react-native';

import MinusAccordionIcon from '../svg/MinusAccordionIcon';
import PlusAccordionIcon from '../svg/PlusAccordionIcon';
import theme, { Box, Text } from './Themed';


const styles = StyleSheet.create({
    accordion: {
        width: 375,
        paddingVertical: 20,
        paddingHorizontal: 10,
        backgroundColor: theme.colors.white,
        // borderWidth: 1.0,
        borderBottomWidth: 1,
        borderBottomColor: theme.colors.border1,
        
      },
      priceAccordion: {
        paddingVertical: 20,
        paddingHorizontal: 15,
        backgroundColor: theme.colors.white,
        // borderWidth: 1.0,
        borderBottomWidth: 1,
        borderBottomColor: theme.colors.border1,
      },
      heading: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
      hidden: {
        height: 0,
    },
    list: {
        overflow: 'hidden',
      },
})

interface Props {
    title: string;
    description: string;
}

const components: React.FC<Props> = ({ title, description}) => {
    const [onlineBoookingAccordion, setOpenOnlineBookingAccordion] =
      useState<boolean>(false);
    
    const toggleOnlineBooking = () => {
        setOpenOnlineBookingAccordion((value) => !value);
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      };
    return (
        <Box style={styles.priceAccordion}>
        <TouchableOpacity
          onPress={toggleOnlineBooking}
          style={styles.heading}
          activeOpacity={0.6}
        >
          <Text variant="SmallerTextM" color="text1">
            {title}
          </Text>
          {onlineBoookingAccordion ? <MinusAccordionIcon />  : <PlusAccordionIcon/>}
        </TouchableOpacity>
        <Box
          style={[
            styles.list,
            !onlineBoookingAccordion ? styles.hidden : undefined,
          ]}
        >
          <Box>
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 16,
              }}
            >
              <Text variant="SmallerTextR" color="text7" ml="xl">
                {description}
              </Text>
            </Box>
          </Box>
        </Box>
      </Box>
  );
}

export default components;
import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';

import theme, { Box, Text } from '../components/Themed';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    height: 65,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'space-around',
    borderTopWidth: 1,
    borderTopColor: theme.colors.border1,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1
  },
  PillActive: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    height: 36,
    borderRadius: 25,
    borderWidth: 2,
    borderColor: theme.colors.primary1,
    paddingVertical: 2,
    paddingHorizontal: 8,
    
  },
  PillInActive: {
    width: 90,
    alignItems: 'center',
    justifyContent: 'center',
    height: 36,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: theme.colors.primary1,
    paddingVertical: 2,
    paddingHorizontal: 8,
  },
});

interface Props {
  active: string;
  handleSwitch: any;
}

const FAQHeader: FC<Props> = ({ active, handleSwitch }) => {
  const handlePress = (state: string) => {
    // Add Haptics here
    handleSwitch();
  };
  return (
    <Box style={styles.container}>
        <Box  ml="xxl">
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => handlePress('general')}
            style={
            active 
                ? styles.PillActive
                : styles.PillInActive
            }
           >
            <Text
                variant={active  ? 'Body2M' : 'Body2R'}
                color={active ? 'primary1' : 'primary1'}
                >
                General
            </Text>
          </TouchableOpacity>
        </Box>

        <Box ml="xxl">
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => handlePress('login')}
            style={
            active 
                ? styles.PillActive
                : styles.PillInActive
            }
           >
            <Text
                variant={active  ? 'Body2M' : 'Body2R'}
                color={active ? 'primary1' : 'primary1'}
                >
                Login
            </Text>
          </TouchableOpacity>
        </Box>
        <Box ml="xxl">
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => handlePress('account')}
            style={
            active 
                ? styles.PillActive
                : styles.PillInActive
            }
           >
            <Text
                variant={active  ? 'Body2M' : 'Body2R'}
                color={active ? 'primary1' : 'primary1'}
                >
                Account
            </Text>
          </TouchableOpacity>
        </Box>
        <Box ml="xxl">
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={() => handlePress('message')}
            style={
            active 
                ? styles.PillActive
                : styles.PillInActive
            }
           >
            <Text
                variant={active  ? 'Body2M' : 'Body2R'}
                color={active ? 'primary1' : 'primary1'}
                >
                Message
            </Text>
          </TouchableOpacity>
        </Box>
    </Box>
  );
};

export default FAQHeader;

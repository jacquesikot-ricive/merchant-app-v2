import React, { FC } from 'react';
import { ActivityIndicator, StyleSheet, TouchableOpacity } from 'react-native';
import FloatingPlus from '../../../svgs/Floatingplus';

import theme, { Box, Text } from '../Themed';

interface Props {
  onPress: () => void;
}

const FloatingButton: FC<Props> = ({ onPress }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={onPress}
      style={styles.touch}
    >
      <Box style={styles.button}>
        <FloatingPlus />
      </Box>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touch: {
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 15,
    marginBottom: 33,
  },
  button: {
    // resizeMode: 'contain',
    width: 60,
    height: 60,
    borderRadius: 50,
    padding: 15,
    alignItems: 'center',
    backgroundColor: theme.colors.primary1,
    zIndex: 1,
  },
});

export default FloatingButton;

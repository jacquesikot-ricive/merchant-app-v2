import React, { useState } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import theme, { Box, Text } from '../components/Themed';
import { useAppSelector } from '../redux/hooks';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { AntDesign } from '@expo/vector-icons';
import CheckedCircleIcon from '../svg/CheckedCircleIcon';
import UncheckedCircleIcon from '../svg/UncheckedCircleIcon';

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    alignItems: 'center',
    marginBottom: 10,
    borderRadius: 8,
    height: 96,
    backgroundColor: theme.colors.bg1,
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  layout: {
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 8,
    justifyContent: 'space-around',
  },
  textWidth: {
    width: 250,
  },
});

export interface GettingStartedProps {
  title: string;
  description: string;
  onPress: () => any;
  active: boolean;
}

const GettingStartedCard: React.FC<GettingStartedProps> = ({
  title,
  description,
  onPress,
  active,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={styles.container}
    >
      {!active ? (
        <TouchableOpacity onPress={onPress}>
          <UncheckedCircleIcon />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity>
          <CheckedCircleIcon />
        </TouchableOpacity>
      )}
      <Box>
        <Text variant="DetailsM" color="text1">
          {title}
        </Text>
        <Box style={styles.textWidth}>
          <Text variant="SmallerTextR" color="text2">
            {description}
          </Text>
        </Box>
      </Box>

      <TouchableOpacity onPress={onPress}>
        <AntDesign name="arrowright" size={18} color={theme.colors.text5} />
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

export default GettingStartedCard;

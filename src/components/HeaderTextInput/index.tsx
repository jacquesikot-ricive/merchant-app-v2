import React, { FC, useState } from 'react';
import {
  StyleSheet,
  TextInput as RNTextInput,
  TouchableOpacity,
  TextInputProps,
} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from '../Themed';
import HeaderTextSearchIcon from '../../../svgs/HeaderTextSearchIcon';

const styles = StyleSheet.create({
  container: {
    // height: 56,
    // marginBottom: 25
  },
  input: {
    height: 40,
    borderRadius: 8,
    // borderWidth: 1,
    backgroundColor: theme.colors.bg14,
    // borderColor: theme.colors.border1,
    paddingLeft: 25,
    fontFamily: 'basier-regular',
    fontSize: 14,
  },
  inputContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    height: 48,
  },
  eye: {
    position: 'absolute',
    right: 20,
  },
  flag: {
    position: 'absolute',
    zIndex: 10,
    marginLeft: 15,
    flexDirection: 'row',
    borderRightWidth: 1.5,
    height: 41,
    alignItems: 'center',
    borderColor: theme.colors.text2,
    paddingRight: 15,
  },
  search: {
    position: 'absolute',
    zIndex: 10,
    marginLeft: 15,
    flexDirection: 'row',
    marginRight: wp(2),
  },
  error: {
    position: 'absolute',
    top: 28,
  },
  location: {
    position: 'absolute',
    right: 20,
    zIndex: 1,
  },
});

interface Props extends TextInputProps {
  type: 'search' | 'url';
  placeholder: string;
  icon?: 'search';
  inputRef?: any;
  error?: string;
  width?: number;
  errorRight?: number;
  location?: () => void;
  height?: number;
}

const TextInput: FC<Props> = ({
  placeholder,
  icon,
  type,
  inputRef,
  error,
  width,
  errorRight,
  location,
  ...props
}) => {
  return (
    <Box style={styles.container}>
      <Box style={styles.inputContainer}>
        {type === 'search' && (
          <Box style={styles.search}>
            <HeaderTextSearchIcon />
          </Box>
        )}

        {location && (
          <TouchableOpacity style={styles.location} onPress={location}>
            <Icon name="map-pin" size={20} color={theme.colors.text2} />
          </TouchableOpacity>
        )}

        <RNTextInput
          placeholder={placeholder}
          style={[
            styles.input,
            {
              // eslint-disable-next-line no-nested-ternary
              paddingLeft: type === 'search' ? 45 : 25,
              width: width || theme.layout.screenWidth,
            },
          ]}
          placeholderTextColor={theme.colors.text5}
          ref={inputRef}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...props}
        />

        {error && (
          <Box style={[styles.error, { right: errorRight || 10 }]}>
            <Text variant="DetailsR" color="error">
              {error}
            </Text>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default TextInput;

import React, { FC } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import { useAppSelector } from '../redux/hooks';
import numberWithCommas from '../utils/numbersWithComma';
import { Feather } from '@expo/vector-icons';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: wp(49.5),
  },
  content: {
    width: wp(42),
    height: 82,
    backgroundColor: theme.colors.white,
    borderRadius: 10,
    padding: 10,
    marginTop: 24,
  },
  numberBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 5,
    marginLeft: 10,
  },
  topText: {
    textAlign: 'right',
  },
  amountText: {
    flexDirection: 'row',
  },
});

interface Props {
  title: string;
  value: string;
  loading: boolean;
  difference?: string;
}

const HomeCard: FC<Props> = ({ title, value, loading, difference }) => {
  const returnNormalColor =
    difference && difference.toString().match(/^-\d+$/)
      ? theme.colors.error
      : theme.colors.primary1;

  const returnUnpaidColor =
    difference && difference.toString().match(/^-\d+$/)
      ? theme.colors.primary1
      : theme.colors.error;

  return (
    <Box style={styles.container}>
      <Box style={styles.content}>
        <Box>
          <Text variant="SmallerTextR" color="text5">
            {title}
          </Text>
          <Box style={styles.amountText}>
            <Text variant={'DetailsM'}>{loading ? '' : value}</Text>
          </Box>
        </Box>
        {difference && difference > '0' && (
          <Box
            style={{
              position: 'absolute',
              right: 10,
              top: 10,
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <Text
              variant={'SmallerTextR'}
              style={{
                color:
                  title === 'Unpaid Invoices'
                    ? returnUnpaidColor
                    : returnNormalColor,
              }}
            >
              {difference.toString() + '%'}
            </Text>

            <Feather
              name={
                difference && difference.toString().match(/^-\d+$/)
                  ? 'arrow-down'
                  : 'arrow-up'
              }
              color={
                title === 'Unpaid Invoices'
                  ? returnUnpaidColor
                  : returnNormalColor
              }
            />
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default HomeCard;

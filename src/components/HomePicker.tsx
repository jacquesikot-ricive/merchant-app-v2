import React, { FC, useState } from 'react';
import { StyleSheet, TouchableOpacity, Modal } from 'react-native';
import { Picker as RNPicker, PickerProps } from '@react-native-picker/picker';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from '../components/Themed';

const styles = StyleSheet.create({
  container: {
    width: 105,
    height: 40,
    justifyContent: 'space-between',
  },
  box: {
    height: 40,
    borderRadius: 8,
    backgroundColor: theme.colors.light3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  picker: {
    backgroundColor: theme.colors.light,
    width: '100%',
    height: '40%',
    position: 'absolute',
    bottom: 0,
  },
  pickerContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    width: '100%',
    height: '100%',
    position: 'absolute',
    bottom: 0,
  },
  error: {
    position: 'absolute',
    top: 87,
    right: 1,
  },
});

interface DataProps {
  label: string;
  value: string;
}

interface Props extends PickerProps {
  label: string;
  placeholder: string;
  value: string;
  setValue: (state: any) => void;
  data: DataProps[];
  error?: string;
}

const HomePicker: FC<Props> = ({
  label,
  placeholder,
  value,
  setValue,
  data,
  error,
}) => {
  const [visible, setVisible] = useState<boolean>(false);

  return (
    <>
      <Box style={styles.container}>
        <Text variant="DetailsR" color="text1" mb="m">
          {label}
        </Text>

        <TouchableOpacity
          style={styles.box}
          activeOpacity={0.7}
          onPress={() => setVisible(!visible)}
        >
          <Text variant="DetailsR" color="text5" mr="s">
            {value !== '' ? value : placeholder}
          </Text>
          <Icon name="chevron-down" color={theme.colors.text2} size={14} />
        </TouchableOpacity>

        {error && (
          <Box style={styles.error}>
            <Text variant="DetailsR" color="error">
              {error}
            </Text>
          </Box>
        )}
      </Box>

      <Modal
        animationType="fade"
        transparent
        visible={visible}
        onRequestClose={() => {
          setVisible(!visible);
        }}
      >
        <Box style={styles.pickerContainer}>
          <Box style={styles.picker}>
            <TouchableOpacity
              style={{ padding: 20 }}
              onPress={() => setVisible(!visible)}
            >
              <Text variant="DetailsR" color="error">
                close
              </Text>
            </TouchableOpacity>
            <RNPicker
              selectedValue={value}
              onValueChange={(itemValue: string) => setValue(itemValue)}
              // eslint-disable-next-line react/jsx-props-no-spreading
            >
              {/* eslint-disable-next-line prettier/prettier */}
              {data.map((d) => (
                <RNPicker.Item label={d.label} value={d.value} key={d.value} />
              ))}
            </RNPicker>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default HomePicker;

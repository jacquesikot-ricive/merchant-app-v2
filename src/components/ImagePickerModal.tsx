import React, { FC, useEffect, useRef, useState } from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, Alert } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { useAnalytics } from '@segment/analytics-react-native';

import theme, { Box, Text } from './Themed';
import { useAppSelector } from '../redux/hooks';
import ProductAppModal from './ProductAppModal';
import GalleryIcon from '../svg/GalleryIcon';
import CameraIcon from '../svg/CameraIcon';
import ImageResizer from 'react-native-image-resizer';
import * as ImagePicker from 'expo-image-picker';
import { Camera } from 'expo-camera';

const styles = StyleSheet.create({
  content: {
    marginTop: 30,
    // marginHorizontal: 20,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  picker: {
    // width: 10,
    marginRight: 5,
  },
  button: {
    marginTop: 112,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  image: {
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 40,
    borderColor: theme.colors.border1,
  },
  bottomSheet: {
    paddingHorizontal: 10,
    marginTop: 17,
  },
  bottomSheetCamera: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 24,
    paddingHorizontal: 20,
  },
  bottomSheetBorder: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
  },
  bottomSheetGallery: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 24,
  },
});

interface Props {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  onPressImage: () => void;
  onPressCamera: () => void;
}

const ProductsCategoryModal: FC<Props> = ({
  visible,
  setVisible,
  onPressImage,
  onPressCamera,
}) => {
  const merchantId = useAppSelector((state) => state.login.user.business.id);

  return (
    <ProductAppModal
      setVisible={setVisible}
      visible={visible}
      heightValue={hp(30)}
      width={wp(100)}
      content={
        <>
          <Box style={styles.content}>
            <Box style={{ marginLeft: 150, marginTop: -10 }}>
              <Text variant="Body2M" color="text1">
                Upload media
              </Text>
            </Box>
            <Box style={styles.bottomSheet}>
              {/* <Box style={styles.bottomSheetBorder} />
              <TouchableOpacity
                style={styles.bottomSheetCamera}
                onPress={onPressCamera}
              >
                <CameraIcon />
                <Text ml="m" variant="DetailsR" color="text1">
                  Take a photo
                </Text>
              </TouchableOpacity>
              <Box style={styles.bottomSheetBorder} /> */}
              <TouchableOpacity
                style={styles.bottomSheetGallery}
                onPress={onPressImage}
              >
                <GalleryIcon />
                <Text ml="m" variant="DetailsR" color="text1">
                  Choose from gallery
                </Text>
              </TouchableOpacity>
              {/* <Box style={styles.bottomSheetBorder} /> */}
            </Box>
          </Box>
        </>
      }
    />
  );
};

export default ProductsCategoryModal;

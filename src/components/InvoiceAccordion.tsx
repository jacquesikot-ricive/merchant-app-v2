import React, { FC, useState } from 'react';
import {
  View,
  Animated,
  useWindowDimensions,
  StyleSheet,
  UIManager,
  LayoutAnimation,
  Platform,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import InvoiceCard from './InvoiceCard';
import ItemMenuIcon from '../svg/ItemMenuIcon';
import { useAppSelector } from '../redux/hooks';

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    width: '100%',
    backgroundColor: theme.colors.white,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: theme.colors.border1,
    borderBottomColor: theme.colors.border1,
  },
  heading: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  hidden: {
    height: 0,
  },
  list: {
    overflow: 'hidden',
    width: wp(100),
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  sectionTitle: {
    fontSize: 16,
    height: 30,
    marginLeft: '5%',
  },
});

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

interface Props {
  invoices: any[];
  navigation: any;
  invoiceItems: any[];
  handleCreateInvoice: () => void;
}

const InvoiceAccordion: FC<Props> = ({
  invoices,
  navigation,
  invoiceItems,
  handleCreateInvoice,
}) => {
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [isOpen, setIsOpen] = useState(true);

  const toggleOpen = () => {
    setIsOpen((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  return (
    <Box style={[styles.container, { paddingBottom: isOpen ? 20 : undefined }]}>
      <TouchableOpacity
        onPress={toggleOpen}
        style={styles.heading}
        activeOpacity={0.6}
      >
        <Text variant="Body2M" color="text6" style={{ padding: 20 }}>
          Invoice
        </Text>

        <Icon
          name={isOpen ? 'chevron-up' : 'chevron-down'}
          size={18}
          color="black"
          style={{
            padding: 20,
          }}
        />
      </TouchableOpacity>
      {isOpen && (
        <FlatList
          data={invoices}
          keyExtractor={(item: any) => item.id.toString()}
          ListFooterComponent={() =>
            userPermissions.create_invoice && (
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={handleCreateInvoice}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginVertical: 10,
                  marginLeft: 20,
                }}
              >
                <Icon
                  name="plus-circle"
                  size={20}
                  color={theme.colors.primary1}
                />
                <Text variant={'DetailsR'} color="primary1" ml="m">
                  Add New Invoice
                </Text>
              </TouchableOpacity>
            )
          }
          renderItem={({ item }) => {
            const firstName = item.customer_customerToinvoice.first_name
              ? item.customer_customerToinvoice.first_name
              : '';
            const lastName = item.customer_customerToinvoice.last_name
              ? item.customer_customerToinvoice.last_name
              : '';

            // console.log('ITEM:', item);
            return (
              <Box key={item.id.toString()} style={styles.list}>
                <InvoiceCard
                  invoiceId={`${item.id.substring(0, 5).toUpperCase()}`}
                  date={new Date(item.created_at).toDateString()}
                  firstName={firstName + ' ' + lastName}
                  customerDetails
                  total={item.total}
                  onPress={() =>
                    navigation.navigate('InvoiceDetail', {
                      dateCreated: new Date(item.created_at).toDateString(),
                      deliveryFee: item.delivery_fee,
                      deliveryFeeType: item.delivery_type && item.delivery_type,
                      id: item.id,
                      isSending: false,
                      serviceType: '',
                      status: item.is_paid ? 'PAID' : 'UNPAID',
                      total: item.total.toString(),
                      customerId: item.order_invoiceToorder.customer,
                      items: item.invoice_item,
                    })
                  }
                  invoice={item}
                />
              </Box>
            );
          }}
        />
      )}
    </Box>
  );
};

export default InvoiceAccordion;

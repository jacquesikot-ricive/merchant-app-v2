import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import numberWithCommas from '../utils/numbersWithComma';
import useCurrency from '../hooks/useCurrency';

export const invoiceData = [
  {
    id: '1',
    invoiceId: '10125',
    date: '2022-06-22 04:39:26',
    firstName: 'Karen',
    lastName: 'Ijezie',
    total: '5000',
  },
  {
    id: '2',
    invoiceId: '10125',
    date: '2022-06-22 04:39:26',
    firstName: 'Karen',
    lastName: 'Ijezie',
    total: '5000',
  },
  {
    id: '3',
    invoiceId: '10125',
    date: '2022-06-22 04:39:26',
    firstName: 'Karen',
    lastName: 'Ijezie',
    total: '5000',
  },
];

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
  },
});

interface Props {
  invoice: any;
  onPress?: () => void;
  customerDetails?: boolean;
  invoiceId: string;
  date: string;
  total: string;
  firstName: string;
  lastName?: string;
  hasPadding?: boolean;
  hasBorder?: boolean;
  fullWidth?: boolean;
}

const InvoiceCard: FC<Props> = ({
  invoice,
  onPress,
  customerDetails,
  invoiceId,
  date,
  total,
  firstName,
  lastName,
  hasPadding,
  hasBorder,
  fullWidth,
}) => {
  const { returnCurrency } = useCurrency();

  const finalLastName = lastName || '';

  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.7}
      style={[
        styles.container,
        {
          paddingHorizontal: hasPadding || fullWidth ? wp(5) : undefined,
          borderBottomWidth: hasBorder ? 1 : undefined,
          borderColor: theme.colors.border4,
          paddingBottom: 10,
          width: fullWidth ? wp(100) : wp(90),
        },
      ]}
    >
      <Box
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: wp(85),
          }}
        >
          <Box
            flexDirection={'row'}
            justifyContent="space-between"
            style={{
              width: wp(90),
            }}
          >
            <Box>
              <Text variant={'DetailsR'} color="text1">
                {'Invoice' + ' ' + '#' + invoiceId}
              </Text>

              {customerDetails && (
                <Text numberOfLines={1} style={{ width: wp(50) }}>
                  {/* <Text variant={'SmallerTextR'} color="text7">
                    {'Customer: '}
                  </Text> */}
                  <Text variant={'SmallerTextR'} color="text7">
                    {firstName + ' ' + finalLastName}
                  </Text>
                </Text>
              )}
            </Box>
            <Box
              style={{
                marginRight: 30,
                width: wp(30),
              }}
            >
              <Text
                variant={'DetailsR'}
                color="text1"
                style={{ textAlign: 'right' }}
              >
                {returnCurrency().code +
                  ' ' +
                  numberWithCommas(total ? total : '')}
              </Text>

              <Text
                variant={'SmallerTextR'}
                color="text7"
                style={{ textAlign: 'right' }}
              >
                {date}
              </Text>
            </Box>
          </Box>
        </Box>
        <TouchableOpacity onPress={onPress}>
          <Icon name="chevron-right" color={theme.colors.text7} size={20} />
        </TouchableOpacity>
      </Box>
    </TouchableOpacity>
  );
};

export default InvoiceCard;

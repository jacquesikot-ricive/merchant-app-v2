import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Feather as Icon, MaterialIcons } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import numberWithCommas from '../utils/numbersWithComma';
import useCurrency from '../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {},
  textRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 25,
  },
  plus: {
    borderRadius: 14,
    borderColor: theme.colors.primary1,
    borderWidth: 1,
    height: 28,
    width: 28,
    alignItems: 'center',
    justifyContent: 'center',
  },
  remove: {
    borderRadius: 16,
    backgroundColor: theme.colors.errorLight,
    height: 32,
    width: 32,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

interface Props {
  name: string;
  price: string;
  handleMinus: () => void;
  handlePlus: () => void;
  handleRemove: () => void;
  count: string;
  image?: string;
  hPadding?: boolean;
}

const InvoiceItem: FC<Props> = ({
  name,
  price,
  handlePlus,
  handleMinus,
  handleRemove,
  count,
  image,
  hPadding,
}) => {
  const { returnCurrency } = useCurrency();
  return (
    <Box
      style={[styles.textRow, { paddingHorizontal: hPadding ? 20 : undefined }]}
    >
      {image && image.length > 0 ? (
        <Image
          source={{
            uri: image,
          }}
          style={{ width: 42, height: 44, borderRadius: 6 }}
        />
      ) : (
        <Box
          style={{
            width: 42,
            height: 44,
            borderRadius: 6,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme.colors.light3,
          }}
        >
          <MaterialIcons
            name="image-not-supported"
            size={20}
            color={theme.colors.border1}
          />
        </Box>
      )}

      <Box>
        <Text
          variant="DetailsR"
          color="text2"
          numberOfLines={1}
          style={{ width: 170 }}
        >
          {name}
        </Text>
        <Text variant="DetailsM" color="text6">
          {`${returnCurrency().code} ${numberWithCommas(price)}`}
        </Text>
      </Box>
      <Box
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          width: 110,
        }}
      >
        <TouchableOpacity
          style={[
            styles.plus,
            { borderWidth: 1, borderColor: theme.colors.border },
          ]}
          onPress={handleMinus}
        >
          <Icon
            name={Number(count) < 2 ? 'trash' : 'minus'}
            color={theme.colors.text2}
            size={14}
          />
        </TouchableOpacity>

        <Text variant="SmallerTextR" color="text6">
          {count}
        </Text>

        <TouchableOpacity
          style={[styles.plus, { backgroundColor: theme.colors.white }]}
          onPress={handlePlus}
        >
          <Icon name="plus" color={theme.colors.primary1} size={14} />
        </TouchableOpacity>
      </Box>

      {/* <TouchableOpacity style={styles.remove} onPress={handleRemove}>
        <Icon name="x" color={theme.colors.error} size={24} />
      </TouchableOpacity> */}
    </Box>
  );
};

export default InvoiceItem;

import React, { useEffect, useState } from 'react';
import {
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Contacts from 'react-native-contacts';

import theme, { Box, Text } from './Themed';
import StackHeader from './StackHeader';
import HeaderTextInput from './HeaderTextInput';
import ListEmpty from './ListEmpty';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    position: 'absolute',
    width: wp(100),
    height: '100%',
    alignItems: 'center',
  },
});

interface Props {
  navigation: any;
  setVisible: (state: boolean) => void;
}
const ListContacts = ({ navigation, setVisible }: Props) => {
  const [contacts, setContacts] = useState<Contacts.Contact[]>([]);
  const [searchResult, setSearchResult] = useState<Contacts.Contact[]>([]);
  const [hasExtraNumber] = useState<boolean>(true);

  useEffect(() => {
    Platform.OS === 'ios'
      ? Contacts.getAll().then((contacts) => {
          setContacts(contacts);
          setSearchResult(contacts);
        })
      : PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          {
            title: 'Contacts',
            message: 'Ricive would like to view your contacts.',
            buttonPositive: 'Accept',
          }
        )
          .then((res) => {
            console.log('Permission: ', res);
            Contacts.getAll()
              .then((contacts) => {
                console.log(contacts);
                setContacts(contacts);
                setSearchResult(contacts);
              })
              .catch((e) => {
                console.log(e);
              });
          })
          .catch((error) => {
            console.error('Permission error: ', error);
          });
  }, []);

  //   useEffect(() => {
  //     PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
  //       title: 'Contacts',
  //       message: 'Ricive would like to view your contacts.',
  //       buttonPositive: 'Accept',
  //     });
  //   }, []);

  const handleSearch = (e: string) => {
    const givenNameSearch =
      contacts &&
      contacts.filter(
        (c: any) =>
          c.givenName &&
          c.givenName.trim().toLowerCase().includes(e.trim().toLowerCase())
      );

    const familyNameSearch =
      contacts &&
      contacts.filter(
        (c: any) =>
          c.familyName &&
          c.familyName.trim().toLowerCase().includes(e.trim().toLowerCase())
      );

    if (e.length > 0) {
      setSearchResult([...givenNameSearch, ...familyNameSearch]);
    } else {
      setSearchResult(contacts);
    }
  };

  return (
    <Box style={styles.container}>
      <StackHeader title="All Contacts" onBackPress={() => setVisible(false)} />

      <HeaderTextInput
        type="search"
        width={wp(90)}
        height={40}
        placeholder="Search contact name"
        onChangeText={handleSearch}
      />

      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingTop: 20,
        }}
        data={searchResult}
        ListEmptyComponent={() => {
          return <ListEmpty topText="No Contacts" bottomText="" invoicePage />;
        }}
        keyExtractor={(_, index) => index.toString()}
        renderItem={({ item }) => (
          <Box
            style={{
              justifyContent: 'center',
              paddingHorizontal: 20,
              paddingVertical: 10,
              width: wp(90),
              borderRadius: 8,
              borderWidth: 1,
              marginBottom: 10,
              borderColor: theme.colors.border,
            }}
          >
            <Box flexDirection={'row'} mb="xs">
              <Text variant={'DetailsM'} color={'text1'}>
                {item.givenName && item.givenName}
              </Text>
              <Text variant={'DetailsM'} color={'text1'} ml="s">
                {item.familyName}
              </Text>
            </Box>

            {hasExtraNumber &&
              item.phoneNumbers.map((phoneNumber, index) => (
                <TouchableOpacity
                  onPress={() => {
                    // setVisible(false);
                    navigation.navigate('AddCustomer', {
                      isEdit: false,
                      phone: phoneNumber.number,
                      firstName: item.givenName,
                      lastName: item.familyName,
                      isContacts: true,
                    });
                  }}
                  style={{
                    borderWidth: 1,
                    borderColor: theme.colors.border,
                    borderRadius: 8,
                    padding: 10,
                    marginTop: 10,
                  }}
                  key={index.toString()}
                >
                  <Text color={'text5'}>{phoneNumber.number}</Text>
                </TouchableOpacity>
              ))}
          </Box>
        )}
      />
    </Box>
  );
};

export default ListContacts;

import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import { Box, Text } from './Themed';
import Button from './Button';
import InvoiceListIcon from '../../svgs/InvoiceListIcon';

const styles = StyleSheet.create({
  orderEmpty: {
    alignItems: 'center',
    marginTop: hp(20),
  },
  gif: {
    justifyContent: 'center',
    alignItems: 'center',
    height: hp(25),
  },
});

interface Props {
  topText: string;
  bottomText: string;
  button?: boolean;
  buttonText?: string;
  onPressButton?: () => void;
  gif?: number;
  noImage?: boolean;
  invoicePage?: boolean;
}

const ListEmpty: FC<Props> = ({
  topText,
  bottomText,
  button,
  buttonText,
  onPressButton,
  gif,
  noImage,
  invoicePage,
}) => {
  return (
    <Box style={styles.orderEmpty}>
      {/* {!noImage && (
        <LottieView
          autoPlay
          loop
          style={styles.gif}
          // eslint-disable-next-line import/extensions
          source={gif || require('../assets/images/emptyListGif.json')}
        />
      )} */}

      {invoicePage ? (
        <>
          <InvoiceListIcon />
          <Text
            variant="DetailsR"
            color="text1"
            style={{ marginBottom: hp(0.2) }}
          >
            {topText}
          </Text>
        </>
      ) : (
        <>
          <Text
            variant="DetailsR"
            color="text1"
            style={{ marginBottom: hp(0.2) }}
          >
            {topText}
          </Text>
        </>
      )}
      <Text
        variant="DetailsR"
        color="text5"
        style={{ marginBottom: hp(2), textAlign: 'center', width: '90%' }}
      >
        {bottomText}
      </Text>

      {button && onPressButton && (
        <Button
          type="primary"
          label={buttonText || 'Button'}
          onPress={onPressButton}
          width={wp(85)}
        />
      )}
    </Box>
  );
};

export default ListEmpty;

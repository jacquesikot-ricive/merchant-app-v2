//import { StatusBar } from "expo-status-bar";
import { StatusBar } from 'react-native';
import React, { FC } from 'react';
import { StyleSheet, ActivityIndicator, Dimensions } from 'react-native';
import { color } from 'react-native-reanimated';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native';

import theme, { Box, Text } from './Themed';

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  backDrop: {
    height: height,
    width: width,
    backgroundColor: theme.colors.text10,
    opacity: 0.7,
    position: 'absolute',
    zIndex: 1,
  },
  indicator: {
    marginTop: '45%',
    height: 60,
    width: 60,
    marginBottom: 20,
  },
});

interface Props {
  color?: string;
}

const LoadingBackdrop: FC<Props> = ({ color }) => {
  return (
    <Box
      style={[StyleSheet.absoluteFillObject, styles.backDrop]}
      marginTop="xxxl"
      alignItems="center"
    >
      <StatusBar translucent backgroundColor={theme.colors.primary2} />
      <Box>
        <LottieView
          autoPlay
          loop
          style={styles.indicator}
          source={require('../assets/images/Loader.json')}
        />
      </Box>

      <Text variant="Body1R" color={'white'} marginTop="xl">
        Loading...
      </Text>
    </Box>
  );
};

export default LoadingBackdrop;

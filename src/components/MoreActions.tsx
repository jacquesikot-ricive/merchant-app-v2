import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AccountIcon from '../../svgs/AccountIcon';
import ArrowRightIcon from '../../svgs/ArrowRightIcon';
import StoreFrontIcon from '../../svgs/StoreFrontIcon';
import TeamsIcon from '../../svgs/TeamsIcon';
import WalletIcon from '../../svgs/WalletIcon';
import LogoutIcon from '../svg/LogoutIcon';
import SettingsIcon from '../svg/SettingsIcon';
import ChartIcon from '../svg/ChartIcon';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp(90),
    justifyContent: 'space-between',
    paddingVertical: 20,
  },
  leftIcon: {
    width: 40,
    height: 40,
    backgroundColor: theme.colors.secondary2,
  },
  border: {
    width: wp(100),
    alignItems: 'center',
    borderBottomColor: theme.colors.bg5,
    borderBottomWidth: 1,
  },
});

interface Props {
  onPress: () => void;
  label: string;
  labelBottom?: string;
  labelColor?: 'logout';
  iconType?:
    | 'teams'
    | 'wallet'
    | 'storefront'
    | 'account'
    | 'support'
    | 'invoice';
}

const MoreActions: FC<Props> = ({
  onPress,
  label,
  iconType,
  labelColor,
  labelBottom,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.6}
      style={styles.container}
    >
      {labelColor === 'logout' ? (
        <Box
          mt="xxl"
          ml="xl"
          style={{ flexDirection: 'row', width: wp(100), paddingLeft: 20 }}
        >
          <Text mr="xl" color="error">
            {label}
          </Text>
          <LogoutIcon color={theme.colors.error} />
        </Box>
      ) : (
        <Box style={styles.border}>
          <Box style={styles.item}>
            <Box flexDirection={'row'} alignItems={'center'}>
              {iconType === 'teams' ? (
                <TeamsIcon />
              ) : iconType === 'wallet' ? (
                <WalletIcon />
              ) : iconType === 'storefront' ? (
                <StoreFrontIcon />
              ) : iconType === 'account' ? (
                <AccountIcon />
              ) : iconType === 'support' ? (
                <SettingsIcon />
              ) : iconType === 'invoice' ? (
                <ChartIcon />
              ) : (
                <Box style={styles.leftIcon} />
              )}
              <Box ml="xl">
                <Text variant={'Body2M'}>{label}</Text>
                <Text variant={'SmallerTextR'} color="text7">
                  {labelBottom}
                </Text>
              </Box>
            </Box>
            <ArrowRightIcon />
          </Box>
        </Box>
      )}
    </TouchableOpacity>
  );
};

export default MoreActions;

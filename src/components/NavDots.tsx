// import React, { FC, ReactNode, useState } from 'react';
// import { StyleSheet, TouchableOpacity } from 'react-native';

// import theme, { Box, Text } from './Themed';
// import CustomerListIcon from '../../svgs/CustomerListIcon';

// const styles = StyleSheet.create({
//   container: {},
//   card: {
//     position: 'absolute',
//     backgroundColor: theme.colors.white,
//     padding: 10,
//     borderRadius: 6,
//     right: 0,
//     shadowColor: theme.colors.text3,
//     shadowOffset: {
//       width: 0,
//       height: 0,
//     },
//     shadowOpacity: 0.3,
//     shadowRadius: 6,
//     elevation: 1,
//   },
// });

// interface DataProps {
//   icon: ReactNode;
//   label: ReactNode;
//   onPress: () => void;
// }

// interface Props {
//   visible: boolean;
//   setVisible: any;
//   data: DataProps[];
// }

// const NavDots: FC<Props> = ({ visible, setVisible, data }) => {
//   //   const [cardShow, setCardShow] = useState<boolean>(false);

//   return (
//     <Box>
//       {visible ? (
//         <Box style={styles.card}>
//           <Text>View customer details</Text>
//         </Box>
//       ) : (
//         <TouchableOpacity onPress={() => setVisible()}>
//           <CustomerListIcon />
//         </TouchableOpacity>
//       )}
//     </Box>
//   );
// };

// export default NavDots;

import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
import * as Linking from 'expo-linking';

import Button from '../Button';
import theme, { Box, Text } from '../Themed';
import { returnStatusColors } from '../UpcomingOrderCard';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 19,
    backgroundColor: theme.colors.bg1,
    borderRadius: 10,
    marginBottom: 10,
  },
  buttonBox: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 10,
    marginTop: 10,
  },
  cardSpacing: {
    paddingHorizontal: 24,
    marginBottom: 20,
  },
  order: {
    flexDirection: 'row',
  },
  customer: {
    width: '100%',
    flexDirection: 'row',
  },
  timestamp: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttons: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
  },
  status: {
    flexDirection: 'row',
    height: 24,
    borderRadius: 8,
    backgroundColor: 'rgba(250, 172, 48, 0.05)',
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
});

export const newOrderData = [
  {
    id: '1',
    orderId: '102051',
    name: 'Karen Ijezie',
    timestamp: 'Wed, Jun 22 2022 | 03:00 - 04:00pm',
  },
  {
    id: '2',
    orderId: '102051',
    name: 'Karen Ijezie',
    timestamp: 'Wed, Jun 22 2022 | 03:00 - 04:00pm',
  },
];

interface Props {
  serviceType?: string;
  orderId: string;
  timestamp: string | null;
  schedule: string;
  onAccept?: () => void;
  onDecline?: () => void;
  acceptLoading?: boolean;
  declineLoading?: boolean;
  hideButtons?: boolean;
  customer?: any;
  name?: string;
  isBooking?: boolean;
  onPressOpen: () => void;
}

const NewOrderCard: React.FC<Props> = ({
  serviceType,
  orderId,
  timestamp,
  onAccept,
  onDecline,
  acceptLoading,
  declineLoading,
  customer,
  hideButtons,
  schedule,
  name,
  isBooking,
  onPressOpen,
}) => {
  return (
    <Box style={styles.container}>
      <Box style={styles.cardSpacing}>
        <Box
          flexDirection={'row'}
          alignItems={'center'}
          justifyContent={'space-between'}
        >
          <Box style={styles.order}>
            <Text variant="DetailsR" color="text1">
              Order #{orderId}
            </Text>

            {isBooking && (
              <Box
                style={[
                  styles.status,
                  {
                    backgroundColor: returnStatusColors('BOOKING').bg,
                  },
                ]}
              >
                <Text
                  variant="DetailsR"
                  style={{ color: returnStatusColors('BOOKING').text }}
                >
                  Booking
                </Text>
              </Box>
            )}
          </Box>
        </Box>
        <Box>
          <Box style={styles.customer}>
            <Text mr="s" variant="DetailsR" color="text5">
              Customer:
            </Text>
            <Text variant="DetailsR" color="text5">
              {customer.name}
            </Text>
            {/* <Text variant="SmallerTextR" color="text5">
                {name}
              </Text> */}
          </Box>

          {/* <Box style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text variant="DetailsR" color="text5">
                Phone
              </Text>
              <Text variant="DetailsR" color="text6">
                {`${customer.phone ? customer.phone : ''}`}
              </Text>
            </Box> */}
        </Box>

        <Box style={styles.timestamp}>
          {/* <Text variant="DetailsR" color="text5">
            Pick up:
          </Text> */}
          <Text variant="SmallerTextR" color="text5">
            {`${timestamp} `}
          </Text>
        </Box>

        {schedule.length > 0 && (
          <Box style={styles.timestamp}>
            <Text variant="SmallerTextR" color="text5">
              Scheduled for
            </Text>
            <Text variant="SmallerTextR" color="text5">
              {' ' + schedule}
            </Text>
          </Box>
        )}

        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            position: 'absolute',
            right: 20,
            top: 10,
          }}
        >
          <TouchableOpacity
            onPress={() => Linking.openURL(`tel:${customer.phone}`)}
            style={{
              width: 40,
              height: 40,
              marginLeft: 80,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.colors.bg5,
              borderRadius: 20,
              marginTop: 10,
            }}
          >
            <Icon name="phone-call" color={theme.colors.text7} size={20} />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={onPressOpen}
            style={{
              width: 40,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              // backgroundColor: theme.colors.bg5,
              borderRadius: 20,
              marginLeft: 30,
              marginTop: 10,
            }}
          >
            <Icon name="chevron-right" color={theme.colors.text7} size={20} />
          </TouchableOpacity>
        </Box>

        <Box
          style={{
            width: wp(100),
            height: 2,
            backgroundColor: theme.colors.bg5,
            alignSelf: 'center',
            marginTop: 10,
            marginBottom: -5,
          }}
        />
      </Box>
      {!hideButtons && (
        <Box style={styles.buttons}>
          <Button
            type={'secondary'}
            label="Decline"
            onPress={onDecline ? onDecline : () => true}
            width={wp(42)}
            loading={declineLoading}
            height={45}
          />
          <Button
            type={'primary'}
            label="Accept"
            onPress={onAccept ? onAccept : () => true}
            width={wp(42)}
            loading={acceptLoading}
            height={45}
          />
        </Box>
      )}
    </Box>
  );
};

export default NewOrderCard;

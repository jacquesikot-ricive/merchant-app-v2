import React, { FC, useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';

import AppModal from './AppModal';
import theme, { Box, Text } from './Themed';
import Button from './Button';
import TextInput from './TextInput';
import productApi from '../api/riciveApi/product';
import { useAppSelector } from '../redux/hooks';
import { useAnalytics } from '@segment/analytics-react-native';
import Checkbox from './Checkbox';
import useCurrency from '../hooks/useCurrency';
import CurrencyTextInput from './CurrencyTextInput';

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    paddingHorizontal: 20,
  },
  button: {
    marginTop: 20,
  },
  checkbox: {
    flexDirection: 'row',
    width: wp(90),
    alignItems: 'center',
    marginTop: 10,
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  setProduct: (state: any) => void;
  products: any[];
}

const NewProductModal: FC<Props> = ({
  visible,
  setVisible,
  setProduct,
  products,
}) => {
  const { track } = useAnalytics();
  const { returnCurrency } = useCurrency();
  const [name, setName] = useState<string>('');
  const [price, setPrice] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [publishProduct, setPublishProduct] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);

  const handleCreateProduct = async () => {
    try {
      setLoading(true);
      const product = await productApi.addProduct({
        name,
        price,
        business: business_id,
        is_published: publishProduct,
        price_type: 'FIXED',
        low_stock_level: '2',
      });
      track('Product Created From Order Screen', {
        business_id,
      });
      setProduct([
        ...products,
        {
          ...product.data,
          quantity: 1,
          item_id: product.data.id,
          total: Number(product.data.price),
        },
      ]);
      setLoading(false);
      setPrice('');
      setVisible(false);
    } catch (error: any) {
      setLoading(false);
      setVisible(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'Cannot add new product now, please try again later..',
      });
      console.log(JSON.stringify(error.response.data));
    }
  };

  return (
    <AppModal
      heightValue={hp(50)}
      visible={visible}
      setVisible={setVisible}
      content={
        <Box style={styles.container}>
          <Box>
            <Text variant={'DetailsR'} color="text6" mt="m" mb="m">
              Name
            </Text>

            <TextInput
              placeholder={'Product Name e.g Shirt'}
              type={'input'}
              width={wp(80)}
              onChangeText={(e) => setName(e)}
              keyboardType="default"
              autoCompleteType="name"
              autoCapitalize="words"
              returnKeyType="done"
              autoCorrect={false}
            />

            <Text variant={'DetailsR'} color="text6" mt="m" mb="m">
              Price
            </Text>

            <CurrencyTextInput
              value={price}
              onChangeValue={(t: string) => setPrice(t.toString())}
              placeholder={`Product Price in ${returnCurrency().name}`}
            />

            <TouchableOpacity
              style={styles.checkbox}
              onPress={() => {
                setPublishProduct(!publishProduct);
              }}
            >
              <Checkbox checked={publishProduct} />

              <Text variant={'Body2R'} color="text6" ml="l">
                Show on website
              </Text>
            </TouchableOpacity>

            <Box style={styles.button}>
              <Button
                disabled={name.length > 0 && price.length > 0 ? false : true}
                type="primary"
                label={'Add product'}
                loading={loading}
                onPress={handleCreateProduct}
                width={wp(80)}
              />
            </Box>
          </Box>
        </Box>
      }
    />
  );
};

export default NewProductModal;

import React, { FC, useEffect } from 'react';
import { StyleSheet } from 'react-native';
import { useNetInfo } from '@react-native-community/netinfo';
import Constants from 'expo-constants';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.colors.error,
    height: 25,
    position: 'absolute',
    top: Constants.statusBarHeight,
    width: '100%',
    zIndex: 1,
  },
});

const OfflineNotice: FC = () => {
  const netInfo = useNetInfo();

  if (netInfo.type !== 'unknown' && netInfo.isInternetReachable === false)
    return (
      <Box style={styles.container}>
        <Text variant="DetailsM" color="text4">
          No Internet Connection..
        </Text>
      </Box>
    );

  return null;
};

export default OfflineNotice;

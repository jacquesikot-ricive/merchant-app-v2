import React, { FC } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: wp(100),
  },
  topText: {
    marginTop: hp(5),
    width: '80%',
    textAlign: 'center',
  },
  bottomText: {
    width: wp(90),
    textAlign: 'center',
    marginTop: hp(2),
  },
  circle: {
    height: wp(90) - 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

interface Props {
  image: number;
  topText: string;
  bottomText: string;
  width: number;
  height: number;
}

const OnboardingSlider: FC<Props> = ({
  image,
  topText,
  bottomText,
  width,
  height,
}) => {
  return (
    <Box style={styles.container}>
      <Box style={styles.circle}>
        <Image
          source={image}
          style={{
            width,
            height,
            position: 'absolute',
            top: 30,
          }}
        />
      </Box>

      <Text style={styles.topText} variant="Body1M" color="text1">
        {topText}
      </Text>

      <Text style={styles.bottomText} variant="DetailsR" color="text9">
        {bottomText}
      </Text>
    </Box>
  );
};

export default OnboardingSlider;

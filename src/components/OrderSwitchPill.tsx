import React, { FC, useRef } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import * as Haptics from 'expo-haptics';

import theme, { Box, Text } from '../components/Themed';

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    flexDirection: 'row',
    height: 20,
  },
  item: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 2,
    width: 120,
    marginTop: -2,
    height: 50, // Check this
  },
});

interface Item {
  name: string;
  value: string;
}

interface Props {
  items: Item[];
  setActiveItem: (active: Item) => void;
  activeItem: Item;
}

const OrderSwitchPill: FC<Props> = ({ items, setActiveItem, activeItem }) => {
  const scrollRef = useRef<any>();
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}
      data={items}
      keyExtractor={(item: Item) => item.value}
      horizontal
      ref={scrollRef}
      renderItem={({ item, index }) => (
        <TouchableOpacity
          onPress={() => {
            setActiveItem(item);
            Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light);
            scrollRef.current.scrollToIndex({ animated: true, index: index });
          }}
          style={[
            styles.item,
            {
              borderColor:
                activeItem.value === item.value
                  ? theme.colors.primary1
                  : theme.colors.white,
            },
            {},
          ]}
        >
          <Text
            variant="DetailsR"
            color={activeItem.value === item.value ? 'primary1' : 'text2'}
          >
            {item.name}
          </Text>
        </TouchableOpacity>
      )}
    />
  );
};

export default OrderSwitchPill;

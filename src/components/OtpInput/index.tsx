import React, { FC } from "react";
import { StyleSheet, TextInput, TextInputProps } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

import theme, { Box, Text } from "../Themed";

const styles = StyleSheet.create({
  container: {
    width: 70,
    height: 60,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: theme.colors.border1,
    marginLeft: 10,
    marginRight: 10,
    marginTop: hp(3),
  },
  input: {
    height: "100%",
    fontSize: 24,
    alignSelf: "center",
    width: "100%",
    paddingLeft: "35%",
    fontFamily: "basier-bold",
    color: theme.colors.black,
  },
});

interface Props extends TextInputProps {
  refCallback?: any;
}

const OtpInput: FC<Props> = ({ refCallback, ...props }) => {
  return (
    <Box style={styles.container}>
      <TextInput
        style={styles.input}
        keyboardType="numeric"
        maxLength={1}
        ref={refCallback}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
      />
    </Box>
  );
};

export default OtpInput;

import React, { FC, useState } from 'react';
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import TextInput from './TextInput';
import Button from './Button';
import invoiceApi from '../api/riciveApi/invoice';
import { navigationRef } from '../navigation/navRef';
import CurrencyTextInput from './CurrencyTextInput';

const styles = StyleSheet.create({
  container: {},
  content: {
    marginTop: 50,
    marginHorizontal: 20,
  },
  button: {
    marginTop: 20,
  },
});

interface Props {
  visible: boolean;
  setVisible: any;
  invoice: any;
  navigation: any;
}

const PayCashModal: FC<Props> = ({
  visible,
  setVisible,
  invoice,
  navigation,
}) => {
  const [amount, setAmount] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  const handlePayCash = async () => {
    try {
      setLoading(true);
      await invoiceApi.updateInvoice({
        amount_collected: amount.toString(),
        customer: invoice.customer,
        id: invoice.id,
        is_cash: true,
        is_paid: true,
        total: invoice.total,
      });
      setLoading(false);
      navigation();
    } catch (error: any) {
      setLoading(false);
      setAmount('');
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We cannot update this invoice now, please try again later...',
      });
      console.log(
        'Error paying cash for invoice:',
        JSON.stringify(error.response.data)
      );
    }
  };
  return (
    <AppModal
      {...{ visible, setVisible }}
      heightValue={300}
      content={
        <Box style={styles.content}>
          <Text variant={'Body2M'}>Add cash payment</Text>

          <Text variant={'DetailsR'} color="text7" mt="m" mb="xxl">
            Enter the cash amount paid for this invoice
          </Text>

          <CurrencyTextInput
            onChangeValue={setAmount}
            value={amount}
            width={wp(80)}
            placeholder="Enter Amount Paid"
          />

          <Box style={styles.button}>
            <Button
              type="primary"
              label="Pay"
              onPress={handlePayCash}
              width={wp(80)}
              loading={loading}
              disabled={amount.length < 1 ? true : false}
            />
          </Box>
        </Box>
      }
    />
  );
};

export default PayCashModal;

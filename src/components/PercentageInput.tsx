import React, { useEffect, useState } from 'react';
import {
  Platform,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import useCurrency from '../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: theme.colors.border1,
    paddingHorizontal: 15,
    fontFamily: 'basier-regular',
    fontSize: 14,
    height: 58,
    borderRadius: 6,
    zIndex: 1,
  },
  input: {
    fontFamily: 'basier-regular',
    fontSize: 14,
    width: '80%',
    height: '100%',
  },
  inputContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
});

interface Props {
  value:
    | {
        amount: string;
        percentage: string;
      }
    | undefined;
  setValue: (
    value?: { amount: string; percentage: string } | undefined
  ) => void;
  total: string;
  placeholder: string;
}
const PercentageInput = ({
  value,
  setValue,
  total,
  placeholder,
  ...props
}: Props) => {
  const { returnCurrency } = useCurrency();
  const [isPercentage, setIsPercentage] = useState<boolean>(true);
  const [showOptions, setShowOptions] = useState<boolean>(false);

  const findPercentage = (currentValue?: string) => {
    const percentage =
      (100 * Number(currentValue ? currentValue : value?.amount)) /
      Number(total);
    setValue({
      amount: value?.amount as string,
      percentage: percentage.toFixed().toString(),
    });
    return percentage.toString();
  };

  const findValue = (currentValue?: string) => {
    const finalValue =
      Number(total) *
      (Number(currentValue ? currentValue : value?.percentage) / 100);
    setValue({
      percentage: value?.percentage as string,
      amount: finalValue.toFixed().toString(),
    });
    return finalValue.toString();
  };

  return (
    <Box style={styles.container}>
      <TextInput
        style={styles.input}
        {...props}
        keyboardType="number-pad"
        value={isPercentage ? value?.percentage : value?.amount}
        onChangeText={(e: string) => {
          isPercentage
            ? setValue({
                percentage: e,
                amount: findValue(e),
              })
            : setValue({
                amount: e,
                percentage: findPercentage(e),
              });
        }}
        placeholder={placeholder}
        placeholderTextColor={theme.colors.text5}
      />

      <TouchableOpacity
        style={{ flexDirection: 'row', alignItems: 'center' }}
        activeOpacity={0.7}
        onPress={() => setShowOptions(!showOptions)}
      >
        <Box
          style={{
            height: '100%',
            width: 1,
            backgroundColor: theme.colors.border1,
          }}
        />

        <Text
          style={{
            fontSize: 14,
            fontFamily: 'basier-regular',
            lineHeight: 16,
            letterSpacing: 2,
            color: theme.colors.text5,
            marginLeft: Platform.OS === 'ios' ? 18 : 30,
          }}
        >
          {isPercentage
            ? Platform.OS === 'ios'
              ? 'PER'
              : '%'
            : returnCurrency().code}
        </Text>

        {Platform.OS === 'ios' && (
          <Icon name="chevron-down" color={theme.colors.text5} size={14} />
        )}
      </TouchableOpacity>

      {showOptions && (
        <Box
          style={{
            paddingVertical: 10,
            paddingHorizontal: 20,
            zIndex: 4,
            position: 'absolute',
            right: 0,
            top: 50,
            borderRadius: 8,
            backgroundColor: theme.colors.white,

            shadowColor: theme.colors.text3,
            shadowOffset: {
              width: 0,
              height: 0,
            },
            shadowOpacity: 0.3,
            shadowRadius: 6,
            elevation: 1,
          }}
        >
          <TouchableOpacity
            style={{ padding: 5 }}
            onPress={() => {
              setIsPercentage(true);
              findPercentage();
              setShowOptions(false);
            }}
          >
            <Text variant={'DetailsM'} color="text5">
              Percentage (%)
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ padding: 5 }}
            onPress={() => {
              setIsPercentage(false);
              findValue();
              setShowOptions(false);
            }}
          >
            <Text variant={'DetailsM'} color="text5">
              {`${returnCurrency().name} (${returnCurrency().code})`}
            </Text>
          </TouchableOpacity>
        </Box>
      )}
    </Box>
  );
};

export default PercentageInput;

import React from 'react';
import { StyleSheet, Image  } from 'react-native';
import theme, { Box, Text } from '../components/Themed';
import { useAppSelector } from '../redux/hooks';
import TextInput from './TextInput';

const styles = StyleSheet.create({
  container: {},
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerEdit: {
    position: 'absolute',
    marginLeft: 330,
  },
  body: {
    marginTop: 45,
    alignItems: 'center',
  },
  avatar: {
    height: 122,
    width: 122,
    backgroundColor: theme.colors.primary4,
    borderRadius: 100,
    marginBottom: 71,
    alignItems: 'center',
    paddingVertical: 40,
  },
  profile: {
    marginTop: 10,
  },
  profileBorder: {
    borderWidth: 1,
    borderColor: theme.colors.text3,
    width: 350,
    height: 56,
    borderRadius: 8,
    marginTop: 15,
  },
  profileText: {
    paddingTop: 0,
    paddingHorizontal: 15,
    marginTop: 15,
  },
});


export interface PersonalInfoProps {
  email?: string;
  first_name: string;
  last_name?: string;
  image?: any;
}

const PersonalInfoCard: React.FC<PersonalInfoProps> = ({
  email,
  first_name,
  last_name,
  image,
}) => {
  const user = useAppSelector((state) => state.login.user);

  const returnInitials = () => {
    return `${first_name.charAt(0)} ${last_name?.charAt(0)}`;
  };

  return (
    <Box style={styles.container}>
      <Box style={styles.body}>
        <Box>
          {image ? (
            <Image
              source={image}
              style={{ width: 122, height: 122, marginRight: 10 }}
            />
          ) : (
            <Box style={styles.avatar}>
              <Text variant="MainTitleM">{returnInitials()}</Text>
            </Box>
          )}
        </Box>
        <Box style={styles.profile}>
          <Box>
            <Text variant="DetailsR" color="text1">
              First name
            </Text>
          </Box>
          <Box style={{ marginTop: 20 }}>
            <TextInput placeholder={user.profile.first_name} placeholderTextColor={theme.colors.text3} type="input" />
          </Box>
        </Box>
        <Box style={styles.profile}>
          <Box>
            <Text variant="DetailsR" color="text1">
              Last name
            </Text>
          </Box>
          <Box style={{ marginTop: 20, paddingBottom: 10 }}>
            <TextInput placeholder={user.profile.last_name} placeholderTextColor={theme.colors.text3} type="input" />
          </Box>
          </Box>
        <Box style={styles.profile}>
          <Box>
            <Text variant="DetailsR" color="text1">
              Email address
            </Text>
          </Box>
          <Box style={styles.profileBorder}>
            <Box style={styles.profileText}>
              <Text variant="DetailsR" color="text3">
                {user.profile.email}
              </Text>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default PersonalInfoCard;

import React from 'react';
import { StyleSheet } from 'react-native';
import PhoneInput from 'react-native-phone-number-input';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import useCurrency from '../hooks/useCurrency';
import theme from './Themed';

const styles = StyleSheet.create({
  container: {},
});

interface Props {
  onChangeFormattedText: any;
  value?: string;
}
const PhoneTextInput = ({ onChangeFormattedText, value }: Props) => {
  const { returnCurrency } = useCurrency();
  return (
    <PhoneInput
      containerStyle={{
        width: wp(90),
        borderWidth: 1,
        borderColor: theme.colors.border1,
        borderRadius: 6,
        backgroundColor: theme.colors.white,
        height: 58,
      }}
      codeTextStyle={{
        fontFamily: 'basier-regular',
        fontSize: 14,
      }}
      textInputStyle={{
        fontFamily: 'basier-regular',
        fontSize: 14,
      }}
      onChangeFormattedText={onChangeFormattedText}
      defaultCode={returnCurrency().inputCode}
      value={value}
    />
  );
};

export default PhoneTextInput;

import React, { FC, useEffect, useState } from 'react';
import { StyleSheet, TouchableOpacity, Modal } from 'react-native';
import { Picker as RNPicker, PickerProps } from '@react-native-picker/picker';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from '../components/Themed';

const styles = StyleSheet.create({
  container: {
    height: 100,
    justifyContent: 'space-between',
  },
  box: {
    height: 56,
    borderRadius: 8,
    backgroundColor: theme.colors.white,
    borderWidth: 1,
    borderColor: theme.colors.border1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 22,
    alignItems: 'center',
  },
  picker: {
    backgroundColor: theme.colors.light,
    width: '100%',
    height: '40%',
    position: 'absolute',
    bottom: 0,
  },
  pickerContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    width: '100%',
    height: '100%',
    position: 'absolute',
    bottom: 0,
  },
  error: {
    position: 'absolute',
    top: 87,
    right: 1,
  },
  cancel: {
    width: 60,
    height: 30,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    backgroundColor: theme.colors.bg4,
  },
});

interface DataProps {
  id: string;
  label: string;
  value: string;
}

interface Props extends PickerProps {
  label: string;
  placeholder: string;
  value: string;
  setValue: (state: string) => void;
  setSelectedId?: (state: string) => void;
  data: DataProps[];
  error?: string;
  widthValue?: number;
}

const Picker: FC<Props> = ({
  label,
  placeholder,
  value,
  setValue,
  data,
  error,
  setSelectedId,
  widthValue,
}) => {
  const [visible, setVisible] = useState<boolean>(false);

  return (
    <>
      <Box
        style={[styles.container, { width: widthValue ? widthValue : wp(90) }]}
      >
        <Text variant="DetailsR" color="text1">
          {label}
        </Text>

        <TouchableOpacity
          style={styles.box}
          activeOpacity={0.7}
          onPress={() => setVisible(!visible)}
        >
          <Text variant="DetailsR" color="text5">
            {value !== '' ? value : placeholder}
          </Text>
          <Icon name="chevron-down" color={theme.colors.text2} size={20} />
        </TouchableOpacity>

        {error && (
          <Box style={styles.error}>
            <Text variant="DetailsR" color="error">
              {error}
            </Text>
          </Box>
        )}
      </Box>

      <Modal
        animationType="fade"
        transparent
        visible={visible}
        onRequestClose={() => {
          setVisible(!visible);
        }}
      >
        <Box style={styles.pickerContainer}>
          <Box style={styles.picker}>
            <TouchableOpacity
              style={{ padding: 20 }}
              onPress={() => setVisible(!visible)}
            >
              <Box style={styles.cancel}>
                <Text variant="Body2B" color="success">
                  Done
                </Text>
              </Box>
            </TouchableOpacity>
            <RNPicker
              selectedValue={value}
              onValueChange={(itemValue: string) => {
                setValue(itemValue);

                const selectedItem = data.filter(
                  (d: any) => d.value === itemValue
                );

                setSelectedId && setSelectedId(selectedItem[0].id);
              }}
              // eslint-disable-next-line react/jsx-props-no-spreading
            >
              {/* eslint-disable-next-line prettier/prettier */}
              {data.map((d) => (
                <RNPicker.Item label={d.label} value={d.value} key={d.value} />
              ))}
            </RNPicker>
          </Box>
        </Box>
      </Modal>
    </>
  );
};

export default Picker;

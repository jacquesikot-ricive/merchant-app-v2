import { isPending } from '@reduxjs/toolkit';
import React from 'react';
import { StyleSheet } from 'react-native';

import ChevronRight from '../../../svgs/ChevronRight';
import theme, { Box, Text } from '../Themed';

const styles = StyleSheet.create({
  container: {
    // height: 187,
    paddingVertical: 19,
    backgroundColor: theme.colors.bg1,
    borderRadius: 10,
  },
});

interface Props {
  address: string;
  timestamp: string | null;
  isBooking?: boolean;
  isPickup?: boolean;
}

const PickupCard: React.FC<Props> = ({
  address,
  timestamp,
  isBooking,
  isPickup,
}) => {
  return (
    <Box style={styles.container}>
      {isBooking && (
        <Box style={{ marginTop: -10 }}>
          <Text variant="DetailsR" color="text5">
            Booking scheduled at:
          </Text>
          <Text variant="DetailsR" color="text6">
            {timestamp ? timestamp : ''}
          </Text>
        </Box>
      )}
      {isPickup && (
        <Box style={{ marginTop: -10 }}>
          <Text variant="DetailsR" color="text5">
            Pick up scheduled at:
          </Text>
          <Text variant="DetailsR" color="text6">
            {timestamp ? timestamp : ''}
          </Text>
        </Box>
      )}
      {address && address.length > 0 && (
        <Box style={{ marginTop: 14 }}>
          <Text variant="DetailsR" color="text5" numberOfLines={3}>
            Delivery address
          </Text>
          <Text variant="DetailsR" color="text6">
            {address}
          </Text>
        </Box>
      )}
    </Box>
  );
};

export default PickupCard;

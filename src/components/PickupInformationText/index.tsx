import React, { FC } from 'react';
import { StyleSheet } from 'react-native';

import theme, { Box, Text } from '../Themed';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    paddingVertical: 30,
  },
});

interface Props {}

const PickupInformationText: FC<Props> = () => {
  return (
    <Box>
      <Text variant="Body2M" color="text6">
        Dispatch Information
      </Text>
    </Box>
  );
};

export default PickupInformationText;

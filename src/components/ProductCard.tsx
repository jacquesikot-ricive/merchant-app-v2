import { View, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import React from 'react';
import theme, { Box } from './Themed';
import { Feather as Icon } from '@expo/vector-icons';
import { Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: theme.colors.border,
    borderRadius: 8,
  },
});

interface Props {
  onPress: () => void;
  // iconName: string;
  width: number;
  height: number;
  title: string;
  paragraph: string;
  active?: boolean;
}
const ProductCard = ({
  onPress,
  width,
  height,
  title,
  paragraph,
  active,
}: Props) => {
  return (
    <Box height={height} width={width}>
      <TouchableOpacity onPress={onPress} style={styles.container}>
        {/* <Icon name={iconName} size={40} /> */}
        <Text textAlign="center" variant="Body2M" color={'text1'}>
          {title}
        </Text>
        <Text
          style={{
            width: '90%',
          }}
          variant={'DetailsR'}
          textAlign="center"
          color={'text2'}
          lineHeight={18}
        >
          {paragraph}
        </Text>
      </TouchableOpacity>
    </Box>
  );
};

export default ProductCard;

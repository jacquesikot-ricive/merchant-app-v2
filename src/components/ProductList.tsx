import React, { FC, useState } from 'react';
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  Animated,
  View,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather, Feather as Icon, MaterialIcons } from '@expo/vector-icons';
import { RectButton } from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';

import theme, { Box, Text } from './Themed';
import { useAppSelector } from '../redux/hooks';
import DeleteIcon from '../../svgs/DeleteIcon';
import EditIcon from '../../svgs/EditIcon';
import numberWithCommas from '../utils/numbersWithComma';
import ItemMenuIcon from '../svg/ItemMenuIcon';
import useCurrency from '../hooks/useCurrency';
import ProductSwipeable from './ProductSwipeable';

const HEIGHT = 73;

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.light3,
    // paddingBottom: 10,
    // paddingTop: 10,
    paddingVertical: 10,
    backgroundColor: theme.colors.white,
    height: HEIGHT,

    zIndex: 10,
  },
  list: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    zIndex: 1,
  },
  listSpacing: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    marginRight: 14,
  },
  bottomText: {},
  iconMargin: {
    marginRight: 32,
  },
  leftAction: {
    // width: wp(20),
    backgroundColor: '#497AFC',
    justifyContent: 'center',
  },
  actionText: {
    color: 'white',
    fontSize: 16,
    backgroundColor: 'transparent',
    padding: 10,
  },
});

interface Props {
  name: string;
  productPrice: any[];
  quantity: string;
  image?: string;
  price: string;
  isPublished?: boolean;
  handleEdit: () => void;
  handleDelete: () => void;
  handleCopy: () => void;
  onPress: () => void;
}

const ProductsList: FC<Props> = ({
  name,
  image,
  price,
  handleDelete,
  handleEdit,
  productPrice,
  isPublished,
  handleCopy,
  quantity,
  onPress,
}) => {
  const { returnCurrency } = useCurrency();
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [showMenu, setShowMenu] = useState<boolean>(false);

  const returnDotBgColor = () => {
    if (Number(quantity) < 1) {
      return theme.colors.error;
    }

    if (Number(quantity) <= 5) {
      return theme.colors.secondary3;
    }

    return theme.colors.primary1;
  };

  return (
    <ProductSwipeable
      onPressCopy={handleCopy}
      onPressDelete={userPermissions.delete_product ? handleDelete : undefined}
      onPressEdit={userPermissions.edit_product ? handleEdit : undefined}
    >
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.8}
        style={styles.container}
      >
        <Box style={styles.list}>
          <Box style={styles.listSpacing}>
            <Box style={styles.image}>
              {image && image.length > 0 ? (
                <Image
                  source={{
                    uri: image,
                  }}
                  style={{ width: 32, height: 32, borderRadius: 4 }}
                />
              ) : (
                <Box
                  style={{
                    width: 32,
                    height: 32,
                    borderRadius: 4,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: theme.colors.light3,
                  }}
                >
                  <MaterialIcons
                    name="image-not-supported"
                    size={20}
                    color={theme.colors.border1}
                  />
                </Box>
              )}
            </Box>

            <Box>
              <Box>
                <Text
                  variant="SmallerTextM"
                  color="text1"
                  numberOfLines={1}
                  style={{ width: wp(45) }}
                >
                  {name}
                </Text>
              </Box>

              <Box flexDirection={'row'} alignItems={'center'}>
                <Box
                  style={{
                    width: 6,
                    height: 6,
                    borderRadius: 3,
                    backgroundColor: returnDotBgColor(),
                  }}
                />
                <Text variant="SmallerTextR" color={'text5'} ml="s">
                  {Number(quantity) < 1
                    ? 'Out of stock'
                    : `${quantity && quantity.toString()}pc${
                        quantity && Number(quantity) > 1 ? 's' : ''
                      } in stock`}
                </Text>
              </Box>
            </Box>
          </Box>

          <Box
            style={{
              height: '100%',
              justifyContent: 'space-around',
              width: wp(25),
              marginRight: 90,
            }}
          >
            <Text
              textAlign={'right'}
              variant={'SmallerTextM'}
              color="text1"
              numberOfLines={1}
            >
              {returnCurrency().code +
                ' ' +
                numberWithCommas(productPrice && productPrice.toString())}
            </Text>

            <Text
              textAlign={'right'}
              variant={'SmallerTextR'}
              color={isPublished ? 'secondary5' : 'accent3'}
            >
              {isPublished ? 'Published' : 'Unpublished'}
            </Text>
          </Box>
        </Box>
        <TouchableOpacity
          style={{
            padding: 10,
            position: 'absolute',
            right: 5,
            height: HEIGHT,
            justifyContent: 'center',
          }}
          onPress={() => {
            setShowMenu(!showMenu);
          }}
        >
          <Feather name="chevron-right" color={theme.colors.text7} size={18} />
        </TouchableOpacity>
      </TouchableOpacity>
    </ProductSwipeable>
  );
};

export default ProductsList;

import React, { Component, ReactNode } from 'react';
import {
  Animated,
  StyleSheet,
  View,
  I18nManager,
  TouchableOpacity,
} from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import Swipeable from 'react-native-gesture-handler/Swipeable';

import CopyIcon from '../../svgs/CopyIcon';
import DeleteProductIcon from '../../svgs/DeleteProductIcon';
import EditIconProduct from '../../svgs/EditIconProduct';
import theme, { Text } from './Themed';

const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);

interface Props {
  children: ReactNode;
  onPressEdit?: () => void;
  onPressCopy: () => void;
  onPressDelete?: () => void;
}

export default class ProductSwipeable extends Component<Props> {
  private renderRightAction = (
    text: string,
    color: string,
    x: number,
    progress: Animated.AnimatedInterpolation,
    icon?: ReactNode,
    onPress?: () => void
  ) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0],
    });

    return (
      <AnimatedTouchable
        onPress={onPress}
        style={{ flex: 1, transform: [{ translateX: trans }] }}
      >
        <RectButton style={[styles.rightAction, { backgroundColor: color }]}>
          {icon}
          <Text variant={'SmallerTextR'} color="text4">
            {text}
          </Text>
        </RectButton>
      </AnimatedTouchable>
    );
  };

  private renderRightActions = (
    progress: Animated.AnimatedInterpolation,
    _dragAnimatedValue: Animated.AnimatedInterpolation,
    onPressObject: {
      copy: () => void;
      edit?: () => void;
      delete?: () => void;
    }
  ) => (
    <View
      style={{
        width: 200,
        flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
      }}
    >
      {onPressObject.edit &&
        this.renderRightAction(
          'Edit',
          theme.colors.primary1,
          192,
          progress,
          <EditIconProduct />,
          onPressObject.edit
        )}
      {this.renderRightAction(
        'Copy',
        '#ffab00',
        128,
        progress,
        <CopyIcon />,
        onPressObject.copy
      )}
      {/* {this.renderRightAction('More', '#dd2c00', 64, progress)} */}
      {onPressObject.delete &&
        this.renderRightAction(
          'Delete',
          theme.colors.error,
          64,
          progress,
          <DeleteProductIcon />,
          onPressObject.delete
        )}
    </View>
  );

  // private swipeableRow?: Swipeable;

  // private updateRef = (ref: Swipeable) => {
  //   this.swipeableRow = ref;
  // };
  // private close = () => {
  //   this.swipeableRow?.close();
  // };
  render() {
    const { children, onPressCopy, onPressDelete, onPressEdit } = this.props;
    return (
      <Swipeable
        // ref={this.updateRef}
        friction={2}
        enableTrackpadTwoFingerGesture
        leftThreshold={30}
        rightThreshold={40}
        renderRightActions={(a, b) =>
          this.renderRightActions(a, b, {
            copy: onPressCopy,
            edit: onPressEdit && onPressEdit,
            delete: onPressDelete,
          })
        }
        // onSwipeableOpen={() => {
        //   console.log(`Opening swipeable from the`);
        // }}

        // onSwipeableClose={() => {
        //   console.log(`Closing swipeable to the`);
        // }}
      >
        {children}
      </Swipeable>
    );
  }
}

const styles = StyleSheet.create({
  leftAction: {
    flex: 1,
    justifyContent: 'center',
  },
  rightAction: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
});

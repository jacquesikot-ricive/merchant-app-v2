import React, { FC, useEffect, useState } from 'react';
import { StyleSheet, Keyboard, TouchableOpacity } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import { useAnalytics } from '@segment/analytics-react-native';

import theme, { Box, Text } from '../Themed';
import Button from '../Button';
import TextInput from '../TextInput';
import { useAppSelector } from '../../redux/hooks';
import AppModal from '../AppModal';
import ProductAppModal from '../ProductAppModal';

const styles = StyleSheet.create({
  content: {
    marginTop: 30,
    // alignItems: 'center',
    // marginHorizontal: 20,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  picker: {
    // width: 10,
    marginRight: 5,
  },
  button: {
    marginTop: 112,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  image: {
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 40,
    borderColor: theme.colors.border1,
  },
  modalTitle: {
    marginLeft: 150,
    marginTop: -10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalContent: {
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 1,
    paddingTop: 17,
    paddingBottom: 18,
    width: wp(100),
    alignItems: 'center',
  },
  modalTextInput: {
    paddingHorizontal: 20,
    marginTop: 18,
  },
});

interface Props {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  isEdit: boolean;
  activeCategory?: {
    id: string;
    name: string;
    business: string;
  };
  handleNewCategory: (name: string) => any;
  loading: boolean;
}

const ProductsCategoryModal: FC<Props> = ({
  visible,
  setVisible,
  isEdit,
  activeCategory,
  handleNewCategory,
  loading,
}) => {
  const [categoryName, setCategoryName] = useState<string>(
    activeCategory &&
      activeCategory.name &&
      activeCategory.name?.length > 0 &&
      isEdit
      ? activeCategory.name
      : ''
  );
  const [modalHeight, setModalHeight] = useState<number>(450);
  const merchantId = useAppSelector((state) => state.login.user.business.id);
  const { track, screen } = useAnalytics();

  useEffect(() => {
    screen('Create category');

    Keyboard.addListener('keyboardDidShow', () => {
      setModalHeight(650);
    });

    Keyboard.addListener('keyboardDidHide', () => {
      setModalHeight(450);
    });

    return () => {
      Keyboard.removeAllListeners('keyboardDidShow');
      Keyboard.removeAllListeners('keyboardDidHide');
    };
  }, []);

  const handleCreate = () => {
    handleNewCategory(categoryName);
    setCategoryName('');
    setVisible(false);
  };

  return (
    <ProductAppModal
      setVisible={setVisible}
      visible={visible}
      heightValue={modalHeight}
      width={wp(100)}
      content={
        <>
          <Box style={styles.content}>
            <Box
              style={{
                marginLeft: wp(35),
                marginTop: -10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <Text variant="Body2M" color="text1">
                Add category
              </Text>
              <TouchableOpacity onPress={handleCreate}>
                <Text variant="Body2M" color="primary1" mr="xxl">
                  Save
                </Text>
              </TouchableOpacity>
            </Box>
            <Box
              style={{
                borderBottomColor: theme.colors.border1,
                borderBottomWidth: 1,
                paddingTop: 17,
                paddingBottom: 18,
                width: wp(100),
                alignItems: 'center',
              }}
            />
            <Box style={{ paddingHorizontal: 20, marginTop: 18 }}>
              <Text mb="xl" variant="SmallerTextR" color="text5">
                Category name
              </Text>
              <TextInput
                placeholder="e.g Beverages"
                type="input"
                autoCompleteType="name"
                autoCorrect={false}
                autoCapitalize="words"
                keyboardType="default"
                returnKeyType="done"
                onChangeText={(e) => setCategoryName(e)}
                value={categoryName}
              />
            </Box>
          </Box>

          <Box style={styles.button}>
            <Button
              disabled={categoryName.length < 1 ? true : false}
              loading={loading}
              type="primary"
              label={isEdit ? 'Update' : 'Save'}
              onPress={handleCreate}
              width={wp(90)}
            />
          </Box>
        </>
      }
    />
  );
};

export default ProductsCategoryModal;

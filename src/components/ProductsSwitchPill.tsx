import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';

import theme, { Box, Text } from '../components/Themed';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: 242,
    height: 42,
    borderRadius: 7,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: theme.colors.secondary6,
  },
  productPillActive: {
    width: '45%',
    backgroundColor: theme.colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    height: '80%',
    borderRadius: 7,
    shadowColor: theme.colors.text7,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.6,
    shadowRadius: 3,
    elevation: 1,
  },
  productPillInActive: {
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    height: '80%',
  },
});

interface Props {
  active: string;
  handleSwitch: any;
}

const ProductSwitchPill: FC<Props> = ({ active, handleSwitch }) => {
  const handlePress = (state: string) => {
    // Add Haptics here
    handleSwitch();
  };
  return (
    <Box style={styles.container}>
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => handlePress('product')}
        style={
          active === 'product'
            ? styles.productPillActive
            : styles.productPillInActive
        }
      >
        <Text
          variant={active === 'product' ? 'Body2M' : 'Body2R'}
          color={active === 'product' ? 'primary1' : 'text7'}
        >
          Product
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => handlePress('category')}
        style={
          active === 'category'
            ? styles.productPillActive
            : styles.productPillInActive
        }
      >
        <Text
          variant={active === 'category' ? 'Body2M' : 'Body2R'}
          color={active === 'category' ? 'primary1' : 'text7'}
        >
          Category
        </Text>
      </TouchableOpacity>
    </Box>
  );
};

export default ProductSwitchPill;

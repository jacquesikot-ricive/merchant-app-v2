import React, { FC, ReactNode, useState } from 'react';
import { LayoutAnimation, Platform, StyleSheet, TouchableOpacity, UIManager, ViewStyle } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { FontAwesome, FontAwesome5, Ionicons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import { Feather as Icon } from '@expo/vector-icons';
import DueInvoiceIcon from '../../svgs/DueInvoiceIcon';
import FailedPaymentIcon from '../../svgs/FailedPaymentIcon';
import ScheduledOrdersIcon from '../../svgs/ScheduledOrdersIcon';
import PersonalInfoIcon from '../svg/PersonalInfoIcon';
import PasswordIcon from '../svg/PasswordIcon';
import LogoutIcon from '../svg/LogoutIcon';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingVertical: 25,
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 0.6,
  },

  heading: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(100),
    paddingHorizontal: 20
    
  },
  hidden: {
    height: 0,
    width: wp(100),
    // borderBottomColor: theme.colors.border1,
    // borderBottomWidth: 0.6,
  },
  list: {
    overflow: 'hidden',
    maxHeight: 320,
    width: wp(100),
    // borderBottomColor: theme.colors.border1,
    // borderBottomWidth: 0.6,

  },
});

interface Props {
  content: ReactNode;
  title: string;
  containerStyle?: ViewStyle;
  titleVariant?: any;
  titleColor?: any;
  startOpen?: boolean;
  labelColor?: 'logout';
  toggleType?: 'account' | 'settings' | 'general';
  subtitle?: string;
}

const ProfileActions: FC<Props> = ({
  content,
  title,
  containerStyle,
  titleVariant,
  titleColor,
  startOpen,
  toggleType,
  
}) => {
  const [isOpen, setIsOpen] = useState(startOpen ? true : false);

  if (
    Platform.OS === 'android' &&
    UIManager.setLayoutAnimationEnabledExperimental
  ) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  const toggleOpenAccount = () => {
    setIsOpen((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const toggleOpenSettings = () => {
    setIsOpen((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const toggleOpenGeneral = () => {
    setIsOpen((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };
  return (
    <Box style={styles.container}>
      <TouchableOpacity
        onPress={
          toggleType === 'account' ? toggleOpenAccount
            : toggleType === 'settings' ? toggleOpenSettings 
            : toggleOpenGeneral
          } 
        style={styles.heading}
        activeOpacity={0.6}
      >
        <Text
          variant={titleVariant || 'DetailsM'}
          color={titleColor || 'text1'}
        >
          {title}
        </Text>

        <Icon
          name={isOpen ? 'chevron-up' : 'chevron-right'}
          size={18}
          color={theme.colors.text7}
        />
      </TouchableOpacity>
      <Box style={[styles.list, !isOpen && styles.hidden]}>
        {content}
        {/* {subtitle1 && (
          <TouchableOpacity
            style={styles.subtitleFlex}
            onPress={subtitle1Press}>
            <Text>{subtitle}</Text>
            <Icon name={'chevron-right'} size={18} color={theme.colors.text7} />
        </TouchableOpacity>
        )} */}
        {/* {subtitle2 && (
          <TouchableOpacity
          style={styles.subtitleFlex}
            onPress={subtitle2Press}>
            <Text>{subtitle}</Text>
            <Icon name={'chevron-right'} size={18} color={theme.colors.text7} />
        </TouchableOpacity>
        )} */}
        {/* {subtitle3 && (
          <TouchableOpacity
            style={styles.subtitleFlex}
            onPress={subtitle3Press}>
            <Text>{subtitle}</Text>
            <Icon name={'chevron-right'} size={18} color={theme.colors.text7} />
        </TouchableOpacity>
        )} */}
      </Box>
    </Box>
  );
};

export default ProfileActions;

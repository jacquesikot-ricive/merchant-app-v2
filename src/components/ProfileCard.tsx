import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import { useSelector } from 'react-redux';
import { useAppSelector } from '../redux/hooks';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: 45,
    height: 45,
    borderRadius: 22.5,
    borderWidth: 1,
    borderColor: theme.colors.text3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    marginLeft: 10,
  },
});

interface Props {
  name: string;
  email?: string;
  business?: string;
  //   imgUrl: string;
  onPress?: () => void;
  icon?: any;
}

const ProfileCard: FC<Props> = ({ name, email, onPress, icon, business }) => {
  // const user = useAppSelector((state) => state.login.user.image);
  const returnInitials = () => {
    const fullName = name.split(' ');
    const firstName = fullName[0];
    // const lastName = fullName[fullName.length - 1];

    return `${firstName.charAt(0).toUpperCase()}`;
    // ${
    //   lastName.length > 0 ? lastName.charAt(0).toUpperCase() : ''
    // }`;
  };

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={styles.container}
    >
      <Box style={styles.image} mr="s">
        <Text variant={'Body2M'}>{returnInitials()}</Text>
      </Box>

      <Box style={styles.textContainer}>
        <Box flexDirection={'row'}>
          <Text variant="DetailsM" color="text1">
            Hello {name}
          </Text>
          <Text variant="SmallerTextM" color="text2" numberOfLines={1}>
            {email}
          </Text>
        </Box>

        {business && (
          <Text variant="DetailsM" color="text2" numberOfLines={1}>
            {business}
          </Text>
        )}
      </Box>

      <Box style={{ flex: 1 }} />

      {icon && (
        <Icon name="chevron-right" color={theme.colors.text1} size={20} />
      )}
    </TouchableOpacity>
  );
};

export default ProfileCard;

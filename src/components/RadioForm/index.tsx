import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

import theme, { Box } from '../Themed';

const styles = StyleSheet.create({
  container: {
    borderRadius: 50,
    borderWidth: 1,
    borderColor: theme.colors.border1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.white
  },
});

interface Props {
  width?: string | number;
  checked?: boolean;
}

const RadioForm: FC<Props> = ({ width, checked }) => {
  const widthValue = width || 23;
  const heightValue = width || 23;

  return (
    <Box style={[styles.container, { width: widthValue, height: heightValue }]}>
          {checked && <FontAwesome name="circle" color={theme.colors.primary1} borderColor={theme.colors.primary1}  size={16} />}
    </Box>
  );
};

export default RadioForm;

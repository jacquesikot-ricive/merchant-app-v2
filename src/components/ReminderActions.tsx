import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import { Feather as Icon } from '@expo/vector-icons';
import DueInvoiceIcon from '../../svgs/DueInvoiceIcon';
import FailedPaymentIcon from '../../svgs/FailedPaymentIcon';
import ScheduledOrdersIcon from '../../svgs/ScheduledOrdersIcon';
import InvoiceIcon from '../svg/homeTab/InvoiceIcon';

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    alignItems: 'center',
    paddingVertical: 5,
  },
  actions: {
    height: 44,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    // paddingVertical: 12,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: theme.colors.bg1,
    borderRadius: 8,
  },

  scheduleActions: {
    height: 48,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    // paddingVertical: 12,
    alignItems: 'center',
    width: wp(90),
    marginBottom: 5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    marginRight: 10,
  },
});

interface Props {
  onPress: () => void;
  label: string;
  iconType: 'new_invoice' | 'new_order' | 'unpaid_invoice';
}

const ReminderActions: FC<Props> = ({ onPress, label, iconType }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Box style={styles.actions}>
        <Box style={styles.row}>
          <Box style={styles.icon}>
            {iconType === 'new_invoice' ? (
              <InvoiceIcon color={theme.colors.primary4} />
            ) : iconType === 'unpaid_invoice' ? (
              <DueInvoiceIcon />
            ) : (
              <ScheduledOrdersIcon />
            )}
          </Box>
          <Box>
            <Text variant={'DetailsR'} color="text6">
              {label}
            </Text>
          </Box>
        </Box>
        <Box style={{ padding: 2 }}>
          <Icon name="arrow-right" size={18} color={theme.colors.text5} />
        </Box>
      </Box>
    </TouchableOpacity>
  );
};

export default ReminderActions;

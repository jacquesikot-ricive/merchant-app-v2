import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import ChevronRight from '../../svgs/ChevronRight';
import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    paddingVertical: 19,
    borderRadius: 10,
    marginBottom: 15,
  },
});

interface Props {
  serviceType: string;
  orderId: string;
  timestamp: string | null;
  customer: string;
  onPress: () => void;
  darkBg?: boolean;
  status?: string;
}

const ScheduledOrderCard: React.FC<Props> = ({
  serviceType,
  status,
  orderId,
  timestamp,
  onPress,
  customer,
  darkBg,
}) => {
  return (
    <Box
      style={[
        styles.container,
        { backgroundColor: darkBg ? theme.colors.bg5 : theme.colors.bg1 },
      ]}
    >
      <Box style={{ paddingHorizontal: 24 }}>
        <Box
          flexDirection={'row'}
          alignItems={'center'}
          justifyContent={'space-between'}
        >
          <Box
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}
          >
            <Box>
              <Text variant="Body1M" color="text6">
                Order {orderId}
              </Text>
            </Box>
          </Box>

          <Box>
            <Box style={{ flexDirection: 'row' }}>
              <Text variant="DetailsR" color="text5">
                Service type:
              </Text>
              <Text style={{ paddingLeft: 5 }} variant="DetailsR" color="text8">
                {serviceType}
              </Text>
            </Box>
            {status && (
              <Box style={{ flexDirection: 'row' }}>
                <Text variant="DetailsR" color="text5">
                  Status:
                </Text>
                <Text
                  style={{ paddingLeft: 5 }}
                  variant="DetailsR"
                  color="text8"
                >
                  {status}
                </Text>
              </Box>
            )}
          </Box>
        </Box>
        <Box>
          <Box>
            <Text variant="DetailsR" color="text5">
              Customer
            </Text>
            <Text variant="Body2R" color="text6">
              {customer}
            </Text>
          </Box>
        </Box>
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <Box>
            <Box style={{ marginTop: 5 }}>
              <Text variant="DetailsR" color="text5">
                Pick up scheduled at:
              </Text>
              <Text variant="Body2R" color="text6">
                {timestamp}
              </Text>
            </Box>
          </Box>
        </Box>
      </Box>
      <TouchableOpacity
        onPress={onPress}
        style={{
          position: 'absolute',
          right: 20,
          top: '60%',
        }}
      >
      </TouchableOpacity>
    </Box>
  );
};

export default ScheduledOrderCard;

import React, { FC } from 'react';
import { ActivityIndicator, StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import LottieView from 'lottie-react-native';

import theme, { Box, Text } from './Themed';
import StackHeader from './StackHeader';
import Button from './Button';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  gif: {
    justifyContent: 'center',
    alignItems: 'center',
    height: hp(20),
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
    width: wp(70),
    marginTop: hp(15),
  },
});

interface Props {
  onBackPress: () => void;
  onRetry: () => void;
}

const ScreenError: FC<Props> = ({ onBackPress, onRetry }) => {
  return (
    <Box style={styles.container}>
      <StackHeader title="" onBackPress={onBackPress} />

      <Box style={styles.content}>
        <LottieView
          autoPlay
          loop
          style={styles.gif}
          source={require('../assets/images/error.json')}
        />

        <Text variant="Body2M" color="text2" style={{ marginBottom: hp(0.2) }}>
          Technical Issues
        </Text>

        <Text
          variant="DetailsR"
          color="text2"
          style={{ marginBottom: hp(5), textAlign: 'center' }}
        >
          Sorry we cannot process the information you need right now, please try
          again shortly..
        </Text>

        <Button
          type="primary"
          label={'Retry'}
          onPress={onRetry}
          width={wp(45)}
        />
      </Box>
    </Box>
  );
};

export default ScreenError;

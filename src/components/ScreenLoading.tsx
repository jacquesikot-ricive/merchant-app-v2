import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {},
  gif: {
    justifyContent: 'center',
    alignItems: 'center',
    height: hp(25),
  },
});

interface Props {
  opacityValue?: number;
  noShow?: boolean;
}

const ScreenLoading: FC<Props> = ({ opacityValue, noShow }) => {
  if (noShow) {
    return null;
  }

  return (
    <Box
      style={{
        flex: 1,
        backgroundColor: theme.colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 0.3,
        position: 'absolute',
        zIndex: 5,
        width: widthPercentageToDP(100),
        height: '100%',
      }}
    >
      {/* <ActivityIndicator />
      <Text variant={'DetailsM'} color="text2" mt="l">
        loading...
      </Text> */}
      <LottieView
        autoPlay
        loop
        style={styles.gif}
        // eslint-disable-next-line import/extensions
        source={require('../assets/images/loading.json')}
      />
    </Box>
  );
};

export default ScreenLoading;

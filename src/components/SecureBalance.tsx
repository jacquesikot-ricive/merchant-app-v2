import React, { FC, useState } from 'react';
import {
  StyleSheet,
  TextInput as RNTextInput,
  TouchableOpacity,
  TextInputProps,
} from 'react-native';
import { Entypo } from '@expo/vector-icons'; 
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import numberWithCommas from '../utils/numbersWithComma';
import { widthPercentageToDP } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
    container: {

        
        // width: wp(90)
        
     },
    amount: {
        flexDirection: 'row',
        marginTop: 5,
    },
    currency: {
        marginLeft: 5,
        marginTop: 12,
    },


});

interface Props extends TextInputProps {
  icon?: 'mail' | 'search';
  secured?: boolean;
  amount: string;
}

const TextInput: FC<Props> = ({
  secured,
  icon,
  amount
   
  
}) => {
  const [balanceHidden, setBalanceHidden] = useState<boolean>(false);

  return (
      <Box style={styles.container}>
           <Text variant="SmallerTextR" color="text5">Current Balance</Text>
          <Box style={{ justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center'}}>
      
          
            <Box style={styles.amount}>
          <Text variant="MainTitleB" color="text6">
            {
              balanceHidden ? 
                '******' :
                numberWithCommas(amount)
                }</Text>
                <Box style={styles.currency}>
                    <Text variant="SmallerTextR" color="text6">(NGN)</Text>
                </Box>
            </Box>
            <TouchableOpacity onPress={() => setBalanceHidden(!balanceHidden)}>
                <Entypo name="eye" size={24} color={theme.colors.text2} />
            </TouchableOpacity>
          
       </Box>

    </Box>
  );
};

export default TextInput;

import React, { FC, useEffect, useState } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import Button from './Button';
import numberWithCommas from '../utils/numbersWithComma';
import HeaderTextInput from './HeaderTextInput';
import ListEmpty from './ListEmpty';
import ProductsCategoryModal from './ProductsCategoryModal';
import categoryApi from '../api/riciveApi/category';
import { navigationRef } from '../navigation/navRef';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50,
    marginHorizontal: 20,
    marginBottom: 50,
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  categories: any[];
  selectedCategories: any[];
  setSelectedCategories: (product: any) => void;
  getCategories?: any;
}
const SelectCategoryModal: FC<Props> = ({
  visible,
  setVisible,
  categories,
  selectedCategories,
  setSelectedCategories,
  getCategories,
}) => {
  const [searchResult, setSearchResult] = useState<any[]>(
    categories && categories
  );
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [categoryLoading, setCategoryLoading] = useState<boolean>(false);

  const isSelected = (id: string) => {
    if (
      selectedCategories &&
      selectedCategories.length > 0 &&
      selectedCategories.filter((p: any) => p.id === id).length > 0
    ) {
      return true;
    } else {
      return false;
    }
  };

  const handleSearch = (e: string) => {
    const result = categories.filter(
      (c: any) => c.name && c.name.trim().includes(e.trim())
    );

    setSearchResult([...result]);
  };

  const handleNewCategory = async (name: string) => {
    try {
      setCategoryLoading(true);

      await categoryApi.createCategory({
        name,
      });
      getCategories && (await getCategories());
      // setVisible(false);
      setCategoryLoading(false);
    } catch (error) {
      setCategoryLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot get data, please try again later...',
      });
      console.log(error);
    }
  };

  useEffect(() => {
    setSearchResult(categories);
  }, [categories]);

  return (
    <>
      <AppModal
        heightValue={hp(90)}
        visible={visible}
        setVisible={setVisible}
        top={-50}
        width={wp(100)}
        content={
          <Box style={styles.container}>
            <Box
              flexDirection={'row'}
              justifyContent={'space-between'}
              alignItems={'center'}
              paddingVertical="xl"
            >
              <Text variant={'Body2M'} color="text5">
                Select Categories
              </Text>

              <TouchableOpacity
                onPress={() => setOpenModal(true)}
                activeOpacity={0.6}
              >
                <Text variant={'Body2M'} color="primary1">
                  New
                </Text>
              </TouchableOpacity>
            </Box>

            <HeaderTextInput
              type="search"
              placeholder="Search"
              onChangeText={handleSearch}
              width={wp(88.5)}
            />

            <Box
              style={{
                alignItems: 'center',
                marginTop: 20,
                height: hp(58),
              }}
            >
              <FlatList
                showsVerticalScrollIndicator={false}
                data={searchResult}
                keyExtractor={(item: any) => item.id.toString()}
                ListEmptyComponent={() => (
                  <Box>
                    <ListEmpty
                      topText="You haven't added any category yet"
                      bottomText=""
                      buttonText="Add a product category"
                      button
                      onPressButton={() => {
                        return setOpenModal(true);
                      }}
                    />
                  </Box>
                )}
                renderItem={({ item, index }) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      if (isSelected(item.id)) {
                        setSelectedCategories([
                          ...selectedCategories.filter(
                            (p: any) => p.id !== item.id
                          ),
                        ]);
                      } else {
                        setSelectedCategories([
                          ...selectedCategories,
                          {
                            ...item,
                          },
                        ]);
                      }
                    }}
                    style={{
                      flexDirection: 'row',
                      paddingHorizontal: 10,
                      alignItems: 'center',
                      borderWidth: 1,
                      height: 45,
                      width: wp(88),
                      marginBottom: 10,
                      borderColor: isSelected(item.id)
                        ? theme.colors.primary1
                        : theme.colors.border,
                      borderRadius: 6,
                    }}
                  >
                    <Box
                      style={{
                        padding: 1,
                        borderWidth: 1,
                        borderColor: isSelected(item.id)
                          ? theme.colors.primary1
                          : theme.colors.border,
                        borderRadius: 6,
                      }}
                    >
                      {isSelected(item.id) ? (
                        <Icon
                          name={'check'}
                          color={theme.colors.primary1}
                          size={20}
                        />
                      ) : (
                        <Box style={{ width: 20, height: 20 }} />
                      )}
                    </Box>

                    <Text
                      variant={'DetailsR'}
                      numberOfLines={1}
                      ml="l"
                      color={isSelected(item.id) ? 'primary1' : 'text5'}
                    >
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </Box>

            <Box style={{ flex: 1 }} />

            {categories && categories.length > 0 && (
              <Button
                label="Done"
                type="primary"
                onPress={() => {
                  setVisible(false);
                }}
              />
            )}

            <ProductsCategoryModal
              visible={openModal}
              setVisible={setOpenModal}
              isEdit={false}
              handleNewCategory={handleNewCategory}
              loading={categoryLoading}
            />
          </Box>
        }
      />
    </>
  );
};

export default SelectCategoryModal;

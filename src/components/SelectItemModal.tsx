import React, { FC, useEffect, useState } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import Button from './Button';
import numberWithCommas from '../utils/numbersWithComma';
import HeaderTextInput from './HeaderTextInput';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50,
    marginHorizontal: 20,
    marginBottom: 50,
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  items: any[];
  selectedItems: any;
  setSelectedItems: (bank: any) => void;
  title: string;
  hasSearch?: boolean;
}
const SelectItemModal: FC<Props> = ({
  visible,
  setVisible,
  items,
  selectedItems,
  title,
  setSelectedItems,
  hasSearch,
}) => {
  const [searchResult, setSearchResult] = useState<any[]>(items);

  const isSelected = (id: string) => {
    if (selectedItems && selectedItems.id === id) {
      return true;
    } else {
      return false;
    }
  };

  const handleSearch = (e: string) => {
    const event = e.toLowerCase().trim();
    const result = items.filter(
      (c: any) => c.label && c.label.toLowerCase().trim().includes(event)
    );

    setSearchResult([...result]);
  };

  useEffect(() => {
    setSearchResult(items);
  }, [visible]);

  return (
    <AppModal
      heightValue={hp(90)}
      visible={visible}
      setVisible={setVisible}
      top={-50}
      width={wp(100)}
      content={
        <Box style={styles.container}>
          <Text variant={'Body2M'} color="text5" mb="l">
            {title}
          </Text>

          {hasSearch && (
            <HeaderTextInput
              type="search"
              placeholder="Search"
              onChangeText={handleSearch}
              width={wp(88.5)}
            />
          )}

          <Box
            style={{
              alignItems: 'center',
              marginTop: 20,
              height: hp(58),
            }}
          >
            <FlatList
              showsVerticalScrollIndicator={false}
              data={searchResult}
              keyExtractor={(item: any) => item.id.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    if (item && item.id && isSelected(item.id)) {
                      setSelectedItems(null);
                    } else {
                      setSelectedItems(item);
                    }
                  }}
                  style={{
                    flexDirection: 'row',
                    paddingHorizontal: 10,
                    alignItems: 'center',
                    borderWidth: 1,
                    height: 45,
                    width: wp(88),
                    marginBottom: 10,
                    borderColor: isSelected(item.id)
                      ? theme.colors.primary1
                      : theme.colors.border,
                    borderRadius: 6,
                  }}
                >
                  <Box
                    style={{
                      padding: 1,
                      borderWidth: 1,
                      borderColor: isSelected(item.id)
                        ? theme.colors.primary1
                        : theme.colors.border,
                      borderRadius: 6,
                    }}
                  >
                    {isSelected(item.id) ? (
                      <Icon
                        name={'check'}
                        color={theme.colors.primary1}
                        size={20}
                      />
                    ) : (
                      <Box style={{ width: 20, height: 20 }} />
                    )}
                  </Box>

                  <Text
                    variant={'DetailsR'}
                    numberOfLines={1}
                    ml="l"
                    color={isSelected(item.id) ? 'primary1' : 'text5'}
                  >
                    {item.label}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </Box>

          <Box style={{ flex: 1 }} />

          <Button
            label="Done"
            type="primary"
            onPress={() => {
              setVisible(false);
            }}
          />
        </Box>
      }
    />
  );
};

export default SelectItemModal;

import React, { FC, useEffect, useState } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import ListEmpty from './ListEmpty';
import TextInput from './HeaderTextInput';
import { returnStatusColors } from './UpcomingOrderCard';

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    alignItems: 'center',
  },
  title: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    justifyContent: 'space-between',
    width: wp(90),
    marginTop: 20,
  },
  list: {
    height: hp(67),
  },
  orderCard: {
    width: wp(100),
    borderRadius: 6,
    marginTop: 10,
    borderBottomWidth: 1,
    borderColor: theme.colors.border1,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  status: {
    flexDirection: 'row',
    height: 24,
    borderRadius: 8,
    backgroundColor: 'rgba(250, 172, 48, 0.05)',
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    position: 'absolute',
    // top: 30,
    // right: '50%',
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  orders: any[];
  setOrder: (state: any) => void;
}

const SelectOrderModal: FC<Props> = ({
  visible,
  setVisible,
  orders,
  setOrder,
}) => {
  //   const [searchResult, setSearchResult] = useState<any[]>();

  //   const handleSearchOrders = (e: string) => {
  //     // Filter by order id or status
  //     const result = orders.filter(
  //       (c) =>
  //         c.id.toString().trim().toLowerCase().includes(e.trim().toLowerCase()) ||
  //         c.order_status.trim().toLowerCase().includes(e.trim().toLowerCase()) ||
  //         c.service_type.name
  //           .trim()
  //           .toLowerCase()
  //           .includes(e.trim().toLowerCase())
  //     );
  //     setSearchResult([...result]);
  //   };

  //   useEffect(() => {
  //     setSearchResult(orders);
  //   }, []);
  return (
    <AppModal
      heightValue={hp(90)}
      visible={visible}
      setVisible={setVisible}
      width={wp(100)}
      content={
        <Box style={styles.container}>
          {/* Title */}
          <Box style={styles.title}>
            <Text variant={'Body2M'} color="text1">
              {`Select Order To Invoice`}
            </Text>
          </Box>

          <Box style={styles.list}>
            <FlatList
              data={orders}
              keyExtractor={(item: any) => item.id.toString()}
              ListEmptyComponent={() => (
                <ListEmpty
                  topText="No Orders found"
                  bottomText="No orders are available for customer yet"
                  button
                  noImage
                />
              )}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    setOrder(item);
                    setVisible(false);
                  }}
                  style={styles.orderCard}
                >
                  <Box flexDirection={'row'}>
                    <Text variant={'SmallerTextM'}>Order</Text>
                    <Text variant={'SmallerTextM'} ml="s">
                      {'#' + item.id.substring(0, 5).toUpperCase()}
                    </Text>

                    <Box>
                      <Box
                        style={[
                          styles.status,
                          {
                            backgroundColor: returnStatusColors(item.status).bg,
                          },
                        ]}
                      >
                        <Text
                          variant="SmallerTextR"
                          style={{
                            color: returnStatusColors(item.status).text,
                          }}
                          ml="s"
                        >
                          {item.status.charAt(0).toUpperCase() +
                            item.status.substring(1).toLocaleLowerCase()}
                        </Text>
                      </Box>
                    </Box>
                  </Box>

                  <Box flexDirection={'row'}>
                    <Text variant={'SmallerTextR'} color="text5">
                      Customer:
                    </Text>
                    <Text variant={'SmallerTextR'} ml="s" color="text5">
                      {orders && orders[0] ? orders[0].customer_name : ''}
                    </Text>
                  </Box>

                  <Box flexDirection={'row'}>
                    <Text variant={'SmallerTextR'} color="text7">
                      {new Date(item.created_at).toDateString()}
                    </Text>
                  </Box>
                </TouchableOpacity>
              )}
            />
          </Box>
        </Box>
      }
    />
  );
};

export default SelectOrderModal;

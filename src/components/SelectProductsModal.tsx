import React, { FC, useEffect, useState } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import Button from './Button';
import numberWithCommas from '../utils/numbersWithComma';
import HeaderTextInput from './HeaderTextInput';
import useCurrency from '../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50,
    marginHorizontal: 20,
    marginBottom: 50,
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  products: any[];
  selectedProducts: any[];
  setSelectedProducts: (product: any) => void;
}
const SelectProductModal: FC<Props> = ({
  visible,
  setVisible,
  products,
  selectedProducts,
  setSelectedProducts,
}) => {
  const { returnCurrency } = useCurrency();
  useEffect(() => {
    setSearchResult(products);
  }, [products]);

  const [searchResult, setSearchResult] = useState<any[]>(products);

  const isSelected = (id: string) => {
    if (selectedProducts.filter((p: any) => p.id === id).length > 0) {
      return true;
    } else {
      return false;
    }
  };

  const handleSearch = async (e: any) => {
    try {
      const nameSearch =
        products &&
        products.filter(
          (c: any) =>
            c.name &&
            c.name.trim().toLowerCase().includes(e.trim().toLowerCase())
        );

      const priceSearch =
        products &&
        products.filter((c: any) =>
          c.price
            ? c.price.trim().toLowerCase().includes(e.trim().toLowerCase())
            : ''
        );

      if (e.length !== '') {
        setSearchResult([...nameSearch, ...priceSearch]);
      } else {
        setSearchResult(products);
      }
    } catch (error) {
      console.log('Error in search:', error);
    }
  };

  return (
    <AppModal
      heightValue={hp(90)}
      visible={visible}
      setVisible={setVisible}
      top={-50}
      width={wp(100)}
      content={
        <Box style={styles.container}>
          <Text variant={'Body2M'} color="text1" mb="s">
            Select Products
          </Text>

          <Text variant={'SmallerTextR'} color="text5" mb="l">
            Add products to customer's invoice.
          </Text>

          <HeaderTextInput
            type="search"
            placeholder="Search"
            onChangeText={handleSearch}
            width={wp(88.5)}
          />

          <Box
            style={{
              width: wp(100),
              height: 1,
              backgroundColor: 'background: rgba(196, 196, 196, 0.2)',
              alignSelf: 'center',
              marginTop: 5,
            }}
          />

          <Box
            style={{
              alignItems: 'center',
              marginTop: 20,
              height: hp(58),
              paddingBottom: 80,
            }}
          >
            <FlatList
              showsVerticalScrollIndicator={false}
              data={searchResult}
              keyExtractor={(_: any, index: number) => index.toString()}
              renderItem={({ item }) => (
                <TouchableOpacity
                  onPress={() => {
                    if (isSelected(item.id)) {
                      setSelectedProducts([
                        ...selectedProducts.filter(
                          (p: any) => p.id !== item.id
                        ),
                      ]);
                    } else {
                      setSelectedProducts([
                        ...selectedProducts,
                        {
                          ...item,
                          quantity: 1,
                          total: Number(item.price),
                          item_id: item.id,
                        },
                      ]);
                    }
                  }}
                  style={{
                    flexDirection: 'row',
                    paddingHorizontal: 10,
                    alignItems: 'center',
                    borderWidth: 1,
                    height: 45,
                    width: wp(88),
                    marginBottom: 10,
                    borderColor: isSelected(item.id)
                      ? theme.colors.primary1
                      : theme.colors.border,
                    borderRadius: 6,
                  }}
                >
                  <Box
                    style={{
                      padding: 1,
                      borderWidth: 1,
                      borderColor: isSelected(item.id)
                        ? theme.colors.primary1
                        : theme.colors.border,
                      borderRadius: 6,
                      marginRight: 10,
                    }}
                  >
                    {isSelected(item.id) ? (
                      <Icon
                        name={'check'}
                        color={theme.colors.primary1}
                        size={20}
                      />
                    ) : (
                      <Box style={{ width: 20, height: 20 }} />
                    )}
                  </Box>

                  <Text
                    variant={'DetailsR'}
                    numberOfLines={1}
                    color={isSelected(item.id) ? 'primary1' : 'text1'}
                  >
                    {item.name}
                  </Text>

                  <Box flex={1} />

                  <Text
                    variant={'DetailsR'}
                    color={isSelected(item.id) ? 'primary1' : 'text1'}
                  >
                    {returnCurrency().code + ' ' + numberWithCommas(item.price)}
                  </Text>
                </TouchableOpacity>
              )}
            />
          </Box>

          <Box
            style={{
              position: 'absolute',
              zIndex: 1,
              bottom: 0,
              borderTopWidth: 1,
              borderColor: theme.colors.border4,
              paddingTop: 15,
              width: wp(100),
              height: 100,
              alignSelf: 'center',
              alignItems: 'center',
              backgroundColor: theme.colors.white,
            }}
          >
            <Button
              label="Done"
              type="primary"
              onPress={() => {
                setVisible(false);
              }}
            />
          </Box>
        </Box>
      }
    />
  );
};

export default SelectProductModal;

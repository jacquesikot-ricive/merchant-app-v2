import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../Themed';
import MenuIcon from '../../../svgs/MenuIcon';
import ArrowIcon from '../../../svgs/ArrowIcon';
import EditIcon from '../../svg/EditIcon';
import DeleteIcon from '../../../svgs/DeleteIcon';

const styles = StyleSheet.create({
  container: {
    marginTop: 28,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.light3,
  },
  list: {
    flexDirection: 'row',
  },
  leftIcon: {
    marginRight: 17,
    borderRadius: 25,
    height: 50,
    width: 50,
    backgroundColor: theme.colors.light3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  description: {
    marginTop: 1,
    width: wp(55),
  },
  spacing: {
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    width: wp(77),
  },
});

interface Props {
  name: string;
  description: string;
  handleEdit: () => void;
  handleDelete: () => void;
}

const ServicesList: FC<Props> = ({
  name,
  description,
  handleEdit,
  handleDelete,
}) => {
  return (
    <Box style={styles.container}>
      <Box style={styles.list}>
        <Box style={styles.leftIcon}>
          <ArrowIcon />
        </Box>
        <Box>
          <Box style={styles.spacing}>
            <Box>
              <Box>
                <Text variant="DetailsM" color="text6">
                  {name}
                </Text>
              </Box>

              <Box style={styles.description}>
                <Text
                  variant="SmallerTextR"
                  numberOfLines={2}
                  color="text5"
                  style={{ height: 50 }}
                >
                  {description}
                </Text>
              </Box>
            </Box>
            <Box
              style={{
                marginLeft: 20,
                height: 70,
                justifyContent: 'space-between',
                position: 'absolute',
                right: 50,
                bottom: 15,
              }}
            >
              <TouchableOpacity onPress={handleEdit}>
                <EditIcon />
              </TouchableOpacity>

              <TouchableOpacity onPress={handleDelete}>
                <DeleteIcon />
              </TouchableOpacity>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default ServicesList;

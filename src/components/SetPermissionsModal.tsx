import React, { FC, useState } from 'react';
import {
  FlatList,
  SectionList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import Button from './Button';
import HeaderTextInput from './HeaderTextInput';
import ListEmpty from './ListEmpty';
import returnPermissionName from '../utils/returnPermissionName';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50,
    marginHorizontal: 20,
    marginBottom: 50,
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  permissions: any[];
  selectedPermissions: any[];
  setSelectedPermissions: (product: any) => void;
}
const SetPermissionsModal: FC<Props> = ({
  visible,
  setVisible,
  permissions,
  selectedPermissions,
  setSelectedPermissions,
}) => {
  const [searchResult, setSearchResult] = useState<any[]>(permissions);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [categoryLoading, setCategoryLoading] = useState<boolean>(false);

  const isSelected = (name: string) => {
    if (
      selectedPermissions &&
      selectedPermissions.length > 0 &&
      selectedPermissions.filter((p: any) => p.name === name).length > 0
    ) {
      return true;
    } else {
      return false;
    }
  };

  // const handleSearch = (e: string) => {
  //   const result = permissions.filter(
  //     (c: any) => c.name && c.name.trim().includes(e.trim())
  //   );

  //   setSearchResult([...result]);
  // };

  const sortArray = (x: any, y: any) => {
    if (x.name < y.name) {
      return -1;
    }
    if (x.name > y.name) {
      return 1;
    }
    return 0;
  };

  return (
    <>
      <AppModal
        heightValue={hp(90)}
        visible={visible}
        setVisible={setVisible}
        top={-50}
        width={wp(100)}
        content={
          <Box style={styles.container}>
            <Box
              flexDirection={'row'}
              justifyContent={'space-between'}
              alignItems={'center'}
              paddingVertical="xl"
            >
              <Text variant={'Body2M'} color="text5">
                Set Team Member Permissions
              </Text>
            </Box>

            {/* <HeaderTextInput
              type="search"
              placeholder="Search"
              onChangeText={handleSearch}
              width={wp(88.5)}
            /> */}

            <Box
              style={{
                alignItems: 'center',
                marginTop: 20,
                height: hp(58),
              }}
            >
              <SectionList
                showsVerticalScrollIndicator={false}
                renderSectionHeader={({ section: { title } }) => (
                  <Text variant={'Body2SB'} mb="l">
                    {title}
                  </Text>
                )}
                sections={[
                  {
                    title: 'Orders',
                    data: [
                      ...permissions
                        .filter((p: any) => {
                          if (
                            p.name === 'create_order' ||
                            p.name === 'view_order' ||
                            p.name === 'update_order_status'
                          ) {
                            return p;
                          }
                        })
                        .sort(sortArray)
                        .reverse(),
                    ],
                  },
                  {
                    title: 'Invoices',
                    data: [
                      ...permissions
                        .filter((p: any) => {
                          if (
                            p.name === 'view_invoice' ||
                            p.name === 'create_invoice' ||
                            p.name === 'edit_invoice' ||
                            p.name === 'delete_invoice'
                          ) {
                            return p;
                          }
                        })
                        .sort(sortArray)
                        .reverse(),
                    ],
                  },
                  {
                    title: 'Products',
                    data: [
                      ...permissions
                        .filter((p: any) => {
                          if (
                            p.name === 'view_product' ||
                            p.name === 'create_product' ||
                            p.name === 'edit_product' ||
                            p.name === 'delete_product'
                          ) {
                            return p;
                          }
                        })
                        .sort(sortArray)
                        .reverse(),
                    ],
                  },
                  {
                    title: 'Categories',
                    data: [
                      ...permissions
                        .filter((p: any) => {
                          if (
                            p.name === 'view_category' ||
                            p.name === 'create_category' ||
                            p.name === 'edit_category' ||
                            p.name === 'delete_category'
                          ) {
                            return p;
                          }
                        })
                        .sort(sortArray)
                        .reverse(),
                    ],
                  },
                  {
                    title: 'Teams',
                    data: [
                      ...permissions
                        .filter((p: any) => {
                          if (
                            p.name === 'view_team' ||
                            p.name === 'create_team' ||
                            p.name === 'edit_team' ||
                            p.name === 'delete_team'
                          ) {
                            return p;
                          }
                        })
                        .sort(sortArray)
                        .reverse(),
                    ],
                  },
                  {
                    title: 'Wallet',
                    data: [
                      ...permissions
                        .filter((p: any) => {
                          if (
                            p.name === 'view_wallet' ||
                            p.name === 'withdraw'
                          ) {
                            return p;
                          }
                        })
                        .sort(sortArray),
                    ],
                  },
                  {
                    title: 'Storefront',
                    data: [
                      ...permissions.filter((p: any) => {
                        if (p.name === 'storefront') {
                          return p;
                        }
                      }),
                    ],
                  },
                  {
                    title: 'Analytics',
                    data: [
                      ...permissions.filter((p: any) => {
                        if (p.name === 'view_analytics') {
                          return p;
                        }
                      }),
                    ],
                  },
                  {
                    title: 'Business Info',
                    data: [
                      ...permissions.filter((p: any) => {
                        if (p.name === 'business_info') {
                          return p;
                        }
                      }),
                    ],
                  },
                ]}
                keyExtractor={(item: any) => item.id.toString()}
                ListEmptyComponent={() => (
                  <Box>
                    <ListEmpty
                      topText="You haven't added any category yet"
                      bottomText=""
                      buttonText="Add a product category"
                      button
                      onPressButton={() => {
                        return setOpenModal(true);
                      }}
                    />
                  </Box>
                )}
                renderItem={({ item, index }) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      if (isSelected(item.name)) {
                        if (item.name === 'view_order') {
                          setSelectedPermissions([
                            ...selectedPermissions.filter(
                              (p: any) =>
                                p.name !== item.name &&
                                p.name === 'updated_order_status' &&
                                p.name === 'create_order'
                            ),
                          ]);
                        } else if (item.name === 'view_invoice') {
                          setSelectedPermissions([
                            ...selectedPermissions.filter(
                              (p: any) =>
                                p.name !== item.name &&
                                p.name === 'edit_invoice' &&
                                p.name === 'delete_invoice' &&
                                p.name === 'create_invoice'
                            ),
                          ]);
                        } else if (item.name === 'view_product') {
                          setSelectedPermissions([
                            ...selectedPermissions.filter(
                              (p: any) =>
                                p.name !== item.name &&
                                p.name === 'edit_product' &&
                                p.name === 'delete_product' &&
                                p.name === 'create_product'
                            ),
                          ]);
                        } else if (item.name === 'view_category') {
                          setSelectedPermissions([
                            ...selectedPermissions.filter(
                              (p: any) =>
                                p.name !== item.name &&
                                p.name === 'edit_category' &&
                                p.name === 'delete_category' &&
                                p.name === 'create_category'
                            ),
                          ]);
                        } else if (item.name === 'view_team') {
                          setSelectedPermissions([
                            ...selectedPermissions.filter(
                              (p: any) =>
                                p.name !== item.name &&
                                p.name === 'edit_team' &&
                                p.name === 'delete_team' &&
                                p.name === 'create_team'
                            ),
                          ]);
                        } else if (item.name === 'view_wallet') {
                          setSelectedPermissions([
                            ...selectedPermissions.filter(
                              (p: any) =>
                                p.name !== item.name && p.name !== 'withdraw'
                            ),
                          ]);
                        } else {
                          setSelectedPermissions([
                            ...selectedPermissions.filter(
                              (p: any) => p.name !== item.name
                            ),
                          ]);
                        }
                      } else {
                        if (
                          item.name === 'update_order_status' ||
                          item.name === 'create_order'
                        ) {
                          setSelectedPermissions([
                            ...selectedPermissions,
                            {
                              name: item.name,
                              value: true,
                            },
                            {
                              name: 'view_order',
                              value: true,
                            },
                          ]);
                        } else if (
                          item.name === 'edit_invoice' ||
                          item.name === 'delete_invoice' ||
                          item.name === 'create_invoice'
                        ) {
                          setSelectedPermissions([
                            ...selectedPermissions,
                            {
                              name: item.name,
                              value: true,
                            },
                            {
                              name: 'view_invoice',
                              value: true,
                            },
                          ]);
                        } else if (
                          item.name === 'edit_product' ||
                          item.name === 'delete_product' ||
                          item.name === 'create_product'
                        ) {
                          setSelectedPermissions([
                            ...selectedPermissions,
                            {
                              name: item.name,
                              value: true,
                            },
                            {
                              name: 'view_product',
                              value: true,
                            },
                          ]);
                        } else if (
                          item.name === 'edit_category' ||
                          item.name === 'delete_category' ||
                          item.name === 'create_category'
                        ) {
                          setSelectedPermissions([
                            ...selectedPermissions,
                            {
                              name: item.name,
                              value: true,
                            },
                            {
                              name: 'view_category',
                              value: true,
                            },
                          ]);
                        } else if (
                          item.name === 'edit_team' ||
                          item.name === 'delete_team' ||
                          item.name === 'create_team'
                        ) {
                          setSelectedPermissions([
                            ...selectedPermissions,
                            {
                              name: item.name,
                              value: true,
                            },
                            {
                              name: 'view_team',
                              value: true,
                            },
                          ]);
                        } else if (item.name === 'withdraw') {
                          setSelectedPermissions([
                            ...selectedPermissions,
                            {
                              name: item.name,
                              value: true,
                            },
                            {
                              name: 'view_wallet',
                              value: true,
                            },
                          ]);
                        } else {
                          setSelectedPermissions([
                            ...selectedPermissions,
                            {
                              name: item.name,
                              value: true,
                            },
                          ]);
                        }
                      }
                    }}
                    style={{
                      flexDirection: 'row',
                      paddingHorizontal: 10,
                      alignItems: 'center',
                      borderWidth: 1,
                      height: 45,
                      width:
                        item.name.includes('view') ||
                        item.name === 'storefront' ||
                        item.name === 'business_info'
                          ? wp(88)
                          : wp(82),
                      marginBottom: 10,
                      borderColor: isSelected(item.name)
                        ? theme.colors.primary1
                        : theme.colors.border,
                      borderRadius: 6,
                      alignSelf: item.name.includes('view')
                        ? 'center'
                        : 'flex-end',
                    }}
                  >
                    <Box
                      style={{
                        padding: 1,
                        borderWidth: 1,
                        borderColor: isSelected(item.name)
                          ? theme.colors.primary1
                          : theme.colors.border,
                        borderRadius: 6,
                      }}
                    >
                      {isSelected(item.name) ? (
                        <Icon
                          name={'check'}
                          color={theme.colors.primary1}
                          size={20}
                        />
                      ) : (
                        <Box style={{ width: 20, height: 20 }} />
                      )}
                    </Box>

                    <Text
                      variant={'DetailsR'}
                      numberOfLines={1}
                      ml="l"
                      color={isSelected(item.name) ? 'primary1' : 'text5'}
                    >
                      {returnPermissionName(item.name)}
                    </Text>
                  </TouchableOpacity>
                )}
              />
            </Box>

            <Box style={{ flex: 1 }} />

            {permissions && permissions.length > 0 && permissions.length > 0 && (
              <Button
                label="Done"
                type="primary"
                onPress={() => {
                  setVisible(false);
                }}
              />
            )}
          </Box>
        }
      />
    </>
  );
};

export default SetPermissionsModal;

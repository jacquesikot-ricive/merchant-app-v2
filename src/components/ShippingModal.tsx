import React, { FC, useEffect, useRef, useState } from 'react';
import { StyleSheet, Keyboard } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { useAnalytics } from '@segment/analytics-react-native';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from './Themed';
import Button from './Button';
import TextInput from './TextInput';
import { useAppSelector } from '../redux/hooks';
import ProductAppModal from './ProductAppModal';
import Picker from './Picker';
import OrderAppModal from './OrderAppModal';
import DatePicker from './DatePicker';
import { shippingData } from '../screens/profile/Shipping';
import shipping from '../api/riciveApi/shipping';

const styles = StyleSheet.create({
  content: {
    marginTop: 30,
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  picker: {
    // width: 10,
    marginRight: 5,
  },
  button: {
    marginTop: 20,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  image: {
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 40,
    borderColor: theme.colors.border1,
  },
});

export interface ShippingProps {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  onPress: () => void;
}

const ShippingModal: FC<ShippingProps> = ({ visible, setVisible, onPress }) => {
  const [loading, setLoading] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const { track } = useAnalytics();
  const [name, setName] = useState<string>('');
  const [cost, setCost] = useState<string>('');
  const [message, setMessage] = useState<string>('');
  const [modalHeight, setModalHeight] = useState<number>(hp(75));

  const handleCreateShipping = async () => {
    setLoading(true);
    try {
      await shipping.createShipping({
        name,
        cost,
        message: message ? message : undefined,
      });

      track('Shipping', {
        business_id,
      });
      setLoading(false);
      setVisible(false);
      await onPress();
      return Toast.show({
        type: 'success',
        text1: 'Shipping Details',
        text2: 'Shipping details have been created',
      });
    } catch (error: any) {
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'Cannot add shipping details now, please try again later...',
      });
    }
  };

  useEffect(() => {
    Keyboard.addListener('keyboardWillShow', () => setModalHeight(hp(93)));

    Keyboard.addListener('keyboardWillHide', () => setModalHeight(hp(75)));

    return () => {
      Keyboard.removeAllListeners('keyboardWillHide');
      Keyboard.removeAllListeners('keyboardWillShow');
    };
  }, []);

  return (
    <OrderAppModal
      setVisible={setVisible}
      visible={visible}
      heightValue={modalHeight}
      width={wp(100)}
      content={
        <>
          <Box style={styles.content}>
            <Box
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 20,
                width: wp(90),
              }}
            >
              <Text variant="DetailsSB" color="text1">
                Create shipping zone
              </Text>
            </Box>
            <Text
              mt="s"
              mb="m"
              variant="SmallerTextR"
              color="text7"
              style={{ width: wp(90) }}
            >
              Enter name of a zone or region that this shipping charge will
              cover.
            </Text>
            <Box>
              <Text variant="SmallerTextR" color="text1" mb="l">
                Name
              </Text>
              <TextInput
                type="input"
                placeholder={'Enter Name'}
                width={wp(90)}
                autoCompleteType="name"
                autoCorrect={false}
                autoCapitalize="words"
                returnKeyType="next"
                value={name}
                onChangeText={(e) => setName(e)}
              />
            </Box>
            <Box>
              <Text variant="SmallerTextR" color="text1" mb="l">
                Shipping cost
              </Text>
              <TextInput
                type="input"
                placeholder={'Enter Cost'}
                width={wp(90)}
                keyboardType="number-pad"
                autoCorrect={false}
                returnKeyType="next"
                value={cost}
                onChangeText={(e) => setCost(e)}
              />
            </Box>
            <Box>
              <Text variant="SmallerTextR" color="text1" mb="l">
                Message (optional)
              </Text>
              <TextInput
                type="input"
                placeholder={'Enter Message'}
                width={wp(90)}
                autoCompleteType="off"
                autoCorrect={false}
                autoCapitalize="words"
                returnKeyType="done"
                onChangeText={(e) => setMessage(e)}
                value={message}
              />
            </Box>
          </Box>

          <Box style={styles.button}>
            <Button
              disabled={name.length < 1 || cost.length < 1 ? true : false}
              loading={loading}
              type="primary"
              label="Save"
              onPress={handleCreateShipping}
              width={wp(90)}
            />
          </Box>
        </>
      }
    />
  );
};

export default ShippingModal;

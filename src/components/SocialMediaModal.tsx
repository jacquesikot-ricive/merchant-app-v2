import React, { FC, useState } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  ScrollViewBase,
  StyleSheet,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../components/Themed';
import AppModal from './AppModal';
import Button from './Button';
import TextInput from './TextInput';

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    paddingHorizontal: 20,
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  isUpdate: boolean;
  setFacebook: (state: string) => void;
  setInstagram: (state: string) => void;
  setTwitter: (state: string) => void;
  instagram: string;
  facebook: string;
  twitter: string;
  isEdit: boolean;
}

const SocialmediaModal: FC<Props> = ({
  setVisible,
  visible,
  isUpdate,
  setFacebook,
  setTwitter,
  setInstagram,
  instagram,
  twitter,
  facebook,
  isEdit,
}) => {
  const [enableAvoidingView, setEnableAvvoidingView] = useState<boolean>(false);
  return (
    <AppModal
      setVisible={setVisible}
      top={enableAvoidingView ? -50 : 0}
      visible={visible}
      heightValue={hp(60)}
      content={
        <ScrollView bounces={false} style={styles.container}>
          <Text variant={'Body2M'} mb="xl">
            Add Social Media
          </Text>

          <Text mb="m" variant={'DetailsR'}>
            Facebook handle
          </Text>
          <TextInput
            placeholder={'Facebook handle'}
            type="input"
            width={wp(80)}
            onChangeText={(e) => setFacebook(e)}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="default"
          />

          <Box style={{ height: 20 }} />

          <Text mb="m" variant={'DetailsR'}>
            Twitter handle
          </Text>
          <TextInput
            placeholder={'Twitter handle'}
            type="input"
            width={wp(80)}
            onChangeText={(e) => setTwitter(e)}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="default"
          />

          <Box style={{ height: 20 }} />

          <Text mb="m" variant={'DetailsR'}>
            Instagram handle
          </Text>
          <TextInput
            placeholder={'Instagram handle'}
            type="input"
            width={wp(80)}
            onChangeText={(e) => setInstagram(e)}
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="default"
            onFocus={() => setEnableAvvoidingView(true)}
            onBlur={() => setEnableAvvoidingView(false)}
          />

          <Box style={{ height: 20 }} />

          <Button
            label={isEdit ? 'Update' : 'Add'}
            onPress={() => setVisible(false)}
            type="primary"
            width={wp(80)}
          />
        </ScrollView>
      }
    />
  );
};

export default SocialmediaModal;

import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../Themed';
import BurgerMenuIcon from '../../svg/drawerNav/BurgerMenuIcon';
import NotificationIcon from '../../../svgs/NotificationIcon';
import { useAppSelector } from '../../redux/hooks';
import ProfileCard from '../ProfileCard';

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    flexDirection: 'row',
    alignItems: 'center',
    height: hp(7),
    justifyContent: 'center',
    width: wp(100),
    // borderBottomWidth: 0.5,
    // borderColor: 'rgba(60, 60, 67, 0.36)',
  },
  backContainer: {
    width: 35,
    height: 35,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: theme.colors.primary1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 20,
  },
  burgerContainer: {
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 10,
  },
  burgerOnBackPress: {
    width: 45,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 10,
    flexDirection: 'row',
  },
  profileContainer: {
    width: 200,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 30,
    flexDirection: 'row',
  },
  back: {
    // paddingLeft: 5
  },
  title: {
    alignItems: 'center',
  },
  iconsContainer: {
    width: '15%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  icon1Container: {
    position: 'absolute',
    right: 20,
  },
});

interface Props {
  title: string;
  rightHandTitle?: string;
  backTitle?: string;
  onBackPress?: () => void;
  icon1?: React.ReactNode;
  icon2?: React.ReactNode;
  onPressIcon1?: () => void;
  onPressIcon2?: () => void;
  burger?: () => void;
  color?: 'white' | 'green';
  profile?: any;
  onPressProfile?: () => void;
  hasBorder?: boolean;
  onPressBusiness?: () => void;
}

const StackHeader: FC<Props> = ({
  title,
  rightHandTitle,
  backTitle,
  onBackPress,
  icon1,
  icon2,
  onPressIcon1,
  onPressIcon2,
  onPressProfile,
  burger,
  color,
  profile,
  hasBorder,
  onPressBusiness,
}) => {
  const user = useAppSelector((state) => state.login.user);
  const businessName = useAppSelector(
    (state) => state.login.user.business.business_name
  );
  return (
    <Box
      style={[
        styles.container,
        hasBorder && {
          borderBottomColor: theme.colors.border1,
          borderBottomWidth: 1,
        },
      ]}
    >
      <Box style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Text style={styles.title} variant={'Body2M'}>
          {title}
        </Text>
      </Box>

      {onBackPress && (
        <TouchableOpacity
          onPress={onBackPress}
          style={styles.burgerOnBackPress}
        >
          <Icon name="chevron-left" color={theme.colors.text1} size={24} />
          <Text variant="Body2R" color="text1" style={styles.back}>
            {backTitle}
          </Text>
        </TouchableOpacity>
      )}

      {burger && (
        <TouchableOpacity onPress={burger} style={styles.burgerContainer}>
          <BurgerMenuIcon />
        </TouchableOpacity>
      )}

      {profile && (
        <TouchableOpacity
          onPress={onPressProfile}
          style={styles.profileContainer}
        >
          <ProfileCard
            name={`${user.profile.first_name}!`}
            business={businessName}
          />

          {onPressBusiness && (
            <TouchableOpacity
              onPress={onPressBusiness}
              style={{
                padding: 5,
                paddingHorizontal: 7,
              }}
            >
              <Icon name="chevron-down" size={20} color={theme.colors.text5} />
            </TouchableOpacity>
          )}
        </TouchableOpacity>
      )}

      {icon1 && (
        <TouchableOpacity onPress={onPressIcon1} style={styles.icon1Container}>
          <Text variant="Body2M" color="primary1">
            {icon1}
          </Text>
        </TouchableOpacity>
      )}
      {icon2 && (
        <TouchableOpacity onPress={onPressIcon2} style={styles.icon1Container}>
          <Text variant="Body2M" color="primary1">
            {icon1}
          </Text>
        </TouchableOpacity>
      )}
    </Box>
  );
};

export default StackHeader;

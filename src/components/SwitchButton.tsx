import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box } from './Themed';

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    borderWidth: 1,
    // borderColor: theme.colors.primary1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
    checkedCircle: {
        backgroundColor: theme.colors.white,
        height: 20,
        width: 20,
        borderRadius: 50,
        marginLeft: 22,
      
    },
    unCheckedCircle: {
        backgroundColor: theme.colors.border1,
        height: 20,
        width: 20,
        borderRadius: 50,
        marginLeft: -20,
    }
});

interface Props {
  width?: string | number;
  checked?: boolean;
}

const SwitchButton: FC<Props> = ({ width, checked }) => {
  const widthValue = width || 43;
  const heightValue = width || 23;

  return (
    <Box
      style={[
        styles.container,
        {
          width: widthValue,
          height: heightValue,
          borderColor: checked ? theme.colors.primary1 : theme.colors.border,
          backgroundColor: checked ? theme.colors.primary1 : theme.colors.white
        },
      ]}
    >
    { checked ? (
        <Box style={styles.checkedCircle} />
          ) :
       <Box style={styles.unCheckedCircle} />
    }
    </Box>
  );
};

export default SwitchButton;

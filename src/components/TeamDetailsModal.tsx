import React, { FC, useRef, useState } from 'react';
import { StyleSheet, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import TeamAppModal from './TeamAppModal';
import TeamDeleteIcon from '../svg/TeamDeleteIcon';
import TeamEditIcon from '../svg/TeamEditIcon';
import Button from './Button';
import { useAppSelector } from '../redux/hooks';

const styles = StyleSheet.create({
  content: {
    marginTop: 30,
    alignItems: 'center',
    textAlign: 'center',
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  picker: {
    // width: 10,
    marginRight: 5,
  },
  button: {
    marginTop: 112,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignSelf: 'center',
  },
  image: {
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 40,
    borderColor: theme.colors.border1,
  },
  modalTitle: {
    marginLeft: 150,
    marginTop: -10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  modalContent: {
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 1,
    paddingTop: 17,
    paddingBottom: 18,
    width: wp(100),
    alignItems: 'center',
  },
  modalTextInput: {
    paddingHorizontal: 20,
    marginTop: 18,
  },
  imageContainer: {
    width: 50,
    height: 50,
    borderRadius: 30,
    backgroundColor: theme.colors.secondary3,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
});

interface Props {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  isEdit: boolean;
  loading: boolean;
  data?: {
    id: string;
    image: any;
    email: string;
    first_name: string;
    last_name: string;
    role: string;
  };
  name?: any;
  colorNum?: any;
  DeletePress: () => void;
  EditPress: () => void;
}

const TeamDetailsModal: FC<Props> = ({
  visible,
  setVisible,
  isEdit,
  loading,
  data,
  colorNum,
  name,
  EditPress,
  DeletePress,
}) => {
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const returnInitials = () => {
    return (
      <Box
        style={{
          flexDirection: 'row',
          backgroundColor: 'rgba(252, 171, 154, 0.3)',
          width: 60,
          height: 60,
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text variant={'Body1SB'} color="text7">
          {data?.first_name[0]}
        </Text>
        <Text variant={'Body1SB'} color="text7">
          {data?.last_name[0]}
        </Text>
      </Box>
    );
  };

  const returnProfileBgColor = (max: number) => {
    const colors = [
      'rgba(252, 171, 154, 0.3)',
      'rgba(127, 241, 252, 0.2)',
      'rgba(193, 250, 179, 0.2)',
      'rgba(217, 217, 271, 0.2)',
      'rgba(251, 173, 49, 0.2)',
      'rgba(253, 143, 50, 0.2)',
      'rgba(48, 79, 254, 0.24)',
    ];

    const colorIndex = Math.floor(Math.random() * (max - 0 + 1) + 0);

    return colors[colorIndex];
  };

  return (
    <TeamAppModal
      setVisible={setVisible}
      visible={visible}
      heightValue={hp(45)}
      width={wp(100)}
      content={
        <>
          <Box style={styles.content}>
            <Box>
              {data?.image ? (
                <Image
                  source={data?.image}
                  style={{
                    width: 60,
                    height: 62,
                    marginRight: 10,
                  }}
                />
              ) : (
                returnInitials()
              )}
            </Box>
            <Box style={{ alignItems: 'center' }}>
              <Box style={{ flexDirection: 'row', marginTop: 10 }}>
                <Text variant="Body2M" mr="s">
                  {data?.first_name}
                </Text>
                <Text variant="Body2M">{data?.last_name}</Text>
              </Box>
              <Box>
                <Text variant="SmallerTextR" color="text7">
                  {data?.email}
                </Text>
              </Box>
            </Box>
            <Box
              style={{
                backgroundColor: theme.colors.secondary6,
                borderRadius: 8,
                paddingHorizontal: 60,
                marginTop: 18,
              }}
            >
              <Text
                variant="SmallerTextR"
                color="secondary5"
                style={{ textTransform: 'lowercase' }}
              >
                {data &&
                  data.role &&
                  data.role.charAt(0).toUpperCase() + data?.role.substring(1)}
              </Text>
            </Box>
            <Box
              style={{
                borderBottomWidth: 1,
                borderBottomColor: theme.colors.border1,
                width: '100%',
                marginTop: 20,
                marginBottom: 10,
              }}
            />
            <Box
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: wp(90),
                marginTop: 10,
              }}
            >
              {userPermissions.edit_team && (
                <TouchableOpacity
                  onPress={EditPress}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: theme.colors.secondary6,
                    height: 38,
                    width: wp(42),
                    borderRadius: 8,
                  }}
                >
                  <TeamEditIcon />
                  <Text ml="l" variant="SmallerTextR" color="primary1">
                    Edit
                  </Text>
                </TouchableOpacity>
              )}
              {userPermissions.delete_team && (
                <TouchableOpacity
                  onPress={DeletePress}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: theme.colors.secondary6,
                    height: 38,
                    width: wp(42),
                    borderRadius: 8,
                  }}
                >
                  <TeamDeleteIcon />
                  <Text ml="l" variant="SmallerTextR" color="error">
                    Delete
                  </Text>
                </TouchableOpacity>
              )}
            </Box>
          </Box>
        </>
      }
    />
  );
};

export default TeamDetailsModal;

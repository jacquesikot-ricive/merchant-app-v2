import React from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import ChevronRight from '../../../svgs/ChevronRight';
import DeleteIcon from '../../../svgs/DeleteIcon';
import EditIcon from '../../../svgs/EditIcon';

import theme, { Box, Text } from '../Themed';

const styles = StyleSheet.create({
  list: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 28,
  },
  textSpacing: {
    paddingTop: 4,
    paddingBottom: 4,
  },
  flex: {
    flexDirection: 'row',
  },
  rightSpacing: {
    paddingRight: 10,
  },
  border: {
    paddingRight: 10,
    fontSize: 20,
  },
  imageContainer: {
    width: 50,
    height: 50,
    borderRadius: 30,
    backgroundColor: theme.colors.secondary3,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 15,
  },
});

interface Props {
  role?: string;
  name: string;
  email: string;
  image?: any;
  handleEdit: () => void;
  handleDelete: () => void;
  colorNum: number;
  onTeamSelect?: () => void;
}

export const returnProfileBgColor = (max: number) => {
  const colors = [
    'rgba(252, 171, 154, 0.3)',
    'rgba(127, 241, 252, 0.2)',
    'rgba(193, 250, 179, 0.2)',
    'rgba(217, 217, 271, 0.2)',
    'rgba(251, 173, 49, 0.2)',
    'rgba(253, 143, 50, 0.2)',
    'rgba(48, 79, 254, 0.24)',
  ];

  const colorIndex = Math.floor(Math.random() * (max - 0 + 1) + 0);

  return colors[colorIndex];
};

const TeamList: React.FC<Props> = ({
  role,
  name,
  email,
  image,
  handleDelete,
  handleEdit,
  colorNum,
  onTeamSelect,
}) => {
  const returnInitials = () => {
    const fullName = name.split(' ');
    const firstName = fullName[0];
    const lastName = fullName[fullName.length - 1];

    return `${firstName.charAt(0).toUpperCase()}${lastName
      .charAt(0)
      .toUpperCase()}`;
  };

  return (
    <TouchableOpacity onPress={onTeamSelect} style={styles.list}>
      <Box>
        {image ? (
          <Image
            source={image}
            style={{ width: 60, height: 62, marginRight: 10 }}
          />
        ) : (
          <Box
            style={[
              styles.imageContainer,
              { backgroundColor: returnProfileBgColor(colorNum) },
            ]}
          >
            <Text variant={'Body1R'}>{returnInitials()}</Text>
          </Box>
        )}
      </Box>
      <Box>
        {/* <Text variant="DetailsR" color="text2">
          {role}
        </Text> */}
        <Box>
          <Text variant="Body2M" color="text6">
            {name}
          </Text>
        </Box>
        <Box style={styles.flex}>
          <Text variant="SmallerTextR" color="text7">
            {email}
          </Text>
        </Box>
      </Box>

      <Box
        style={{
          marginLeft: 20,
          height: 25,
          justifyContent: 'space-between',
          position: 'absolute',
          right: 0,
        }}
      >
        <TouchableOpacity onPress={onTeamSelect}>
          <ChevronRight />
        </TouchableOpacity>
        {/* <TouchableOpacity onPress={handleEdit}>
          <EditIcon />
        </TouchableOpacity>

        <TouchableOpacity onPress={handleDelete}>
          <DeleteIcon />
        </TouchableOpacity> */}
      </Box>
    </TouchableOpacity>
  );
};

export default TeamList;

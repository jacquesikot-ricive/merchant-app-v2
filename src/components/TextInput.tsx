import React, { FC, useState } from 'react';
import {
  StyleSheet,
  TextInput as RNTextInput,
  TouchableOpacity,
  TextInputProps,
  Platform,
} from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { MaterialIcons as MaterialIcon } from '@expo/vector-icons';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {},
  input: {
    borderWidth: 1,
    borderColor: theme.colors.border1,
    paddingHorizontal: 15,
    fontFamily: 'basier-regular',
    fontSize: 14,
  },
  inputContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    height: 48,
  },
  eye: {
    position: 'absolute',
    right: 20,
  },
  flag: {
    position: 'absolute',
    zIndex: 10,
    marginLeft: 15,
    flexDirection: 'row',
    borderRightWidth: 1.5,
    height: 41,
    alignItems: 'center',
    borderColor: theme.colors.text2,
    paddingRight: 15,
  },
  search: {
    position: 'absolute',
    zIndex: 10,
    marginLeft: 15,
    flexDirection: 'row',
    marginRight: wp(2),
  },
  error: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  location: {
    position: 'absolute',
    right: 20,
    zIndex: 1,
  },
});

interface Props extends TextInputProps {
  type: 'number' | 'input' | 'search' | 'description' | 'profileInput';
  placeholder: string;
  icon?: 'mail' | 'search';
  rightIcon?: 'chevron-down-outline' | 'chevron-up-outline' | 'eye-off' | 'eye';
  secured?: boolean;
  inputRef?: any;
  error?: string;
  width?: number;
  errorLeft?: number;
  location?: () => void;
  rightIconPress?: () => void;
  height?: number;
  noBorderRadius?: boolean;
}

const TextInput: FC<Props> = ({
  placeholder,
  secured,
  rightIcon,
  icon,
  type,
  inputRef,
  error,
  width,
  errorLeft,
  location,
  rightIconPress,
  noBorderRadius,
  height: heightValue,
  ...props
}) => {
  const [visible, setVisible] = useState<boolean>(false);
  const [isSelected, setSelection] = useState(false);

  return (
    <Box
      style={[
        styles.container,
        {
          borderRadius: noBorderRadius ? undefined : 6,
          height: heightValue ? heightValue : 45,
        },
      ]}
    >
      <Box
        style={[
          styles.inputContainer,
          {
            borderRadius: noBorderRadius ? undefined : 6,
            height: heightValue ? heightValue : 45,
          },
        ]}
      >
        {type === 'search' && (
          <Box style={styles.search}>
            <Icon name="search" color="#C4C4C4" size={20} />
          </Box>
        )}

        {location && (
          <TouchableOpacity style={styles.location} onPress={location}>
            <Icon name="map-pin" size={20} color={theme.colors.text2} />
          </TouchableOpacity>
        )}
        {rightIcon ? (
          <TouchableOpacity style={styles.location} onPress={rightIconPress}>
            <Ionicons name={rightIcon} size={15} color={theme.colors.text2} />
          </TouchableOpacity>
        ) : null}

        <RNTextInput
          placeholder={placeholder}
          style={[
            styles.input,
            {
              paddingTop:
                props.multiline && Platform.OS === 'ios' ? 15 : undefined,
              width: width || theme.layout.screenWidth,
              height: heightValue
                ? heightValue
                : type === 'input'
                ? 56
                : type === 'description'
                ? 100
                : 53
                ? type === 'profileInput'
                  ? 43
                  : 53
                : 100,
              borderRadius: noBorderRadius ? undefined : 6,
            },
          ]}
          placeholderTextColor={theme.colors.text5}
          ref={inputRef}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...props}
          secureTextEntry={secured ? !visible : false}
        />

        {secured && (
          <TouchableOpacity
            onPress={() => setVisible(!visible)}
            style={styles.eye}
          >
            {visible ? (
              <Icon name="eye-off" color={theme.colors.text2} size={20} />
            ) : (
              <Icon name="eye" color={theme.colors.text2} size={20} />
            )}
          </TouchableOpacity>
        )}

        {icon && (
          <Box style={styles.eye}>
            <Icon name={icon} color={theme.colors.text2} size={20} />
          </Box>
        )}
      </Box>
      {error && (
        <Box style={[styles.error, { left: errorLeft }]}>
          <MaterialIcon name="error" color={theme.colors.error} size={20} />
          <Text marginLeft="xs" variant="DetailsR" color="error">
            {error}
          </Text>
        </Box>
      )}
    </Box>
  );
};

export default TextInput;

import { createTheme, createText, createBox } from '@shopify/restyle';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

const theme = createTheme({
  colors: {
    white: '#FFFFFF',
    black: '#000000',

    primary1: '#4E903C',
    primary2: '#91CE33',
    primary3: '#4BA87D',
    primary4: '#FBC231',
    primary5: '#128065',

    secondary1: '#5DB78E',
    secondary2: '#9FD9AE',
    secondary3: '#FCD165',
    secondary4: '#FEF0CC',
    secondary5: '#38CB89',
    secondary6: '#F3FAF2',

    tint1: '#D6EDB3',
    tint2: '#E3F3CC',
    tint3: '#F1F9E6',

    text1: '#171717',
    text2: '#636366',
    text3: '#C4C4C4',
    text4: '#FFFFFF',
    text5: '#636366',
    text6: '#111111',
    text7: '#908F8F',
    text8: '#12362A',
    text9: '#908F8F',
    text10: '#777777',
    text11: '#631111',

    bg1: '#FFFFFF',
    bg2: '#EDF1D0',
    bg3: '#91CE33',
    bg4: '#F6F6F6',
    bg5: '#F7F7F7',
    bg6: '#FEF6E9',
    bg7: '#1B2559',
    bg8: '#8F9BBA',
    bg9: '#DADADA',
    bg10: '#FCF0DE',
    bg11: 'rgba(144, 143, 143, 0.2)',
    bg12: '#FAFAFA',
    bg13: '#F3FAF2',
    bg14: '#F5F5F5',

    accent1: '#4BA87D',
    accent2: '#FBC231',
    accent3: '#FF5F00',
    accent4: '#73E97E',
    success: '#38CB89',
    error: '#F34040',
    errorLight: 'rgba(243, 64, 64, 0.2)',

    border: '#B9B9BB',
    border1: '#D9DBE1',
    border2: '#DADADA',
    border3: '#D9DBE1',
    border4: 'rgba(196, 196, 196, 0.2)',

    light: '#F6F6F6',
    light2: '#E0E0E0',
    light3: '#F0F0F0',
  },

  layout: {
    screenWidth: wp(90),
  },

  spacing: {
    xs: 2.5,
    s: 5,
    m: 10,
    l: 15,
    xl: 20,
    xxl: 30,
    xxxl: 40,
  },
  borderRadii: {
    none: 0,
    s: 5,
    m: 10,
    l: 15,
    xl: 20,
  },
  textVariants: {
    // Regular
    MainTitleR: {
      fontSize: 28,
      fontFamily: 'basier-regular',
      lineHeight: 40,
    },
    TitleR: {
      fontSize: 24,
      fontFamily: 'basier-regular',
      lineHeight: 32,
    },
    Body1R: {
      fontSize: 18,
      fontFamily: 'basier-regular',
      lineHeight: 30,
    },
    Body2R: {
      fontSize: 16,
      fontFamily: 'basier-regular',
      lineHeight: 30,
    },
    DetailsR: {
      fontSize: 14,
      fontFamily: 'basier-regular',
      lineHeight: 24,
    },
    SmallerTextR: {
      fontSize: 12,
      fontFamily: 'basier-regular',
      lineHeight: 24,
    },

    // Medium
    MainTitleM: {
      fontSize: 28,
      fontFamily: 'basier-medium',
      lineHeight: 40,
    },
    TitleM: {
      fontSize: 24,
      fontFamily: 'basier-medium',
      lineHeight: 32,
    },
    Body1M: {
      fontSize: 18,
      fontFamily: 'basier-medium',
      lineHeight: 30,
    },
    Body2M: {
      fontSize: 16,
      fontFamily: 'basier-medium',
      lineHeight: 30,
    },
    DetailsM: {
      fontSize: 14,
      fontFamily: 'basier-medium',
      lineHeight: 24,
    },
    SmallerTextM: {
      fontSize: 12,
      fontFamily: 'basier-medium',
      lineHeight: 24,
    },

    // SemiBold
    MainTitleSB: {
      fontSize: 28,
      fontFamily: 'basier-semiBold',
      lineHeight: 40,
    },
    TitleSB: {
      fontSize: 24,
      fontFamily: 'basier-semiBold',
      lineHeight: 32,
    },
    Body1SB: {
      fontSize: 18,
      fontFamily: 'basier-semiBold',
      lineHeight: 30,
    },
    Body2SB: {
      fontSize: 16,
      fontFamily: 'basier-semiBold',
      lineHeight: 30,
    },
    DetailsSB: {
      fontSize: 14,
      fontFamily: 'basier-semiBold',
      lineHeight: 24,
    },
    SmallerTextSB: {
      fontSize: 12,
      fontFamily: 'basier-semiBold',
      lineHeight: 24,
    },

    // Bold
    MainTitleB: {
      fontSize: 28,
      fontFamily: 'basier-bold',
      lineHeight: 40,
    },
    TitleB: {
      fontSize: 24,
      fontFamily: 'basier-bold',
      lineHeight: 32,
    },
    Body1B: {
      fontSize: 18,
      fontFamily: 'basier-bold',
      lineHeight: 30,
    },
    Body2B: {
      fontSize: 16,
      fontFamily: 'basier-bold',
      lineHeight: 30,
    },
    DetailsB: {
      fontSize: 14,
      fontFamily: 'basier-bold',
      lineHeight: 24,
    },
    SmallerTextB: {
      fontSize: 12,
      fontFamily: 'basier-bold',
      lineHeight: 24,
    },
  },
  breakpoints: {
    phone: 0,
    tablet: 768,
  },
});

export type Theme = typeof theme;
export const Text = createText<Theme>();
export const Box = createBox<Theme>();
export default theme;

import React, { FC, useState } from "react";
import { StyleSheet, TouchableOpacity, Modal, Platform } from "react-native";
import DateTimePicker, { Event } from "@react-native-community/datetimepicker";
import { Feather as Icon } from "@expo/vector-icons";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

import theme, { Box, Text } from "./Themed";

const styles = StyleSheet.create({
  container: {
    borderWidth: 0.3,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: theme.colors.white,
  },
});

interface Props {
  time: Date;
  setTime: (time: any) => void;
}

const TimePicker: FC<Props> = ({ time, setTime }) => {
  const [visible, setVisible] = useState<boolean>(false);
  const onChangeIOS = (event: Event, selectedTime: any) => {
    const currentTime = selectedTime || time;
    setTime(currentTime);
  };

  const onChangeAndroid = (event: Event, selectedTime: any) => {
    const currentTime = selectedTime || time;
    setTime(currentTime);
    setVisible(false);
  };

  return (
    <TouchableOpacity
      onPress={() => setVisible(!visible)}
      style={styles.container}
    >
      <DateTimePicker
        testID="timePicker"
        value={time}
        mode="time"
        display="default"
        onChange={Platform.OS === "ios" ? onChangeIOS : onChangeAndroid}
        style={{
          width: 80,
          paddingVertical: 5,
          alignSelf: "flex-start",
        }}
        textColor={theme.colors.text5}
      />
      <Icon
        name={visible ? "chevron-up" : "chevron-down"}
        size={20}
        style={{ marginLeft: 6, marginRight: 3 }}
      />
    </TouchableOpacity>
  );
};

export default TimePicker;

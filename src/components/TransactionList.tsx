import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import ArrowIcon from '../../svgs/ArrowIcon';
import DownArrowIcon from '../../svgs/DownArrowIcon';
import { Feather as Icon } from '@expo/vector-icons';
import numberWithCommas from '../utils/numbersWithComma';

import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: wp(100),
    borderBottomColor: theme.colors.light2,
    borderBottomWidth: 1,
    paddingVertical: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 26,
  },
  list: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
  },
  withdrawIcon: {
    borderRadius: 20,
    height: 40,
    width: 40,
    backgroundColor: theme.colors.bg5,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  depositIcon: {
    borderRadius: 20,
    height: 40,
    width: 40,
    backgroundColor: theme.colors.light3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  leftList: {
    flexDirection: 'row',
  },
  leftText: {
    marginBottom: 2,
  },
  rightText: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
});

export const data = [
  {
    id: 1,
    transactionStatus: 'WDL/Karen Ijezie/GTB',
    transactionType: 'Cash withdrawal',
    timestamp: '2021-08-02 04:39:26',
    amount: '5000',
    balance: '40059.83',
    transactionStatusIcon: 'withdraw',
  },
  {
    id: 2,
    transactionStatus: 'WDL/Karen Ijezie/GTB',
    transactionType: 'Cash deposit',
    timestamp: '2021-08-02 04:39:26',
    amount: '5000',
    balance: '40059.83',
    transactionStatusIcon: 'deposit',
  },
  {
    id: 3,
    transactionStatus: 'WDL/Karen Ijezie/GTB',
    transactionType: 'Cash withdrawal',
    timestamp: '2021-08-02 04:39:26',
    amount: '100',
    balance: '40059.83',
    transactionStatusIcon: 'withdraw',
  },
  {
    id: 4,
    transactionStatus: 'WDL/Karen Ijezie/GTB',
    transactionType: 'Cash withdrawal',
    timestamp: '2021-08-02 04:39:26',
    amount: '100',
    balance: '40059.83',
    transactionStatusIcon: 'withdraw',
  },
];

interface Props {
  transactionStatusIcon?: 'withdraw' | 'deposit';
  transactionStatus: string;
  transactionType: string;
  timestamp: string;
  amount: string;
  balance: string;
  fees: string;
  reason?: string;
}

const TransactionList: FC<Props> = ({
  transactionStatusIcon,
  transactionStatus,
  transactionType,
  timestamp,
  amount,
  balance,
  fees,
  reason,
}) => {
  return (
    <Box style={styles.container}>
      <Box style={styles.list}>
        <Box style={styles.leftList}>
          <Box>
            {transactionType === 'charge.success' ? (
              <Box
                style={[
                  styles.withdrawIcon,
                  {
                    backgroundColor:
                      transactionType === 'charge.success'
                        ? theme.colors.bg13
                        : theme.colors.bg6,
                  },
                ]}
              >
                <Icon
                  name="arrow-down"
                  color={theme.colors.accent4}
                  size={24}
                />
              </Box>
            ) : (
              <Box
                style={[
                  styles.withdrawIcon,
                  {
                    backgroundColor:
                      transactionType === 'charge.success'
                        ? theme.colors.bg13
                        : theme.colors.bg6,
                  },
                ]}
              >
                <Icon name="arrow-up" color={theme.colors.error} size={24} />
              </Box>
            )}
          </Box>
          <Box>
            <Text variant="SmallerTextR" color="text6">
              {transactionStatus.toUpperCase()}
            </Text>
            {/* <Box style={styles.leftText}>
              <Text variant="SmallerTextR" color="text2">
                {transactionType}
              </Text>
            </Box> */}

            <Text variant="SmallerTextR" color="text9">
              {reason ? reason : transactionType}
            </Text>

            <Text variant="SmallerTextR" color="text9">
              {timestamp}
            </Text>
          </Box>
        </Box>
        <Box>
          <Box style={styles.rightText}>
            <Text variant="SmallerTextR" color={'text1'}>
              {numberWithCommas(amount ? amount : '')}
            </Text>
          </Box>
          {/* <Text variant="SmallerTextR" color="text9">
            {numberWithCommas(balance ? balance : '')}
          </Text> */}
          <Box style={styles.leftText}>
            <Text variant="SmallerTextR" color="text2">
              {`Fee: N ${(Number(fees) / 100).toFixed(2).toString()}`}
            </Text>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default TransactionList;

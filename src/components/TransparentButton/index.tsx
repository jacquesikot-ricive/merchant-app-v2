import React, { FC } from 'react';
import { ActivityIndicator, StyleSheet, TouchableOpacity } from 'react-native';

import theme, { Box, Text } from '../Themed';

const styles = StyleSheet.create({
  container: {
    height: 56,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1.5,
    borderColor: theme.colors.primary1,
  },
});

interface Props {
  type: | 'transparent';
  label: string;
  onPress: () => void;
  width?: number;
  loading?: boolean;
}

const TransparentButton: FC<Props> = ({ type, label, onPress, width, loading }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.6}
      style={[
        styles.container,
        {
          backgroundColor:
            type === 'transparent' ? theme.colors.white : theme.colors.primary1,
          width: width || theme.layout.screenWidth,
        },
      ]}
      onPress={onPress}
    >
      {loading ? (
        <ActivityIndicator
          color={
            type === 'transparent' ? theme.colors.primary1 : theme.colors.text7
          }
        />
      ) : (
        <Text
          variant="Body1M"
          color={type === 'transparent' ? 'primary1' : 'primary1'}
        >
          {label}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export default TransparentButton;

import React from 'react';
import { StyleSheet } from 'react-native';

import ChevronRight from '../../../svgs/ChevronRight';
import theme, { Box, Text } from '../Themed';

const styles = StyleSheet.create({
  layout: {
    paddingHorizontal: 15,
    width: '100%',
  },
  container: {
    paddingVertical: 12,
    paddingHorizontal: 17,
    backgroundColor: theme.colors.bg5,
    borderRadius: 10,
  },
});

interface Props {
  orderStatus: string;
  serviceType: string;
  bagsForPickup?: string;
  customerName: string;
  phoneNumber: string;
  email?: string;
}

const UpcomingInvoiceCard: React.FC<Props> = ({
  orderStatus,
  serviceType,
  bagsForPickup,
  customerName,
  phoneNumber,
  email,
}) => {
  return (
    <Box style={styles.layout}>
      <Box style={styles.container}>
        <Box style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <Text variant="SmallerTextR" color="text2">
            Order status
          </Text>
          <Text
            variant="SmallerTextR"
            color={
              orderStatus && orderStatus === 'COMPLETED'
                ? 'primary1'
                : 'primary4'
            }
          >
            {orderStatus}
          </Text>
        </Box>
        {/* <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 14,
          }}
        >
          <Text variant="DetailsR" color="text2">
            Service type
          </Text>
          <Text variant="DetailsR" color="text8">
            {serviceType}
          </Text>
        </Box> */}
        {bagsForPickup && (
          <Box
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 14,
            }}
          >
            <Text variant="SmallerTextR" color="text2">
              Bags for pickup
            </Text>
            <Text variant="SmallerTextR" color="text8">
              {bagsForPickup}
            </Text>
          </Box>
        )}
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 14,
          }}
        >
          <Text variant="SmallerTextR" color="text2">
            Customer
          </Text>
          <Text variant="SmallerTextR" color="text8">
            {customerName}
          </Text>
        </Box>
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 14,
          }}
        >
          <Text variant="SmallerTextR" color="text2">
            Phone number
          </Text>
          <Text variant="SmallerTextR" color="text8">
            {phoneNumber}
          </Text>
        </Box>

        {email && (
          <Box
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 14,
            }}
          >
            <Text variant="SmallerTextR" color="text2">
              Email
            </Text>
            <Text variant="SmallerTextR" color="text8">
              {email}
            </Text>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default UpcomingInvoiceCard;

import React from 'react';
import { Linking, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';

import ChevronRight from '../../svgs/ChevronRight';
import theme, { Box, Text } from './Themed';

const styles = StyleSheet.create({
  container: {
    width: wp(100),
    paddingVertical: 8,
    borderRadius: 10,
    marginBottom: 10,
    // height: 92,
  },
  cardContent: {
    paddingHorizontal: 24,
  },
  order: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  status: {
    flexDirection: 'row',
    height: 24,
    borderRadius: 8,
    backgroundColor: 'rgba(250, 172, 48, 0.05)',
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  customer: {
    flexDirection: 'row',
    width: 120,
    height: 23,
    borderRadius: 6,
    marginTop: 3,
  },
  pickup: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  pickupText: {
    marginTop: 5,
    flexDirection: 'row',
  },
  icon: {
    position: 'absolute',
    right: 20,
    top: '40%',
    padding: 10,
  },
});

export const orderData = [
  {
    id: '1',
    orderId: '12345',
    timestamp: 'Wed, Jun 22 2022 | 03:00 - 04:00 pm',
    customer: 'Grace Peters',
    status: 'Scheduled',
  },
];

interface Props {
  serviceType?: string;
  orderId: string;
  timestamp: string | null;
  customer: string;
  onPress: () => void;
  darkBg?: boolean;
  status?: string;
  isSchedule?: boolean;
  isBooking?: boolean;
  scheduleTime?: string;
  createdAt: string;
}

export const returnStatusColors = (status: string) => {
  if (status === 'COMPLETED') {
    return {
      bg: 'rgba(56, 203, 137, 0.05)',
      text: '#38CB89',
    };
  }

  if (status === 'BOOKING') {
    return {
      bg: 'rgba(27, 37, 89, 0.05)',
      text: '#1B2559',
    };
  }

  return {
    bg: 'rgba(250, 172, 48, 0.05)',
    text: '#FAAC30',
  };
};

const UpcomingOrderCard: React.FC<Props> = ({
  serviceType,
  status,
  orderId,
  timestamp,
  onPress,
  customer,
  darkBg,
  isSchedule,
  scheduleTime,
  createdAt,
  isBooking,
}) => {
  return (
    <Box
      style={[
        styles.container,
        { backgroundColor: darkBg ? theme.colors.bg5 : theme.colors.bg1 },
      ]}
    >
      <Box style={styles.cardContent}>
        <Box
          flexDirection={'row'}
          alignItems={'center'}
          justifyContent={'space-between'}
        >
          <Box style={styles.order}>
            <Box flexDirection={'row'} alignItems="center">
              <Text variant="SmallerTextM" color="text6">
                Order #{orderId}
              </Text>
            </Box>
            {status && (
              <Box
                style={[
                  styles.status,
                  {
                    backgroundColor: returnStatusColors(status).bg,
                  },
                ]}
              >
                <Text
                  variant="DetailsR"
                  style={{ color: returnStatusColors(status).text }}
                  ml="s"
                >
                  {status.charAt(0).toUpperCase() +
                    status.substring(1).toLocaleLowerCase()}
                </Text>
              </Box>
            )}

            {isBooking && (
              <Box
                style={[
                  styles.status,
                  {
                    backgroundColor: returnStatusColors('BOOKING').bg,
                  },
                ]}
              >
                <Text
                  variant="DetailsR"
                  style={{ color: returnStatusColors('BOOKING').text }}
                >
                  Booking
                </Text>
              </Box>
            )}
          </Box>

          <Box>
            {/* <Box style={{ flexDirection: 'row' }}>
              <Text variant="DetailsR" color="text5">
                Service type:
              </Text>
              <Text style={{ paddingLeft: 5 }} variant="DetailsR" color="text8">
                {serviceType}
              </Text>
            </Box> */}
          </Box>
        </Box>

        {/* {status && (
          <Box style={styles.status}>
            <Text variant="DetailsR" color="text5">
              Status:
            </Text>

            <Text variant="DetailsR" color="primary1" ml="s">
              {status.charAt(0).toUpperCase() +
                status.substring(1).toLocaleLowerCase()}
            </Text>
          </Box>
        )} */}

        <Box>
          <Box style={styles.customer}>
            <Text variant="SmallerTextR" color="text5">
              Customer:
            </Text>
            <Text variant="SmallerTextR" color="text5" ml="s">
              {customer}
            </Text>
          </Box>
        </Box>

        <Box style={styles.pickup}>
          <Box>
            <Box style={styles.pickupText}>
              <Text mr="s" variant="SmallerTextR" color="text5">
                Created At:
              </Text>
              <Text variant="SmallerTextR" color="text5">
                {createdAt}
              </Text>
            </Box>
          </Box>
        </Box>

        {isSchedule && (
          <Box style={styles.pickup}>
            <Box>
              <Box style={styles.pickupText}>
                <Text mr="s" variant="SmallerTextR" color="text5">
                  Scheduled:
                </Text>
                <Text variant="SmallerTextR" color="text5">
                  {timestamp}
                </Text>
              </Box>
            </Box>
          </Box>
        )}
      </Box>
      <TouchableOpacity onPress={onPress} style={styles.icon}>
        <ChevronRight />
      </TouchableOpacity>
    </Box>
  );
};

export default UpcomingOrderCard;

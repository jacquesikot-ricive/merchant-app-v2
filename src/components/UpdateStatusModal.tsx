import React, { useEffect, useState } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from './Themed';
import AppModal from './AppModal';
import Button from './Button';
import Checkbox from './Checkbox';

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  orderPill: {
    borderWidth: 1,
    borderRadius: 8,
    height: 58,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginVertical: 5,
    borderColor: theme.colors.border,
  },
});

interface Props {
  visible: boolean;
  setVisible: (state: boolean) => void;
  handleChangeStatus: (status: string) => Promise<void>;
  status: string;
  loading: boolean;
}

const statuses = [
  {
    id: 1,
    value: 'CREATED',
    label: 'Created',
  },
  {
    id: 2,
    value: 'ACCEPTED',
    label: 'Accepted',
  },
  {
    id: 3,
    value: 'SCHEDULED',
    label: 'Scheduled',
  },
  {
    id: 4,
    value: 'IN STORE',
    label: 'In Store',
  },
  {
    id: 5,
    value: 'PROCESSING',
    label: 'Processing',
  },
  {
    id: 6,
    value: 'DELIVERY',
    label: 'Delivery',
  },
  {
    id: 7,
    value: 'COMPLETED',
    label: 'Completed',
  },
];

const UpdateStatusModal = ({
  visible,
  setVisible,
  handleChangeStatus,
  status,
  loading,
}: Props) => {
  const [activeStatus, setActiveStatus] = useState<string>(status);

  console.log(status);

  useEffect(() => {
    setActiveStatus(status);
  }, [status]);

  const handleUpdate = async () => {
    await handleChangeStatus(activeStatus);
  };

  return (
    <AppModal
      {...{ visible, setVisible }}
      heightValue={hp(85)}
      width={widthPercentageToDP(100)}
      content={
        <Box style={styles.container}>
          <Box
            style={{
              width: widthPercentageToDP(90),
            }}
          >
            <Text variant={'Body1M'}>Select Order Status</Text>
          </Box>

          <ScrollView
            bounces={true}
            style={{ width: '100%', marginBottom: 30, marginTop: 15 }}
          >
            {statuses.map((s) => {
              const isSelected = activeStatus === s.value;

              return (
                <TouchableOpacity
                  onPress={() => setActiveStatus(s.value)}
                  key={s.id.toString()}
                  style={[
                    styles.orderPill,
                    {
                      borderColor: isSelected
                        ? theme.colors.primary1
                        : theme.colors.border,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                      paddingHorizontal: 20,
                    },
                  ]}
                >
                  <Box
                    style={{
                      position: 'absolute',
                      left: 20,
                    }}
                  >
                    <Checkbox checked={isSelected} />
                  </Box>

                  <Text
                    variant={'DetailsR'}
                    color={isSelected ? 'primary1' : 'black'}
                  >
                    {s.label}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </ScrollView>

          <Box
            style={{
              marginBottom: -70,
              paddingTop: 20,
              position: 'absolute',
              bottom: 0,
              width: widthPercentageToDP(100),
              backgroundColor: theme.colors.white,
              borderTopWidth: 1,
              alignItems: 'center',
              borderColor: theme.colors.border4,
            }}
          >
            <Button
              label={`Set Status to "${activeStatus}"`}
              width={widthPercentageToDP(90)}
              onPress={handleUpdate}
              type="primary"
              loading={loading}
            />
          </Box>
        </Box>
      }
    />
  );
};

export default UpdateStatusModal;

export const states = [
  {
    id: 0,
    label: '',
    value: '',
  },
  {
    id: 1,
    label: 'Abia',
    value: 'ABIA',
  },
  {
    id: 2,
    label: 'Adamawa',
    value: 'ADAMAWA',
  },
  {
    id: 3,
    label: 'Akwa Ibom',
    value: 'AKWAIBOM',
  },
  {
    id: 4,
    label: 'Anambra',
    value: 'ANAMBRA',
  },
  {
    id: 5,
    label: 'Bauchi',
    value: 'BAUCHI',
  },
  {
    label: 'Benue',
    value: 'BENUE',
  },
  {
    id: 6,
    label: 'Borno',
    value: 'BORNO',
  },
  {
    id: 7,
    label: 'Cross River',
    value: 'CROSSRIVER',
  },
  {
    id: 8,
    label: 'Delta',
    value: 'DELTA',
  },
  {
    id: 9,
    label: 'Ebonyi',
    value: 'EBONYI',
  },
  {
    id: 10,
    label: 'Edo',
    value: 'EDO',
  },
  {
    id: 11,
    label: 'Kogi',
    value: 'KOGI',
  },
  {
    id: 12,
    label: 'Kwara',
    value: 'KWARA',
  },
  {
    id: 13,
    label: 'Plateau',
    value: 'PLATEAU',
  },
  {
    id: 14,
    label: 'Rivers',
    value: 'RIVERS',
  },
  {
    id: 15,
    label: 'Sokoto',
    value: 'SOKOTO',
  },
  {
    id: 16,
    label: 'Taraba',
    value: 'TARABA',
  },
  {
    id: 17,
    label: 'Yobe',
    value: 'YOBE',
  },
  {
    id: 18,
    label: 'Kaduna',
    value: 'KADUNA',
  },
  {
    id: 19,
    label: 'Katsina',
    value: 'KATSINA',
  },
  {
    id: 20,
    label: 'Abuja',
    value: 'ABUJA',
  },
  {
    id: 21,
    label: 'Ogun',
    value: 'OGUN',
  },
  {
    id: 22,
    label: 'Oyo',
    value: 'OYO',
  },
  {
    id: 23,
    label: 'Osun',
    value: 'OSUN',
  },
  {
    id: 24,
    label: 'Ondo',
    value: 'ONDO',
  },
  {
    id: 25,
    label: 'Kwara',
    value: 'KWARA',
  },
  {
    id: 26,
    label: 'Enugu',
    value: 'ENUGU',
  },
  {
    id: 27,
    label: 'Lagos',
    value: 'LAGOS',
  },
];

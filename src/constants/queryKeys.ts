const queryKeys = {
  ANALYTICS_DATA: 'analytics_data',
  ORDERS_DATA: 'orders_data',
  CURRENCY_DATA: 'currency_data',
  PRODUCTS_DATA: 'products_data',
  CUSTOMERS_DATA: 'customers_data',
  INVOICE_DATA: 'invoice_data',
  STOREFRONT_DATA: 'storefront_data',
  CATEGORY_DATA: 'category_data',
  TEAM_DATA: 'team_data',
};

export default queryKeys;

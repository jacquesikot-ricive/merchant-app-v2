import { useEffect, useState } from 'react';

const useApi = (apiFunc: () => Promise<any>) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [data, setData] = useState<any>();

  useEffect(() => {
    const callFunc = async () => {
      setLoading(true);
      const res = await apiFunc();
      if (res) {
        setData(res);
        setLoading(false);
      }
      setLoading(false);
    };

    callFunc();
  }, []);

  return {
    data,
    loading,
  };
};

export default useApi;

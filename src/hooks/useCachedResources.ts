import { FontAwesome } from '@expo/vector-icons';
import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import * as SplashScreen from 'expo-splash-screen';
import { useEffect, useState } from 'react';

const assets: number[] = [
  require('../assets/images/onboarding/onboarding1.png'),
  require('../assets/images/onboarding/onboarding2.png'),
  require('../assets/images/onboarding/onboarding3.png'),

  require('../assets/images/onboarding/welcome.png'),
  require('../assets/images/EmailSent.png'),
  require('../assets/images/verifyemail.png'),
];

const loadAssets = () => {
  assets.map(async (a) => Asset.loadAsync(a));
};

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          ...FontAwesome.font,
          'space-mono': require('../assets/fonts/SpaceMono-Regular.ttf'),
          'basier-bold': require('../assets/fonts/BasierCircle-Bold.ttf'),
          'basier-boldItalic': require('../assets/fonts/BasierCircle-BoldItalic.ttf'),
          'basier-italic': require('../assets/fonts/BasierCircle-Italic.ttf'),
          'basier-medium': require('../assets/fonts/BasierCircle-Medium.ttf'),
          'basier-mediumItalic': require('../assets/fonts/BasierCircle-MediumItalic.ttf'),
          'basier-regular': require('../assets/fonts/BasierCircle-Regular.ttf'),
          'basier-semiBold': require('../assets/fonts/BasierCircle-SemiBold.ttf'),
          'basier-semiBoldItalic': require('../assets/fonts/BasierCircle-SemiBoldItalic.ttf'),
        });

        loadAssets();
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}

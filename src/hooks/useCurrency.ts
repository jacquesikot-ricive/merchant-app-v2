import { useAppSelector } from '../redux/hooks';

const useCurrency = () => {
  const currency = useAppSelector(
    (state) => state.login.user.business.currency
  );

  const returnCurrency = (): {
    code: string;
    name: 'Naira' | 'Kwacha';
    inputCode: 'NG' | 'ZM';
  } => {
    if (currency === '390dc4e5-d2c9-4310-b333-e61647a53f3c')
      return {
        code: 'NGN',
        name: 'Naira',
        inputCode: 'NG',
      };
    if (currency === '918f40e9-9bb9-4f50-bffb-06a179ba0f7e')
      return {
        code: 'ZK',
        name: 'Kwacha',
        inputCode: 'ZM',
      };
    return {
      code: 'NGN',
      name: 'Naira',
      inputCode: 'NG',
    };
  };

  return {
    returnCurrency,
  };
};

export default useCurrency;

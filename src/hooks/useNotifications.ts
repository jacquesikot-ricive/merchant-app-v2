/* eslint-disable no-alert */
import { useState, useRef, useEffect } from 'react';
import { Platform } from 'react-native';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';

import theme from '../components/Themed';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import notificationApi from '../api/riciveApi/notification';
import { createNotificationToken } from '../redux/app';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

async function registerForPushNotification() {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } =
      await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    // if (finalStatus !== 'granted') {
    //   alert('Failed to get push token for push notification!');
    //   return;
    // }
    // if (finalStatus !== 'granted') {
    //   return Alert.alert(
    //     'Warning',
    //     'You will not receive reminders if you do not enable push notifications. If you would like to receive reminders, please enable push notifications for Ricive in your settings.',
    //     [
    //       { text: 'Cancel' },
    //       // If they said no initially and want to change their mind,
    //       // we can automatically open our app in their settings
    //       // so there's less friction in turning notifications on
    //       {
    //         text: 'Enable Notifications',
    //         onPress: () =>
    //           Platform.OS === 'ios'
    //             ? Linking.openURL('app-settings:')
    //             : Linking.openSettings(),
    //       },
    //     ]
    //   );
    // }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    // await storage.storeData('EXPO_PUSH_TOKEN', token);
  } else {
    // alert('Must use physical device for Push Notifications');
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: theme.colors.primary1,
    });
  }

  // eslint-disable-next-line consistent-return
  return token;
}

const useNotifications = (): void => {
  const user_id = useAppSelector((state) => state.login.user.profile.id);
  const [expoPushToken, setExpoPushToken] = useState<string>();
  const notificationListener = useRef<any>();
  const responseListener = useRef<any>();
  const dispatch = useAppDispatch();

  useEffect(() => {
    registerForPushNotification().then(async (token) => {
      // console.log('TOKEN:', token);
      setExpoPushToken(token as string);
      if (expoPushToken && expoPushToken.length > 0) {
        return dispatch(
          createNotificationToken({
            token: expoPushToken,
            user_id,
          })
        );
      }
    });

    notificationListener.current =
      Notifications.addNotificationReceivedListener(
        async (notificationEvent) => {
          await notificationApi.updateNotification({
            id: notificationEvent.request.content.data.id as string,
            is_delivered: true,
            is_opened: false,
          });
        }
      );

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener(
        async (response) => {
          // console.log('clicked', response.notification.request.content);
          // RootNavigation.navigate('OrdersHome');
        }
      );

    return () => {
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, [expoPushToken]);
};

export default useNotifications;

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import Onboarding from '../screens/auth/Onboarding';
import SetupStore from '../screens/auth/SetupStore';
import SignIn from '../screens/auth/SignIn';
import SignUpProfile from '../screens/auth/SignUpProfile';
import SignUpAuth from '../screens/auth/SignUpAuth';
import VerifyOtp from '../screens/auth/VerifyOtp';
import VerifyOtpInput from '../screens/auth/VerifyOtpInput';
import Welcome from '../screens/auth/Welcome';
import TeamSignIn from '../screens/auth/TeamSignIn';
import ForgotPassword from '../screens/auth/ForgotPassword';
import EmailSent from '../screens/auth/EmailSent';
import ProfileHome from '../screens/profile/ProfileHome';
import PersonalInformation from '../screens/profile/PersonalInformation';
import EditProfile from '../screens/profile/EditProfile';
import ChangePassword from '../screens/profile/ChangePassword';
import Support from '../screens/Support';
import Auth from '../api/riciveApi/auth';
import ProductsNav from './ProductsNav';

export type AuthNavParamList = {
  SignIn: {
    token?: string;
  };
  SignUpProfile: undefined;
  SignUpAuth: {
    first_name: string;
    last_name: string;
    email: string;
    referralCode?: string;
  };
  Support: {
    isAuth?: boolean;
  };
  EmailSent: undefined;
  Onboarding: undefined;
  Welcome: undefined;
  TeamSignIn: undefined;
  SetupStore: {
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    password: string;
    username: string;
  };
  VerifyOtp: undefined;
  VerifyOtpInput: undefined;
  Orders: undefined;
  UpcomingOrders: undefined;
  Invoice: undefined;
  ForgotPassword: undefined;
  ProfileHome: undefined;
  ProductsNav: undefined;
};

const AuthStack = createNativeStackNavigator<AuthNavParamList>();

function AuthNav(): JSX.Element {
  return (
    <AuthStack.Navigator screenOptions={{ headerShown: false }}>
      <AuthStack.Screen name="Onboarding" component={Onboarding} />
      <AuthStack.Screen name="SignIn" component={SignIn} />
      <AuthStack.Screen name="SignUpProfile" component={SignUpProfile} />
      <AuthStack.Screen name="EmailSent" component={EmailSent} />
      <AuthStack.Screen name="SignUpAuth" component={SignUpAuth} />
      <AuthStack.Screen name="SetupStore" component={SetupStore} />
      <AuthStack.Screen name="ForgotPassword" component={ForgotPassword} />
      <AuthStack.Screen name="VerifyOtp" component={VerifyOtp} />
      <AuthStack.Screen name="VerifyOtpInput" component={VerifyOtpInput} />
      <AuthStack.Screen name="Support" component={Support} />
      <AuthStack.Screen name="Welcome" component={Welcome} />
      <AuthStack.Screen name="TeamSignIn" component={TeamSignIn} />
    </AuthStack.Navigator>
  );
}

export default AuthNav;

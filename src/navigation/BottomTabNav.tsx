import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as Haptics from 'expo-haptics';

import theme, { Box, Text } from '../components/Themed';
import useColorScheme from '../hooks/useColorScheme';
import CustomerIcon from '../svg/homeTab/CustomersIcon';
import MoreIcon from '../svg/homeTab/MoreIcon';
import OrdersIcon from '../svg/homeTab/OrdersIcon';
import OrderNav from './OrdersNav';
import MoreNav from './MoreNav';
import CustomerNav from './CustomerNav';
import HomeNav from './HomeNav';
import { useAppSelector } from '../redux/hooks';
import ProductsNav from './ProductsNav';
import ProductsHomeIcon from '../svg/Home/ProductsHomeIcon';
import HomeNavIcon from '../svg/drawerNav/HomeNavIcon';

export type BottomTabParamList = {
  Home: undefined;
  Orders: undefined;
  Customers: undefined;
  InvoiceMain: undefined;
  TeamsNav: undefined;
  ProductsHome: undefined;
  MoreNav: undefined;
};

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

const BottomTabNav = () => {
  const newOrderCount = useAppSelector((state) => state.app.newOrders);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: theme.colors.primary1,
        headerShown: false,
      }}
    >
      <BottomTab.Screen
        name="Home"
        component={HomeNav}
        options={() => ({
          title: 'Home',
          tabBarIcon: ({ color }) => <HomeNavIcon color={color} />,
          // unmountOnBlur: true,
        })}
        // listeners={() => ({
        //   tabPress: () =>
        //     Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light),
        // })}
      />
      {userPermissions && userPermissions.view_order && (
        <BottomTab.Screen
          name="Orders"
          component={OrderNav}
          options={{
            title: 'Orders',
            tabBarIcon: ({ color }) => {
              return (
                <Box>
                  {Number(newOrderCount) > 0 && (
                    <Box
                      style={{
                        width: 18,
                        height: 18,
                        backgroundColor: theme.colors.error,
                        borderRadius: 9,
                        justifyContent: 'center',
                        alignItems: 'center',
                        position: 'absolute',
                        right: -5,
                        top: -5,
                        zIndex: 1,
                      }}
                    >
                      <Text
                        color="white"
                        style={{ fontSize: 10, fontWeight: 'bold' }}
                      >
                        {newOrderCount}
                      </Text>
                    </Box>
                  )}
                  <OrdersIcon color={color} />
                </Box>
              );
            },
          }}
          // listeners={() => ({
          //   tabPress: () =>
          //     Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light),
          // })}
        />
      )}

      {userPermissions && userPermissions.view_product && (
        <BottomTab.Screen
          name="ProductsHome"
          component={ProductsNav}
          options={{
            title: 'Products',
            tabBarIcon: ({ color }) => <ProductsHomeIcon color={color} />,
            // unmountOnBlur: true,
          }}
          // listeners={() => ({
          //   tabPress: () =>
          //     Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light),
          // })}
        />
      )}

      {userPermissions && userPermissions.view_customer && (
        <BottomTab.Screen
          name="Customers"
          component={CustomerNav}
          options={{
            title: 'Customers',
            tabBarIcon: ({ color }) => <CustomerIcon color={color} />,
            // unmountOnBlur: true,
          }}
          // listeners={() => ({
          //   tabPress: () =>
          //     Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light),
          // })}
        />
      )}
      <BottomTab.Screen
        name="MoreNav"
        component={MoreNav}
        options={{
          title: 'More',
          tabBarIcon: ({ color }) => <MoreIcon color={color} />,
          // unmountOnBlur: true,
        }}
        // listeners={() => ({
        //   tabPress: () =>
        //     Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light),
        // })}
      />

      {/* <BottomTab.Screen
        name="InvoiceMain"
        component={InvoiceNav}
        options={{
          title: 'Invoice',
          tabBarIcon: ({ color }) => <InvoiceIcon color={color} />,
          unmountOnBlur: true,
        }}
        listeners={() => ({
          tabPress: () =>
            Haptics.impactAsync(Haptics.ImpactFeedbackStyle.Light),
        })}
      /> */}
    </BottomTab.Navigator>
  );
};

export default BottomTabNav;

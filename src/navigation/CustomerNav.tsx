import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import AddCustomer from '../screens/customer/AddCustomer';
import Customer from '../screens/customer/Customer';
import CustomerDetail from '../screens/customer/CustomerDetail';
import OrderDetails from '../screens/order/OrderDetails';
import InvoiceDetail from '../screens/order/InvoiceDetail';
import Invoice from '../screens/order/Invoice';

export type CustomerNavParamList = {
  Customer: undefined;
  AddCustomer: {
    isEdit: boolean;
    phone?: string;
    firstName?: string;
    lastName?: string;
    isContacts?: boolean;
    customer?: any;
  };
  CustomerDetail: {
    customerId: string;
  };
  OrderDetails: {
    id: number;
  };
  Invoice: {
    order: any;
  };
  InvoiceDetail: {
    id: string;
    dateCreated: string;
    status: 'PAID' | 'UNPAID';
    serviceType: string;
    items: any[];
    total: string;
    deliveryFee: any;
    deliveryFeeType?: string;
    isSending: boolean;
    customerId: string;
    isInvoiceNav?: boolean;
    customerPhone: string;
  };
  navigation: undefined;
};

const CustomerStack = createNativeStackNavigator<CustomerNavParamList>();

function CustomerNav(): JSX.Element {
  return (
    <CustomerStack.Navigator screenOptions={{ headerShown: false }}>
      <CustomerStack.Screen name="Customer" component={Customer} />
      <CustomerStack.Screen name="AddCustomer" component={AddCustomer} />
      <CustomerStack.Screen name="CustomerDetail" component={CustomerDetail} />
      <CustomerStack.Screen name="OrderDetails" component={OrderDetails} />
      <CustomerStack.Screen name="InvoiceDetail" component={InvoiceDetail} />
      <CustomerStack.Screen name="Invoice" component={Invoice} />
    </CustomerStack.Navigator>
  );
}

export default CustomerNav;

import React, { useContext } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Home from '../screens/home/Home';
import TeamsNav from './TeamsNav';
import DrawerContent from '../components/DrawerContent';
import BottomTabNav from './BottomTabNav';
import { useAppSelector } from '../redux/hooks';
import RiderBottomTabNav from './RiderBottomTabNav';
import ServicesNav from './ServicesNav';
import ProductsNav from './ProductsNav';
import WalletNav from './WalletNav';
import StoreFrontNav from './StoreFrontNav';
import ProfileNav from './ProfileNav';
import SupportNav from './SupportNav';

export type DrawerNavParamList = {
  HomeNav: undefined;
  ProductsNav: undefined;
  ServicesNav: undefined;
  TeamsNav: undefined;
  WalletNav: undefined;
  StoreFrontNav: undefined;
  Support: undefined;
  Settings: undefined;
  ProfileNav: undefined;
  SupportNav: undefined;
};

const Drawer = createDrawerNavigator<DrawerNavParamList>();

const DrawerNav = (): JSX.Element => {
  const role = useAppSelector((state) => state.login.user.profile.role);
  const isSuperAdmin = role === 'SUPERADMIN';
  const isAdmin = role === 'ADMIN';
  const isTeam = role === 'TEAM';
  return (
    <Drawer.Navigator
      drawerContent={(props: any) => <DrawerContent {...props} />}
      screenOptions={{
        headerShown: false,
      }}
    >
      <Drawer.Screen
        name="HomeNav"
        options={{ unmountOnBlur: true }}
        component={role === 'DELIVERY' ? RiderBottomTabNav : BottomTabNav}
      />
      {isSuperAdmin && (
        <>
          <Drawer.Screen name="TeamsNav" component={TeamsNav} />
          <Drawer.Screen name="ServicesNav" component={ServicesNav} />
          <Drawer.Screen
            name="ProductsNav"
            component={ProductsNav}
            options={{ unmountOnBlur: true }}
          />
          <Drawer.Screen
            name="WalletNav"
            component={WalletNav}
            options={{ unmountOnBlur: true }}
          />
          <Drawer.Screen name="StoreFrontNav" component={StoreFrontNav} />
          <Drawer.Screen name="ProfileNav" component={ProfileNav} />
          <Drawer.Screen name="SupportNav" component={SupportNav} />
        </>
      )}

      {isTeam && (
        <>
          <Drawer.Screen name="ProfileNav" component={ProfileNav} />
          <Drawer.Screen name="SupportNav" component={SupportNav} />
        </>
      )}

      {isAdmin && (
        <>
          <Drawer.Screen name="ProfileNav" component={ProfileNav} />
          <Drawer.Screen name="SupportNav" component={SupportNav} />
          <Drawer.Screen name="TeamsNav" component={TeamsNav} />
          <Drawer.Screen name="ServicesNav" component={ServicesNav} />
          <Drawer.Screen
            name="WalletNav"
            component={WalletNav}
            options={{ unmountOnBlur: true }}
          />
          <Drawer.Screen
            name="ProductsNav"
            component={ProductsNav}
            options={{ unmountOnBlur: true }}
          />
          <Drawer.Screen name="StoreFrontNav" component={StoreFrontNav} />
        </>
      )}
    </Drawer.Navigator>
  );
};

export default DrawerNav;

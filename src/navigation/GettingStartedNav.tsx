import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import GettingStarted from '../screens/gettingStarted/GettingStarted';
import AboutBusiness from '../screens/gettingStarted/AboutBusiness';
import MoreBusiness from '../screens/gettingStarted/MoreBusiness';
import ProfileNav from './ProfileNav';
import Services from '../screens/Services';
import AddService from '../screens/AddService';
import EditProduct from '../screens/product/EditProducts';

export type GettingStartedNavParamList = {
  GettingStarted: undefined;
  AboutBusiness: {
    isProfileNav?: boolean;
  };
  ProfileNav: undefined;
  MoreBusiness: {
    isProfileNav?: boolean;
    type: any;
    city: string;
    district: string;
    country: string;
  };
  Services: {
    isGettingStarted: boolean;
  };
  AddService: {
    isEdit: boolean;
    name?: string;
    description?: string;
    id?: string;
  };
  AddProduct: {
    isGettingStartedNav?: boolean;
  };
};

const Stack = createNativeStackNavigator<GettingStartedNavParamList>();

function GettingStartedNav(): JSX.Element {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      {/* <Stack.Screen name="ProfileNav" component={ProfileNav} /> */}
      <Stack.Screen name="GettingStarted" component={GettingStarted} />
      <Stack.Screen name="AboutBusiness" component={AboutBusiness} />
      <Stack.Screen name="MoreBusiness" component={MoreBusiness} />
      <Stack.Screen name="Services" component={Services} />
      <Stack.Screen name="AddService" component={AddService} />
      <Stack.Screen name="AddProduct" component={EditProduct} />
    </Stack.Navigator>
  );
}

export default GettingStartedNav;

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import Home from '../screens/home/Home';
import Notifications from '../screens/home/Notifications';
import DueInvoices from '../screens/home/DueInvoices';
import FailedPayments from '../screens/home/FailedPayments';
import ScheduledOrders from '../screens/home/ScheduledOrders';
import CreateInvoice from '../screens/invoice/CreateInvoice';
import Invoice from '../screens/order/Invoice';
import InvoiceDetail from '../screens/order/InvoiceDetail';
import GettingStartedNav from './GettingStartedNav';
import CreateOrder from '../screens/order/CreateOrder';
import ProfileNav from './ProfileNav';
import InvoiceNav from './InvoiceNav';
import SelectBusiness from '../screens/home/SelectBusiness';
import OrderCreateDetail from '../screens/order/OrderCreateDetail';

export type HomeNavParamList = {
  HomeScreen: undefined;
  Notification: undefined;
  DueInvoices: undefined;
  FailedPayments: undefined;
  ScheduledOrders: undefined;
  CreateInvoice: undefined;
  SelectBusiness: undefined;
  Invoice: {
    order: any;
    sendSMS?: boolean;
  };
  InvoiceDetail: {
    id: string;
    dateCreated: string;
    status: 'PAID' | 'UNPAID';
    serviceType: string;
    items: any[];
    total: string;
    deliveryFee: any;
    deliveryFeeType?: string;
    isSending: boolean;
    userId: number;
    customerId: string;
    customerPhone: string;
    isInvoiceNav?: boolean;
    sendSms?: boolean;
  };
  GettingStartedNav: undefined;
  CreateOrder: {
    isHomeNav?: boolean;
  };
  ProfileNav: undefined;
  InvoiceNav: undefined;
  InvoiceHome: undefined;
  OrderCreateDetail: {
    invoice: {
      is_paid: boolean;
      created_at: string;
      total: string;
    };
    invoiceItems: [
      {
        quantity: any;
        name: string;
        total: string;
        item_id: string;
      }
    ];
    deliveryFee: {
      amount: string;
      type: string;
    };
    isDelivery: boolean;
    isPickup: boolean;
    activeCustomer: any;
    finalAddress: any;
    selectedDate: any;
    selectedTime: any;
    sendSms: boolean;
    isHomeNav?: boolean;
  };
};

const Stack = createNativeStackNavigator<HomeNavParamList>();

function HomeNav(): JSX.Element {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="HomeScreen" component={Home} />
      <Stack.Screen name="GettingStartedNav" component={GettingStartedNav} />
      <Stack.Screen name="Notification" component={Notifications} />
      <Stack.Screen name="DueInvoices" component={DueInvoices} />
      <Stack.Screen name="FailedPayments" component={FailedPayments} />
      <Stack.Screen name="ScheduledOrders" component={ScheduledOrders} />
      <Stack.Screen name="CreateInvoice" component={CreateInvoice} />
      <Stack.Screen name="Invoice" component={Invoice} />
      <Stack.Screen name="InvoiceDetail" component={InvoiceDetail} />
      <Stack.Screen name="CreateOrder" component={CreateOrder} />
      <Stack.Screen name="ProfileNav" component={ProfileNav} />
      <Stack.Screen name="InvoiceHome" component={DueInvoices} />
      <Stack.Screen name="InvoiceNav" component={InvoiceNav} />
      <Stack.Screen
        name="SelectBusiness"
        component={SelectBusiness}
        // options={{ headerShown: false, presentation: 'modal' }}
      />
      <Stack.Screen name="OrderCreateDetail" component={OrderCreateDetail} />
    </Stack.Navigator>
  );
}

export default HomeNav;

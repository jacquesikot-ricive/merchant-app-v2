import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import InvoiceHome from '../screens/invoice/InvoiceHome';
import CreateInvoice from '../screens/invoice/CreateInvoice';
import Invoice from '../screens/order/Invoice';
import InvoiceDetail from '../screens/order/InvoiceDetail';
import GettingStartedNav from './GettingStartedNav';

export type InvoiceNavParamList = {
  InvoiceHome: undefined;
  CreateInvoice: {
    isInvoiceNav?: boolean;
    isHomeNav?: boolean;
    isOrderNav?: boolean;
    order?: any;
    customer?: any;
    isEdit?: boolean;
    hasProducts?: boolean;
    products?: any[];
    dispatch?: string;
    address?: string;
    discount?: string;
    deposit?: string;
    tax?: string;
    invoice?: any;
  };
  Invoice: {
    order?: any;
    customerId?: string;
    customerPhone?: string;
    isInvoiceNav?: boolean;
    isHomeNav?: boolean;
  };
  InvoiceNavDetail: {
    invoice: any;
    invoiceItems: any[];
  };
  InvoiceDetail: {
    id: string;
    dateCreated: string;
    status: 'PAID' | 'UNPAID';
    serviceType: string;
    items: any[];
    total: string;
    deliveryFee: any;
    deliveryFeeType?: string;
    isSending: boolean;
    customerId: string;
    isInvoiceNav?: boolean;
    customerPhone: string;
  };
  GettingStartedInvoice: undefined;
};

const Stack = createNativeStackNavigator<InvoiceNavParamList>();

function InvoiceNav(): JSX.Element {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="InvoiceHome" component={InvoiceHome} />
      <Stack.Screen name="CreateInvoice" component={CreateInvoice} />
      <Stack.Screen name="Invoice" component={Invoice} />
      {/* <Stack.Screen name="InvoiceNavDetail" component={InvoiceNavDetail} /> */}
      <Stack.Screen name="InvoiceDetail" component={InvoiceDetail} />
      <Stack.Screen
        name="GettingStartedInvoice"
        component={GettingStartedNav}
      />
    </Stack.Navigator>
  );
}

export default InvoiceNav;

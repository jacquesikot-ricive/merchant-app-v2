import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import More from '../screens/More';
import Deliveries from '../screens/profile/Deliveries';
import DeliveryDetail from '../screens/profile/DeliveryDetail';
import OrderDetails from '../screens/order/OrderDetails';
import InvoiceNav from './InvoiceNav';
import TeamsNav from './TeamsNav';
import WalletNav from './WalletNav';
import StoreFrontNav from './StoreFrontNav';
import ProfileNav from './ProfileNav';
import SupportNav from './SupportNav';

export type MoreNavParamList = {
  MoreHome: undefined;
  TeamsNav: undefined;
  WalletNav: undefined;
  StoreFrontNav: undefined;
  ProfileNav: undefined;
  SupportNav: undefined;
  Deliveries: undefined;
  DeliveryDetail: {
    id: number;
  };
  InvoiceNav: undefined;
};

const Stack = createNativeStackNavigator<MoreNavParamList>();

function MoreNav(): JSX.Element {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="MoreHome" component={More} />
      <Stack.Screen name="TeamsNav" component={TeamsNav} />
      <Stack.Screen name="WalletNav" component={WalletNav} />
      <Stack.Screen name="StoreFrontNav" component={StoreFrontNav} />
      <Stack.Screen name="ProfileNav" component={ProfileNav} />
      <Stack.Screen name="Deliveries" component={Deliveries} />
      <Stack.Screen name="DeliveryDetail" component={OrderDetails} />
      <Stack.Screen name="InvoiceNav" component={InvoiceNav} />
      <Stack.Screen name="SupportNav" component={SupportNav} />
    </Stack.Navigator>
  );
}

export default MoreNav;

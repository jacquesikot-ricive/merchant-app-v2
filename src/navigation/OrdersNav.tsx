import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import OrdersHome from '../screens/order/OrdersHome';
import OrderDetails from '../screens/order/OrderDetails';
import Invoice from '../screens/order/Invoice';
import InvoiceDetail from '../screens/order/InvoiceDetail';
import CreateOrder from '../screens/order/CreateOrder';
import AcceptOrder from '../screens/order/AcceptOrder';
import ScheduleDelivery from '../screens/order/ScheduleDelivery';
import InvoiceHome from '../screens/invoice/InvoiceHome';
import GettingStartedNav from './GettingStartedNav';
import OrderCreateDetail from '../screens/order/OrderCreateDetail';
import CreateInvoice from '../screens/invoice/CreateInvoice';

export type OrderNavParaList = {
  OrdersHome: undefined;
  OrderDetails: {
    id: string;
  };
  CreateInvoice: {
    isInvoiceNav?: boolean;
    isHomeNav?: boolean;
    isOrderNav?: boolean;
    order?: any;
    customer?: any;
    isEdit?: boolean;
    hasProducts?: boolean;
    products?: any[];
    dispatch?: string;
    address?: string;
    discount?: string;
    deposit?: string;
    tax?: string;
    invoice?: any;
  };
  Invoice: {
    order: any;
    customerId?: string;
    customerPhone?: string;
    isInvoiceNav?: boolean;
    isHomeNav?: boolean;
  };
  InvoiceDetail: {
    id: string;
    dateCreated: string;
    status: 'PAID' | 'UNPAID';
    // serviceType: string;
    items: any[];
    total: string;
    deliveryFee: any;
    deliveryFeeType?: string;
    isSending: boolean;
    customerId: string;
    isInvoiceNav?: boolean;
    customerPhone: string;
    isNewOrder?: boolean;
    isHomeNav?: boolean;
  };
  CreateOrder: {
    isHomeNav?: boolean;
  };
  AcceptOrder: {
    order_id: string;
    pickup_date: string;
    pickup_time: string;
    is_pickup: boolean;
  };
  ScheduleDelivery: {
    order: number;
    merchant: number;
    user: number;
  };
  InvoiceHome: undefined;
  GettingStartedOrder: undefined;
  OrderCreateDetail: {
    invoice: {
      is_paid: boolean;
      created_at: string;
      total: string;
    };
    invoiceItems: [
      {
        quantity: any;
        name: string;
        total: string;
        item_id: string;
      }
    ];
    deliveryFee: {
      amount: string;
      type: string;
    };
    isDelivery: boolean;
    isPickup: boolean;
    activeCustomer: any;
    finalAddress: any;
    selectedDate: any;
    selectedTime: any;
    sendSms: boolean;
    isHomeNav?: boolean;
  };
};

const OrderStack = createNativeStackNavigator<OrderNavParaList>();

function OrderNav(): JSX.Element {
  return (
    <OrderStack.Navigator screenOptions={{ headerShown: false }}>
      <OrderStack.Screen name="OrdersHome" component={OrdersHome} />
      <OrderStack.Screen name="OrderDetails" component={OrderDetails} />
      <OrderStack.Screen name="Invoice" component={Invoice} />
      <OrderStack.Screen name="InvoiceDetail" component={InvoiceDetail} />
      <OrderStack.Screen name="CreateOrder" component={CreateOrder} />
      <OrderStack.Screen name="InvoiceHome" component={InvoiceHome} />
      <OrderStack.Screen name="CreateInvoice" component={CreateInvoice} />
      <OrderStack.Screen
        name="OrderCreateDetail"
        component={OrderCreateDetail}
      />
      <OrderStack.Screen
        name="GettingStartedOrder"
        component={GettingStartedNav}
      />
      <OrderStack.Screen
        name="AcceptOrder"
        component={AcceptOrder}
        options={{ headerShown: false, presentation: 'modal' }}
      />
      <OrderStack.Screen
        name="ScheduleDelivery"
        component={ScheduleDelivery}
        options={{ headerShown: false, presentation: 'modal' }}
      />
    </OrderStack.Navigator>
  );
}

export default OrderNav;

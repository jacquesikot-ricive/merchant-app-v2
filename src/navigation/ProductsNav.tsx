import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Products from '../screens/product/Products';
import EditProducts from '../screens/product/EditProducts';
import EditCategory from '../screens/EditCategory';
import ProductDetail from '../screens/product/ProductDetail';
import AddSimpleProduct from '../screens/AddSimpleProduct';

export type ProductsNavParamList = {
  Products: undefined;
  EditProduct: {
    isEdit: boolean;
    product?: any;
    isGettingStartedNav?: boolean;
  };
  EditCategory: {
    name: string;
    id: string;
  };
  ProductDetail: {
    name: string;
    price: string;
    quantity: string;
    status: string;
    copyLink: string;
    id: string;
    product: any;
  };
  SimpleAdd: undefined;
};

const ProductsStack = createNativeStackNavigator<ProductsNavParamList>();

function ProductsNav(): JSX.Element {
  return (
    <ProductsStack.Navigator screenOptions={{ headerShown: false }}>
      <ProductsStack.Screen name="Products" component={Products} />
      <ProductsStack.Screen name="EditProduct" component={EditProducts} />
      <ProductsStack.Screen name="EditCategory" component={EditCategory} />
      <ProductsStack.Screen name="ProductDetail" component={ProductDetail} />
      <ProductsStack.Screen name="SimpleAdd" component={AddSimpleProduct} />
    </ProductsStack.Navigator>
  );
}

export default ProductsNav;

import { createNativeStackNavigator } from "@react-navigation/native-stack";
import * as React from "react";
import AboutBusiness from "../screens/gettingStarted/AboutBusiness";
import MoreBusiness from "../screens/gettingStarted/MoreBusiness";
import ChangePassword from "../screens/profile/ChangePassword";
import EditProfile from "../screens/profile/EditProfile";
import PersonalInformation from "../screens/profile/PersonalInformation";
import ProfileHome from "../screens/profile/ProfileHome";
import ShippingNav from "./ShippingNav";
import WorkingHoursNav from "./WorkingHoursNav";

export type ProfileNavParamList = {
  ProfileHome: undefined;
  PersonalInformation: undefined;
  EditProfile: undefined;
  ChangePassword: undefined;
  AboutBusiness: {
    isProfileNav?: boolean;
  };
  MoreBusiness: {
    isProfileNav?: boolean;
    type: any;
    city: string;
    district: string;
    country: string;
  };
  ShippingNav: undefined;
  WorkingHoursNav: undefined;
};

const ProfileStack = createNativeStackNavigator<ProfileNavParamList>();

function ProfileNav(): JSX.Element {
  return (
    <ProfileStack.Navigator screenOptions={{ headerShown: false }}>
      <ProfileStack.Screen name="ProfileHome" component={ProfileHome} />
      <ProfileStack.Screen
        name="PersonalInformation"
        component={PersonalInformation}
      />
      {/* <ProfileStack.Screen name="EditProfile" component={EditProfile} /> */}
      <ProfileStack.Screen name="ChangePassword" component={ChangePassword} />
      <ProfileStack.Screen name="AboutBusiness" component={AboutBusiness} />
      <ProfileStack.Screen name="MoreBusiness" component={MoreBusiness} />
      <ProfileStack.Screen name="ShippingNav" component={ShippingNav} />
      <ProfileStack.Screen name="WorkingHoursNav" component={WorkingHoursNav} />
    </ProfileStack.Navigator>
  );
}

export default ProfileNav;

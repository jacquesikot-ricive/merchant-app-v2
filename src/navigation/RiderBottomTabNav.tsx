import { FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import theme from '../components/Themed';
import useColorScheme from '../hooks/useColorScheme';
import Home from '../screens/home/Home';
import CustomerIcon from '../svg/homeTab/CustomersIcon';
import HomeIcon from '../svg/homeTab/HomeIcon';
import MoreIcon from '../svg/homeTab/MoreIcon';
import OrdersIcon from '../svg/homeTab/OrdersIcon';
import OrderNav from './OrdersNav';
import TeamsNav from './TeamsNav';
import MoreNav from './MoreNav';
import Deliveries from '../screens/profile/Deliveries';
import RiderOrderNav from './RiderOrderNav';

export type RiderBottomTabParamList = {
  Home: undefined;
  Orders: undefined;
  Customers: undefined;
};

const BottomTab = createBottomTabNavigator<RiderBottomTabParamList>();

const RiderBottomTabNav = () => {
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: theme.colors.primary1,
        headerShown: false,
      }}
    >
      <BottomTab.Screen
        name="Home"
        component={Home}
        options={() => ({
          title: 'Home',
          tabBarIcon: ({ color }) => <HomeIcon color={color} />,
        })}
      />
      <BottomTab.Screen
        name="Orders"
        component={RiderOrderNav}
        options={{
          title: 'Orders',
          tabBarIcon: ({ color }) => <OrdersIcon color={color} />,
        }}
      />

      <BottomTab.Screen
        name="Customers"
        component={Home}
        options={{
          title: 'Customers',
          tabBarIcon: ({ color }) => <CustomerIcon color={color} />,
        }}
      />

      {/* <BottomTab.Screen
        name="More"
        component={MoreNav}
        options={{
          title: 'More',
          tabBarIcon: ({ color }) => <MoreIcon color={color} />,
        }}
      /> */}
    </BottomTab.Navigator>
  );
};

function TabBarIcon(props: {
  name: React.ComponentProps<typeof FontAwesome>['name'];
  color: string;
}) {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}

export default RiderBottomTabNav;

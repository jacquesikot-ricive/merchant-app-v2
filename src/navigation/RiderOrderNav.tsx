import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';

import OrderDetails from '../screens/order/OrderDetails';
import Deliveries from '../screens/profile/Deliveries';

export type RiderOrderNavParaList = {
  Deliveries: undefined;
  DeliveryDetail: {
    id: number;
  };
};

const Stack = createNativeStackNavigator<RiderOrderNavParaList>();

function RiderOrderNav(): JSX.Element {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Deliveries" component={Deliveries} />
      <Stack.Screen name="DeliveryDetail" component={OrderDetails} />
    </Stack.Navigator>
  );
}

export default RiderOrderNav;

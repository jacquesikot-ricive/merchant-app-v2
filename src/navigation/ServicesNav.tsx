import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Services from '../screens/Services';
import AddService from '../screens/AddService';

export type ServicesNavParamList = {
  Services: undefined;
  AddService: {
    isEdit: boolean;
    name?: string;
    description?: string;
    id?: string;
  };
};

const ServicesStack = createNativeStackNavigator<ServicesNavParamList>();

function ServicesNav(): JSX.Element {
  return (
    <ServicesStack.Navigator screenOptions={{ headerShown: false }}>
      <ServicesStack.Screen name="Services" component={Services} />
      <ServicesStack.Screen name="AddService" component={AddService} />
    </ServicesStack.Navigator>
  );
}

export default ServicesNav;

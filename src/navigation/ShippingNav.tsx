import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Shipping from '../screens/profile/Shipping';
import FAQ from '../screens/FAQ';
import EditShipping from '../screens/profile/EditShipping';

export type ShippingNavParamList = {
  Shipping: {
    reload?: boolean;
  };
  EditShipping: {
    id: string;
    name: string;
    message: string;
    cost: string;
  };
};

const SupportStack = createNativeStackNavigator<ShippingNavParamList>();

function ShippingNav(): JSX.Element {
  return (
    <SupportStack.Navigator screenOptions={{ headerShown: false }}>
      <SupportStack.Screen name="Shipping" component={Shipping} />
      <SupportStack.Screen name="EditShipping" component={EditShipping} />
    </SupportStack.Navigator>
  );
}

export default ShippingNav;

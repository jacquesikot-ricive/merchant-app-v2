import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import StoreFront from '../screens/storefront/StoreFront';
import EditStorefront from '../screens/storefront/EditStorefront';
import GettingStartedNav from './GettingStartedNav';

export type StoreFrontNavParamList = {
  StoreFront: undefined;
  EditStorefront: {
    isEdit: boolean;
    domain?: string;
    tagline?: string;
    id?: string;
    color?: string;
    logo?: string;
    facebook_url?: string;
    twitter_url?: string;
    instagram_url?: string;
  };
  GettingStartedStorefront: undefined;
};

const StoreFrontStack = createNativeStackNavigator<StoreFrontNavParamList>();

function StoreFrontNav(): JSX.Element {
  return (
    <StoreFrontStack.Navigator screenOptions={{ headerShown: false }}>
      <StoreFrontStack.Screen name="StoreFront" component={StoreFront} />
      <StoreFrontStack.Screen
        name="EditStorefront"
        component={EditStorefront}
      />
      <StoreFrontStack.Screen
        name="GettingStartedStorefront"
        component={GettingStartedNav}
      />
    </StoreFrontStack.Navigator>
  );
}

export default StoreFrontNav;

import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Support from '../screens/Support';
import FAQ from '../screens/FAQ';

export type SupportNavParamList = {
  Support: {
    isAuth?: boolean;
  };
  FAQ: undefined;
};

const SupportStack = createNativeStackNavigator<SupportNavParamList>();

function SupportNav(): JSX.Element {
  return (
    <SupportStack.Navigator screenOptions={{ headerShown: false }}>
      <SupportStack.Screen name="Support" component={Support} />
      <SupportStack.Screen name="FAQ" component={FAQ} />
    </SupportStack.Navigator>
  );
}

export default SupportNav;

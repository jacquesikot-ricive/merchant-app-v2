import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import AddMember from '../screens/team/AddMember';
import Teams from '../screens/team/Teams';

export type TeamsNavParamList = {
  Teams: undefined;
  AddMember: {
    isEdit: boolean;
    first_name?: string;
    last_name?: string;
    username?: string;
    role?: string;
    id?: string;
    permissions?: any;
  };
};

const TeamsStack = createNativeStackNavigator<TeamsNavParamList>();

function TeamsNav(): JSX.Element {
  return (
    <TeamsStack.Navigator screenOptions={{ headerShown: false }}>
      <TeamsStack.Screen name="Teams" component={Teams} />
      <TeamsStack.Screen name="AddMember" component={AddMember} />
    </TeamsStack.Navigator>
  );
}

export default TeamsNav;

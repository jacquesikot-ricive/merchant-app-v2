import * as React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

// import AddMember from '../screens/AddMember';
// import Wallet from '../screens/wallet/Wallet';
import TransactionPin from "../screens/wallet/TransactionPin";
import TransactionSuccess from "../screens/wallet/TransactionSuccess";
import Withdraw from "../screens/wallet/Withdraw";
import WithdrawFunds from "../screens/wallet/WithdrawFunds";
import Wallet from "../screens/wallet/Wallet";
import TransactionDetails from "../screens/wallet/TransactionDetails";

export type WalletNavParamList = {
  Wallet: undefined;
  TransactionPin: {
    isCreate: boolean;
  };
  TransactionSuccess: undefined;
  Withdraw: undefined;
  WithdrawFunds: {
    amount: string;
    accountNumber: string;
    accountName: string;
    bankCode: string;
    selectedBank: string;
    pin: string;
    reason: string;
  };
  TransactionDetails: {
    item: {};
  };
};

const WalletStack = createNativeStackNavigator<WalletNavParamList>();

function WalletNav(): JSX.Element {
  return (
    <WalletStack.Navigator screenOptions={{ headerShown: false }}>
      <WalletStack.Screen name="Wallet" component={Wallet} />
      <WalletStack.Screen name="TransactionPin" component={TransactionPin} />
      <WalletStack.Screen
        name="TransactionSuccess"
        component={TransactionSuccess}
      />
      <WalletStack.Screen
        name="TransactionDetails"
        component={TransactionDetails}
      />
      <WalletStack.Screen name="Withdraw" component={Withdraw} />
      <WalletStack.Screen name="WithdrawFunds" component={WithdrawFunds} />
    </WalletStack.Navigator>
  );
}

export default WalletNav;

import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import WorkingHours from '../screens/profile/WorkingHours';

export type WorkingHoursNavParamList = {
  WorkingHours: undefined;
};
const SupportStack = createNativeStackNavigator<WorkingHoursNavParamList>();

function WorkingHoursNav(): JSX.Element {
  return (
    <SupportStack.Navigator screenOptions={{ headerShown: false }}>
      <SupportStack.Screen name="WorkingHours" component={WorkingHours} />
    </SupportStack.Navigator>
  );
}

export default WorkingHoursNav;

/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import {
  CompositeScreenProps,
  NavigationContainer,
} from '@react-navigation/native';
import {
  createNativeStackNavigator,
  NativeStackScreenProps,
} from '@react-navigation/native-stack';
import React, { useState } from 'react';
import AppLoading from 'expo-app-loading';

import NotFoundScreen from '../screens/NotFoundScreen';
import BottomTabNav, { BottomTabParamList } from './BottomTabNav';
import LinkingConfiguration from './LinkingConfiguration';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import AuthNav from './AuthNav';
import storage from '../utils/storage';
import storageKeys from '../constants/storageKeys';
import { setIsLoggedIn, setUser } from '../redux/login';
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { OrderNavParaList } from './OrdersNav';
import { MoreNavParamList } from './MoreNav';
import DrawerNav from './DrawerNav';
import { TeamsNavParamList } from './TeamsNav';
import useNotifications from '../hooks/useNotifications';
import { CustomerNavParamList } from './CustomerNav';
import authApi from '../api/riciveApi/auth';
import useAuth from '../auth/useAuth';
import LogoutIcon from '../svg/LogoutIcon';
import orderApi from '../api/riciveApi/order';
import { setNewOrders } from '../redux/app';
import RiderBottomTabNav from './RiderBottomTabNav';

type RootStackParamList = {
  MainNav: undefined;
  NotFound: undefined;
};

export type Props = CompositeScreenProps<
  NativeStackScreenProps<RootStackParamList, 'MainNav'>,
  CompositeScreenProps<
    BottomTabScreenProps<BottomTabParamList>,
    CompositeScreenProps<
      NativeStackScreenProps<OrderNavParaList>,
      CompositeScreenProps<
        NativeStackScreenProps<MoreNavParamList>,
        NativeStackScreenProps<TeamsNavParamList>
      >
    >
  >
>;

export default function Navigation() {
  // useNotifications();
  const dispatch = useAppDispatch();
  const [isReady, setIsReady] = useState<boolean>(false);
  const auth = useAuth();

  const restoreUser = async () => {
    const userData = await storage.getData(storageKeys.USER);

    // // Fetch counts
    // const ordersData = await orderApi.getOrders();

    // const newOrders =
    //   ordersData &&
    //   ordersData.data
    //     .filter((order: any) => order.is_accepted === false)
    //     .sort((a: any, b: any) => {
    //       return (
    //         new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
    //       );
    //     });

    // dispatch(setNewOrders(newOrders.length.toString()));

    if (userData) {
      await auth.login(userData);
    } else {
      await auth.logout();
    }

    // if (userData) {
    //   const isAllowedToLogin = await authApi.validateLogin({
    //     refresh_token: userData.refresh_token,
    //     user_id: userData.auth.id,
    //   });

    //   if (isAllowedToLogin.data.status !== 'success') {
    //     await auth.logout();
    //   } else {
    //     await auth.login(userData);
    //   }
    // } else {
    //   await auth.logout();
    // }
  };

  const isAuthenticated = useAppSelector((state) => state.login.isLoggedIn);

  if (!isReady)
    return (
      <AppLoading
        startAsync={restoreUser}
        onFinish={() => setIsReady(true)}
        onError={console.warn}
      />
    );
  return (
    <NavigationContainer linking={LinkingConfiguration}>
      {isAuthenticated ? <RootNavigator /> : <AuthNav />}
    </NavigationContainer>
  );
}

const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  const role = useAppSelector((state) => state.login.user.profile.role);
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="MainNav"
        component={role === 'DELIVERY' ? RiderBottomTabNav : BottomTabNav}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="NotFound"
        component={NotFoundScreen}
        options={{ title: 'Oops!' }}
      />
    </Stack.Navigator>
  );
}

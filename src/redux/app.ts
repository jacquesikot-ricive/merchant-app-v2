import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import notificationApi from '../api/riciveApi/notification';
import storage from '../utils/storage';

export interface UserSignInResponse {
  auth: any;
  business: any;
  profile: any;
  refresh_token: string;
  access_token: string;
}

interface NewToken {
  token: string;
  user_id: string;
}

const createToken = async (action: any) => {
  console.log(action);
  try {
    const tokenData = await notificationApi.newExpoToken({
      token: action.payload.token,
      user: action.payload.user_id,
    });
    await storage.storeData('EXPO_TOKEN', tokenData.data);
  } catch (error) {
    console.log('createTokenError', error);
  }
};

const deleteToken = async () => {
  try {
    const token = await storage.getData('EXPO_TOKEN');
    if (token) {
      await notificationApi.deleteExpoToken(token.id);
    }
  } catch (error) {
    console.log('delete token error:', error);
  }
};

const appSlice = createSlice({
  name: 'login',
  initialState: {
    newOrders: '0',
  },
  reducers: {
    setNewOrders(state, action: PayloadAction<string>) {
      state.newOrders = action.payload;
    },
    createNotificationToken(state, action: PayloadAction<NewToken>) {
      createToken(action);
    },
    deleteNotificationToken(state, action: PayloadAction<NewToken>) {
      deleteToken();
    },
  },
});

export const {
  setNewOrders,
  createNotificationToken,
  deleteNotificationToken,
} = appSlice.actions;
export default appSlice.reducer;

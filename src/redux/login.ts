import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface UserSignInResponse {
  auth: any;
  business: any;
  profile: any;
  permissions: any;
  refresh_token: string;
  access_token: string;
}

const loginSlice = createSlice({
  name: 'login',
  initialState: {
    isLoggedIn: false,
    user: {
      auth: {
        id: '',
      },
      business: {
        id: '',
        phone: '',
        business_name: '',
        address: '',
        country: '',
        city: '',
        type: '',
        state: '',
        currency: '',
      },
      profile: {
        id: '',
        first_name: '',
        last_name: '',
        email: '',
        role: '',
        image: '',
      },
      permissions: {
        id: '',
        user: '',
        create_order: null,
        view_order: null,
        update_order_status: null,
        create_invoice: null,
        view_analytics: null,
        view_product: null,
        create_product: null,
        edit_product: null,
        delete_product: null,
        view_category: null,
        create_category: null,
        edit_category: null,
        delete_category: null,
        view_customer: null,
        edit_customer: null,
        create_customer: null,
        delete_customer: null,
        view_team: null,
        create_team: null,
        edit_team: null,
        delete_team: null,
        view_wallet: null,
        withdraw: null,
        storefront: null,
        business_info: null,
        view_invoice: null,
        edit_invoice: null,
        delete_invoice: null,
        created_at: null,
        updated_at: null,
      },
      refresh_token: '',
      access_token: '',
    },
  },
  reducers: {
    // SignUpProps should be response from signin api call
    setUser(state, action: PayloadAction<UserSignInResponse>) {
      state.user = action.payload;
    },
    setIsLoggedIn(state, action: PayloadAction<boolean>) {
      state.isLoggedIn = action.payload;
    },
  },
});

export const { setUser, setIsLoggedIn } = loginSlice.actions;
export default loginSlice.reducer;

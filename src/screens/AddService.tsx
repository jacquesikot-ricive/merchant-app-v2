import React, { useRef, useState } from 'react';
import {
  Keyboard,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Toast from 'react-native-toast-message';
import { useAnalytics } from '@segment/analytics-react-native';

import theme, { Box, Text } from '../components/Themed';
import TextInput from '../components/TextInput';
import { useAppSelector } from '../redux/hooks';
import Button from '../components/Button';
import StackHeader from '../components/StackHeader';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ServicesNavParamList } from '../navigation/ServicesNav';
import serviceApi from '../api/riciveApi/service';

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    width: '100%',
    height: '100%',
  },
  content: {
    marginTop: 20,
    marginHorizontal: 20,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  button: {
    marginTop: 50,
    alignSelf: 'center',
  },
});

type Props = NativeStackScreenProps<ServicesNavParamList, 'AddService'>;

const AddService = ({ navigation, route }: Props) => {
  const {
    isEdit,
    name: editName,
    description: editDescription,
    id: serviceId,
  } = route.params;
  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const userId = useAppSelector((state) => state.login.user.profile.id);
  const userBusinessId = useAppSelector(
    (state) => state.login.user.business.id
  );
  const { track } = useAnalytics();

  const handleAddNewService = async () => {
    setLoading(true);
    try {
      if (name.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Name cannot be empty',
          visibilityTime: 3000,
        });
      }

      await serviceApi.createService({
        name,
        description,
        business: userBusinessId,
      });
      setLoading(false);
      track('AddService', {
        business: userBusinessId
      })
      Toast.show({
        type: 'success',
        text1: 'Service Added',
        text2: `${name} has been added successfully`,
      });
      return navigation.navigate('Services');
    } catch (error: any) {
      console.log(JSON.stringify(error.response.data));
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We cannot process your request now, please try again later..',
      });
    }
  };

  const handleUpdateService = async () => {
    try {
      setLoading(true);
      const updateData = {
        name: name.length > 0 ? name : (editName as string),
        description: description.length > 0 ? description : editDescription,
        business: userBusinessId,
        id: serviceId,
      };

      await serviceApi.updateService(updateData);

      setLoading(false);
      Toast.show({
        type: 'success',
        text1: 'Service Updated',
        text2: `${name} has been updated successfully`,
      });
      return navigation.navigate('Services');
    } catch (error) {
      console.log(error);
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We cannot process your request now, please try again later..',
      });
    }
  };

  return (
    <ScrollView
      bounces={false}
      style={styles.container}
      contentContainerStyle={{ alignItems: 'center' }}
    >
      <StackHeader
        title={isEdit ? 'Update Service' : 'Add New Service'}
        onBackPress={() => navigation.goBack()}
      />
      <Box style={styles.content}>
        <Text variant={'DetailsR'} color="text6" mt="m" mb="s">
          Name
        </Text>

        <TextInput
          placeholder={isEdit && editName ? editName : 'Name of service'}
          type={'input'}
          onChangeText={(e) => setName(e)}
          keyboardType="default"
          autoCompleteType="name"
          autoCapitalize="words"
          enablesReturnKeyAutomatically
          returnKeyType="next"
          autoCorrect={false}
        />

        <Text mt="l" mb="xxl" variant={'DetailsR'} color="text6">
          Description
        </Text>
        <TextInput
          placeholder={
            isEdit && editDescription ? editDescription : 'Service description'
          }
          type={'description'}
          onChangeText={(e) => setDescription(e)}
          keyboardType="default"
          autoCompleteType="name"
          autoCorrect={false}
          multiline
          onKeyPress={(e) => {
            const key = e.nativeEvent.key;
            if (key === 'Enter') {
              Keyboard.dismiss();
            }
          }}
        />
      </Box>
      <Box style={styles.button}>
        <Button
          disabled={
            isEdit
              ? name.length > 0 || description.length > 0
                ? false
                : true
              : name.length > 0 && description.length > 0
              ? false
              : true
          }
          loading={loading}
          type="primary"
          label={isEdit ? 'Update Service' : 'Add service'}
          onPress={isEdit ? handleUpdateService : handleAddNewService}
        />
      </Box>
    </ScrollView>
  );
};

export default AddService;

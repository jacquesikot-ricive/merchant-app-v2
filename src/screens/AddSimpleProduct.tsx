import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { FC, useState } from 'react';
import {
  View,
  SafeAreaView,
  Image,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import PlusIcon from '../../svgs/PlusIcon';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import ImageResizer from 'react-native-image-resizer';
import theme, { Box, Text } from '../components/Themed';
import Button from '../components/Button';
import TextInput from '../components/TextInput';
import queryKeys from '../constants/queryKeys';
import categoryApi from '../api/riciveApi/category';
import productApi from '../api/riciveApi/product';
import { useAppSelector } from '../redux/hooks';
import { useAnalytics } from '@segment/analytics-react-native';
import Checkbox from '../components/Checkbox';
import useCurrency from '../hooks/useCurrency';
import * as ImagePicker from 'expo-image-picker';
import CurrencyTextInput from '../components/CurrencyTextInput';
import ImagePickerModal from '../components/ImagePickerModal';
import uploadApi from '../api/riciveApi/upload';
import SelectCategoryModal from '../components/SelectCategoryModal';
import { useQuery } from 'react-query';
import { ProductsNavParamList } from '../navigation/ProductsNav';
import StackHeader from '../components/StackHeader';

const styles = StyleSheet.create({
  main: {
    backgroundColor: theme.colors.white,
    flex: 1,
  },
  container: {
    paddingHorizontal: 20,
  },
  button: {
    marginTop: 20,
  },
  checkbox: {
    flexDirection: 'row',
    width: wp(90),
    alignItems: 'center',
    marginTop: 20,
  },
  image: {
    width: wp(90),
    alignItems: 'center',
    marginTop: 10,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 20,
    borderColor: theme.colors.border1,
  },
  categoryPicker: {
    width: wp(90),
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: theme.colors.border1,
    marginTop: 10,
  },
});

type Props = NativeStackScreenProps<ProductsNavParamList, 'SimpleAdd'>;

const AddSimpleProduct = ({ navigation }: Props) => {
  const { track } = useAnalytics();
  const { returnCurrency } = useCurrency();
  const [name, setName] = useState<string>('');
  const [price, setPrice] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [publishProduct, setPublishProduct] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [image, setImage] = useState<string>('');
  const [serverImage, setServerImage] = useState<string>('');
  const [openCategorySelect, setOpenCategorySelect] = useState<boolean>(false);
  const [categories, setCategories] = useState<any[]>([]);
  const [selectedCategories, setSelectedCategires] = useState<any[]>([]);

  const {
    isLoading: isCategoryLoading,
    isError: isCategoryError,
    refetch: refetchCategory,
    isFetching: isFetchingCategory,
  } = useQuery(
    queryKeys.CATEGORY_DATA,
    async () => {
      try {
        const categoryData = await categoryApi.getCategories(business_id);
        setCategories(categoryData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2:
            'Sorry we cannot fetch categories now, please try again later...',
        });
        console.log(
          'Error getting categories:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const handleCreateProduct = async () => {
    try {
      setLoading(true);
      const imageUrl =
        image.length > 0
          ? await uploadApi.upload({
              file: image,
            })
          : undefined;

      await productApi.addProduct({
        name,
        price,
        business: business_id,
        image: image.length > 0 ? imageUrl.data.url : undefined,
        category:
          selectedCategories && selectedCategories.length > 0
            ? selectedCategories.map((p: any) => p.id)
            : undefined,
        is_published: publishProduct,
        stock_quantity: '1',
        price_type: 'FIXED',
        low_stock_level: '2',
      });
      track('Product Created From Simple Add Screen', {
        business_id,
      });

      setLoading(false);
      console.log('product created successfully');
      navigation.navigate('Products');
    } catch (error: any) {
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'Cannot add new product now, please try again later..',
      });
      console.log(JSON.stringify(error.response.data));
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      quality: 0.8,
      base64: true,
    });

    if (!result.cancelled) {
      const res = await ImageResizer.createResizedImage(
        result.uri,
        800,
        500,
        'JPEG',
        100
      );
      setImage(res.uri);
    }
  };

  return (
    <Box style={styles.main}>
      <StackHeader
        title={'Add New Product'}
        onBackPress={() => navigation.goBack()}
      />

      <ScrollView>
        <Box style={styles.container}>
          <Box>
            <Box mb="m">
              <Text variant={'DetailsR'} color="text6" mt="m" mb="m">
                Name
              </Text>
              <TextInput
                placeholder={'Product Name e.g Shirt'}
                type={'input'}
                width={wp(90)}
                onChangeText={(e) => setName(e)}
                keyboardType="default"
                autoCompleteType="name"
                autoCapitalize="words"
                returnKeyType="done"
                autoCorrect={false}
              />
            </Box>
            <Box>
              <Text variant={'DetailsR'} color="text6" mt="m" mb="s">
                Price
              </Text>

              <CurrencyTextInput
                width={wp(90)}
                value={price}
                onChangeValue={(t: string) => setPrice(t.toString())}
                placeholder={`Product Sale Price in ${returnCurrency().name}`}
              />
            </Box>
            <Box style={{ marginBottom: 10 }}>
              <Text
                mt="l"
                variant={'DetailsR'}
                color="text6"
                style={{ width: wp(90) }}
              >
                Add product image (optional)
              </Text>

              <Box style={styles.image}>
                {image.length > 0 || serverImage.length > 0 ? (
                  <>
                    <TouchableOpacity
                      style={{
                        padding: 5,
                      }}
                      onPress={() => {
                        setImage('');
                        setServerImage('');
                      }}
                    >
                      <Icon
                        name="trash"
                        color={theme.colors.border}
                        size={24}
                      />
                    </TouchableOpacity>

                    <Box
                      style={{
                        position: 'absolute',
                        left: 20,
                        top: 15,
                      }}
                    >
                      <Image
                        source={{
                          uri: image.length > 0 ? image : serverImage,
                        }}
                        style={{
                          width: 90,
                          height: 60,
                          borderRadius: 6,
                        }}
                      />
                    </Box>

                    <Text variant="DetailsM" color="text2">
                      Remove image
                    </Text>
                  </>
                ) : (
                  <TouchableOpacity
                    onPress={() => pickImage()}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Box>
                      <PlusIcon />
                    </Box>
                  </TouchableOpacity>
                )}
              </Box>
            </Box>

            <Text
              mt="s"
              variant={'DetailsR'}
              color="text6"
              style={{ width: wp(90), textAlign: 'left' }}
            >
              Product categories
            </Text>

            <TouchableOpacity
              style={styles.categoryPicker}
              onPress={() => setOpenCategorySelect(true)}
              activeOpacity={0.6}
            >
              {selectedCategories.length > 0 ? (
                <Box
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                >
                  <Box>
                    {selectedCategories.map((s: any, i: number) => (
                      <Text numberOfLines={1}>
                        {`${s.name}${
                          i === selectedCategories.length - 1 ? '' : ', '
                        }`}
                      </Text>
                    ))}
                  </Box>

                  <Box
                    style={{
                      position: 'absolute',
                      right: 0,
                    }}
                  >
                    <Icon
                      name="chevron-right"
                      size={24}
                      color={theme.colors.border}
                    />
                  </Box>
                </Box>
              ) : (
                <Box
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <Text variant={'DetailsR'} color="text5">
                    Select Product Categories
                  </Text>
                  <Icon
                    name="chevron-right"
                    size={24}
                    color={theme.colors.border}
                  />
                </Box>
              )}
            </TouchableOpacity>
            {/* <Box marginTop="l">
                <Box>
                  <Text variant={'DetailsR'} color="text6">
                    Stock quantity
                  </Text>
                </Box>
                <Box mt="m">
                  <TextInput
                    placeholder="Qty in stock, defaults to 1"
                    type="number"
                    height={58}
                    autoCorrect={false}
                    keyboardType="number-pad"
                    returnKeyType="next"
                    onChangeText={(e) => setStockQuantity(e)}
                    value={stockQuantity}
                  />
                </Box>
              </Box> */}
            <TouchableOpacity
              style={styles.checkbox}
              onPress={() => {
                setPublishProduct(!publishProduct);
              }}
            >
              <Checkbox checked={publishProduct} />

              <Text variant={'DetailsR'} color="text6" ml="l">
                Show on website
              </Text>
            </TouchableOpacity>

            <Box style={styles.button}>
              <Button
                disabled={name.length > 0 && price.length > 0 ? false : true}
                type="primary"
                label={'Add product'}
                loading={loading}
                onPress={handleCreateProduct}
                width={wp(90)}
              />
            </Box>
          </Box>
          <Box marginTop="xxxl" />
        </Box>
      </ScrollView>
      <SelectCategoryModal
        categories={categories}
        selectedCategories={selectedCategories}
        setSelectedCategories={setSelectedCategires}
        setVisible={setOpenCategorySelect}
        visible={openCategorySelect}
        getCategories={refetchCategory}
      />
    </Box>
  );
};

export default AddSimpleProduct;

import React, { useState } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  StyleSheet,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Toast from 'react-native-toast-message';

import Button from '../components/Button';
import StackHeader from '../components/StackHeader';
import TextInput from '../components/TextInput';
import theme, { Box, Text } from '../components/Themed';
import { ProductsNavParamList } from '../navigation/ProductsNav';
import categoryApi from '../api/riciveApi/category';
import { useAppSelector } from '../redux/hooks';
import { widthPercentageToDP } from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
});
type Props = NativeStackScreenProps<ProductsNavParamList, 'EditCategory'>;

const EditCategory = ({ navigation, route }: Props) => {
  const { name, id } = route.params;
  const [loading, setLoading] = useState<boolean>(false);
  const [deleteLoading, setDeleteLoading] = useState<boolean>(false);
  const [newName, setNewName] = useState<string>(name);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );

  const handleUpdateCategory = async () => {
    if (newName.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Category Name',
        text2: 'Name must not be empty, enter name to continue',
      });
    }

    try {
      setLoading(true);

      await categoryApi.editCategory({
        name: newName,
        id,
      });
      setLoading(false);
      navigation.goBack();
      Toast.show({
        type: 'success',
        text1: 'Caetgory Updated',
        text2: 'Category name has been changed!',
      });
    } catch (error) {
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot update data, please try again later...',
      });
    }
  };

  const handleDeleteCategory = () => {
    Alert.alert(
      'Delet Category',
      'Are you sure you want to delete the category?',
      [
        {
          style: 'default',
          text: 'No',
        },
        {
          style: 'destructive',
          text: 'Yes',
          onPress: async () => {
            try {
              setDeleteLoading(true);
              await categoryApi.deleteCategory(id);
              setDeleteLoading(false);
              navigation.goBack();
              Toast.show({
                type: 'success',
                text1: 'Caetgory Deleted',
                text2: 'Category has been deleted!',
              });
            } catch (error: any) {
              console.log(JSON.stringify(error.response.data));
              setDeleteLoading(false);
              Toast.show({
                type: 'error',
                text1: 'Server Error',
                text2: 'Cannot get data, please try again later...',
              });
            }
          },
        },
      ]
    );
  };
  return (
    <Box style={styles.container}>
      <StackHeader
        title="Edit Category"
        onBackPress={() => navigation.goBack()}
      />

      <Box
        style={{
          marginTop: 20,
        }}
      >
        <Text
          variant={'DetailsR'}
          color="text1"
          style={{ width: widthPercentageToDP(90), marginBottom: 15 }}
        >
          Category Name
        </Text>

        <TextInput
          onChangeText={(e) => {
            setNewName(e);
          }}
          type="input"
          autoCompleteType="name"
          autoCorrect={false}
          autoCapitalize="words"
          keyboardType="default"
          returnKeyType="next"
          placeholder="Edit Category Name"
          value={newName}
        />

        {userPermissions.edit_category && (
          <Box style={{ marginTop: 20 }}>
            <Button
              type="primary"
              label="Update Category"
              onPress={handleUpdateCategory}
              loading={loading}
            />
          </Box>
        )}

        {userPermissions.delete_category && (
          <TouchableOpacity
            onPress={handleDeleteCategory}
            activeOpacity={0.6}
            style={{
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 8,
              backgroundColor: theme.colors.error,
              height: 56,
            }}
          >
            {deleteLoading ? (
              <ActivityIndicator color={theme.colors.white} />
            ) : (
              <Text variant={'Body2M'} color="white">
                Delete Category
              </Text>
            )}
          </TouchableOpacity>
        )}
      </Box>
    </Box>
  );
};

export default EditCategory;

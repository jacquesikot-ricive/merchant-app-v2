import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { LayoutAnimation, StyleSheet, TouchableOpacity } from 'react-native';
import FAQAccordion from '../components/FAQAccordion';
import FAQHeader from '../components/FAQHeader';
import StackHeader from '../components/StackHeader';
import theme, { Box } from '../components/Themed';
import { SupportNavParamList } from '../navigation/SupportNav';


// import { Container } from './styles';
type Props = NativeStackScreenProps<SupportNavParamList, 'FAQ'>;

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.colors.bg1,
        height: 700,
      },
})

const FAQ = ({ navigation, route }: Props): JSX.Element => {
  const [activeItem, setActiveItem] = useState<'general' | 'account'>('general');



    return (
        <Box style={styles.container}>
        <Box>
          <StackHeader
            title="FAQs"
            onBackPress={() => navigation.goBack()}
          />
          <Box>
            <FAQHeader
              active={activeItem}
              handleSwitch={() => {
                setActiveItem(activeItem === 'general'?'account': 'general')
              }}
            />
            {activeItem === 'general' && (
              <Box>
                <FAQAccordion title="Is there a free trial available?"
                  description={`Yes you can try us for free for 30 days. if you want, we'll provide you with a free, personalized 30-minute ${'\n'} onboarding call to get up and running immediately.`}
                />
                <FAQAccordion title="Can I change my plan later?"
                  description={`Yes you can try us for free for 30 days. if you want, we'll provide you with a free, personalized 30-minute ${'\n'} onboarding call to get up and running immediately.`}
                />
                 <FAQAccordion title="What is your cancellation policy?"
                  description={`Yes you can try us for free for 30 days. if you want, we'll provide you with a free, personalized 30-minute ${'\n'} onboarding call to get up and running immediately.`}
                />
                <FAQAccordion title="Can other info be added to an invoice?"
                  description={`Yes you can try us for free for 30 days. if you want, we'll provide you with a free, personalized 30-minute ${'\n'} onboarding call to get up and running immediately.`}
                />
                <FAQAccordion title="How do I change my account email?"
                  description={`Yes you can try us for free for 30 days. if you want, we'll provide you with a free, personalized 30-minute ${'\n'} onboarding call to get up and running immediately.`}
                />
                <FAQAccordion title="How does billing work?"
                  description={`Yes you can try us for free for 30 days. if you want, we'll provide you with a free, personalized 30-minute ${'\n'} onboarding call to get up and running immediately.`}
                />
              </Box>
            )}
          </Box>
        </Box>
      </Box>
  );
}

export default FAQ;
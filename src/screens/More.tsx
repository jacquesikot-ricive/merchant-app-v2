import React, { useState } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import theme, { Box, Text } from '../components/Themed';
import { Props } from '../navigation';
import StackHeader from '../components/StackHeader';
import TeamsIcon from '../../svgs/TeamsIcon';
import ArrowRightIcon from '../../svgs/ArrowRightIcon';
import WalletIcon from '../../svgs/WalletIcon';
import InvoiceIcon from '../svg/homeTab/InvoiceIcon';
import StoreFrontIcon from '../../svgs/StoreFrontIcon';
import AccountIcon from '../../svgs/AccountIcon';
import LogoutIcon from '../svg/LogoutIcon';
import MoreActions from '../components/MoreActions';
import useAuth from '../auth/useAuth';
import AppModal from '../components/AppModal';
import { useAppSelector } from '../redux/hooks';
import Intercom from '@intercom/intercom-react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    width: wp(90),
    justifyContent: 'space-between',
    paddingVertical: 20,
  },
  leftIcon: {
    width: 40,
    height: 40,
    backgroundColor: theme.colors.secondary2,
  },
  border: {
    width: wp(100),
    alignItems: 'center',
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 1,
  },

  modal: {
    marginTop: 60,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  modalDescription: {
    alignItems: 'center',
    textAlign: 'center',
    width: '85%',
    marginTop: 10,
  },
  modalText: {
    textAlign: 'center',
  },
  modalButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '80%',
    marginTop: 15,
  },
  cancelLogoutButton: {
    backgroundColor: theme.colors.bg11,
    borderRadius: 6,
    height: 36,
    width: 119,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoutButton: {
    backgroundColor: theme.colors.errorLight,
    borderRadius: 6,
    height: 36,
    width: 119,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const More = ({ navigation }: Props): JSX.Element => {
  const { logout } = useAuth();
  const [openDelete, setOpenDelete] = useState<boolean>(false);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );

  return (
    <Box style={styles.container}>
      <StackHeader title={''} />
      <Box style={styles.border} />
      <ScrollView>
        {userPermissions.view_team && (
          <MoreActions
            onPress={() => navigation.navigate('TeamsNav')}
            label="Teams"
            labelBottom="Add & Manage your Teams"
            iconType="teams"
          />
        )}
        {userPermissions.view_wallet && (
          <MoreActions
            onPress={() => navigation.navigate('WalletNav')}
            label="Wallet"
            labelBottom="Manage wallet"
            iconType="wallet"
          />
        )}
        {userPermissions.storefront && (
          <MoreActions
            onPress={() => navigation.navigate('StoreFrontNav')}
            label="Storefront"
            labelBottom="Storefront details"
            iconType="storefront"
          />
        )}
        <MoreActions
          onPress={() => navigation.navigate('ProfileNav')}
          label="Account & Settings"
          labelBottom="Personal info, shipping info..."
          iconType="account"
        />
        {userPermissions.view_invoice && (
          <MoreActions
            onPress={() => navigation.navigate('InvoiceNav')}
            label="Invoice"
            labelBottom="Manage your invoices"
            iconType="invoice"
          />
        )}
        <MoreActions
          onPress={() => Intercom.displayMessenger()}
          label="Support"
          labelBottom="Contact support"
          iconType="support"
        />

        {/* List Item - Deliveries */}
        {/* <TouchableOpacity
          activeOpacity={0.6}
          onPress={() => navigation.navigate('Deliveries')}
          style={styles.item}
        >
          <Box flexDirection={'row'} alignItems={'center'}>
            <TeamsIcon />
            <Text ml="xl" variant={'Body2M'}>
              Deliveries
            </Text>
          </Box>
          <Icon name="chevron-right" color={theme.colors.text7} size={24} />
        </TouchableOpacity> */}
        <MoreActions
          onPress={() => setOpenDelete(true)}
          label="LOG OUT"
          labelColor="logout"
        />
      </ScrollView>

      <AppModal
        visible={openDelete}
        setVisible={setOpenDelete}
        heightValue={243}
        content={
          <Box style={styles.modal}>
            <Text variant="Body2M" color="text1">
              Are you sure?
            </Text>
            <Box style={styles.modalDescription}>
              <Text style={styles.modalText} variant="DetailsR" color="text2">
                You are about to log out from your account
              </Text>
            </Box>
            <Box style={styles.modalButton}>
              <TouchableOpacity
                style={styles.cancelLogoutButton}
                onPress={() => setOpenDelete(false)}
              >
                <Text variant="SmallerTextR" color="text5">
                  Don't Log out
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.logoutButton}
                onPress={() => {
                  logout();
                  Intercom.logout();
                }}
              >
                <Text variant="SmallerTextR" color="error">
                  Yes, Log out
                </Text>
              </TouchableOpacity>
            </Box>
          </Box>
        }
      />
    </Box>
  );
};

export default More;

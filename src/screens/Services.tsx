import React, { useEffect, useState } from 'react';
import { Alert, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import theme, { Box, Text } from '../components/Themed';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { DrawerActions } from '@react-navigation/native';
import { Feather as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import HeaderTextInput from '../components/HeaderTextInput';
import ServiceList from '../components/ServicesList';
import StackHeader from '../components/StackHeader';
import { useAppSelector } from '../redux/hooks';
import ListEmpty from '../components/ListEmpty';
import ScreenLoading from '../components/ScreenLoading';
import ScreenError from '../components/ScreenError';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ServicesNavParamList } from '../navigation/ServicesNav';
import serviceApi from '../api/riciveApi/service';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  layout: {
    width: wp(90),
    marginTop: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  rightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    paddingRight: 5,
  },
  list: {
    height: hp(68),
    marginTop: 10,
  },
});

type Props = NativeStackScreenProps<ServicesNavParamList, 'Services'>;

const Services = ({ navigation, route }: Props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [services, setServices] = useState<any>();
  const [searchResult, setSearchResult] = useState<any[]>([]);
  const [error, setError] = useState<boolean>(false);

  const getServices = async () => {
    setError(false);
    try {
      setLoading(true);
      const data = await serviceApi.getServices(business_id);
      if (data.status === 'failed') {
        setLoading(false);
        setError(true);
        console.log('Error fetching services:', error);
        return Toast.show({
          type: 'error',
          text1: 'Error getting services',
          text2: data.message,
        });
      }
      setServices(data.data);
      setSearchResult(data.data || []);
      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      setError(true);
      console.log(
        'Error fetching services:',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot get services, please try again later...',
      });
    }
  };

  const handleDelete = (id: string) => {
    try {
      Alert.alert(
        'Delete Service',
        'Are you sure you want to delete this service?',
        [
          {
            style: 'destructive',
            text: 'yes',
            onPress: async () => {
              try {
                setLoading(true);
                await serviceApi.deleteService(id, 'SOFT');

                getServices();
                setLoading(false);
                return Toast.show({
                  type: 'success',
                  text1: 'Success',
                  text2: 'Service deleted successfully!',
                });
              } catch (error) {
                setLoading(false);
                setError(true);
                console.log('Error deleting service:', error);
                Toast.show({
                  type: 'error',
                  text1: 'Server Error',
                  text2: 'Cannot delete service now, please try again later...',
                });
              }
            },
          },
          {
            style: 'cancel',
            text: 'No',
          },
        ]
      );
    } catch (error) {
      setLoading(false);
      setError(true);
      console.log('Error fetching services:', error);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot get services, please try again later...',
      });
    }
  };

  useEffect(() => {
    getServices();

    navigation.addListener('focus', async () => await getServices());

    return () => {
      navigation.removeListener('focus', async () => await getServices());
    };
  }, []);

  const handleSearch = (e: string) => {
    const result = services.filter((s: any) =>
      s.name.trim().toLowerCase().includes(e.trim().toLowerCase())
    );

    setSearchResult([...result]);
  };

  if (loading) {
    return <ScreenLoading />;
  }

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => await getServices()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      <StackHeader
        burger={() => navigation.dispatch(DrawerActions.openDrawer())}
        title=""
      />
      <Box style={styles.layout}>
        <Box style={styles.header}>
          <Text variant="TitleM" color="text6">
            Services
          </Text>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('AddService', {
                isEdit: false,
              })
            }
            style={styles.rightHeader}
          >
            <Box style={styles.icon}>
              <Icon name="plus" color={theme.colors.primary1} size={18} />
            </Box>
            <Text variant="Body1M" color="primary1">
              Add service
            </Text>
          </TouchableOpacity>
        </Box>
        <Box>
          <HeaderTextInput
            type="search"
            placeholder="Search"
            onChangeText={handleSearch}
          />
        </Box>
        <Box style={styles.list}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={searchResult}
            ListEmptyComponent={() => (
              <ListEmpty
                topText="Get your business up and running by"
                bottomText="adding your services"
                buttonText="Add service"
                button
                onPressButton={() =>
                  navigation.navigate('AddService', { isEdit: false })
                }
              />
            )}
            keyExtractor={(item: any) => item.id.toString()}
            renderItem={({ item }) => (
              <Box>
                <ServiceList
                  name={item.name}
                  description={item.description}
                  handleDelete={() => handleDelete(item.id)}
                  handleEdit={() =>
                    navigation.navigate('AddService', {
                      isEdit: true,
                      name: item.name,
                      description: item.description,
                      id: item.id,
                    })
                  }
                />
              </Box>
            )}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default Services;

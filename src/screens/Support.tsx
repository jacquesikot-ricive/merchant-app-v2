import { DrawerActions } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import StackHeader from '../components/StackHeader';
import theme, { Box, Text } from '../components/Themed';
import * as Linking from 'expo-linking';

import { SupportNavParamList } from '../navigation/SupportNav';
import CallIcon from '../svg/CallIcon';
import MailIcon from '../svg/MailIcon';
import ChevronRight from '../../svgs/ChevronRight';
import ArrowRightIcon from '../../svgs/ArrowRightIcon';

type Props = NativeStackScreenProps<SupportNavParamList, 'Support'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.bg1,
    flex: 1,
  },
  layout: {
    backgroundColor: theme.colors.secondary6,
    flex: 1,
    width: wp(100),
    alignItems: 'center',
    borderTopRightRadius: 13,
    borderTopLeftRadius: 13,
  },
  body: {
    height: 120,
    marginTop: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  firstCard: {
    marginTop: 53,
    paddingHorizontal: 20,
  },
  secondCard: {
    marginTop: 32,
    paddingHorizontal: 20,
  },
  textBg: {
    backgroundColor: theme.colors.bg1,
    height: 68,
    width: 350,
    borderRadius: 8,
  },
  textCard: {
    paddingVertical: 10,
    paddingHorizontal: 22,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
const Support = ({ navigation, route }: Props): JSX.Element => {
  // const { isAuth } = route.params;
  return (
    <Box style={styles.container}>
      <Box>
        <StackHeader
          title="Support"
          // burger={
          //   !isAuth
          //     ? () => navigation.dispatch(DrawerActions.openDrawer())
          //     : undefined
          // }
          // onBackPress={isAuth ? () => navigation.goBack() : undefined}
          onBackPress={() => navigation.goBack()}
        />
      </Box>
      <Box />
      <Box style={styles.layout}>
        <Box style={styles.body}>
          <Box>
            <Text variant="SmallerTextM" color="primary1">
              SUPPORT
            </Text>
            <Text mt="s" mb="s" variant="Body2M" color="text1">
              Need to contact support?
            </Text>
            <Box style={{ width: 342 }}>
              <Text variant="SmallerTextR" color="text7">
                Our team of professionals are always ready to{'\n'}answer any
                question you may have
              </Text>
            </Box>
          </Box>
        </Box>
        <Box>
          <Box style={styles.firstCard}>
            <TouchableOpacity
              style={styles.textBg}
              onPress={() => Linking.openURL(`mailto:support@ricive.com`)}
            >
              <Box style={styles.textCard}>
                <Box>
                  <Text variant="SmallerTextR" color="text7">
                    Email
                  </Text>
                  <Text variant="DetailsR" color="text1">
                    support@ricive.com
                  </Text>
                </Box>
                <MailIcon />
              </Box>
            </TouchableOpacity>
          </Box>
          <Box style={styles.secondCard}>
            <TouchableOpacity
              style={styles.textBg}
              onPress={() => Linking.openURL(`tel:+2347061172646`)}
            >
              <Box style={styles.textCard}>
                <Box>
                  <Text variant="SmallerTextR" color="text7">
                    Call us
                  </Text>
                  <Text variant="DetailsR" color="text1">
                    +2347061172646
                  </Text>
                </Box>
                <CallIcon />
              </Box>
            </TouchableOpacity>
          </Box>
          {/* <Box style={styles.secondCard}>
            <TouchableOpacity
              style={styles.textBg}
              onPress={() => navigation.navigate('FAQ')}
            >
              <Box style={styles.textCard}>
                <Box>
                  <Text variant="SmallerTextR" color="text7">
                    FAQs
                  </Text>
                  <Text variant="DetailsR" color="text1">
                    You might have a question?
                  </Text>
                </Box>
                <ArrowRightIcon />
              </Box>
            </TouchableOpacity>
          </Box> */}
        </Box>
      </Box>
    </Box>
  );
};

export default Support;

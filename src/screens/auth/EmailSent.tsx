import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useRef, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../../components/Themed';
import { AuthNavParamList } from '../../navigation/AuthNav';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  layout: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 250,
  },
  verifyText: {
    marginBottom: 35,
    alignItems: 'center',
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, 'ForgotPassword'>;

const EmailSent = ({ navigation }: Props): JSX.Element => {
  const handleResend = () => {};

  return (
    <Box style={styles.container}>
      <Box style={styles.layout}>
        <Image
          source={require('../../assets/images/verifyemail.png')}
          style={{
            width: 129,
            height: 129,
            marginBottom: 35,
          }}
        />
        <Box style={{ marginBottom: 8 }}>
          <Text variant="Body1M" color="text1">
            Verify your email
          </Text>
        </Box>
        <Box style={styles.verifyText}>
          <Text variant="Body2R" color="text5">
            We've sent a verification link to your email
          </Text>
          <Text variant="Body2R" color="text5">
            address.
          </Text>
        </Box>
        {/* <Box
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            // top: 10,
            padding: 10,
            bottom: 0,
          }}
        >
          <Text variant="Body2R" color="text5" style={{ paddingRight: 10 }}>
            Haven't received any email?
          </Text>
          <TouchableOpacity onPress={handleResend}>
            <Text variant="Body2M" color="primary1">
              Resend
            </Text>
          </TouchableOpacity>
        </Box> */}

        <Box
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            // top: 10,
            // padding: 10,
            // bottom: 0,
          }}
        >
          <Text variant="Body2R" color="text5">
            Ready to login?
          </Text>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('SignIn', {
                token: undefined,
              })
            }
          >
            <Text variant="Body2M" color="primary1" ml="s">
              Login
            </Text>
          </TouchableOpacity>
        </Box>
      </Box>
    </Box>
  );
};

export default EmailSent;

import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useRef, useState } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

import theme, { Box, Text } from "../../components/Themed";
import Button from "../../components/Button";
import Checkbox from "../../components/Checkbox";
import TextInput from "../../components/TextInput";
import { AuthNavParamList } from "../../navigation/AuthNav";
import StackHeader from "../../components/StackHeader";
import auth from "../../api/riciveApi/auth";
import Toast from "react-native-toast-message";
import useAuth from "../../auth/useAuth";
import validateEmail from "../../utils/validateEmail";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  title: {
    textAlign: "center",
    alignItems: "center",
    marginBottom: 30,
  },
  form: {
    marginBottom: 30,
    alignItems: "center",
  },
  baseText: {
    // marginTop: 20,
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, "ForgotPassword">;

const ForgotPassword = ({ navigation }: Props): JSX.Element => {
  const [email, setEmail] = useState<string>("");
  const [email_error, setEmailError] = useState<string>();
  const [loading, setLoading] = useState<boolean>(false);

  const handleSignIn = () => {
    navigation.navigate("SignIn", {
      token: undefined,
    });
  };

  const forgotPasswordApi = auth.forgotPassword;

  const Emailvalidate = () => {
    email.length < 1
      ? setEmailError("feild cannot be empty")
      : validateEmail(email) == false
      ? setEmailError("please enter a valid email")
      : setEmailError(undefined);
  };

  const handleForgotPassword = async () => {
    try {
      setLoading(true);

      if (email && email_error === undefined) {
        const result = await forgotPasswordApi({
          email,
        });

        setLoading(false);
        Toast.show({
          type: "success",
          text1: "Email Sent",
          text2: "Password reset successfull",
          autoHide: true,
          position: "top",
          visibilityTime: 3000,
        });

        const res = result as any;
      }
    } catch (error) {
      setLoading(false);
      console.log("Error Changing Password:", error);
      Toast.show({
        type: "error",
        text1: "Server Error",
        text2: "Process failed! Account does not exist",
        visibilityTime: 3000,
      });
    }
  };

  return (
    <ScrollView
      bounces={false}
      style={styles.container}
      contentContainerStyle={{ alignItems: "center", justifyContent: "center" }}
    >
      <StackHeader
        title="Forgot your password?"
        onBackPress={() => navigation.goBack()}
      />
      <>
        <Box style={{ paddingTop: 30, width: wp(90), alignItems: "center" }}>
          <Text variant="DetailsR" color="text1">
            Ensure your email address is typed in correctly and we'll send you
            an email with instructions to reset it.
          </Text>
        </Box>
        <Box style={styles.form}>
          <Box style={{ marginTop: 20 }}>
            <TextInput
              type="input"
              placeholder="Email address"
              autoCompleteType="email"
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              returnKeyType="next"
              onChangeText={(e) => {
                setEmail(e);
                Emailvalidate();
              }}
              error={email_error}
            />
          </Box>
        </Box>
        <Box style={{ alignItems: "center" }}>
          <Button
            type="primary"
            label="Reset Password"
            onPress={handleForgotPassword}
            loading={loading}
          />
        </Box>
        <Box
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            top: 10,
            padding: 10,
            bottom: 0,
          }}
        >
          <Text variant="Body2R" color="text5" style={{ paddingRight: 10 }}>
            Already have an account?
          </Text>
          <TouchableOpacity onPress={handleSignIn}>
            <Text variant="Body2M" color="primary1">
              Sign in
            </Text>
          </TouchableOpacity>
        </Box>
      </>
    </ScrollView>
  );
};

export default ForgotPassword;

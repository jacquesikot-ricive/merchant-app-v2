import React, { useState, useRef } from "react";
import {
  StyleSheet,
  SafeAreaView,
  Animated,
  TouchableOpacity,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { NativeStackScreenProps } from "@react-navigation/native-stack";

import theme, { Box, Text } from "../../components/Themed";
import OnboardingSlider from "../../components/OnboardingSlider";
import Paginator from "../../components/Paginator";
import { AuthNavParamList } from "../../navigation/AuthNav";
import Button from "../../components/Button";
import ScreenPattern from "../../svg/onboarding/ScreenPattern";

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    alignItems: "center",
    flex: 1,
  },
  slider: {
    marginTop: hp(8),
    height: hp(66),
  },
  bottom: {
    width: wp(90),
    height: 80,
    alignItems: "center",
    justifyContent: "space-between",
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, "Onboarding">;

const data = [
  {
    id: 1,
    image: require("../../assets/images/onboarding/onboarding1.png"),
    topText: "Setup a website in minutes",
    bottomText:
      "Create a free website easily, add your product & services,set your pricing and start accepting orders and payments in minuites.",
    width: 355,
    height: 284,
  },
  {
    id: 2,
    image: require("../../assets/images/onboarding/onboarding2.png"),
    topText: "Simplify your business operations",
    bottomText:
      "Increase efficiency in your laundry operations. Process orders, send invoices, collect and track payments on your mobile.",
    width: 355,
    height: 284,
  },
  {
    id: 3,
    image: require("../../assets/images/onboarding/onboarding3.png"),
    topText: "Run your business like a pro",
    bottomText:
      "Track your sales and orders in real time with valuable insights to help you grow your business from your mobile.",
    width: 355,
    height: 284,
  },
];

const Onboarding = ({ navigation }: Props): JSX.Element => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const scrollX = useRef(new Animated.Value(0)).current;
  const viewableItemsChanged = useRef(({ viewableItems }: any) => {
    setCurrentIndex(viewableItems[0].index);
  }).current;

  const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;
  const slidesRef = useRef<any>();

  return (
    <SafeAreaView style={styles.container}>
      <Box
        style={{
          position: "absolute",
          right: 0,
          top: hp(45),
        }}
      >
        <ScreenPattern />
      </Box>

      <Box
        style={{
          position: "absolute",
          left: 0,
          bottom: -5,
        }}
      >
        <ScreenPattern />
      </Box>

      <TouchableOpacity
        onPress={() => {
          console.log("pressed!");
          navigation.navigate("Welcome");
        }}
        style={{
          position: "absolute",
          top: 60,
          right: 30,
          zIndex: 2,
        }}
      >
        <Text variant={"Body2R"} color="primary1">
          Skip
        </Text>
      </TouchableOpacity>

      <Box style={styles.slider}>
        <Animated.FlatList
          data={data}
          renderItem={({ item }: any) => {
            return (
              <OnboardingSlider
                image={item.image}
                topText={item.topText}
                bottomText={item.bottomText}
                width={item.width}
                height={item.height}
              />
            );
          }}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          bounces={false}
          keyExtractor={(item: any) => item.id.toString()}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
            {
              useNativeDriver: true,
            }
          )}
          scrollEventThrottle={32}
          onViewableItemsChanged={viewableItemsChanged}
          viewabilityConfig={viewConfig}
          ref={slidesRef}
        />
      </Box>

      <Box style={styles.bottom}>
        <Paginator data={data} scrollX={scrollX} />

        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => navigation.navigate("Welcome")}
        >
          <Button
            label={currentIndex < data.length - 1 ? "Next" : "Get Started"}
            onPress={() => {
              if (currentIndex < data.length - 1) {
                setCurrentIndex(currentIndex + 1);
                return slidesRef?.current.scrollToIndex({
                  animated: true,
                  index: currentIndex + 1,
                });
              } else {
                return navigation.navigate("Welcome");
              }
            }}
            type="primary"
            width={200}
          />
        </TouchableOpacity>
      </Box>
    </SafeAreaView>
  );
};

export default Onboarding;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';
import auth from '../../api/riciveApi/auth';

import Button from '../../components/Button';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import theme, { Box, Text } from '../../components/Themed';
import { AuthNavParamList } from '../../navigation/AuthNav';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  title: {
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  form: {
    marginBottom: 80,
  },
  baseText: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 22,
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, 'SetupStore'>;

const SetupStore = ({ navigation, route }: Props): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false);
  const { email, first_name, last_name, phone, password } = route.params;
  const [business, setBusiness] = useState<string>('');
  const [address, setAddress] = useState<string>('');

  const handleSignUp = async () => {
    try {
      setLoading(true);
      if (email && first_name && last_name && phone && password && business) {
        await auth.signup({
          email,
          first_name,
          last_name,
          // phone,
          password,
          // address,
          business,
        });

        setLoading(false);
        navigation.navigate('SignIn', {
          token: undefined,
        });
        Toast.show({
          text1: 'Register Success',
          text2: 'Please check your email to confirm',
          type: 'success',
          visibilityTime: 6000,
        });
      }
    } catch (error: any) {
      setLoading(false);
      console.log('Error registernig new merchant:', error);
      Toast.show({
        text1: 'Register Error',
        text2: error || 'Error signing you up',
        type: 'error',
        visibilityTime: 3500,
      });
    }
  };
  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{ alignItems: 'center' }}
    >
      <StackHeader title="" onBackPress={() => navigation.goBack()} />
      <Box>
        <Box style={styles.title}>
          <Text style={{ marginBottom: 25 }} variant="TitleB" color="text6">
            Set up store
          </Text>
          <Text
            style={{ textAlign: 'center', paddingLeft: 20, paddingRight: 20 }}
            variant="Body2R"
            color="text5"
          >
            Add your public business name and full business address
          </Text>
        </Box>
        <Box style={styles.form}>
          <Box>
            <TextInput
              type="input"
              placeholder="Business name"
              autoCapitalize="words"
              autoCorrect={false}
              onChangeText={(e) => setBusiness(e)}
            />
          </Box>
          <Box style={{ marginTop: 40 }}>
            <TextInput
              type="description"
              placeholder="Business address"
              autoCapitalize="words"
              autoCompleteType="street-address"
              autoCorrect={false}
              multiline
              onChangeText={(e) => setAddress(e)}
            />
          </Box>
        </Box>
        <Button
          type="primary"
          label="Set up"
          onPress={handleSignUp}
          loading={loading}
        />
      </Box>
    </ScrollView>
  );
};

export default SetupStore;

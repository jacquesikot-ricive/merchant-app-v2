import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState, useRef, useEffect } from 'react';
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import Toast from 'react-native-toast-message';
import Intercom from '@intercom/intercom-react-native';

import theme, { Box, Text } from '../../components/Themed';
import { AuthNavParamList } from '../../navigation/AuthNav';
import useAuth from '../../auth/useAuth';
import StackHeader from '../../components/StackHeader';
import auth from '../../api/riciveApi/auth';
import storage from '../../utils/storage';
import storageKeys from '../../constants/storageKeys';
import LoadingBackdrop from '../../components/Loadinbackdrop';
import validateEmail from '../../utils/validateEmail';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  title: {
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
  form: {
    marginBottom: 80,
  },
  baseText: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 22,
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, 'SignIn'>;

const SignIn = ({ navigation, route }: Props): JSX.Element => {
  // const { token } = route.params;
  const [loading, setLoading] = useState<boolean>(false);
  const [email, setEmail] = useState<string>('');
  const [email_error, setEmailError] = useState<string>();
  const [password, setPassword] = useState<string>('');
  const [password_error, setPasswordError] = useState<string>();
  const { login: loginUser } = useAuth();
  const passwordRef = useRef<any>();

  // useEffect(() => {
  //   if (token) {
  //     console.log(token);
  //   }
  // }, []);

  const handleEmailvalidation = () => {
    email.length < 1
      ? setEmailError('Enter email to continue')
      : validateEmail(email) == false
      ? setEmailError('please enter a valid email')
      : setEmailError(undefined);
  };
  const handlePasswordValidation = () => {
    !password
      ? setPasswordError('Enter password to continue')
      : password.length < 4
      ? setPasswordError('Password must be at least 4 characters')
      : setPasswordError(undefined);
  };

  const loginApi = auth.signin;
  const handleSignIn = async () => {
    if (password_error != undefined || email_error != undefined) {
      return;
    }
    try {
      setLoading(true);
      const result = await loginApi({
        email,
        password,
      });

      await loginUser({
        auth: result.data.auth,
        business: result.data.business,
        profile: result.data.profile,
        access_token: result.data.access_token,
        refresh_token: result.data.refresh_token,
        permissions: result.data.permissions
          ? result.data.permissions
          : undefined,
      });

      Intercom.registerIdentifiedUser({ email, userId: result.data.auth.id });

      setLoading(false);

      Toast.show({
        type: 'success',
        text1: 'Sign In',
        text2: 'Sign in Success!',
      });
    } catch (error: any) {
      setLoading(false);
      console.log('Error logging in:', JSON.stringify(error.response.data));
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We could not log you in right now, please try again later...',
        visibilityTime: 3000,
      });
    }
  };

  const handleSignUp = () => {
    navigation.navigate('SignUpProfile');
  };

  const handleForgotPassword = () => {
    navigation.navigate('ForgotPassword');
  };

  return (
    <>
      {loading ? <LoadingBackdrop /> : null}
      <ScrollView
        bounces={false}
        contentContainerStyle={{ alignItems: 'center' }}
        style={styles.container}
      >
        <StatusBar hidden={loading ? true : false} />
        <StackHeader onBackPress={() => navigation.goBack()} title="" />
        <Box style={styles.title}>
          <Text style={{ marginBottom: 25 }} variant="TitleB" color="text6">
            Sign In
          </Text>
          <Text style={{}} variant="Body2R" color="text5">
            Welcome, please sign in to get started
          </Text>
        </Box>
        <Box style={styles.form}>
          <Box>
            <TextInput
              type="input"
              placeholder="Email Address"
              autoCompleteType="name"
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              returnKeyType="next"
              onChangeText={(e) => {
                setEmail(e);
                handleEmailvalidation();
              }}
              error={email_error}
              onEndEditing={() => {
                passwordRef.current.focus();
                handleEmailvalidation();
              }}
            />
          </Box>
          {email_error ? <Box marginBottom="l" /> : null}
          <Box style={{ marginTop: 20 }}>
            <TextInput
              inputRef={passwordRef}
              type="input"
              placeholder="Password"
              secured
              autoCompleteType="password"
              autoCorrect={false}
              autoCapitalize="none"
              returnKeyType="done"
              error={password_error}
              onChangeText={(e) => {
                setPassword(e);
                handlePasswordValidation();
              }}
              onEndEditing={handlePasswordValidation}
            />
          </Box>
          {password_error ? <Box marginBottom="l" /> : null}
          <TouchableOpacity
            onPress={handleForgotPassword}
            style={styles.baseText}
          >
            <Text variant="DetailsR" color="primary1">
              Forgot Password?
            </Text>
          </TouchableOpacity>
        </Box>

        <Button type="primary" label="Sign In" onPress={handleSignIn} />
        <Box style={{ padding: 10 }} />
        <Box
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            top: 15,
            padding: 10,
            bottom: 0,
          }}
        >
          <Text variant="Body2R" color="text5" style={{ paddingRight: 10 }}>
            Dont have an account?
          </Text>
          <TouchableOpacity onPress={handleSignUp}>
            <Text variant="Body2M" color="primary1">
              Sign up
            </Text>
          </TouchableOpacity>
        </Box>
      </ScrollView>
    </>
  );
};

export default SignIn;

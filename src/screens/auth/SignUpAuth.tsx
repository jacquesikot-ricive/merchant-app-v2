import React, { useState } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StatusBar,
} from 'react-native';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import Toast from 'react-native-toast-message';
import useAuth from '../../auth/useAuth';

import theme, { Box, Text } from '../../components/Themed';
import { AuthNavParamList } from '../../navigation/AuthNav';
import StackHeader from '../../components/StackHeader';
import authApi from '../../api/riciveApi/auth';
import register from '../../api/auth/register';
import LoadingBackdrop from '../../components/Loadinbackdrop';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  title: {
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
  form: {
    marginBottom: 30,
    alignItems: 'center',
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, 'SignUpAuth'>;

const SignUpAuth = ({ navigation, route }: Props): JSX.Element => {
  const { first_name, last_name, email, referralCode } = route.params;
  const [password, setPassword] = useState<string>('');
  const [password_error, setPasswordError] = useState<string>();
  const [loading, setLoading] = useState<boolean>(false);
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [confirmPassword_error, setConfirmPasswordError] = useState<string>();
  const { login } = useAuth();

  // TODO: Handle password difficulty, min length, special character, etc
  const validatePassword = () => {
    password.length < 4
      ? setPasswordError('Password must be at least 4 characters')
      : setPasswordError(undefined);
  };
  const validateConfirmPassword = () => {
    password !== confirmPassword
      ? setConfirmPasswordError('Passwords must match')
      : setConfirmPasswordError(undefined);
  };
  const handleSetUp = async () => {
    if (password !== confirmPassword) {
      return Toast.show({
        type: 'error',
        text1: 'Password error',
        text2: 'Password does not match!',
      });
    }

    if (password.length < 4) {
      return Toast.show({
        type: 'error',
        text1: 'Password error',
        text2: 'Password must be more than 4 characters',
      });
    }

    try {
      setLoading(true);
      await authApi.signup({
        email,
        first_name,
        last_name,
        password,
        role: 'SUPERADMIN',
        referral_code: referralCode,
      });

      const result = await authApi.signin({
        email,
        password,
      });

      await login({
        auth: result.data.auth,
        business: result.data.business,
        profile: result.data.profile,
        access_token: result.data.access_token,
        refresh_token: result.data.refresh_token,
        permissions: result.data.permissions,
      });

      setLoading(false);
      Toast.show({
        type: 'success',
        text1: 'Register Success',
        text2: 'You have been registered and logged in!',
      });
      // return navigation.navigate('EmailSent');
    } catch (error: any) {
      console.log(
        'Error signing up user profile:',
        JSON.stringify(error.response.data.message)
      );
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error!',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We cannot process your request now',
      });
    }
  };

  return (
    <>
      {loading ? <LoadingBackdrop /> : null}
      <ScrollView
        bounces={false}
        style={styles.container}
        contentContainerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <StatusBar
          hidden={loading ? true : false}
          backgroundColor="transparent"
          translucent
        />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'position' : 'height'}
          contentContainerStyle={{ alignItems: 'center' }}
        >
          <>
            {/* Header */}
            <StackHeader
              title="Create your password"
              onBackPress={() => navigation.goBack()}
            />

            {/* Title */}
            {/* <Box style={styles.title}>
            <Text variant="Body1M" color="text1">
              Create your password
            </Text>
          </Box> */}

            {/* Form */}
            <Box style={styles.form}>
              {/* <Box>
              <TextInput
                type="input"
                placeholder="Username"
                autoCompleteType="name"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="default"
                returnKeyType="next"
                onChangeText={(e) => setUsername(e)}
              />
            </Box> */}
              <Box style={{ marginTop: 15 }}>
                <TextInput
                  type="input"
                  placeholder="Password"
                  autoCompleteType="off"
                  secured
                  returnKeyType="next"
                  onChangeText={(e) => setPassword(e)}
                  // error={password_error}
                  onEndEditing={validatePassword}
                />
              </Box>
              {/* {password_error ? <Box marginTop="l" /> : null} */}
              <Box style={{ marginTop: 20, marginBottom: 50 }}>
                <TextInput
                  type="input"
                  placeholder="Confirm Password"
                  autoCompleteType="off"
                  secured
                  returnKeyType="done"
                  onChangeText={(e) => setConfirmPassword(e)}
                  // error={confirmPassword_error}
                  onEndEditing={validateConfirmPassword}
                />
              </Box>
              {/* {confirmPassword_error ? <Box marginTop="l" /> : null} */}

              <Button
                type="primary"
                label="Create Account"
                onPress={handleSetUp}
                // loading={loading}
              />
            </Box>
          </>
        </KeyboardAvoidingView>
      </ScrollView>
    </>
  );
};

export default SignUpAuth;

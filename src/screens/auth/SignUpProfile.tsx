import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useRef, useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import Button from '../../components/Button';
import Checkbox from '../../components/Checkbox';
import TextInput from '../../components/TextInput';
import Toast from 'react-native-toast-message';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import * as Linking from 'expo-linking';

import theme, { Box, Text } from '../../components/Themed';
import { AuthNavParamList } from '../../navigation/AuthNav';
import StackHeader from '../../components/StackHeader';
import validateEmail from '../../utils/validateEmail';
import ContainsSpecialChars from '../../utils/ContainsSpecialCharacters';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  title: {
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
  form: {
    marginBottom: 30,
    alignItems: 'center',
  },
});

function hasWhiteSpace(s: string) {
  return /\s/g.test(s);
}

type Props = NativeStackScreenProps<AuthNavParamList, 'SignUpProfile'>;

const SignUpProfile = ({ navigation }: Props): JSX.Element => {
  const [first_name, setFirstName] = useState<string>('');
  const [first_name_error, setFirstNameError] = useState<string>();
  const [last_name, setLastName] = useState<string>('');
  const [last_name_error, setLastNameError] = useState<string>();
  const [email, setEmail] = useState<string>('');
  const [referralCode, setReferralCode] = useState<string>('');
  const [email_error, setEmailError] = useState<string>();
  const [checked, setChecked] = useState<boolean>(false);
  const firstNameRef = useRef<any>();
  const lastNameRef = useRef<any>();
  const emailRef = useRef<any>();
  const [inviteFocused, setInviteFocused] = useState<boolean>(false);

  const handleSignIn = () => {
    navigation.navigate('SignIn', {
      token: undefined,
    });
  };

  const validateFirstName = () => {
    first_name.length < 1
      ? setFirstNameError('First name is required')
      : hasWhiteSpace(first_name)
      ? setFirstNameError('enter only first name with no space')
      : ContainsSpecialChars(first_name)
      ? setFirstNameError('name cannot contain special characters')
      : setFirstNameError(undefined);
  };
  const validateSurname = () => {
    last_name.length < 1
      ? setLastNameError('feild cannot be empty')
      : hasWhiteSpace(last_name)
      ? setLastNameError('enter only last name with no space')
      : ContainsSpecialChars(first_name)
      ? setFirstNameError('name cannot contain special characters')
      : setLastNameError(undefined);
  };
  const Emailvalidation = () => {
    email.length < 1
      ? setEmailError('feild cannot be empty')
      : validateEmail(email) == false
      ? setEmailError('please enter a valid email')
      : setEmailError(undefined);
  };

  const handleSetUp = () => {
    if (!checked) {
      return Toast.show({
        type: 'error',
        text1: 'Agree to Terms & Conditions',
        text2: 'Check conditions to continue..',
      });
    }

    // if (
    //   first_name_error != undefined ||
    //   last_name_error != undefined ||
    //   email_error != undefined
    // ) {
    //   return;
    // }
    return navigation.navigate('SignUpAuth', {
      first_name,
      last_name,
      email: email.trim(),
      referralCode,
    });
  };

  return (
    <ScrollView
      bounces={false}
      style={styles.container}
      contentContainerStyle={{ alignItems: 'center', justifyContent: 'center' }}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'position' : 'height'}
        contentContainerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <>
          {/* Header */}
          <StackHeader title="" onBackPress={() => navigation.goBack()} />

          {/* Title */}
          <Box style={styles.title}>
            <Text
              style={{ textAlign: 'left', width: wp(90) }}
              variant="Body1M"
              color="text1"
            >
              Welcome! Let's get you started with your Ricive account.
            </Text>
          </Box>

          {/* Form */}
          <Box style={styles.form}>
            <Box>
              <TextInput
                inputRef={firstNameRef}
                type="input"
                placeholder="First Name"
                autoCompleteType="name"
                autoCorrect={false}
                autoCapitalize="words"
                keyboardType="default"
                returnKeyType="next"
                onChangeText={(e) => {
                  setFirstName(e);
                  validateFirstName();
                }}
                // error={first_name_error}
                onEndEditing={validateFirstName}
              />
            </Box>
            {/* {first_name_error ? <Box marginBottom="l" /> : null} */}
            <Box style={{ marginTop: 20 }}>
              <TextInput
                inputRef={lastNameRef}
                type="input"
                placeholder="Last Name"
                autoCompleteType="name"
                autoCorrect={false}
                autoCapitalize="words"
                keyboardType="default"
                returnKeyType="next"
                onChangeText={(e) => {
                  setLastName(e);
                  validateSurname();
                }}
                // error={last_name_error}
                onEndEditing={validateSurname}
              />
            </Box>
            {/* {last_name_error ? <Box marginBottom="l" /> : null} */}
            <Box style={{ marginTop: 20 }}>
              <TextInput
                inputRef={emailRef}
                type="input"
                placeholder="Email Address"
                autoCompleteType="email"
                autoCorrect={false}
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="done"
                onChangeText={(e) => {
                  setEmail(e);
                  Emailvalidation();
                }}
                // error={email_error}
                onEndEditing={Emailvalidation}
              />
            </Box>

            <Box style={{ marginTop: 20 }}>
              <TextInput
                type="input"
                placeholder="Referral Code"
                autoCorrect={false}
                keyboardType="default"
                returnKeyType="done"
                onChangeText={(e) => {
                  setReferralCode(e);
                }}
              />
            </Box>
            {email_error ? <Box marginBottom="l" /> : null}
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                width: wp(90),
                marginTop: 15,
                marginBottom: 50,
              }}
            >
              <TouchableOpacity onPress={() => setChecked(!checked)}>
                <Checkbox checked={checked} />
              </TouchableOpacity>
              <Box>
                <Box
                  style={{
                    justifyContent: 'center',
                    top: 5,
                    padding: 10,
                    bottom: 0,
                    width: wp(70),
                  }}
                >
                  <Box style={{ flexDirection: 'row' }}>
                    <Text
                      variant="SmallerTextR"
                      color="text5"
                      style={{ paddingRight: 2 }}
                    >
                      I have read, understood and I agree to Ricive
                    </Text>
                    <TouchableOpacity
                      onPress={async () =>
                        await Linking.openURL('https://ricive.com/privacy')
                      }
                    >
                      <Text variant="SmallerTextR" color="primary1">
                        Privacy Policy
                      </Text>
                    </TouchableOpacity>
                  </Box>
                  <Box
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'flex-start',
                    }}
                  >
                    {/* <Box>
                      <Text variant="SmallerTextR" color="primary1">
                        Policy,
                      </Text>
                    </Box> */}
                    {/* <Text
                      variant="SmallerTextR"
                      color="text5"
                      style={{ paddingHorizontal: 2 }}
                    >
                      and
                    </Text> */}

                    {/* <TouchableOpacity
                      onPress={async () =>
                        await Linking.openURL(
                          'https://ricive.com/merchant-agreement'
                        )
                      }
                    >
                      <Text variant="SmallerTextR" color="primary1">
                        Merchant Agreement
                      </Text>
                    </TouchableOpacity> */}

                    <Text
                      variant="SmallerTextR"
                      color="text5"
                      style={{ paddingHorizontal: 2 }}
                    >
                      and
                    </Text>

                    <TouchableOpacity
                      onPress={async () =>
                        await Linking.openURL('https://ricive.com/terms')
                      }
                    >
                      <Text variant="SmallerTextR" color="primary1">
                        Terms and conditions
                      </Text>
                    </TouchableOpacity>
                  </Box>
                </Box>
              </Box>
            </Box>
            <Button type="primary" label="Next" onPress={handleSetUp} />
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                top: 10,
                padding: 10,
                bottom: 0,
              }}
            >
              <Text variant="Body2R" color="text5" style={{ paddingRight: 10 }}>
                Already have an account?
              </Text>
              <TouchableOpacity onPress={handleSignIn}>
                <Text variant="Body2M" color="primary1">
                  Sign in
                </Text>
              </TouchableOpacity>
            </Box>
          </Box>
        </>
        <Box style={{ padding: 10 }} />
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default SignUpProfile;

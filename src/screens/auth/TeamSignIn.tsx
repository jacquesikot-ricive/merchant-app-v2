import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import { widthPercentageToDP } from 'react-native-responsive-screen';

import theme, { Box, Text } from '../../components/Themed';
import { AuthNavParamList } from '../../navigation/AuthNav';
import { useAppDispatch } from '../../redux/hooks';
import useAuth from '../../auth/useAuth';
import signinApi from '../../api/riciveApi/auth';
import Toast from 'react-native-toast-message';
import StackHeader from '../../components/StackHeader';
import login from '../../api/auth/login';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  title: {
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 30,
  },
  form: {
    marginBottom: 80,
  },
  baseText: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 22,
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, 'TeamSignIn'>;

const TeamSignIn = ({ navigation }: Props): JSX.Element => {
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState<boolean>(false);
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const { login } = useAuth();

  const handleSignIn = async () => {
    try {
      setLoading(true);
      if (!email) {
        setLoading(false);
        return Toast.show({
          text1: 'No Email!',
          text2: 'Enter email to continue',
          type: 'error',
          visibilityTime: 4000,
        });
      }

      if (!password) {
        setLoading(false);
        return Toast.show({
          text1: 'No Password!',
          text2: 'Enter password to continue',
          type: 'error',
          visibilityTime: 4000,
        });
      }

      const result = await signinApi.signin({
        email,
        password,
      });

      await login({
        auth: result.data.auth,
        business: result.data.business,
        profile: result.data.profile,
        access_token: result.data.access_token,
        refresh_token: result.data.refresh_token,
        permissions: result.data.permissions,
      });

      setLoading(false);

      Toast.show({
        type: 'success',
        text1: 'Sign In',
        text2: 'Sign in Success!',
      });
    } catch (error: any) {
      setLoading(false);
      console.log('Error logging in:', JSON.stringify(error.response.data));
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We could not log you in right now, please try again later...',
        visibilityTime: 3000,
      });
    }
  };
  return (
    <Box style={styles.container}>
      <StackHeader title="Team Login" onBackPress={() => navigation.goBack()} />

      <Box style={styles.title}>
        <Text style={{}} variant="Body2R" color="text5">
          Welcome, please sign in to join your team
        </Text>
      </Box>
      <Box style={styles.form}>
        <Box>
          <TextInput
            type="input"
            autoCompleteType="email"
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            placeholder="Email"
            onChangeText={(e) => setEmail(e)}
          />
        </Box>
        <Box style={{ marginTop: 20 }}>
          <TextInput
            type="input"
            placeholder="Password"
            secured
            onChangeText={(e) => setPassword(e)}
          />
        </Box>
        {/* <Box style={styles.baseText}>
          <Text variant="DetailsR" color="primary1">
            Forgot Password?
          </Text>
        </Box> */}
      </Box>
      <Button
        type="primary"
        label="Sign In"
        onPress={handleSignIn}
        loading={loading}
        disabled={email.length < 1 && password.length < 1 ? true : false}
      />
    </Box>
  );
};

export default TeamSignIn;

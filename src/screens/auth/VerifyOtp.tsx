import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet, SafeAreaView, TextInputBase } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../../components/Button';
import RadioForm from '../../components/RadioForm';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';

import theme, { Box, Text } from '../../components/Themed';
import { RootStackParamList } from '../../types';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  form: {
    marginBottom: 80,
    justifyContent: 'flex-start',
    paddingLeft: 20,
    width: '100%',
  },
  baseText: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 22,
  },
});

const VerifyOtp = ({
  navigation,
}: NativeStackScreenProps<RootStackParamList>): JSX.Element => {
  const handleVerify = () => {
    navigation.navigate('VerifyOtpInput');
  };
  return (
    <Box>
      <Box style={{ marginTop: 50 }}>
        <StackHeader title="Back" onBackPress={() => navigation.goBack()} />
      </Box>
      <Box style={styles.container}>
        <Box style={styles.title}>
          <Text style={{ marginBottom: 25 }} variant="TitleB" color="text6">
            Verification
          </Text>
          <Text
            style={{ textAlign: 'center', paddingLeft: 10, paddingRight: 10 }}
            variant="Body2R"
            color="text5"
          >
            Please select how you would want to recieve
          </Text>
          <Text
            style={{ textAlign: 'center', paddingLeft: 20, paddingRight: 20 }}
            variant="Body2R"
            color="text5"
          >
            {' '}
            your verification code?
          </Text>
        </Box>
        <Box style={styles.form}>
          <Box
            style={{
              justifyContent: 'flex-start',
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
            }}
          >
            <RadioForm checked />
            <Box style={{ paddingLeft: 18 }}>
              <Text variant="Body2R" color="text7">
                By Phone Number
              </Text>
            </Box>
          </Box>
          <Box
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 20,
            }}
          >
            <RadioForm />
            <Box style={{ paddingLeft: 18 }}>
              <Text variant="Body2M" color="text7">
                Email Address
              </Text>
            </Box>
          </Box>
        </Box>
        <Button type="primary" label="Next" onPress={handleVerify} />
      </Box>
    </Box>
  );
};

export default VerifyOtp;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet, SafeAreaView, TextInputBase } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Button from '../../components/Button';
import OtpInput from '../../components/OtpInput';
import RadioForm from '../../components/RadioForm';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';

import theme, { Box, Text } from '../../components/Themed';
import { RootStackParamList } from '../../types';

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: 'center',
  },
  title: {
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  form: {
    //   marginBottom: 80,
    flexDirection: 'row',
  },
  baseText: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 22,
  },
});

const VerifyOtpInput = ({
  navigation,
}: NativeStackScreenProps<RootStackParamList>): JSX.Element => {
  const handleSignUp = () => {
    navigation.navigate('SignUp');
  };
  return (
    <Box>
      <Box style={{ marginTop: 50 }}>
        <StackHeader title="Back" onBackPress={() => navigation.goBack()} />
      </Box>
      <Box style={styles.container}>
        <Box style={styles.title}>
          <Text style={{ marginBottom: 25 }} variant="TitleB" color="text6">
            Verify OTP
          </Text>
          <Text
            style={{ textAlign: 'center', paddingLeft: 10, paddingRight: 10 }}
            variant="Body2R"
            color="text5"
          >
            We've sent you a verification code
          </Text>
        </Box>
        <Box style={styles.form}>
          <OtpInput />
          <OtpInput />
          <OtpInput />
          <OtpInput />
        </Box>
        <Box
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            top: 10,
            padding: 10,
            marginBottom: 80,
          }}
        >
          <Text variant="Body2R" color="text5" style={{ paddingRight: 10 }}>
            Didn't get the code?
          </Text>
          <Box>
            <Text variant="Body2M" color="primary1">
              Resend {'(50s)'}
            </Text>
          </Box>
        </Box>
        <Button type="primary" label="Verify" onPress={() => {}} />
      </Box>
    </Box>
  );
};

export default VerifyOtpInput;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Image } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
// import {
//   GoogleSignin,
//   GoogleSigninButton,
//   statusCodes,
// } from '@react-native-google-signin/google-signin';

import theme, { Box, Text } from '../../components/Themed';
import { AuthNavParamList } from '../../navigation/AuthNav';
import Button from '../../components/Button';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: theme.colors.white,
  },
});

type Props = NativeStackScreenProps<AuthNavParamList, 'Welcome'>;

const Welcome = ({ navigation }: Props): JSX.Element => {
  // useEffect(() => {
  //   GoogleSignin.configure({
  //     // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  //     // webClientId: '<FROM DEVELOPER CONSOLE>', // client ID of type WEB for your server (needed to verify user ID and offline access)
  //     offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  //     // hostedDomain: '', // specifies a hosted domain restriction
  //     // forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
  //     // accountName: '', // [Android] specifies an account name on the device that should be used
  //     // iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
  //     // googleServicePlistPath: '', // [iOS] if you renamed your GoogleService-Info file, new name here, e.g. GoogleService-Info-Staging
  //     // openIdRealm: '', // [iOS] The OpenID2 realm of the home web server. This allows Google to include the user's OpenID Identifier in the OpenID Connect ID token.
  //     profileImageSize: 120, // [iOS] The desired height (and width) of the profile image. Defaults to 120px
  //   });
  // }, []);

  return (
    <Box style={styles.container}>
      <Box
        style={{
          position: 'absolute',
          top: 60,
          right: 30,
        }}
      >
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('Support', {
              isAuth: true,
            })
          }
        >
          <Text variant={'Body2R'} color="primary1">
            Contact Us
          </Text>
        </TouchableOpacity>
      </Box>

      <Box>
        <Image
          source={require('../../assets/images/onboarding/welcome.png')}
          style={{
            width: 355,
            height: 284,
          }}
        />
      </Box>

      <Box
        style={{
          backgroundColor: theme.colors.white,
          width: wp(100),
          alignItems: 'center',
          paddingVertical: 50,
          paddingHorizontal: 20,
        }}
      >
        <Text
          style={{
            width: '90%',
            textAlign: 'center',
          }}
          variant={'Body2M'}
        >
          Business operations made simple
        </Text>

        <Text variant={'DetailsR'} color="text9" textAlign={'center'} mt="m">
          All-in-one platform to power your Business
        </Text>

        <Box
          flexDirection={'row'}
          justifyContent={'space-between'}
          mt="xxl"
          style={{ width: '100%' }}
        >
          <Button
            label="Register"
            type="primary"
            onPress={() => navigation.navigate('SignUpProfile')}
            width={wp(42)}
          />

          <Button
            label="Sign In"
            type="secondary"
            onPress={() =>
              navigation.navigate('SignIn', {
                token: undefined,
              })
            }
            width={wp(42)}
          />
        </Box>

        {/* <TouchableOpacity
          style={{
            marginTop: 35,
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
          }}
          onPress={() => navigation.navigate('TeamSignIn')}
        >
          <Text variant={'Body2R'} color="primary1" mr="m">
            Join Existing Business
          </Text>
          <Icon
            name="arrow-right"
            color={theme.colors.primary1}
            size={18}
            style={{ marginTop: 3 }}
          />
        </TouchableOpacity> */}
      </Box>
    </Box>
  );
};

export default Welcome;

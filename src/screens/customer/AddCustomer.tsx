import React, { useState } from 'react';
import { ScrollView, StyleSheet, Keyboard } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import NigerianPhone from 'validate_nigerian_phone';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useAnalytics } from '@segment/analytics-react-native';

import theme, { Box } from '../../components/Themed';
import TextInput from '../../components/TextInput';
import Button from '../../components/Button';
import validateEmail from '../../utils/validateEmail';
import StackHeader from '../../components/StackHeader';
import { CustomerNavParamList } from '../../navigation/CustomerNav';
import customerApi from '../../api/riciveApi/customer';
import { useAppSelector } from '../../redux/hooks';
import PhoneTextInput from '../../components/PhoneTextInput';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  header: {
    justifyContent: 'flex-start',
    width: wp(70),
    marginBottom: 38,
  },
  textHeader: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  form: {
    alignItems: 'center',
    marginTop: 20,
  },
  input: {
    marginBottom: 24,
  },
  innerText: {
    marginBottom: 10,
    justifyContent: 'flex-start',
    width: wp(90),
  },
  description: {
    marginTop: 24,
  },
});

type Props = NativeStackScreenProps<CustomerNavParamList, 'AddCustomer'>;

const AddCustomer = ({ navigation, route }: Props) => {
  const { track } = useAnalytics();
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const {
    isEdit,
    customer,
    phone: contactPhone,
    firstName: firstNamePhone,
    lastName: lastNamePhone,
    isContacts,
  } = route.params;

  const finalContactFirstname = firstNamePhone || '';
  const finalContactLastname = lastNamePhone || '';
  const finalContactPhone = contactPhone || '';

  const [firstName, setFirstName] = useState<string>(
    isContacts
      ? finalContactFirstname
      : !isEdit
      ? ''
      : customer && customer.first_name
      ? customer.first_name
      : ''
  );
  const [lastName, setLastName] = useState<string>(
    isContacts
      ? finalContactLastname
      : !isEdit
      ? ''
      : customer && customer.last_name
      ? customer.last_name
      : ''
  );
  const [email, setEmail] = useState<string>(
    !isEdit ? '' : customer && customer.email ? customer.email : ''
  );
  const [phone, setPhone] = useState<string>(
    isContacts
      ? finalContactPhone
      : !isEdit
      ? ''
      : customer && customer.phone
      ? customer.phone
      : ''
  );
  const [address, setAddress] = useState<string>(
    !isEdit ? '' : customer && customer.address ? customer.address : ''
  );
  const [loading, setLoading] = useState<boolean>(false);

  const handleAddCustomer = async () => {
    try {
      setLoading(true);
      const fullPhone = phone;
      const phoneIsValid = new NigerianPhone(fullPhone);
      if (firstName.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'First name cannot be empty',
          visibilityTime: 3000,
        });
      }

      if (email.length > 0) {
        if (!validateEmail(email)) {
          setLoading(false);
          return Toast.show({
            type: 'error',
            text1: 'Input Error',
            text2: 'Email not correct',
            visibilityTime: 3000,
          });
        }
      }

      // if (!phoneIsValid.isValid()) {
      //   setLoading(false);
      //   return Toast.show({
      //     type: 'error',
      //     text1: 'Input Error',
      //     text2: 'Phone not correct',
      //     visibilityTime: 3000,
      //   });
      // }

      await customerApi.createCustomer({
        address: address.length > 0 ? address : undefined,
        first_name: firstName,
        last_name: lastName ? lastName : undefined,
        email: email.length > 0 ? email : undefined,
        phone: fullPhone,
      });

      track('Add Customer', {
        business_id,
      });

      setLoading(false);
      Toast.show({
        type: 'success',
        text1: 'New Customer Added',
        text2: `${firstName} has been added successfully`,
      });
      navigation.navigate('Customer');
    } catch (error: any) {
      console.log(
        'Error adding customer:',
        JSON.stringify(error.response.data)
      );
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We cannot process your request now, please try again later..',
      });
    }
  };

  const handleUpdateCustomer = async () => {
    const fullPhone = phone;
    const phoneIsValid = new NigerianPhone(fullPhone);
    try {
      setLoading(true);
      if (phone.length > 0 && !phoneIsValid.isValid()) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Phone not correct',
          visibilityTime: 3000,
        });
      }

      await customerApi.updateCustomer({
        id: customer.id,
        address:
          address.length > 0
            ? address
            : customer.address && customer.address.length > 0
            ? customer.address
            : undefined,
        first_name: firstName.length > 0 ? firstName : customer.first_name,
        last_name:
          lastName.length > 0
            ? lastName
            : customer.last_name && customer.last_name.length > 0
            ? customer.last_name
            : undefined,
        email:
          email.length > 0
            ? email
            : customer.email
            ? customer.email
            : undefined,
        phone: phone.length > 0 ? fullPhone : customer.phone,
      });
      track('Update Customer', {
        business: business_id,
      });
      setLoading(false);
      Toast.show({
        type: 'success',
        text1: 'Update Customer Done',
        text2: `${firstName} has been added successfully`,
      });
      navigation.navigate('Customer');
    } catch (error: any) {
      setLoading(false);
      // console.log(
      //   'Error updating customer:',
      //   JSON.stringify(error.response.data)
      // );
      // console.log('Error updating customer:', JSON.stringify(error.message));
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.message) ||
          'We cannot process your request now, please try again later..',
      });
    }
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{ justifyContent: 'center', paddingBottom: 100 }}
    >
      <StackHeader
        title={isEdit ? 'Update Customer' : 'Add Customer'}
        onBackPress={() => navigation.goBack()}
      />

      <Box style={styles.form}>
        <Box style={styles.input}>
          <TextInput
            type="input"
            value={firstName}
            placeholder={isEdit ? `${customer.first_name}` : 'First Name'}
            autoCompleteType="name"
            autoCorrect={false}
            autoCapitalize="words"
            keyboardType="default"
            returnKeyType="next"
            onChangeText={(e) => setFirstName(e)}
          />
        </Box>

        <Box style={styles.input}>
          <TextInput
            type="input"
            value={lastName}
            placeholder={
              isEdit && customer.last_name
                ? `${customer.lastName}`
                : 'Last Name'
            }
            autoCompleteType="name"
            autoCorrect={false}
            autoCapitalize="words"
            keyboardType="default"
            returnKeyType="next"
            onChangeText={(e) => setLastName(e)}
          />
        </Box>

        <Box style={styles.input}>
          <TextInput
            type="input"
            value={email}
            placeholder={
              isEdit && customer.email && customer.email.length > 0
                ? customer.email
                : 'Email Address'
            }
            autoCompleteType="email"
            autoCorrect={false}
            autoCapitalize="none"
            keyboardType="email-address"
            returnKeyType="next"
            onChangeText={(e) => setEmail(e)}
          />
        </Box>
        <Box style={styles.input}>
          {/* <TextInput
            type="input"
            value={phone}
            autoCorrect={false}
            keyboardType="phone-pad"
            returnKeyType="next"
            placeholder={
              isEdit && customer.phone && customer.phone.length > 0
                ? customer.phone
                : 'Phone number'
            }
            onChangeText={(e) => setPhone(e)}
          /> */}

          <PhoneTextInput
            onChangeFormattedText={(t: string) => setPhone(t)}
            value={phone.replace(/[- )(]/g, '').replace('+', '')}
          />
        </Box>
        <Box style={styles.description}>
          <TextInput
            value={address}
            type="description"
            multiline
            autoCorrect={false}
            keyboardType="default"
            returnKeyType="done"
            placeholder={
              isEdit && customer.address && customer.address.length > 0
                ? customer.address
                : 'Address'
            }
            autoCapitalize="words"
            onKeyPress={(e) => {
              if (e.nativeEvent.key === 'Enter') {
                Keyboard.dismiss();
              }
            }}
            onChangeText={(e) => setAddress(e)}
          />
        </Box>
        <Box style={{ marginTop: 126 }}>
          <Button
            type="primary"
            label={isEdit ? 'Update Customer' : 'Create Customer'}
            onPress={isEdit ? handleUpdateCustomer : handleAddCustomer}
            disabled={
              isEdit
                ? false
                : firstName.length > 0 && phone.length > 0
                ? false
                : true
            }
            loading={loading}
          />
        </Box>
      </Box>
    </ScrollView>
  );
};

export default AddCustomer;

import React, { useEffect, useState } from 'react';
import { Alert, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import theme, { Box, Text } from '../../components/Themed';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { useIsFocused } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useAnalytics } from '@segment/analytics-react-native';
import Toast from 'react-native-toast-message';
import { useQuery } from 'react-query';

import HeaderTextInput from '../../components/HeaderTextInput';
import CustomerList from '../../components/CustomerList';
import ListEmpty from '../../components/ListEmpty';
import ScreenLoading from '../../components/ScreenLoading';
import { CustomerNavParamList } from '../../navigation/CustomerNav';
import customerApi from '../../api/riciveApi/customer';
import ScreenError from '../../components/ScreenError';
import PlusCircleIcon from '../../../svgs/PlusCircleIcon';
import { useAppSelector } from '../../redux/hooks';
import queryKeys from '../../constants/queryKeys';
import AppModal from '../../components/AppModal';
import ProductCard from '../../components/ProductCard';
import ListContacts from '../../components/ListContacts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  layout: {
    width: wp(90),
    marginTop: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 70,
    width: wp(90),
  },
  rightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    paddingRight: 5,
  },
});

type Props = NativeStackScreenProps<CustomerNavParamList, 'Customer'>;

const Customer = ({ navigation }: Props) => {
  const isFocused = useIsFocused();
  const { screen } = useAnalytics();
  const [customerList, setCustomerList] = useState<any>([]);
  const [showCustomerOption, setShowCustomerOption] = useState<boolean>(false);
  const [showListContact, setShowListContact] = useState<boolean>(false);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [searchResult, setSearchResult] = useState<any[]>();
  const [error, setError] = useState<boolean>(false);
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [activeCustomer, setActiveCustomer] = useState<any>({});

  const {
    isLoading: isCustomersLoading,
    isError: isCustomersError,
    refetch: refetchCustomers,
    isFetching: isFetchingCustomers,
  } = useQuery(queryKeys.CUSTOMERS_DATA, async () => {
    try {
      const customers = await customerApi.getCustomers();
      setCustomerList(customers.data);
      setSearchResult(customers.data);
    } catch (error: any) {
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Sorry we cannot fetch customers now, please try again later...',
      });
      console.log(
        'Error getting customers:',
        JSON.stringify(error.response.data)
      );
    }
  });

  const handleDelete = async (id: string) => {
    Alert.alert(
      'Delete Customer',
      'Are you sure you want to delete this customer?',
      [
        {
          style: 'destructive',
          text: 'Yes',
          onPress: async () => {
            try {
              setLoading(true);
              await customerApi.deleteCustomer(id);
              await refetchCustomers();
              setLoading(false);
              return Toast.show({
                type: 'success',
                text1: 'Delete Success',
                text2: 'Customer deleted successfully!',
              });
            } catch (error: any) {
              setLoading(false);
              console.log(JSON.stringify(error.response.data));
              return Toast.show({
                type: 'error',
                text1: 'Delete Error',
                text2:
                  JSON.stringify(error.response.message) ||
                  'Error deleting your customer, try again later..',
              });
            }
          },
        },
        {
          text: 'No',
          style: 'default',
        },
      ]
    );
  };

  useEffect(() => {
    screen('Customers');
  }, []);

  useEffect(() => {
    isFocused && refetchCustomers();
  }, [isFocused]);

  const handleSearch = async (e: any) => {
    try {
      const firstNameSearch =
        customerList &&
        customerList.filter(
          (c: any) =>
            c.first_name &&
            c.first_name.trim().toLowerCase().includes(e.trim().toLowerCase())
        );

      const lastNameSearch =
        customerList &&
        customerList.filter((c: any) =>
          c.last_name
            ? c.last_name.trim().toLowerCase().includes(e.trim().toLowerCase())
            : ''
        );

      const phoneSearch =
        customerList &&
        customerList.filter((c: any) =>
          c.phone
            ? c.phone.trim().toLowerCase().includes(e.trim().toLowerCase())
            : ''
        );

      const emailSearch =
        customerList &&
        customerList.filter((c: any) =>
          c.email
            ? c.email
                .toString()
                .trim()
                .toLowerCase()
                .includes(e.trim().toLowerCase())
            : ''
        );

      if (e.length !== '') {
        setSearchResult([
          ...firstNameSearch,
          ...lastNameSearch,
          ...phoneSearch,
          ...emailSearch,
        ]);
      } else {
        setSearchResult(customerList);
      }
    } catch (error) {
      console.log('Error in search:', error);
    }
  };

  if (isCustomersError) {
    return (
      <ScreenError
        onRetry={async () => await refetchCustomers()}
        onBackPress={() => navigation.goBack()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {isCustomersLoading && <ScreenLoading />}

      <Box style={styles.header}>
        <Text variant="Body2M" color="text1">
          Customer List
        </Text>
      </Box>
      <Box style={styles.layout}>
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: wp(90),
          }}
        >
          <HeaderTextInput
            type="search"
            width={userPermissions.create_customer ? wp(75) : wp(90)}
            height={40}
            placeholder="Search"
            onChangeText={handleSearch}
          />
          {userPermissions.create_customer && (
            <TouchableOpacity onPress={() => setShowCustomerOption(true)}>
              <PlusCircleIcon />
            </TouchableOpacity>
          )}
        </Box>

        <Box style={{ height: hp(70), marginTop: 10 }}>
          <FlatList
            data={searchResult}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => {
              if (isCustomersLoading || isFetchingCustomers) {
                return null;
              } else {
                return (
                  <ListEmpty
                    invoicePage
                    topText=""
                    bottomText={
                      userPermissions.create_customer
                        ? 'Add a customer to get started'
                        : 'No Customers'
                    }
                    buttonText="Add Customer"
                    button={userPermissions.create_customer ? true : false}
                    onPressButton={() =>
                      navigation.navigate('AddCustomer', {
                        isEdit: false,
                      })
                    }
                  />
                );
              }
            }}
            keyExtractor={(item: any, index: number) => index.toString()}
            renderItem={({ item, index }) => (
              <Box
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  paddingBottom:
                    index === customerList.length - 1 ? 150 : undefined,
                }}
              >
                <CustomerList
                  colorMax={Number((Math.random() * 10).toFixed())}
                  id={item.id}
                  name={`${item.first_name} ${
                    item.last_name ? item.last_name : ''
                  }`}
                  email={item.email}
                  image={item.image}
                  phone={item.phone}
                  onPress={() =>
                    navigation.navigate('CustomerDetail', {
                      customerId: item.id,
                    })
                  }
                  handleDelete={() => handleDelete(item.id)}
                  handleEdit={() =>
                    navigation.navigate('AddCustomer', {
                      isEdit: true,
                      customer: item,
                    })
                  }
                />
              </Box>
            )}
          />
        </Box>
      </Box>

      {showListContact && (
        <ListContacts navigation={navigation} setVisible={setShowListContact} />
      )}

      <AppModal
        visible={showCustomerOption}
        setVisible={setShowCustomerOption}
        heightValue={hp(35)}
        top={100}
        content={
          <Box
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: hp(10),
              paddingHorizontal: 30,
            }}
          >
            <ProductCard
              onPress={() => {
                setShowCustomerOption(false);
                navigation.navigate('AddCustomer', {
                  isEdit: false,
                  isContacts: false,
                });
              }}
              width={wp(35)}
              height={hp(17)}
              title={'Manual'}
              paragraph={'Add customer with their data'}
            />
            <ProductCard
              onPress={() => {
                setShowCustomerOption(false);
                setShowListContact(true);
              }}
              width={wp(35)}
              height={hp(17)}
              title={'Contacts'}
              paragraph={'Add customers from your contacts'}
            />
          </Box>
        }
      />
    </Box>
  );
};

export default Customer;

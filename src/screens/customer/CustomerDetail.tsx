import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useEffect, useState } from 'react';
import { Alert, FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import { useQuery } from 'react-query';
import { useIsFocused } from '@react-navigation/native';

import theme, { Box, Text } from '../../components/Themed';
import { CustomerNavParamList } from '../../navigation/CustomerNav';
import AppAccordion from '../../components/AppAccordion';
import StackHeader from '../../components/StackHeader';
import customerApi from '../../api/riciveApi/customer';
import ScreenLoading from '../../components/ScreenLoading';
import SwitchPill from '../../components/SwitchPill';
import UpcomingOrderCard from '../../components/UpcomingOrderCard';
import InvoiceCard from '../../components/InvoiceCard';
import ListEmpty from '../../components/ListEmpty';
import orderApi from '../../api/riciveApi/order';
import invoiceApi from '../../api/riciveApi/invoice';
import ScreenError from '../../components/ScreenError';
import ItemMenuIcon from '../../svg/ItemMenuIcon';
import { useAppSelector } from '../../redux/hooks';
import queryKeys from '../../constants/queryKeys';
import CustomerOrderCard from '../../components/CustomerOrderCard';
import InvoiceItem from '../../components/InvoiceItem';
import BurgerOptions from '../../components/BurgerOptions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  content: {
    width: wp(100),
    alignSelf: 'center',
    marginTop: 10,
    paddingHorizontal: 20,
  },
  customerInfo: {
    padding: 15,
    backgroundColor: theme.colors.bg5,
    marginTop: 20,
    width: wp(100),
  },
  customerInfoText: {
    flexDirection: 'row',
    marginTop: 12,
    justifyContent: 'space-between',
  },
  subMenu: {
    width: wp(100),
    height: 55,
    backgroundColor: theme.colors.bg5,
    alignSelf: 'center',
    paddingHorizontal: 20,
    alignItems: 'center',
    marginVertical: 20,
    flexDirection: 'row',
  },
  list: {
    height: hp(55),
    width: wp(100),
    alignSelf: 'center',
  },
});

type Props = NativeStackScreenProps<CustomerNavParamList, 'CustomerDetail'>;

const CustomerDetail = ({ navigation, route }: Props): JSX.Element => {
  const isFocused = useIsFocused();
  const { customerId } = route.params;
  const [customer, setCustomer] = useState<any>();
  const [orders, setOrders] = useState<any>();
  const [invoices, setInvoices] = useState<any>();
  const [deleteLoading, setDeleteLoading] = useState<boolean>(false);
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [activeItem, setActiveItem] = useState<any>({
    name: 'Orders',
    value: 'orders',
  });
  const [selectedOrder, setSelectedOrder] = useState<'ongoing' | 'completed'>(
    'ongoing'
  );
  const [selectedInvoice, setSelectedInvoice] = useState<'paid' | 'unpaid'>(
    'paid'
  );

  const {
    isLoading: isCustomerLoading,
    isError: isCustomerError,
    refetch: refetchCustomer,
    isFetching: isFethchingCustomer,
  } = useQuery(
    queryKeys.CUSTOMERS_DATA + customerId,
    async () => {
      try {
        const customerData = await customerApi.getSingleCustomer(customerId);
        setCustomer(customerData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Sorry we cannot get customer data now, try again later...',
        });
        console.log(
          'Error getting customer data orders:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!customerId,
    }
  );

  const {
    isLoading: isOrdersLoading,
    isError: isOrdersError,
    refetch: refetchOrders,
    isFetching: isFethchingOrders,
  } = useQuery(
    queryKeys.ORDERS_DATA,
    async () => {
      try {
        const orderData = await orderApi.getOrders(customerId);
        setOrders(orderData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Sorry we cannot fetch orders now, please try again later...',
        });
        console.log(
          'Error getting merchant orders:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!customerId,
    }
  );

  const {
    isLoading: isInvoiceLoading,
    isError: isInvoiceError,
    refetch: refetchInvoice,
    isFetching: isFethchingInvoice,
  } = useQuery(
    queryKeys.INVOICE_DATA + customerId,
    async () => {
      try {
        const invoiceData = await invoiceApi.getInvoicesByCustomer(customerId);
        setInvoices(invoiceData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2:
            'Sorry we cannot fetch invoice data now, please try again later...',
        });
        console.log(
          'Error getting invoice data orders:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!customerId,
    }
  );

  const refreshData = async () => {
    await refetchCustomer();
    await refetchOrders();
    await refetchInvoice();
  };

  useEffect(() => {
    isFocused && refreshData();
  }, [isFocused]);

  const ongoingOrders =
    orders &&
    orders.filter(
      (order: any) =>
        order.status === 'IN STORE' ||
        order.status === 'PROCESSING' ||
        order.status === 'DELIVERY' ||
        order.status === 'SCHEDULED'
    );

  const completedOrders =
    orders && orders.filter((order: any) => order.is_complete === true);

  const paidInvoices =
    invoices && invoices.filter((invoice: any) => invoice.is_paid === true);
  const upPaidInvoices =
    invoices && invoices.filter((invoice: any) => invoice.is_paid === false);

  const returnData = () => {
    if (activeItem.value === 'orders') {
      if (selectedOrder === 'ongoing') {
        return ongoingOrders;
      } else {
        return completedOrders;
      }
    } else {
      if (selectedInvoice === 'paid') {
        return paidInvoices;
      } else {
        return upPaidInvoices;
      }
    }
  };

  const handleDelete = async (id: string) => {
    Alert.alert(
      'Delete Customer',
      'Are you sure you want to delete this customer?',
      [
        {
          style: 'destructive',
          text: 'Yes',
          onPress: async () => {
            try {
              setDeleteLoading(true);
              await customerApi.deleteCustomer(id);
              setDeleteLoading(false);
              navigation.navigate('Customer');
              return Toast.show({
                type: 'success',
                text1: 'Delete Success',
                text2: 'Customer deleted successfully!',
              });
            } catch (error: any) {
              setDeleteLoading(false);
              console.log(JSON.stringify(error.response.data));
              return Toast.show({
                type: 'error',
                text1: 'Delete Error',
                text2:
                  JSON.stringify(error.response.message) ||
                  'Error deleting your customer, try again later..',
              });
            }
          },
        },
        {
          text: 'No',
          style: 'default',
        },
      ]
    );
  };

  if (isCustomerError || isOrdersError || isInvoiceError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={refreshData}
      />
    );
  }

  const isLoading =
    isCustomerLoading ||
    isFethchingCustomer ||
    isOrdersLoading ||
    isFethchingOrders ||
    isInvoiceLoading ||
    isFethchingInvoice ||
    deleteLoading;

  return (
    <Box style={styles.container}>
      {isLoading && <ScreenLoading />}

      {/* Header */}
      <StackHeader
        title="Customer"
        onBackPress={() => navigation.goBack()}
        icon1={
          <TouchableOpacity
            style={{
              justifyContent: 'center',
              alignItems: 'flex-end',
              paddingTop: 10,
              padding: 10,
            }}
            onPress={() => {
              setShowMenu(!showMenu);
            }}
          >
            <ItemMenuIcon />
          </TouchableOpacity>
        }
        onPressIcon1={() => setShowMenu(!showMenu)}
      />

      {/* Burger Menu */}
      {showMenu && (
        <BurgerOptions
          onEdit={() => {
            navigation.navigate('AddCustomer', {
              isEdit: true,
              customer,
            });
          }}
          onDelete={
            userPermissions.delete_customer
              ? () => handleDelete(customerId)
              : undefined
          }
          deleteLabel="Delete"
          editLabel="Edit"
        />
      )}

      <Box style={styles.content}>
        {/* Customer Information Accordion */}
        <AppAccordion
          fullWidth
          title={`Customer's Information`}
          containerStyle={{
            paddingVertical: 10,
            width: wp(100),
            alignSelf: 'center',
          }}
          titleVariant={'Body2M'}
          titleColor={'text1'}
          content={
            <Box style={styles.customerInfo}>
              {/* Customer Name */}
              <Box style={styles.customerInfoText}>
                <Text variant={'SmallerTextR'} color="text5">
                  Customer:
                </Text>
                <Text
                  variant={'SmallerTextR'}
                  color="text6"
                  style={{
                    width: wp(60),
                    textAlign: 'right',
                  }}
                >
                  {customer &&
                    `${customer.first_name} ${
                      customer.last_name ? customer.last_name : ''
                    }`}
                </Text>
              </Box>

              {/* Customer Phone */}
              <Box style={styles.customerInfoText}>
                <Text variant={'SmallerTextR'} color="text5">
                  Phone number:
                </Text>
                <Text variant={'SmallerTextR'} color="text6">
                  {customer && customer.phone ? `${customer.phone}` : ''}
                </Text>
              </Box>

              {/* Customer Email */}
              {customer && customer.email && (
                <Box style={styles.customerInfoText}>
                  <Text variant={'SmallerTextR'} color="text5">
                    Email:
                  </Text>
                  <Text variant={'SmallerTextR'} color="text6">
                    {customer && customer.email}
                  </Text>
                </Box>
              )}

              {/* Customer Address */}
              <Box style={styles.customerInfoText}>
                <Text variant={'SmallerTextR'} color="text5">
                  Address:
                </Text>
                <Text
                  variant={'SmallerTextR'}
                  color="text6"
                  style={{
                    width: '70%',
                    textAlign: 'right',
                  }}
                >
                  {customer && customer.address}
                </Text>
              </Box>
            </Box>
          }
        />

        {/* Order / Invoice Switch */}
        <Box mt="l">
          <SwitchPill
            items={[
              {
                name: 'Orders',
                value: 'orders',
              },
              {
                name: 'Invoices',
                value: 'invoices',
              },
            ]}
            {...{ activeItem, setActiveItem }}
          />
        </Box>

        {/* Ongoing / Completed - Paid / Unpaid */}
        {activeItem.value === 'orders' ? (
          <Box style={styles.subMenu}>
            <Box>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => setSelectedOrder('ongoing')}
              >
                <Box
                  style={{
                    width: 15,
                    height: 15,
                    backgroundColor:
                      selectedOrder === 'ongoing'
                        ? theme.colors.primary1
                        : theme.colors.light3,
                  }}
                />
                <Text
                  variant={
                    selectedOrder === 'ongoing' ? 'DetailsM' : 'DetailsR'
                  }
                  color="text5"
                  ml="m"
                >
                  Ongoing Orders
                </Text>
              </TouchableOpacity>
            </Box>

            <Box ml="xxl">
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => setSelectedOrder('completed')}
              >
                <Box
                  style={{
                    width: 15,
                    height: 15,
                    backgroundColor:
                      selectedOrder === 'completed'
                        ? theme.colors.primary1
                        : theme.colors.light3,
                  }}
                />
                <Text
                  variant={
                    selectedOrder === 'completed' ? 'DetailsM' : 'DetailsR'
                  }
                  color="text5"
                  ml="m"
                >
                  Completed Orders
                </Text>
              </TouchableOpacity>
            </Box>
          </Box>
        ) : (
          <Box style={styles.subMenu}>
            <Box>
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => setSelectedInvoice('paid')}
              >
                <Box
                  style={{
                    width: 15,
                    height: 15,
                    backgroundColor:
                      selectedInvoice === 'paid'
                        ? theme.colors.primary1
                        : theme.colors.light3,
                  }}
                />
                <Text
                  variant={selectedInvoice === 'paid' ? 'DetailsM' : 'DetailsR'}
                  color="text5"
                  ml="m"
                >
                  Paid Invoices
                </Text>
              </TouchableOpacity>
            </Box>

            <Box ml="xxl">
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
                onPress={() => setSelectedInvoice('unpaid')}
              >
                <Box
                  style={{
                    width: 15,
                    height: 15,
                    backgroundColor:
                      selectedInvoice === 'unpaid'
                        ? theme.colors.primary1
                        : theme.colors.light3,
                  }}
                />
                <Text
                  variant={
                    selectedInvoice === 'unpaid' ? 'DetailsM' : 'DetailsR'
                  }
                  color="text5"
                  ml="m"
                >
                  Unpaid Invoices
                </Text>
              </TouchableOpacity>
            </Box>
          </Box>
        )}

        {/* Orders & Invoices Flatlist */}
        <Box style={styles.list}>
          <FlatList
            contentContainerStyle={{
              alignItems: 'center',
            }}
            data={returnData()}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => (
              <Box
                style={{
                  marginTop: -70,
                }}
              >
                <ListEmpty
                  bottomText={isLoading ? 'Loading' : 'No data'}
                  topText=""
                  invoicePage
                />
              </Box>
            )}
            keyExtractor={(item: any) => item.id.toString()}
            renderItem={({ item, index }) => {
              if (activeItem.value === 'orders') {
                return (
                  <Box
                    style={{
                      marginBottom:
                        index === orders.length - 1 ? 650 : undefined,
                    }}
                  >
                    <CustomerOrderCard
                      onPress={() =>
                        navigation.navigate('OrderDetails', { id: item.id })
                      }
                      id={item.id.substring(0, 5).toUpperCase()}
                      date={new Date(item.created_at).toDateString()}
                      amount={
                        item && item.invoice && item.invoice.length > 0
                          ? item.invoice[0].total
                          : ''
                      }
                    />
                  </Box>
                );
              } else {
                const firstName = item.customer_customerToinvoice.first_name
                  ? item.customer_customerToinvoice.first_name
                  : '';
                const lastName = item.customer_customerToinvoice.last_name
                  ? item.customer_customerToinvoice.last_name
                  : '';
                return (
                  <Box
                    style={{
                      paddingBottom: index === invoices.length - 1 ? 650 : 10,
                    }}
                  >
                    <InvoiceCard
                      hasBorder
                      fullWidth
                      customerDetails
                      invoiceId={`${item.id.substring(0, 5).toUpperCase()}`}
                      date={new Date(item.created_at).toDateString()}
                      firstName={firstName + ' ' + lastName}
                      total={item.total}
                      invoice={item}
                      onPress={() =>
                        navigation.navigate('InvoiceDetail', {
                          items: item.invoice_item,
                          dateCreated: new Date(item.created_at).toDateString(),
                          deliveryFee: item.delivery_fee,
                          id: item.id,
                          isSending: false,
                          serviceType: '',
                          status: item.is_paid ? 'PAID' : 'UNPAID',
                          total: item.total,
                          isInvoiceNav: false,
                          customerId: item.customer,
                          deliveryFeeType: item.delivery_type,
                          customerPhone: item.customer_customerToinvoice
                            ? item.customer_customerToinvoice.phone
                            : '',
                        })
                      }
                    />
                  </Box>
                );
              }
            }}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default CustomerDetail;

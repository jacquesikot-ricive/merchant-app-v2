import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useEffect, useState } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import { Feather as Icon } from '@expo/vector-icons';

import AboutBusinessPicker from '../../components/AboutBusinessPicker';
import Button from '../../components/Button';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import theme, { Box, Text } from '../../components/Themed';
import { GettingStartedNavParamList } from '../../navigation/GettingStartedNav';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import SelectItemModal from '../../components/SelectItemModal';
import business from '../../api/riciveApi/business';

type Props = NativeStackScreenProps<
  GettingStartedNavParamList,
  'AboutBusiness'
>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(250, 250, 250, 0.93)',
    flex: 1,
  },
  body: {
    backgroundColor: theme.colors.bg5,
    borderRadius: 8,
    flex: 1,
    alignItems: 'center',
  },
  bodyWidth: {
    width: wp(90),
  },
  title: {
    marginTop: 21,
  },
  content: {
    alignItems: 'center',
    marginTop: 24,
    marginBottom: 8,
  },
  pickerSpacing: {
    marginTop: 25,
  },
  button: {
    marginTop: 74,
    marginBottom: 50,
  },
  label: {
    flexDirection: 'row',
  },
});

const businessTypes = [
  {
    id: 1,
    label: 'Online Store',
    value: 'ONLINE_STORE',
  },
  {
    id: 2,
    label: 'Laundry',
    value: 'LAUNDRY',
  },
  {
    id: 3,
    label: 'Physical Store',
    value: 'PHYSICAL_STORE',
  },
  {
    id: 4,
    label: 'Online & Physical Store',
    value: 'PHYSICAL_ONLINE_STORE',
  },
  {
    id: 5,
    label: 'Service Based',
    value: 'SERVICE',
  },
  {
    id: 6,
    label: 'E-Commerce',
    value: 'ECOMMERCE',
  },
  { id: 7, label: 'Other', value: 'OTHER' },
];

const countryData = [
  {
    id: 1,
    label: 'Nigeria',
    value: 'Nigeria',
  },
  {
    id: 2,
    label: 'Zambia',
    value: 'Zambia',
  },
];

const states = [
  {
    id: 0,
    label: 'Abuja (FCT)',
    value: 'Abuja (FCT)',
  },
  {
    id: 1,
    label: 'Abia',
    value: 'Abia',
  },
  {
    id: 2,
    label: 'Adamawa',
    value: 'Adamawa',
  },
  {
    id: 3,
    label: 'Akwa Ibom',
    value: 'Akwa Ibom',
  },
  {
    id: 4,
    label: 'Anambra',
    value: 'Anambra',
  },
  {
    id: 5,
    label: 'Bauchi',
    value: 'Bauchi',
  },
  {
    id: 6,
    label: 'Bayelsa',
    value: 'Bayelsa',
  },
  {
    id: 7,
    label: 'Benue',
    value: 'Benue',
  },
  {
    id: 8,
    label: 'Borno',
    value: 'Borno',
  },
  {
    id: 9,
    label: 'Cross River',
    value: 'Cross River',
  },
  {
    id: 10,
    label: 'Delta',
    value: 'Delta',
  },
  {
    id: 11,
    label: 'Ebonyi',
    value: 'Ebonyi',
  },
  {
    id: 12,
    label: 'Edo',
    value: 'Edo',
  },
  {
    id: 13,
    label: 'Ekiti',
    value: 'Ekiti',
  },
  {
    id: 14,
    label: 'Enugu',
    value: 'Enugu',
  },
  {
    id: 15,
    label: 'Gombe',
    value: 'Gombe',
  },
  {
    id: 16,
    label: 'Imo',
    value: 'Imo',
  },
  {
    id: 17,
    label: 'Jigawa',
    value: 'Jigawa',
  },
  {
    id: 18,
    label: 'Kaduna',
    value: 'Kaduna',
  },
  {
    id: 19,
    label: 'Kano',
    value: 'Kano',
  },
  {
    id: 20,
    label: 'Katsina',
    value: 'Katsina',
  },
  {
    id: 21,
    label: 'Kebbi',
    value: 'Kebbi',
  },
  {
    id: 22,
    label: 'Kogi',
    value: 'Kogi',
  },
  {
    id: 23,
    label: 'Kwara',
    value: 'Kwara',
  },
  {
    id: 24,
    label: 'Lagos',
    value: 'Lagos',
  },
  {
    id: 25,
    label: 'Nasarawa',
    value: 'Nasarawa',
  },
  {
    id: 26,
    label: 'Niger',
    value: 'Niger',
  },
  {
    id: 27,
    label: 'Ogun',
    value: 'Ogun',
  },
  {
    id: 28,
    label: 'Ondo',
    value: 'Ondo',
  },
  {
    id: 29,
    label: 'Osun',
    value: 'Osun',
  },
  {
    id: 30,
    label: 'Oyo',
    value: 'Oyo',
  },
  {
    id: 31,
    label: 'Plateau',
    value: 'Plateau',
  },
  {
    id: 32,
    label: 'Rivers',
    value: 'Rivers',
  },
  {
    id: 33,
    label: 'Sokoto',
    value: 'Sokoto',
  },
  {
    id: 34,
    label: 'Taraba',
    value: 'Taraba',
  },
  {
    id: 35,
    label: 'Yobe',
    value: 'Yobe',
  },
  {
    id: 36,
    label: 'Zamfara',
    value: 'Zamfara',
  },
];

const zambianStates = [
  {
    id: 0,
    label: 'Central',
    value: 'Central',
  },
  {
    id: 1,
    label: 'Copperbelt',
    value: 'Copperbelt',
  },
  {
    id: 2,
    label: 'Eastern',
    value: 'Eastern',
  },
  {
    id: 3,
    label: 'Luapula',
    value: 'Luapula',
  },
  {
    id: 4,
    label: 'Lusaka',
    value: 'Lusaka',
  },
  {
    id: 5,
    label: 'North-Western',
    value: 'North-Western',
  },
  {
    id: 6,
    label: 'Northern',
    value: 'Northern',
  },
  {
    id: 7,
    label: 'Western',
    value: 'Western',
  },
  {
    id: 8,
    label: 'Southern',
    value: 'Southern',
  },
];

const AboutBusiness = ({ navigation, route }: Props): JSX.Element => {
  const isProfileNav =
    route && route.params && route.params.isProfileNav
      ? route.params.isProfileNav
      : false;
  const user = useAppSelector((state) => state.login.user);
  const [type, setSelectedType] = useState<any>(
    user && user.business && user.business.type
      ? businessTypes.filter((b) => b.label === user.business.type)[0]
      : businessTypes[0]
  );
  const [country, setSelectedCountry] = useState<any>(
    user && user.business && user.business.country
      ? countryData.filter((c) => c.value === user.business.country)[0]
      : countryData[0]
  );

  const [showSelectCountry, setShowSelectCountry] = useState<boolean>(false);

  const correctDistrict =
    country.label === 'Nigeria' ? states[1] : zambianStates[1];

  const [district, setSelectedDistrict] = useState<any>(
    user && user.business && user.business.state
      ? [...zambianStates, ...states].filter(
          (c: any) => c.value === user.business.state
        )[0]
      : correctDistrict
  );
  const [city, setSelectedCity] = useState<string>(
    user && user.business && user.business.city ? user.business.city : ''
  );
  const [loading, setLoading] = useState<boolean>(false);
  // const [statesLoading, setStatesLoading] = useState<boolean>(false);
  const [avoidingView, setAvoidingView] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [allStates, setAllStates] = useState<any[]>([]);
  const [showSelectedState, setShowSelectedState] = useState<boolean>(false);
  const [showSelectedType, setShowSelectedType] = useState<boolean>(false);

  const handleRegisterBusiness = () => {
    if (type && type.label.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Business type cannot be empty!',
        text2: 'Select business type to continue..',
      });
    }

    if (country && country.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Country cannot be empty!',
        text2: 'Select country to continue..',
      });
    }

    return navigation.navigate('MoreBusiness', {
      type: type.label,
      country: country.label,
      district: district.label,
      city,
      isProfileNav: isProfileNav ? isProfileNav : false,
    });
  };

  if (user && user.business && !user.business.country) {
    useEffect(() => {
      if (country.label === 'Nigeria') {
        setSelectedDistrict(states[0]);
      } else {
        setSelectedDistrict(zambianStates[0]);
      }
    }, [country]);
  }

  return (
    <Box style={styles.container}>
      {/* {statesLoading && <ScreenLoading />} */}
      <Box>
        <StackHeader
          title=""
          onBackPress={() => navigation.goBack()}
          hasBorder
        />
      </Box>
      <ScrollView bounces={false}>
        <KeyboardAvoidingView
          style={styles.body}
          behavior={Platform.OS === 'ios' ? 'position' : 'padding'}
          enabled={avoidingView}
        >
          <Box>
            <Box style={styles.bodyWidth}>
              <Box style={styles.title}>
                <Text variant="DetailsM" color="text1">
                  Tell us a bit about your business.
                </Text>
              </Box>
              <Box style={styles.content}>
                {/* <Box>
                  <AboutBusinessPicker
                    label="What type of business do you own?"
                    placeholder={
                      user.business.type
                        ? user.business.type
                        : 'Choose an option'
                    }
                    data={businessTypes}
                    setValue={setSelectedType}
                    value={type}
                  />
                </Box> */}

                <Box>
                  <Box style={styles.label}>
                    <Text variant="DetailsR" color="text1" mb="m">
                      What type of business do you own
                    </Text>
                    <Text color="error">*</Text>
                  </Box>

                  <TouchableOpacity
                    onPress={() => setShowSelectedType(true)}
                    activeOpacity={0.6}
                    style={{
                      width: wp(90),
                      height: 58,
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: theme.colors.border1,
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingHorizontal: 20,
                      flexDirection: 'row',
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'basier-regular',
                        fontSize: 14,
                        color: theme.colors.text5,
                      }}
                    >
                      {type ? type.label : 'Choose an option'}
                    </Text>

                    <Icon
                      name="chevron-down"
                      size={20}
                      color={theme.colors.border}
                    />
                  </TouchableOpacity>
                </Box>

                <Box style={styles.pickerSpacing}>
                  {/* <AboutBusinessPicker
                    label="What country is your business based?"
                    placeholder={
                      user.business.country
                        ? user.business.country
                        : 'Choose an option'
                    }
                    data={countryRole}
                    setValue={setSelectedCountry}
                    value={country}
                  /> */}
                  <Box style={styles.label}>
                    <Text variant="DetailsR" color="text1" mb="m">
                      Country
                    </Text>
                    <Text color="error">*</Text>
                  </Box>

                  <TouchableOpacity
                    onPress={() => setShowSelectCountry(true)}
                    activeOpacity={0.6}
                    style={{
                      width: wp(90),
                      height: 58,
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: theme.colors.border1,
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingHorizontal: 20,
                      flexDirection: 'row',
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'basier-regular',
                        fontSize: 14,
                        color: theme.colors.text5,
                      }}
                    >
                      {country ? country.label : 'Select Countrye'}
                    </Text>

                    <Icon
                      name="chevron-down"
                      size={20}
                      color={theme.colors.border}
                    />
                  </TouchableOpacity>
                </Box>
                <Box style={[styles.pickerSpacing]}>
                  <Box style={styles.label}>
                    <Text variant="DetailsR" color="text1" mb="m">
                      State
                    </Text>
                    <Text color="error">*</Text>
                  </Box>

                  <TouchableOpacity
                    onPress={() => setShowSelectedState(true)}
                    activeOpacity={0.6}
                    style={{
                      width: wp(90),
                      height: 58,
                      borderRadius: 8,
                      borderWidth: 1,
                      borderColor: theme.colors.border1,
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingHorizontal: 20,
                      flexDirection: 'row',
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: 'basier-regular',
                        fontSize: 14,
                        color: theme.colors.text5,
                      }}
                    >
                      {district ? district.label : 'Set District'}
                    </Text>

                    <Icon
                      name="chevron-down"
                      size={20}
                      color={theme.colors.border}
                    />
                  </TouchableOpacity>
                </Box>
                <Box style={[styles.pickerSpacing, { marginTop: 20 }]}>
                  <Box style={styles.label}>
                    <Text variant="DetailsR" color="text1" mb="m">
                      City
                    </Text>
                    {/* <Text color="error">*</Text> */}
                  </Box>

                  <TextInput
                    type="input"
                    placeholder={'Enter Your City'}
                    autoCorrect={false}
                    autoCapitalize="words"
                    keyboardType="default"
                    returnKeyType="next"
                    value={city}
                    onChangeText={(e) => setSelectedCity(e)}
                    onFocus={() => setAvoidingView(true)}
                    onBlur={() => setAvoidingView(false)}
                  />
                </Box>
              </Box>
            </Box>
            <Box style={styles.button}>
              <Button
                type="primary"
                label="Continue"
                onPress={handleRegisterBusiness}
                disabled={
                  type &&
                  type.label.length > 0 &&
                  country.label.length > 0 &&
                  district &&
                  district.label.length > 0 &&
                  city.length > 0
                    ? false
                    : true
                }
                loading={loading}
              />
            </Box>
          </Box>
        </KeyboardAvoidingView>
      </ScrollView>

      <SelectItemModal
        visible={showSelectedState}
        setVisible={setShowSelectedState}
        items={country.label === 'Nigeria' ? states : zambianStates}
        title="Select State"
        selectedItems={district}
        setSelectedItems={setSelectedDistrict}
        hasSearch
      />

      <SelectItemModal
        visible={showSelectCountry}
        setVisible={setShowSelectCountry}
        items={countryData}
        title="Select Country"
        selectedItems={country}
        setSelectedItems={setSelectedCountry}
      />

      <SelectItemModal
        visible={showSelectedType}
        setVisible={setShowSelectedType}
        items={businessTypes}
        title="Select Business Type"
        selectedItems={type}
        setSelectedItems={setSelectedType}
      />
    </Box>
  );
};

export default AboutBusiness;

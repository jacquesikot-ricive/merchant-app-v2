import React, { useEffect, useState } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import GettingStartedCard from '../../components/GettingStartedCard';
import Toast from 'react-native-toast-message';

import StackHeader from '../../components/StackHeader';
import theme, { Box, Text } from '../../components/Themed';
import { GettingStartedNavParamList } from '../../navigation/GettingStartedNav';
import metricsApi from '../../api/riciveApi/metrics';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import queryKeys from '../../constants/queryKeys';
import { useQuery } from 'react-query';

type Props = NativeStackScreenProps<
  GettingStartedNavParamList,
  'GettingStarted'
>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(250, 250, 250, 0.93)',
    flex: 1,
  },
});

const GettingStarted = ({ navigation }: Props): JSX.Element => {
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [onboardingStatus, setOnboardingStatus] = useState<string>('');
  const [onboardingArray, setOnboardingArray] = useState<number[]>([]);

  const {
    isLoading: isMetricsLoading,
    isError: isMetricsError,
    refetch: refetchMetrics,
  } = useQuery(
    queryKeys.ANALYTICS_DATA,
    async () => {
      const metricsData = await metricsApi.getData(business_id);
      const businessIsSetup = metricsData.data.hasCompleteBusiness ? 1 : 0;
      const productIsSetup = metricsData.data.hasProducts ? 1 : 0;

      const onboardingValue = [businessIsSetup, productIsSetup].reduce(
        (a, b) => a + b
      );

      setOnboardingStatus(((onboardingValue / 2) * 100).toFixed().toString());
      setOnboardingArray([businessIsSetup, productIsSetup]);
    },
    {
      enabled: !!business_id,
    }
  );

  useEffect(() => {
    navigation.addListener('focus', async () => {
      await refetchMetrics();
    });

    return () => {
      navigation.removeListener('focus', async () => {
        await refetchMetrics();
      });
    };
  }, []);

  if (isMetricsError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => {
          await refetchMetrics();
        }}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {isMetricsLoading && <ScreenLoading />}
      <Box>
        <StackHeader
          title=""
          onBackPress={() => navigation.goBack()}
          hasBorder
        />
      </Box>
      <Box
        style={{ backgroundColor: theme.colors.bg5, borderRadius: 8, flex: 1 }}
      >
        <Box
          style={{
            height: 120,
            width: wp(100),
            marginTop: 19,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 20,
          }}
        >
          <Box>
            <Text variant="DetailsM" color="text1">
              Getting started
            </Text>
            <Box style={{ width: 342 }}>
              <Text variant="SmallerTextR" color="text7">
                Great to have you onboard! Feel free to explore, or get a{`\n`}
                head start below.
              </Text>
            </Box>
            <Box>
              <Text variant="SmallerTextR" color="secondary5">
                {`${onboardingStatus}%`} complete
              </Text>
            </Box>
          </Box>
        </Box>
        <Box style={{ marginTop: 20, alignItems: 'center' }}>
          <GettingStartedCard
            active={onboardingArray[0] === 1 ? true : false}
            title={'Tell us about your business'}
            description={
              'Provide us with your business information to help personalize your experience.'
            }
            onPress={() =>
              navigation.navigate('AboutBusiness', {
                isProfileNav: false,
              })
            }
          />
          {/* <GettingStartedCard
            active={onboardingArray[1] === 1 ? true : false}
            title={'Add service(s)'}
            description="Start by adding your services"
            onPress={() =>
              navigation.dispatch(
                CommonActions.navigate({
                  name: 'ServicesNav',
                })
              )
            }
          /> */}
          <GettingStartedCard
            active={onboardingArray[1] === 1 ? true : false}
            title={'Add product(s)'}
            description="Don’t start empty, begin adding your products & price into database."
            onPress={() =>
              navigation.navigate('AddProduct', {
                isGettingStartedNav: true,
              })
            }
          />
          {/* <GettingStartedCard
            active={onboardingArray[2] === 1 ? true : false}
            title={'Setup your website'}
            description="Begin increasing your reach to more customers."
            onPress={() =>
              navigation.dispatch(
                CommonActions.navigate({
                  name: 'StoreFrontNav',
                })
              )
            }
          /> */}
        </Box>
      </Box>
    </Box>
  );
};

export default GettingStarted;

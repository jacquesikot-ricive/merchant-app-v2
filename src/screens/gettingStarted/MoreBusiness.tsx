import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState, useEffect } from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import Button from '../../components/Button';
import getBusinessApi from '../../api/riciveApi/business';

import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import theme, { Box, Text } from '../../components/Themed';
import { GettingStartedNavParamList } from '../../navigation/GettingStartedNav';
import { useAppDispatch, useAppSelector } from '../../redux/hooks';
import currencyApi from '../../api/riciveApi/currency';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import storefrontApi from '../../api/riciveApi/storefront';
import { setUser } from '../../redux/login';
import PhoneTextInput from '../../components/PhoneTextInput';

type Props = NativeStackScreenProps<GettingStartedNavParamList, 'MoreBusiness'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(250, 250, 250, 0.93)',
    flex: 1,
  },
  layout: {
    backgroundColor: theme.colors.bg5,
    borderRadius: 8,
    flex: 1,
    alignItems: 'center',
  },
  content: {
    width: wp(90),
  },
  body: {
    alignItems: 'center',
    marginTop: 24,
  },
  title: {
    marginTop: 21,
  },
  inputText: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  inputSpacing: {
    marginTop: 8,
  },
  pickerSpacing: {
    marginTop: 15,
  },
  button: {
    marginTop: 74,
  },
  url: {
    marginTop: 19,
  },
  urlText: {
    marginBottom: 10,
  },
});

const bill = [
  {
    label: '',
    value: '',
  },
  {
    label: 'Naira (NGN)',
    value: 'Naira (NGN)',
  },
  {
    label: 'Zambian Kwacha (ZK)',
    value: 'Zambian Kwacha (ZK)',
  },
];

const MoreBusiness = ({ navigation, route }: Props): JSX.Element => {
  const dispatch = useAppDispatch();
  const { type, country, district, city, isProfileNav } = route.params;
  const businessOwner = useAppSelector((state) => state.login.user.business.id);
  const user = useAppSelector((state) => state.login.user);
  const [business_name, setBusinessName] = useState<string>('');
  const [currencies, setCurrencies] = useState<any[]>([]);
  const [address, setAddress] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [selectedTimeFrame, setSelectedTimeFrame] = useState<any>();
  const [loading, setLoading] = useState<boolean>(false);
  const [currencyLoading, setCurrencyLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [currencyId, setCurrecyId] = useState<string>('');
  const [avoidingView, setAvoidingView] = useState<boolean>(false);
  const [domain, setDomain] = useState<string>('');

  const handleRegisterProfile = async () => {
    if (!isProfileNav) {
      if (domain.length < 1) {
        return Toast.show({
          type: 'error',
          text1: 'Website name cannot be empty!',
          text2: 'Enter website name to continue..',
        });
      }
    }
    if (business_name.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Business name cannot be empty!',
        text2: 'Enter business name to continue..',
      });
    }
    if (address.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Address cannot be empty!',
        text2: 'Enter address to continue..',
      });
    }

    if (phone.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Phone number cannot be empty!',
        text2: 'Enter phone number to continue..',
      });
    }

    if (currencyId && currencyId.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Currency cannot be empty!',
        text2: 'Enter currency to continue..',
      });
    }

    try {
      setLoading(true);

      await getBusinessApi.updateBusiness({
        type,
        country,
        city,
        business_name,
        currency:
          country === 'Nigeria'
            ? currencies.filter((c) => c.short_code === 'NGN')[0].id
            : currencies.filter((c) => c.short_code === 'ZK')[0].id,
        state: district,
        address,
        phone:
          country === 'Nigeria' ? `+234${phone.substring(1)}` : `+26${phone}`,
        id: businessOwner,
      });

      if (!isProfileNav) {
        await storefrontApi.createStorefront({
          domain,
          tagline: 'A Ricive Business',
        });
      }

      dispatch(
        setUser({
          ...user,
          business: {
            ...user.business,
            currency:
              country === 'Nigeria'
                ? currencies.filter((c) => c.short_code === 'NGN')[0].id
                : currencies.filter((c) => c.short_code === 'ZK')[0].id,
            business_name,
            city,
            type,
          },
        })
      );

      setLoading(false);

      if (!isProfileNav) {
        navigation.navigate('GettingStarted');
      } else {
        navigation.navigate('ProfileHome');
      }
      return Toast.show({
        type: 'success',
        text1: 'Business Updated!',
        text2: 'Your business setup is complete',
      });
    } catch (error: any) {
      console.log(
        'Error registering business',
        JSON.stringify(error.response.data)
      );
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: JSON.stringify(error.response.data.message) || 'Server Error!',
        text2: 'We cannot process your request now...',
      });
    }
  };

  const getCurrencies = async () => {
    try {
      setError(false);
      setCurrencyLoading(true);
      const currencyData = await currencyApi.getData();

      setCurrencies(currencyData.data);
      setCurrencyLoading(false);
    } catch (error: any) {
      console.log(JSON.stringify(error.response.data));
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot get currency data',
      });
    }
  };

  useEffect(() => {
    getCurrencies();
  }, []);

  if (currencyLoading) {
    return <ScreenLoading />;
  }

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={getCurrencies}
      />
    );
  }

  return (
    <ScrollView bounces={false} style={styles.container}>
      <Box style={{ marginLeft: -10 }}>
        <StackHeader title="" onBackPress={() => navigation.goBack()} />
      </Box>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'position' : 'padding'}
        enabled={avoidingView}
      >
        <Box style={styles.layout}>
          <Box style={styles.content}>
            <Box style={styles.title}>
              <Text variant="Body2M" color="text1">
                Great! Now let's set your store up.
              </Text>
            </Box>

            <Box style={styles.body}>
              {!isProfileNav && (
                <Box style={styles.url}>
                  <Box style={styles.urlText}>
                    <Text variant="DetailsR" color="text6">
                      Store domain URL
                    </Text>
                  </Box>
                  <Box
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <Box
                      style={{
                        height: 56,
                        width: wp(15),
                        borderTopLeftRadius: 6,
                        borderBottomLeftRadius: 6,
                        justifyContent: 'center',
                        alignItems: 'flex-end',
                        backgroundColor: theme.colors.bg5,
                        borderLeftWidth: 1,
                        borderTopWidth: 1,
                        borderBottomWidth: 1,
                        borderColor: theme.colors.border1,
                      }}
                    >
                      <Text variant={'DetailsR'} color="text5" mr="s">
                        www.
                      </Text>
                    </Box>
                    <TextInput
                      placeholder={'domain'}
                      type="input"
                      width={wp(45)}
                      noBorderRadius
                      autoCapitalize="none"
                      autoCompleteType="off"
                      keyboardType="url"
                      autoCorrect={false}
                      onChangeText={(e) => setDomain(e)}
                    />
                    <Box
                      style={{
                        height: 56,
                        width: wp(31),
                        borderTopRightRadius: 6,
                        borderBottomRightRadius: 6,
                        justifyContent: 'center',
                        backgroundColor: theme.colors.bg5,
                        borderRightWidth: 1,
                        borderTopWidth: 1,
                        borderBottomWidth: 1,
                        borderColor: theme.colors.border1,
                      }}
                    >
                      <Text variant={'DetailsR'} color="text5" ml="s">
                        .ricive.com
                      </Text>
                    </Box>
                  </Box>
                </Box>
              )}

              <Box style={styles.inputSpacing}>
                <Box style={styles.inputText}>
                  <Text variant="DetailsR" color="text1">
                    Business name
                  </Text>
                  <Text variant="DetailsR" color="error">
                    *
                  </Text>
                </Box>

                <Box>
                  <TextInput
                    placeholder={
                      user.business.business_name
                        ? user.business.business_name
                        : 'Enter your business name'
                    }
                    type="input"
                    autoCorrect={false}
                    autoCapitalize="words"
                    keyboardType="default"
                    returnKeyType="next"
                    onChangeText={(e) => setBusinessName(e)}
                  />
                </Box>
              </Box>

              <Box style={styles.inputSpacing}>
                <Box style={styles.inputText}>
                  <Text variant="DetailsR" color="text1">
                    Business phone number
                  </Text>
                  <Text variant="DetailsR" color="error">
                    *
                  </Text>
                </Box>

                <PhoneTextInput
                  onChangeFormattedText={(t: string) => setPhone(t)}
                  value={phone.substring(4)}
                />
              </Box>

              <Box style={styles.inputSpacing}>
                <Box style={styles.inputText}>
                  <Text variant="DetailsR" color="text1">
                    Business address
                  </Text>
                  <Text variant="DetailsR" color="error">
                    *
                  </Text>
                </Box>
                <Box>
                  <TextInput
                    placeholder={
                      user.business.address
                        ? user.business.address
                        : 'Enter your address'
                    }
                    type="input"
                    autoCorrect={false}
                    autoCapitalize="words"
                    height={90}
                    keyboardType="default"
                    returnKeyType="next"
                    multiline
                    onFocus={() => setAvoidingView(true)}
                    onBlur={() => setAvoidingView(false)}
                    onChangeText={(e) => setAddress(e)}
                  />
                </Box>
              </Box>
              {/* <Box style={styles.inputSpacing}>
                <Box style={styles.inputText} mt="l">
                  <Text variant="DetailsR" color="text1">
                    Business phone number
                  </Text>
                  <Text variant="DetailsR" color="error">
                    *
                  </Text>
                </Box>
                <Box>
                  <TextInput
                    placeholder={
                      user.business.phone
                        ? user.business.phone
                        : '080-XXX-XXX-XX'
                    }
                    type="number"
                    autoCompleteType="tel"
                    keyboardType="phone-pad"
                    onFocus={() => setAvoidingView(true)}
                    onBlur={() => setAvoidingView(false)}
                    onChangeText={(e) => setPhone(e)}
                  />
                </Box>
              </Box> */}
              {/* <Box style={styles.pickerSpacing}>
                <AboutBusinessPicker
                  label="How would you like to bill your customers?"
                  placeholder={
                    user.business.currency
                      ? user.business.currency
                      : 'Choose an option'
                  }
                  data={[
                    {
                      label: '',
                      value: '',
                    },
                    ...currencies.map((c) => {
                      return {
                        label: c.name,
                        value: c.name,
                        id: c.id,
                      };
                    }),
                  ]}
                  setValue={setSelectedTimeFrame}
                  value={selectedTimeFrame}
                  setSelectedId={setCurrecyId}
                  optional
                />
              </Box> */}
            </Box>
          </Box>
          <Box style={styles.button} mb="xxxl">
            <Button
              type="primary"
              label="Set Up"
              onPress={handleRegisterProfile}
              loading={loading}
              disabled={
                type.length > 0 &&
                country.length > 0 &&
                district.length > 0 &&
                city.length > 0 &&
                business_name.length > 0 &&
                address.length > 0 &&
                phone.length > 0
                  ? false
                  : true
              }
            />
          </Box>
        </Box>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default MoreBusiness;

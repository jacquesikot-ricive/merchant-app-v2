import React, { FC, useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../../components/Themed';
import StackHeader from '../../components/StackHeader';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HomeNavParamList } from '../../navigation/HomeNav';
import supabase from '../../../supabase';
import db from '../../api/db';
import { useAppSelector } from '../../redux/hooks';
import order from '../../api/orders/order';
import customer from '../../api/customer/customer';
import Toast from 'react-native-toast-message';
import ScreenLoading from '../../components/ScreenLoading';
import TextInput from '../../components/HeaderTextInput';
import invoiceApi from '../../api/riciveApi/invoice';
import InvoiceCard from '../../components/InvoiceCard';
import ListEmpty from '../../components/ListEmpty';
import DueInvoiceCard from '../../components/DueInvoiceCard';
import numberWithCommas from '../../utils/numbersWithComma';
import { useQuery } from 'react-query';
import queryKeys from '../../constants/queryKeys';
import useCurrency from '../../hooks/useCurrency';
import { useIsFocused } from '@react-navigation/native';
import ScreenError from '../../components/ScreenError';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  list: {
    height: hp(78),
  },
  content: {
    alignItems: 'center',
  },
  dataBox: {
    width: wp(42),
    height: 68,
    borderRadius: 8,
    backgroundColor: theme.colors.white,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
});

type Props = NativeStackScreenProps<HomeNavParamList, 'DueInvoices'>;

const DueInvoices: FC<Props> = ({ navigation }: Props) => {
  const { returnCurrency } = useCurrency();
  const isFocused = useIsFocused();

  const [invoices, setInvoices] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [searchResult, setSearchResult] = useState<any[]>([]);
  const [invoiceTotal, setInvoiceTotal] = useState<string>('');
  const [unpaidInvoices, setUnpaidInvoices] = useState<any[]>([]);

  const {
    isLoading: isInvoiceLoading,
    isError: isInvoiceError,
    refetch: refetchInvoice,
    data: invoiceApiData,
  } = useQuery(queryKeys.INVOICE_DATA, async () => {
    try {
      const invoiceData = await invoiceApi.getInvoices({
        order: undefined,
        service: undefined,
      });
      const unpaidInvoices = invoiceData.data
        .filter((i: any) => i.is_paid === false)
        .sort((a: any, b: any) => a.created_at - b.created_at);
      setInvoices(unpaidInvoices);
      setSearchResult(unpaidInvoices);
      setUnpaidInvoices(unpaidInvoices);
    } catch (error: any) {
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Sorry we cannot fetch invoices now, please try again later...',
      });
      console.log(
        'Error getting invoice data:',
        JSON.stringify(error.response.data)
      );
    }
  });

  const calculateUnPaidTotal = () => {
    const value = searchResult
      .map((item: any) => Number(item.total))
      .reduce((a: any, b: any) => a + b, 0);
    setInvoiceTotal(value.toString());
  };

  useEffect(() => {
    calculateUnPaidTotal();
  }, [searchResult]);

  useEffect(() => {
    isFocused && refetchInvoice();
  }, [isFocused]);

  // search filter method
  const handleSearch = async (e: any) => {
    try {
      const firstNameSearch =
        unpaidInvoices &&
        unpaidInvoices.filter(
          (c: any) =>
            c.customer_customerToinvoice.first_name &&
            c.customer_customerToinvoice.first_name
              .trim()
              .toLowerCase()
              .includes(e.trim().toLowerCase())
        );

      const lastNameSearch =
        unpaidInvoices &&
        unpaidInvoices.filter((c: any) =>
          c.customer_customerToinvoice.last_name
            ? c.customer_customerToinvoice.last_name
                .trim()
                .toLowerCase()
                .includes(e.trim().toLowerCase())
            : ''
        );

      const phoneSearch =
        unpaidInvoices &&
        unpaidInvoices.filter((c: any) =>
          c.customer_customerToinvoice.phone
            ? c.customer_customerToinvoice.phone
                .trim()
                .toLowerCase()
                .includes(e.trim().toLowerCase())
            : ''
        );

      const idSearch =
        unpaidInvoices &&
        unpaidInvoices.filter((c: any) =>
          c.id
            ? c.id
                .toString()
                .trim()
                .toLowerCase()
                .includes(e.trim().toLowerCase())
            : ''
        );

      if (e.length !== '') {
        setSearchResult([
          ...firstNameSearch,
          ...lastNameSearch,
          ...phoneSearch,
          ...idSearch,
        ]);
      } else {
        setSearchResult(unpaidInvoices);
      }
    } catch (error) {
      console.log('Error in search:', error);
    }
  };

  if (isInvoiceError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={refetchInvoice}
      />
    );
  }

  return (
    <Box style={styles.container}>
      <StackHeader
        title="Unpaid Invoices"
        onBackPress={() => navigation.goBack()}
      />
      <Box style={styles.content}>
        <TextInput
          type={'search'}
          placeholder="Search customer name or phone"
          onChangeText={(e) => handleSearch(e)}
        />

        <Box style={styles.list}>
          <FlatList
            contentContainerStyle={{
              paddingBottom: 100,
            }}
            showsVerticalScrollIndicator={false}
            data={searchResult}
            keyExtractor={(item, index) => index.toString()}
            ListHeaderComponent={() => (
              // Invoice Data Cards
              <Box
                flexDirection={'row'}
                justifyContent={'space-between'}
                alignItems={'center'}
                style={{
                  width: wp(100),
                  paddingHorizontal: 20,
                  marginVertical: 10,
                  backgroundColor: theme.colors.bg12,
                  paddingVertical: 15,
                }}
              >
                <Box style={styles.dataBox}>
                  <Text variant={'SmallerTextR'} color="text5">
                    {'Total Amount'}
                  </Text>
                  <Text>
                    <Text variant={'Body1B'} color="text1">
                      {numberWithCommas(invoiceTotal)}
                    </Text>
                    <Text variant={'SmallerTextR'} color="text1">
                      {` (${returnCurrency().code})`}
                    </Text>
                  </Text>
                </Box>

                <Box style={styles.dataBox}>
                  <Text variant={'SmallerTextR'} color="text5">
                    {`Total Unpaid Invoices`}
                  </Text>
                  <Text variant={'Body1B'} color="text1">
                    {searchResult.length}
                  </Text>
                </Box>
              </Box>
            )}
            ListEmptyComponent={() => {
              if (isInvoiceLoading) {
                return <ActivityIndicator />;
              }
              return (
                <ListEmpty
                  topText=""
                  bottomText={`No Due Invoice`}
                  invoicePage
                />
              );
            }}
            renderItem={({ item, index }) => (
              <Box
                style={{
                  paddingTop: 15,
                  width: wp(100),
                  alignSelf: 'center',
                }}
              >
                <InvoiceCard
                  hasPadding
                  hasBorder
                  invoice={item}
                  onPress={() =>
                    navigation.navigate('InvoiceDetail', {
                      items: item.invoice_item,
                      dateCreated: new Date(item.created_at).toDateString(),
                      deliveryFee: item.delivery_fee,
                      id: item.id,
                      userId: item.customer,
                      isSending: false,
                      serviceType: '',
                      status: item.is_paid ? 'PAID' : 'UNPAID',
                      total: item.total,
                      isInvoiceNav: false,
                      customerId: item.customer,
                      deliveryFeeType: item.delivery_type,
                      customerPhone: item.customer_customerToinvoice
                        ? item.customer_customerToinvoice.phone
                        : '',
                    })
                  }
                  customerDetails
                  invoiceId={`${item.id.substring(0, 5).toUpperCase()}`}
                  date={new Date(item.created_at).toDateString()}
                  total={item.total}
                  firstName={
                    item.customer_customerToinvoice
                      ? item.customer_customerToinvoice.first_name
                      : ''
                  }
                  lastName={
                    item.customer_customerToinvoice
                      ? item.customer_customerToinvoice.last_name
                      : ''
                  }
                />
              </Box>
            )}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default DueInvoices;

import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../../components/Themed';
import StackHeader from '../../components/StackHeader';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HomeNavParamList } from '../../navigation/HomeNav';
import ListEmpty from '../../components/ListEmpty';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  title: {
    width: wp(90),
    paddingHorizontal: 20
  }
});

type Props = NativeStackScreenProps<HomeNavParamList, 'FailedPayments'>;

const FailedPayments: FC<Props> = ({ navigation }: Props) => {
  return (
    <Box style={styles.container}>
      <StackHeader
        title=""
        onBackPress={() => navigation.goBack()}
      />
      <Text variant="TitleSB" color="text6" style={styles.title}>
            Failed Payments
      </Text>
      <Box>
      <ListEmpty
        topText="Falied Payments"
        bottomText={`No Failed Payments`}
        />
      </Box>

    </Box>
  );
};

export default FailedPayments;

import React, { useState, useEffect } from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import { CommonActions } from '@react-navigation/native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import Toast from 'react-native-toast-message';
import { useAnalytics } from '@segment/analytics-react-native';
import { useDispatch } from 'react-redux';
import { useQuery } from 'react-query';
import { AntDesign } from '@expo/vector-icons';

import theme, { Box, Text } from '../../components/Themed';
import StackHeader from '../../components/StackHeader';
import NotificationIcon from '../../../svgs/NotificationIcon';
import HomePicker from '../../components/HomePicker';
import HomeCard from '../../components/HomeCard';
import ReminderActions from '../../components/ReminderActions';
import { HomeNavParamList } from '../../navigation/HomeNav';
import { useAppSelector } from '../../redux/hooks';
import metricsApi from '../../api/riciveApi/metrics';
import numberWithCommas from '../../utils/numbersWithComma';
import useNotifications from '../../hooks/useNotifications';
import orderApi from '../../api/riciveApi/order';
import { setNewOrders } from '../../redux/app';
import useCurrency from '../../hooks/useCurrency';
import queryKeys from '../../constants/queryKeys';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.bg1,
  },
  content: {
    alignItems: 'center',
  },
  icon: {
    marginTop: 48,
    paddingRight: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: wp(85),
    marginTop: 10,
  },
  title: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: wp(90),
  },
  picker: {
    marginTop: -50,
  },
  dashboard: {
    backgroundColor: theme.colors.bg5,
    marginTop: 7,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  dashboardCard: {
    width: wp(100),
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingVertical: 10,
  },
  reminderTitle: {
    width: wp(90),
    paddingHorizontal: 20,
    marginBottom: 8,
    marginTop: 8,
  },
  reminder: {
    // backgroundColor: theme.colors.white,
    // borderRadius: 8,
  },
});

const roles = [
  {
    label: 'All Time',
    value: 'All Time',
  },
  {
    label: 'Today',
    value: 'Daily',
  },
  {
    label: 'This Week',
    value: 'Weekly',
  },
  {
    label: 'This Month',
    value: 'Monthly',
  },
  {
    label: 'This Year',
    value: 'Yearly',
  },
];

type Props = NativeStackScreenProps<HomeNavParamList, 'HomeScreen'>;

const Home = ({ navigation }: Props) => {
  useNotifications();
  const dispatch = useDispatch();
  const { screen } = useAnalytics();
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const { returnCurrency } = useCurrency();
  const [selectedTimeFrame, setSelectedTimeFrame] = useState<
    'All Time' | 'Daily' | 'Weekly' | 'Monthly' | 'Yearly'
  >('All Time');
  const [loading, setLoading] = useState<boolean>(false);
  const [refreshing, setRefreshing] = useState<boolean>(false);
  const [metrics, setMetrics] = useState<any>();
  const [onboardingStatus, setOnboardingStatus] = useState<string>('');
  const [showBusinessModal, setShowBusinessModal] = useState<boolean>(false);

  const {
    isLoading: isMetricsLoading,
    isError: isMetricsError,
    data: metricsData,
    refetch: refetchMetrics,
  } = useQuery(
    queryKeys.ANALYTICS_DATA,
    async () => {
      try {
        const metricsData = await metricsApi.getData(business_id);

        const businessIsSetup = metricsData.data.hasCompleteBusiness ? 1 : 0;
        const productIsSetup = metricsData.data.hasProducts ? 1 : 0;

        // Fetch counts
        const newOrders =
          ordersData &&
          ordersData.data
            .filter((order: any) => order.is_accepted === false)
            .sort((a: any, b: any) => {
              return (
                new Date(b.created_at).getTime() -
                new Date(a.created_at).getTime()
              );
            });

        dispatch(setNewOrders(newOrders && newOrders.length.toString()));

        const onboardingValue = [businessIsSetup, productIsSetup].reduce(
          (a, b) => a + b
        );

        setOnboardingStatus(((onboardingValue / 2) * 100).toFixed().toString());

        setMetrics(metricsData.data);
      } catch (error: any) {
        // Toast.show({
        //   type: 'error',
        //   text1: 'Server Error',
        //   text2: 'Error getting business metrics',
        // });
        console.log(error);
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const {
    isLoading: isOrdersLoading,
    isError: isOrdersError,
    data: ordersData,
    refetch: refetchOrders,
  } = useQuery(queryKeys.ORDERS_DATA, async () => await orderApi.getOrders());

  const onRefresh = async () => {
    await refetchMetrics();
    await refetchOrders();
  };

  const returnAnalytics = (selectedTime: string) => {
    if (selectedTime === 'All Time') {
      return {
        revenue: {
          total: (metrics && metrics.revenue.total) || '0',
          difference: '0',
        },
        customer: {
          total: (metrics && metrics.customer.total) || '0',
          difference: '0',
        },
        invoice: {
          total: (metrics && metrics.invoice.total) || '0',
          difference: '0',
        },
        unpaidInvoice: {
          total: (metrics && metrics.unpaid_invoice.total) || '0',
          difference: '0',
        },
      };
    }

    if (selectedTime === 'Daily') {
      return {
        revenue: {
          total: (metrics && metrics.revenue.daily.total) || '0',
          difference: (metrics && metrics.revenue.daily.difference) || '0',
        },
        customer: {
          total: (metrics && metrics.customer.daily.total) || '0',
          difference: (metrics && metrics.customer.daily.difference) || '0',
        },
        invoice: {
          total: (metrics && metrics.invoice.daily.total) || '0',
          difference: (metrics && metrics.invoice.daily.difference) || '0',
        },
        unpaidInvoice: {
          total: (metrics && metrics.unpaid_invoice.daily.total) || '0',
          difference:
            (metrics && metrics.unpaid_invoice.daily.difference) || '0',
        },
      };
    }

    if (selectedTime === 'Weekly') {
      return {
        revenue: {
          total: (metrics && metrics.revenue.weekly.total) || '0',
          difference: (metrics && metrics.revenue.weekly.difference) || '0',
        },
        customer: {
          total: (metrics && metrics.customer.weekly.total) || '0',
          difference: (metrics && metrics.customer.weekly.difference) || '0',
        },
        invoice: {
          total: (metrics && metrics.invoice.weekly.total) || '0',
          difference: (metrics && metrics.invoice.weekly.difference) || '0',
        },
        unpaidInvoice: {
          total: (metrics && metrics.unpaid_invoice.weekly.total) || '0',
          difference:
            (metrics && metrics.unpaid_invoice.weekly.difference) || '0',
        },
      };
    }

    if (selectedTime === 'Monthly') {
      return {
        revenue: {
          total: (metrics && metrics.revenue.monthly.total) || '0',
          difference: (metrics && metrics.revenue.monthly.difference) || 0,
        },
        customer: {
          total: (metrics && metrics.customer.monthly.total) || '0',
          difference: (metrics && metrics.customer.monthly.difference) || 0,
        },
        invoice: {
          total: (metrics && metrics.invoice.monthly.total) || '0',
          difference: (metrics && metrics.invoice.monthly.difference) || 0,
        },
        unpaidInvoice: {
          total: (metrics && metrics.unpaid_invoice.monthly.total) || '0',
          difference:
            (metrics && metrics.unpaid_invoice.monthly.difference) || '0',
        },
      };
    }

    if (selectedTime === 'Yearly') {
      return {
        revenue: {
          total: (metrics && metrics.revenue.yearly.total) || '0',
          difference: (metrics && metrics.revenue.yearly.difference) || 0,
        },
        customer: {
          total: (metrics && metrics.customer.yearly.total) || '0',
          difference: (metrics && metrics.customer.yearly.difference) || 0,
        },
        invoice: {
          total: (metrics && metrics.invoice.yearly.total) || '0',
          difference: (metrics && metrics.invoice.yearly.difference) || 0,
        },
        unpaidInvoice: {
          total: (metrics && metrics.unpaid_invoice.yearly.total) || '0',
          difference:
            (metrics && metrics.unpaid_invoice.yearly.difference) || '0',
        },
      };
    }

    return {
      revenue: {
        total: '0',
        difference: '0',
      },
    };
  };

  useEffect(() => {
    refetchMetrics();
  }, [business_id, navigation]);

  useEffect(() => {
    screen('Home'); // Track screen with segment
  }, []);

  return (
    <Box style={styles.container}>
      <Box style={styles.header}>
        <StackHeader
          profile
          onPressProfile={() => navigation.navigate('ProfileNav')}
          title=""
          icon1={<NotificationIcon />}
          onPressIcon1={() => navigation.navigate('Notification')}
          onPressBusiness={() => navigation.navigate('SelectBusiness')}
        />
      </Box>
      <Box
        style={{
          backgroundColor: theme.colors.bg5,
          borderTopLeftRadius: 18,
          borderTopRightRadius: 18,
        }}
      >
        <ScrollView
          style={{ flex: 1 }}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          <Box style={styles.dashboard}>
            {userPermissions && userPermissions.view_analytics && (
              <>
                <Box style={styles.content}>
                  {onboardingStatus.length > 0 &&
                    Number(onboardingStatus) < 99 &&
                    !isMetricsLoading && (
                      <Box
                        style={{
                          height: 120,
                          width: wp(90),
                          marginTop: 19,
                          borderRadius: 8,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          backgroundColor: theme.colors.bg1,
                          paddingHorizontal: 20,
                        }}
                      >
                        <TouchableOpacity
                          activeOpacity={0.8}
                          onPress={() =>
                            navigation.navigate('GettingStartedNav')
                          }
                        >
                          <Text variant="DetailsM" color="text1">
                            Getting Started
                          </Text>
                          <Box style={{ width: 249 }}>
                            <Text variant="SmallerTextR" color="text7">
                              Great to have you onboard! feel free to {'\n'}
                              explore or get a head start below.
                            </Text>
                          </Box>
                          <Box>
                            <Text variant="SmallerTextR" color="secondary5">
                              {`${onboardingStatus}%`} complete
                            </Text>
                          </Box>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() =>
                            navigation.navigate('GettingStartedNav')
                          }
                          style={{ paddingRight: 10 }}
                        >
                          <AntDesign
                            name="arrowright"
                            size={18}
                            color={theme.colors.text5}
                          />
                        </TouchableOpacity>
                      </Box>
                    )}
                  <Box style={styles.title}>
                    <Text variant="Body1M" color="text6">
                      Business Overview
                    </Text>

                    <Box style={styles.picker}>
                      <HomePicker
                        placeholder=""
                        label=""
                        data={roles}
                        setValue={setSelectedTimeFrame}
                        value={selectedTimeFrame}
                      />
                    </Box>
                  </Box>
                </Box>

                <Box style={styles.dashboardCard}>
                  <HomeCard
                    title="Total Revenue"
                    value={
                      numberWithCommas(
                        returnAnalytics(
                          selectedTimeFrame
                        ).revenue?.total.toString()
                      ) +
                        ' ' +
                        `(${returnCurrency().code})` ?? '0'
                    }
                    {...{
                      loading,
                    }}
                    difference={
                      returnAnalytics(
                        selectedTimeFrame
                      ).revenue?.difference.toString() ?? '0'
                    }
                  />
                  <HomeCard
                    title="Active Customers"
                    value={
                      returnAnalytics(
                        selectedTimeFrame
                      ).customer?.total.toString() ?? '0'
                    }
                    {...{
                      loading,
                    }}
                    difference={
                      returnAnalytics(
                        selectedTimeFrame
                      ).customer?.difference.toString() ?? '0'
                    }
                  />
                  <HomeCard
                    title="Paid Invoices"
                    value={
                      returnAnalytics(
                        selectedTimeFrame
                      ).invoice?.total.toString() ?? '0'
                    }
                    {...{
                      loading,
                    }}
                    difference={
                      returnAnalytics(
                        selectedTimeFrame
                      ).invoice?.difference.toString() ?? '0'
                    }
                  />
                  <HomeCard
                    title="Unpaid Invoices"
                    value={
                      returnAnalytics(
                        selectedTimeFrame
                      ).unpaidInvoice?.total.toString() ?? '0'
                    }
                    {...{
                      loading,
                    }}
                    difference={
                      returnAnalytics(
                        selectedTimeFrame
                      ).unpaidInvoice?.difference.toString() ?? '0'
                    }
                  />
                </Box>
              </>
            )}
            <Box style={styles.reminderTitle}>
              <Text variant="Body1M" color="text6">
                Quick Tasks
              </Text>
            </Box>
          </Box>
          <Box style={styles.reminder}>
            {userPermissions &&
              userPermissions.create_invoice &&
              metrics &&
              metrics.hasCompleteBusiness &&
              metrics.hasStorefront && (
                <ReminderActions
                  label="Create Invoice"
                  onPress={() =>
                    navigation.dispatch(
                      CommonActions.navigate({
                        name: 'CreateInvoice',
                        params: {
                          isHomeNav: true,
                          isInvoiceNav: false,
                        },
                      })
                    )
                  }
                  iconType="new_invoice"
                />
              )}
            {userPermissions &&
              userPermissions.create_order &&
              metrics &&
              metrics.hasCompleteBusiness &&
              metrics.hasStorefront && (
                <ReminderActions
                  label="Create New Order"
                  onPress={() =>
                    navigation.navigate('CreateOrder', {
                      isHomeNav: true,
                    })
                  }
                  iconType="new_order"
                />
              )}
            {userPermissions && userPermissions.view_invoice && (
              <ReminderActions
                label="Unpaid Invoices"
                onPress={() => navigation.navigate('DueInvoices')}
                iconType="unpaid_invoice"
              />
            )}
          </Box>

          <Box style={{ height: 150 }} />
        </ScrollView>
      </Box>
    </Box>
  );
};

export default Home;

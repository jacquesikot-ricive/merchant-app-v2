import React, { FC, useState, useEffect } from 'react';
import { FlatList, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from '../../components/Themed';
import StackHeader from '../../components/StackHeader';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HomeNavParamList } from '../../navigation/HomeNav';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import ListEmpty from '../../components/ListEmpty';
import notificationApi from '../../api/riciveApi/notification';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  notifBox: {
    width: wp(90),
    maxHeight: 100,
    borderWidth: 1,
    borderColor: theme.colors.border,
    marginBottom: 15,
    borderRadius: 6,
    padding: 10,
  },
});

type Props = NativeStackScreenProps<HomeNavParamList, 'Notification'>;

const Notifications: FC<Props> = ({ navigation }: Props) => {
  const user_id = useAppSelector((state) => state.login.user.profile.id);
  const [notifications, setNotifications] = useState<any>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  
  const getNotifications = async () => {
    try {
      setError(false);
      setLoading(true);
      const notificationsData = await notificationApi.getNotifications(user_id);
      setNotifications(notificationsData.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
      setError(true);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot get notifications',
      });
    }
  };

  useEffect(() => {
    getNotifications();
  }, []);

  if (loading) {
    return <ScreenLoading />;
  }

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => await getNotifications()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      <StackHeader
        title="Notifications"
        onBackPress={() => navigation.goBack()}
      />

      <Box>
        <FlatList
          data={notifications}
          keyExtractor={(item: any) => item.id.toString()}
          ListEmptyComponent={() => (
            <ListEmpty topText="No Notifications" bottomText="" />
          )}
          renderItem={({ item }) => (
            <Box style={styles.notifBox}>
              <Text variant={'Body2M'} color="text1">
                {item.title}
              </Text>
              <Text numberOfLines={2} variant={'DetailsR'}>
                {item.body}
              </Text>
            </Box>
          )}
        />
      </Box>
    </Box>
  );
};

export default Notifications;

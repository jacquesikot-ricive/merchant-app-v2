import React, { FC, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../../components/Themed';
import StackHeader from '../../components/StackHeader';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { HomeNavParamList } from '../../navigation/HomeNav';
import HeaderTextInput from '../../components/HeaderTextInput';
import ScheduledOrderCard from '../../components/ScheduledOrderCard';
import supabase from '../../../supabase';
import db from '../../api/db';
import { useAppSelector } from '../../redux/hooks';
import Toast from 'react-native-toast-message';
import ListEmpty from '../../components/ListEmpty';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  content: {
    alignItems: 'center',
    marginTop: 40,
  },
  list: {
    paddingTop: 20,
    height: hp(75),
    paddingBottom: 80,
  },
  title: {
    width: wp(90),
    paddingHorizontal: 20,
  },
});

type Props = NativeStackScreenProps<HomeNavParamList, 'ScheduledOrders'>;

const ScheduledOrders: FC<Props> = ({ navigation }: Props) => {
  const merchant_id = useAppSelector((state) => state.login.id);
  const [loading, setLoading] = useState<boolean>(false);
  const [refreshing, setRefreshing] = useState<boolean>(false);
  const [orders, setOrders] = useState<any>();
  const [searchResult, setSearchResult] = useState<any[]>([]);

  // get orders API by merchaant id
  //   const getMerchantOrders = async () => {
  //     try {
  //       setLoading(true);
  //       const ordersData = await order.getOrders(merchant_id);
  //       if (ordersData) {
  //         setOrders(ordersData);
  //         setSearchResult(ordersData);
  //         setLoading(false);
  //       }
  //       setLoading(false);
  //     } catch (error) {
  //       setLoading(false);
  //       console.log('Error getting scheduled orders:', error);
  //       Toast.show({
  //         type: 'error',
  //         text1: 'Server Error',
  //         text2:
  //           'Sorry we cannot fetch scheduled orders now, please try again later...',
  //       });
  //     }
  //   };

  // subscribe and unsbsribe from orders api
  //   useEffect(() => {
  //     getMerchantOrders();

  //     const orderSub = supabase
  //       .from(db.tables.merchant_order)
  //       .on('*', () => {
  //         getMerchantOrders();
  //       })
  //       .subscribe();

  //     navigation.addListener('focus', async () => await getMerchantOrders());

  //     return () => {
  //       supabase.removeSubscription(orderSub);
  //       navigation.removeListener('focus', async () => await getMerchantOrders());
  //     };
  //   }, []);

  const ongoingOrders =
    searchResult &&
    searchResult
      .filter(
        (order: any) =>
          order.order_status === 'IN STORE' ||
          order.order_status === 'PROCESSING' ||
          order.order_status === 'DELIVERY'
      )
      .sort((a: any, b: any) => {
        return (
          new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );
      });

  // search filter method
  const handleSearch = (e: string) => {
    const result = orders.filter(
      (c: any) =>
        c.user.name.trim().toLowerCase().includes(e.trim().toLowerCase()) ||
        c.user.phone.trim().toLowerCase().includes(e.trim().toLowerCase()) ||
        c.service_type.name
          .trim()
          .toLowerCase()
          .includes(e.trim().toLowerCase()) ||
        new Date(c.pick_up_date)
          .toDateString()
          .trim()
          .toLowerCase()
          .includes(e.trim().toLowerCase()) ||
        c.id.toString().trim().toLowerCase().includes(e.trim().toLowerCase()) ||
        c.pick_up_time.trim().toLowerCase().includes(e.trim().toLowerCase())
    );
    setSearchResult([...result]);
  };

  const onRefresh = async () => {
    await getMerchantOrders();
  };

  return (
    <Box style={styles.container}>
      <StackHeader title="" onBackPress={() => navigation.goBack()} />
      <Text variant="TitleSB" color="text6" style={styles.title}>
        Scheduled Orders
      </Text>
      <Box style={styles.content}>
        <HeaderTextInput
          type={'search'}
          placeholder="Search scheduled order"
          onChangeText={(e) => handleSearch(e)}
        />
        {/* get ongoing orders data */}
        <Box style={styles.list}>
          <FlatList
            data={ongoingOrders}
            keyExtractor={(item: any) => item.id.toString()}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => {
              if (loading) {
                return <ActivityIndicator />;
              }
              return (
                <ListEmpty
                  topText="Orders"
                  bottomText={`No Scheduled Orders`}
                />
              );
            }}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            renderItem={({ item }) => {
              return (
                <ScheduledOrderCard
                  serviceType={item.service_type.name}
                  orderId={`#${item.id}`}
                  timestamp={`${new Date(item.pick_up_date).toDateString()} | ${
                    item.pick_up_time
                  }`}
                  customer={item.user.name}
                  status={item.order_status}
                  onPress={() => {}}
                  darkBg
                />
              );
            }}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default ScheduledOrders;

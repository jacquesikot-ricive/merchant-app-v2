import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState, useEffect, useCallback } from 'react';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Toast from 'react-native-toast-message';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Button from '../../components/Button';
import StackHeader from '../../components/StackHeader';
import useAuth from '../../auth/useAuth';

import theme, { Box, Text } from '../../components/Themed';
import { HomeNavParamList } from '../../navigation/HomeNav';
import { useAppSelector } from '../../redux/hooks';
import businessApi from '../../api/riciveApi/business';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: theme.colors.white,
    flex: 1,
  },
});

type Props = NativeStackScreenProps<HomeNavParamList, 'SelectBusiness'>;

const SelectBusiness = ({ route, navigation }: Props) => {
  const { logout, login } = useAuth();
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const auth_id = useAppSelector((state) => state.login.user.auth.id);
  const [createBusiness, setCreateBusiness] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [switchLoading, setSwitchLoading] = useState<boolean>(false);
  const [screenLoading, setScreenLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const [userBusinesses, setUserBusinesses] = useState<any[]>([]);

  const [activeBusiness, setActiveBusiness] = useState<any>();

  const isSelected = (id: string) => {
    if (activeBusiness && activeBusiness.id === id) {
      return true;
    } else {
      return false;
    }
  };

  const fetchBusinesses = useCallback(async () => {
    setScreenLoading(true);
    setError(false);
    try {
      const business = await businessApi.getBusinesses(auth_id);

      const currentActiveBusiness = business.filter(
        (b: any) => b.id === business_id
      )[0];

      setUserBusinesses(business);
      setActiveBusiness(currentActiveBusiness);
      setScreenLoading(false);
    } catch (error) {
      setScreenLoading(false);
      setError(true);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Error getting user business',
      });
    }
  }, []);

  const handleNewBusiness = async () => {
    Alert.prompt(
      'Business Name',
      'Please enter a business name',
      async (text: string) => {
        try {
          setLoading(true);

          await businessApi.createBusiness(text);

          const business = await businessApi.getBusinesses(auth_id);
          setUserBusinesses(business);

          setLoading(false);

          Toast.show({
            type: 'success',
            text1: 'Business Created',
            text2: 'Your business has been created',
          });
        } catch (error) {
          setLoading(false);
          Toast.show({
            type: 'error',
            text1: 'Server Error',
            text2: 'Error creating a new business',
          });
        }
      }
    );
  };
  const handleDone = async () => {
    try {
      setSwitchLoading(true);

      const business = await businessApi.loginBusiness({
        auth_id,
        business_id: activeBusiness.id,
      });

      login({
        access_token: business.data.access_token,
        auth: business.data.auth,
        business: business.data.business,
        permissions: business.data.permissions,
        profile: business.data.profile,
        refresh_token: business.data.refresh_token,
      });

      setSwitchLoading(false);

      Toast.show({
        type: 'success',
        text1: 'Business Switched',
        text2: 'Your business is now active',
      });

      navigation.goBack();
    } catch (error) {
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Error creating a new business',
      });
    }
  };

  useEffect(() => {
    fetchBusinesses();
  }, []);

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={fetchBusinesses}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {screenLoading && <ScreenLoading />}
      <StackHeader
        title="Select Active Business"
        onBackPress={() => navigation.goBack()}
      />

      {loading ? (
        <Box
          style={{
            height: hp(55),
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <ActivityIndicator color={theme.colors.primary1} />
        </Box>
      ) : (
        <Box
          style={{
            height: hp(55),
          }}
        >
          <FlatList
            data={userBusinesses}
            keyExtractor={(item) => item.id.toString()}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => {
                  if (item && item.id && isSelected(item.id)) {
                    setActiveBusiness(null);
                  } else {
                    setActiveBusiness(item);
                  }
                }}
                style={{
                  height: 57,
                  width: wp(90),
                  borderRadius: 8,
                  borderWidth: 1,
                  justifyContent: 'center',
                  paddingHorizontal: 20,
                  marginBottom: 10,
                  borderColor: isSelected(item.id)
                    ? theme.colors.primary1
                    : theme.colors.border,
                }}
              >
                <Text variant={'Body2M'} color="text1">
                  {item.business_name ? item.business_name : 'No Business Name'}
                </Text>
              </TouchableOpacity>
            )}
          />
        </Box>
      )}

      <Box style={{ marginTop: 30 }}>
        <Button
          type="transparent"
          label="Add New Business"
          onPress={async () => {
            if (userBusinesses && userBusinesses.length < 6) {
              return await handleNewBusiness();
            } else {
              return Toast.show({
                type: 'info',
                text1: 'Max Business',
                text2: 'You can create a maximum of 5 businesses',
              });
            }
          }}
          loading={loading}
        />
        <Box mt="l">
          <Button
            type="primary"
            label="Switch Business"
            onPress={handleDone}
            loading={switchLoading}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default SelectBusiness;

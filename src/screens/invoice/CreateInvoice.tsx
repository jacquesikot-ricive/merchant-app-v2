import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { useAnalytics } from '@segment/analytics-react-native';
import { useIsFocused } from '@react-navigation/native';
import { useQuery } from 'react-query';

import theme, { Box, Text } from '../../components/Themed';
import { InvoiceNavParamList } from '../../navigation/InvoiceNav';
import AddCustomerModal from '../../components/AddCustomerModal';
import StackHeader from '../../components/StackHeader';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import SelectOrderModal from '../../components/SelectOrderModal';
import Checkbox from '../../components/Checkbox';
import Button from '../../components/Button';
import orderApi from '../../api/riciveApi/order';
import invoiceApi from '../../api/riciveApi/invoice';
import customerApi from '../../api/riciveApi/customer';
import ScreenError from '../../components/ScreenError';
import queryKeys from '../../constants/queryKeys';
import { returnStatusColors } from '../../components/UpcomingOrderCard';
import SelectProductModal from '../../components/SelectProductsModal';
import NewProductModal from '../../components/NewProductModal';
import productApi from '../../api/riciveApi/product';
import CurrencyTextInput from '../../components/CurrencyTextInput';
import InvoiceItem from '../../components/InvoiceItem';
import useCurrency from '../../hooks/useCurrency';
import numberWithCommas from '../../utils/numbersWithComma';
import AdditionalPaymentModal from '../../components/AdditionalPaymentModal';
import invoice from '../../api/riciveApi/invoice';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  customerCard: {
    marginTop: 10,
    width: wp(90),
    height: 82,
    paddingVertical: 5,
    borderRadius: 6,
    justifyContent: 'center',
    paddingLeft: 20,
    borderWidth: 1,
    borderStyle: 'dashed',
    marginBottom: 20,
    borderColor: theme.colors.border1,
  },
  removeCustomer: {
    height: 30,
    width: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 10,
    backgroundColor: theme.colors.errorLight,
    zIndex: 1,
  },
  removeOrder: {
    height: 30,
    width: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    right: 10,
    backgroundColor: theme.colors.errorLight,
    zIndex: 1,
  },
  checkbox: {
    flexDirection: 'row',
    width: wp(90),
    alignItems: 'center',
    marginBottom: 25,
  },
  status: {
    flexDirection: 'row',
    height: 24,
    borderRadius: 8,
    backgroundColor: 'rgba(250, 172, 48, 0.05)',
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    position: 'absolute',
    top: -47,
    right: '47%',
  },
});

type Props = NativeStackScreenProps<InvoiceNavParamList, 'CreateInvoice'>;

const CreateInvoice = ({ navigation, route }: Props): JSX.Element => {
  const {
    isHomeNav,
    isInvoiceNav,
    isOrderNav,
    order,
    customer,
    isEdit,
    invoice: isEditInvoice,
    hasProducts: editHasProducts,
    products: isEditProducts,
    dispatch: isEditDispatch,
    discount: isEditDiscount,
    deposit: isEditDeposit,
    tax: isEditTax,
    address: isEditAddress,
  } = route.params;
  const { track } = useAnalytics();
  const isFocused = useIsFocused();
  const { returnCurrency } = useCurrency();
  const [activeCustomer, setActiveCustomer] = useState<any>(
    isOrderNav ? customer : isEdit ? customer : undefined
  );
  const [addCustomerModal, setAddCustomerModal] = useState<boolean>(false);
  const [allCustomers, setAllCustomers] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [customerOrders, setCustomerOrders] = useState<any[]>([]);
  const [activeOrder, setActiveOrder] = useState<any>(
    isOrderNav ? order : isEdit ? order : undefined
  );
  const [selectOrderModal, setSelectOrderModal] = useState<boolean>(false);
  const [hasProducts, setHasProducts] = useState<boolean>(
    isEdit && editHasProducts ? editHasProducts : false
  );
  const [invoiceAmount, setInvoiceAmount] = useState<string>('');
  const [error, setError] = useState<boolean>(false);
  const [sendSms, setSendSms] = useState<boolean>(false);
  const [showAddProduct, setShowAddProduct] = useState<boolean>(false);
  const [showProductModal, setShowProductModal] = useState<boolean>(false);
  const [products, setProducts] = useState<any>(
    isEdit && isEditProducts && isEditProducts.length > 0 ? isEditProducts : []
  );
  const [fetchedProducts, setFetchedProducts] = useState<any[]>([]);
  const [createInvoiceLoading, setCreateInvoiceLoading] =
    useState<boolean>(false);
  const storeName = useAppSelector(
    (state) => state.login.user.business.business_name
  );
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [avoidingView, setAvoidingView] = useState<boolean>(false);
  const [invoiceTotal, setInvoiceTotal] = useState<string>('');
  const [showAdditionalPayment, setShowAdditionalPayment] =
    useState<boolean>(false);

  const [dispatchFee, setDispatchFee] = useState<string>(
    isEdit && isEditDispatch ? isEditDispatch : ''
  );
  const [address, setAddress] = useState<string>(
    isEdit && isEditAddress ? isEditAddress : ''
  );

  const [discount, setDiscount] = useState<
    | {
        amount: string;
        percentage: string;
      }
    | undefined
  >();
  const [deposit, setDeposit] = useState<string>(
    isEdit && isEditDeposit ? isEditDeposit : ''
  );
  const [tax, setTax] = useState<
    | {
        amount: string;
        percentage: string;
      }
    | undefined
  >();

  const additions =
    Number(invoiceTotal) +
    Number(dispatchFee && dispatchFee) +
    Number(
      tax && tax.percentage && tax.percentage.length > 0 ? tax.amount : '0'
    );

  const subtractions =
    Number(
      discount && discount.percentage && discount.percentage.length > 0
        ? discount.amount
        : '0'
    ) + Number(deposit ? deposit : '0');

  const finalTotal = additions - subtractions;

  // console.log({
  //   additions,
  //   subtractions,
  //   final: additions - subtractions,
  // });

  const hasAdditionalInfo =
    dispatchFee.toString().length > 0 ||
    (discount && discount.percentage) ||
    deposit.toString().length > 0 ||
    (tax && tax.percentage);

  const discountPercentage = discount?.percentage || undefined;
  const discountAmount = discount?.amount || undefined;
  const taxPercentage = tax?.percentage || undefined;
  const taxAmount = tax?.amount || undefined;

  const handleProcessInvoice = async () => {
    try {
      if (!activeCustomer) {
        return Toast.show({
          type: 'error',
          text1: 'No Customer',
          text2: 'Please add a customer to continue',
        });
      }

      if (!hasProducts && invoiceAmount.length < 1) {
        return Toast.show({
          type: 'error',
          text1: 'No Amount',
          text2: 'Please add invoice amount to continue',
        });
      }

      setCreateInvoiceLoading(true);
      if (isEdit) {
        await invoiceApi.updateInvoice({
          is_paid:
            isEditInvoice && isEditInvoice.is_paid && isEditInvoice.is_paid,
          amount_collected:
            isEditInvoice && isEditInvoice.amount_collected
              ? isEditInvoice.amount_collected.toString()
              : undefined,
          id: isEditInvoice && isEditInvoice.id && isEditInvoice.id,
          total: hasProducts
            ? finalTotal.toFixed().toString()
            : Number(invoiceAmount).toFixed().toString(),
          customer: activeCustomer.id,
          order: activeOrder ? activeOrder.id : undefined,
          is_cash:
            isEditInvoice && isEditInvoice.is_cash && isEditInvoice.is_cash,
          delivery_fee:
            dispatchFee.toString().length > 0
              ? dispatchFee.toString()
              : undefined,
          invoice_items: products,
          deposit:
            deposit.toString().length > 0 ? deposit.toString() : undefined,
          discount:
            discount && discountAmount && discount.amount.length > 0
              ? discount.amount
              : undefined,
          tax:
            tax && taxAmount && tax.amount.length > 0 ? tax.amount : undefined,
          address: address.toString().length > 0 ? address : undefined,
        });
      } else {
        await invoiceApi.createInvoice({
          total: hasProducts
            ? finalTotal.toFixed().toString()
            : Number(invoiceAmount).toFixed().toString(),
          customer: activeCustomer.id,
          order: activeOrder ? activeOrder.id : undefined,
          is_cash: false,
          sendSMS: sendSms,
          delivery_fee:
            dispatchFee.toString().length > 0
              ? dispatchFee.toString()
              : undefined,
          invoice_items: products,
          deposit:
            deposit.toString().length > 0 ? deposit.toString() : undefined,
          discount:
            discount && discountAmount && discount.amount.length > 0
              ? discount.amount
              : undefined,
          tax:
            tax && taxAmount && tax.amount.length > 0 ? tax.amount : undefined,
          address: address.toString().length > 0 ? address : undefined,
        });
      }
      setCreateInvoiceLoading(false);

      Toast.show({
        type: 'success',
        text1: `New Invoice ${isEdit ? 'Updated!' : 'Created!'}`,
        text2: `Your Invoice has been ${
          isEdit ? 'updated!' : 'created!'
        } and sent successfully.`,
      });
      navigation.goBack();
    } catch (error: any) {
      setCreateInvoiceLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We could not create your invoice now, please try again later..',
      });
      console.log(
        'Error sending invoice:',
        JSON.stringify(error.response.data)
      );
    }
  };

  const {
    isLoading: isOrdersLoading,
    isError: isOrdersError,
    refetch: refetchOrders,
  } = useQuery(
    `${queryKeys.ORDERS_DATA}`,
    async () => {
      try {
        const customerOrdersData = await orderApi.getOrders(
          activeCustomer && activeCustomer.id
        );

        setCustomerOrders(customerOrdersData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Sorry we cannot fetch  orders now, please try again later...',
        });
        console.log('Error getting metrics:', error);
      }
    },
    {
      enabled: !!activeCustomer,
    }
  );

  const {
    isLoading: isCustomersLoading,
    isError: isCustomersError,
    refetch: refetchCustomers,
  } = useQuery(queryKeys.CUSTOMERS_DATA, async () => {
    try {
      const customersData = await customerApi.getCustomers();
      if (customersData) setAllCustomers(customersData.data);
    } catch (error: any) {
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Sorry we cannot fetch customers now, please try again later...',
      });
      console.log(
        'Error getting metrics:',
        JSON.stringify(error.response.data)
      );
    }
  });

  const {
    isLoading: isProductsLoading,
    isError: isProductsError,
    refetch: refetchProducts,
  } = useQuery(
    queryKeys.PRODUCTS_DATA,
    async () => {
      try {
        const data = await productApi.getProducts(business_id);
        setFetchedProducts(data.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Error getting products data, try again later..',
        });
        console.log(
          'Error getting create order data',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const onRefreshData = async () => {
    await refetchOrders();
    await refetchCustomers();
    await refetchProducts();
  };

  const findPercentage = (value: string, total?: string) => {
    const finalValue = (100 * Number(value)) / Number(total);

    return finalValue.toFixed(2).toString();
  };

  useEffect(() => {
    isFocused && onRefreshData();
  }, [isFocused]);

  useEffect(() => {
    const calculateTotal = () => {
      const value = products
        .map((item: any) => Number(item.total))
        .reduce((a: any, b: any) => a + b, 0);

      setInvoiceTotal(value.toString());

      setDiscount({
        percentage:
          isEdit && isEditDiscount && !isNaN(Number(isEditDiscount))
            ? findPercentage(isEditDiscount, value)
            : discount &&
              discount.percentage &&
              !isNaN(Number(discount.percentage))
            ? discount.percentage
            : '',
        amount: (
          Number(value) *
          (Number(
            isEdit && isEditDiscount
              ? findPercentage(isEditDiscount, value)
              : discount &&
                discount.percentage &&
                !isNaN(Number(discount.percentage))
              ? discount.percentage
              : ''
          ) /
            100)
        ).toString(),
      });

      setTax({
        percentage:
          isEdit && isEditTax && !isNaN(Number(isEditTax))
            ? findPercentage(isEditTax, value)
            : tax && tax.percentage && !isNaN(Number(tax.percentage))
            ? tax.percentage
            : '',
        amount: (
          Number(value) *
          (Number(
            isEdit && isEditTax && !isNaN(Number(isEditTax))
              ? findPercentage(isEditTax, value)
              : tax && tax.percentage && !isNaN(Number(tax.percentage))
              ? tax.percentage
              : ''
          ) /
            100)
        ).toString(),
      });

      // To ensure no additional information is saved without products
      if (products.length < 1) {
        setDispatchFee('');
        setDiscount(undefined);
        setDeposit('');
        setTax(undefined);
      }
    };

    calculateTotal();
  }, [products]);

  if (isOrdersError || isCustomersError || isProductsError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={onRefreshData}
      />
    );
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'position' : 'padding'}
      style={styles.container}
      contentContainerStyle={{ alignItems: 'center' }}
      enabled={avoidingView}
    >
      {(isCustomersLoading || isOrdersLoading || isProductsLoading) && (
        <ScreenLoading />
      )}
      <StackHeader
        title={isEdit ? 'Edit Invoice' : 'Create Invoice'}
        onBackPress={() => navigation.goBack()}
        hasBorder
      />

      <ScrollView
        contentContainerStyle={{ paddingBottom: 200 }}
        showsVerticalScrollIndicator={false}
      >
        <Box style={{ width: wp(90), alignSelf: 'center' }}>
          {/* Customer Selector */}
          <Text
            variant={'SmallerTextR'}
            color="text1"
            style={{ width: wp(90), marginTop: 20 }}
          >
            Select Customer
          </Text>

          {/* Remove active customer */}
          <TouchableOpacity
            activeOpacity={isOrderNav ? 1 : 0.7}
            onPress={() => {
              if (isOrderNav) {
                return;
              } else {
                setAddCustomerModal(true);
              }
            }}
            style={[
              styles.customerCard,
              {
                borderStyle: activeCustomer ? undefined : 'dashed',
              },
            ]}
          >
            {activeCustomer && !isOrderNav && (
              <TouchableOpacity
                onPress={() => setActiveCustomer(undefined)}
                style={styles.removeCustomer}
              >
                <Icon name="x" color={theme.colors.error} size={15} />
              </TouchableOpacity>
            )}

            {activeCustomer ? (
              <>
                <Text variant={'DetailsM'}>{`${activeCustomer.first_name} ${
                  activeCustomer.last_name ? activeCustomer.last_name : ''
                }`}</Text>
                <Text variant={'DetailsR'} color="text5">
                  {activeCustomer.phone}
                </Text>
              </>
            ) : (
              <Box
                style={{
                  width: 32,
                  height: 32,
                  borderRadius: 16,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  backgroundColor: 'background: rgba(145, 206, 51, 0.2)',
                }}
              >
                <Icon name="plus" color={theme.colors.primary1} size={17} />
              </Box>
            )}
          </TouchableOpacity>

          {/* Order Selector */}
          <Text
            variant={'SmallerTextR'}
            color="text1"
            style={{ width: wp(90), marginTop: 10 }}
          >
            Select Order to Invoice (optional)
          </Text>

          <TouchableOpacity
            activeOpacity={isOrderNav ? 1 : 0.7}
            onPress={() => {
              if (isOrderNav) {
                return;
              }

              if (activeCustomer) {
                return setSelectOrderModal(true);
              } else {
                return Toast.show({
                  type: 'error',
                  text1: 'Select Customer',
                  text2: 'Please select customer first',
                });
              }
            }}
            style={[
              styles.customerCard,
              {
                borderStyle: activeOrder ? undefined : 'dashed',
              },
            ]}
          >
            {activeOrder && !isOrderNav && (
              <TouchableOpacity
                onPress={() => setActiveOrder(undefined)}
                style={styles.removeOrder}
              >
                <Icon name="x" color={theme.colors.error} size={15} />
              </TouchableOpacity>
            )}

            {activeOrder ? (
              <>
                <Box flexDirection={'row'}>
                  <Text variant={'SmallerTextM'}>Order</Text>
                  <Text variant={'SmallerTextM'} ml="s">
                    {'#' + activeOrder &&
                      activeOrder.id.substring(0, 5).toUpperCase()}
                  </Text>
                </Box>

                <Box flexDirection={'row'}>
                  <Text variant={'SmallerTextR'} color="text5">
                    Customer:
                  </Text>
                  <Text variant={'SmallerTextR'} ml="s" color="text5">
                    {activeCustomer && activeCustomer.first_name
                      ? activeCustomer.first_name +
                        ' ' +
                        activeCustomer.last_name
                      : ''}
                  </Text>
                </Box>

                <Box>
                  <Box
                    style={[
                      styles.status,
                      {
                        backgroundColor: returnStatusColors(activeOrder.status)
                          .bg,
                      },
                    ]}
                  >
                    <Text
                      variant="SmallerTextR"
                      style={{
                        color: returnStatusColors(activeOrder.status).text,
                      }}
                      ml="s"
                    >
                      {activeOrder.status.charAt(0).toUpperCase() +
                        activeOrder.status.substring(1).toLocaleLowerCase()}
                    </Text>
                  </Box>
                </Box>

                <Box flexDirection={'row'}>
                  <Text variant={'SmallerTextR'} color="text5">
                    {new Date(activeOrder.created_at).toDateString()}
                  </Text>
                </Box>
              </>
            ) : (
              <Box
                style={{
                  width: 32,
                  height: 32,
                  borderRadius: 16,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  backgroundColor: 'background: rgba(145, 206, 51, 0.2)',
                }}
              >
                <Icon name="plus" color={theme.colors.primary1} size={17} />
              </Box>
            )}
          </TouchableOpacity>

          {/* Add Product checkbox */}
          <TouchableOpacity
            style={styles.checkbox}
            onPress={() => {
              setHasProducts(!hasProducts);
            }}
          >
            <Checkbox checked={hasProducts} />

            <Text variant={'DetailsR'} color="text1" ml="m">
              Do you want to add items to this invoice?
            </Text>
          </TouchableOpacity>

          {/* Add product */}
          {hasProducts && (
            <Box
              flexDirection={'row'}
              alignItems="center"
              justifyContent={'space-between'}
            >
              <Text variant={'DetailsR'}>Products</Text>

              <Box flexDirection={'row'}>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                  onPress={() => setShowAddProduct(true)}
                >
                  <Box
                    style={{
                      width: 17,
                      height: 17,
                      borderRadius: 9,
                      borderWidth: 1,
                      borderColor: theme.colors.primary1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Icon name="plus" size={12} color={theme.colors.primary1} />
                  </Box>
                  <Text variant={'DetailsM'} color="primary1" ml="s" mr="m">
                    New Product
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => setShowProductModal(true)}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                >
                  <Box
                    style={{
                      width: 17,
                      height: 17,
                      borderRadius: 9,
                      borderWidth: 1,
                      borderColor: theme.colors.primary1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Icon
                      name="chevron-right"
                      size={13}
                      color={theme.colors.primary1}
                    />
                  </Box>
                  <Text variant={'DetailsM'} color="primary1" ml="s">
                    Select Products
                  </Text>
                </TouchableOpacity>
              </Box>
            </Box>
          )}

          {/* Product List */}
          {products.length > 0 &&
            products.map((item: any, index: number) => (
              <InvoiceItem
                image={item.image}
                key={item.id.toString()}
                count={item.quantity}
                name={item.name}
                price={item.price}
                handleMinus={() => {
                  if (Number(item.quantity) > 1) {
                    const newQuantity = Number(item.quantity) - 1;

                    const data = {
                      ...item,
                      quantity: newQuantity.toString(),
                      total: Number(item.price) * newQuantity,
                    };

                    const res = products.filter((i: any) => i.id !== item.id);

                    res.splice(index, 0, data);

                    setProducts(res);
                  } else {
                    setProducts([
                      ...products.filter((i: any) => i.id !== item.id),
                    ]);
                  }
                }}
                handlePlus={() => {
                  const newQuantity = Number(item.quantity) + 1;

                  const data = {
                    ...item,
                    quantity: newQuantity.toString(),
                    total: Number(item.price) * newQuantity,
                  };

                  const res = products.filter((i: any) => i.id !== item.id);

                  res.splice(index, 0, data);

                  setProducts(res);
                }}
                handleRemove={() => {
                  const res = products.filter((i: any) => i.id !== item.id);
                  setProducts(res);
                }}
              />
            ))}

          {/* Invoice amount button */}
          {!hasProducts && (
            <CurrencyTextInput
              placeholder="Enter Amount"
              onChangeValue={(e: string) => setInvoiceAmount(e)}
              value={invoiceAmount}
              width={wp(90)}
            />
          )}
        </Box>

        {/* Sub Total - Spacing after Subtotal is deliberate */}
        {hasProducts && (
          <Text
            style={{
              width: wp(90),
              textAlign: 'right',
              marginTop: 50,
            }}
            variant={'SmallerTextM'}
            color={'text1'}
          >
            {`Subtotal:  ${returnCurrency().code} ${numberWithCommas(
              Number(invoiceTotal).toFixed(2).toString()
            )}`}
          </Text>
        )}

        {/* Additional Payment information */}
        {hasProducts && (
          <Box
            style={{
              marginTop: 10,
              borderWidth: 1,
              borderColor: theme.colors.border4,
              paddingVertical: 7,
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 20,
              borderStyle: 'dashed',
              width: wp(100),
            }}
          >
            <TouchableOpacity
              onPress={() => {
                if (products.length > 0) {
                  setShowAdditionalPayment(true);
                } else {
                  Toast.show({
                    text1: 'No Products',
                    text2: 'Add Products to Continue',
                    type: 'error',
                  });
                }
              }}
              activeOpacity={0.7}
              style={{
                width: wp(90),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Box flexDirection={'row'} alignItems={'center'}>
                {!hasAdditionalInfo && (
                  <Icon name="plus" color={theme.colors.primary1} size={12} />
                )}
                <Text
                  variant={hasAdditionalInfo ? 'SmallerTextM' : 'SmallerTextR'}
                  color={hasAdditionalInfo ? 'text1' : 'primary1'}
                  ml={hasAdditionalInfo ? undefined : 's'}
                >
                  Additional Payment Information
                </Text>
              </Box>

              {hasAdditionalInfo ? (
                <Text variant={'SmallerTextM'} color="primary1">
                  Edit
                </Text>
              ) : (
                <Icon
                  name="chevron-down"
                  size={14}
                  color={theme.colors.text1}
                />
              )}
            </TouchableOpacity>

            {/* Dispatch Fee  */}
            {dispatchFee.toString().length > 0 && (
              <>
                <Box
                  style={{
                    flexDirection: 'row',
                    width: wp(90),
                    justifyContent: 'space-between',
                    marginTop: 15,
                  }}
                >
                  <Text variant={'SmallerTextR'} color="text1">
                    Dispatch Details
                  </Text>

                  <Text variant={'SmallerTextR'} color="text7">
                    {'+' + ' ' + numberWithCommas(dispatchFee.toString())}
                  </Text>
                </Box>

                <Text
                  style={{ width: wp(90) }}
                  variant={'SmallerTextR'}
                  color="text5"
                  numberOfLines={1}
                >
                  {address}
                </Text>
              </>
            )}

            {/* Discount */}
            {discount && discountPercentage && discount.percentage.length > 0 && (
              <>
                <Box
                  style={{
                    flexDirection: 'row',
                    width: wp(90),
                    justifyContent: 'space-between',
                    marginTop: 15,
                  }}
                >
                  <Text variant={'SmallerTextR'} color="text1">
                    {`Discount (${discount.percentage}%)`}
                  </Text>

                  <Text variant={'SmallerTextR'} color="text7">
                    {'-' +
                      ' ' +
                      numberWithCommas(
                        Number(discount.amount).toFixed(2).toString() || '0'
                      )}
                  </Text>
                </Box>
              </>
            )}

            {/* Deposit */}
            {deposit.toString().length > 0 && (
              <>
                <Box
                  style={{
                    flexDirection: 'row',
                    width: wp(90),
                    justifyContent: 'space-between',
                    marginTop: 15,
                  }}
                >
                  <Text variant={'SmallerTextR'} color="text1">
                    {`Deposit`}
                  </Text>

                  <Text variant={'SmallerTextR'} color="text7">
                    {'-' + ' ' + numberWithCommas(deposit)}
                  </Text>
                </Box>
              </>
            )}

            {/* Tax */}
            {tax && taxPercentage && tax.percentage.length > 0 && (
              <>
                <Box
                  style={{
                    flexDirection: 'row',
                    width: wp(90),
                    justifyContent: 'space-between',
                    marginTop: 15,
                  }}
                >
                  <Text variant={'SmallerTextR'} color="text1">
                    {`Tax deducted (${tax.percentage}%)`}
                  </Text>

                  <Text variant={'SmallerTextR'} color="text7">
                    {'+' +
                      ' ' +
                      numberWithCommas(
                        Number(tax.amount).toFixed(2).toString() || '0'
                      )}
                  </Text>
                </Box>
              </>
            )}
          </Box>
        )}

        {/* Total - Spacing after Total is deliberate */}
        {hasProducts && (
          <Text
            style={{
              width: wp(90),
              textAlign: 'right',
              marginTop: 10,
            }}
            variant={'SmallerTextM'}
            color={'text1'}
          >
            {`Total:  ${returnCurrency().code} ${numberWithCommas(
              finalTotal.toFixed(2).toString()
            )}`}
          </Text>
        )}

        {/* Horizontal Line */}
        <Box
          style={{
            width: wp(100),
            height: 1,
            backgroundColor: theme.colors.border4,
            marginTop: 20,
          }}
        />

        {/* Set sms checkbox */}
        {!isEdit && (
          <TouchableOpacity
            style={[
              styles.checkbox,
              {
                width: wp(90),
                alignSelf: 'center',
                marginBottom: 10,
                marginTop: 20,
              },
            ]}
            onPress={() => {
              setSendSms(!sendSms);
            }}
          >
            <Checkbox checked={sendSms} />

            <Text variant={'DetailsR'} color="text1" ml="m">
              Send invoice link to customer via SMS
            </Text>
          </TouchableOpacity>
        )}

        {/* Continue Button */}
        <Box style={{ marginTop: 20, alignSelf: 'center' }}>
          <Button
            label={isEdit ? 'Save Changes' : 'Create Invoice'}
            type="primary"
            onPress={handleProcessInvoice}
            loading={createInvoiceLoading}
          />
        </Box>
      </ScrollView>

      {/* Add Customer Modal */}
      <AddCustomerModal
        visible={addCustomerModal}
        setVisible={setAddCustomerModal}
        customers={allCustomers}
        setActiveCustomer={(item) => {
          setActiveCustomer(item);
          setCustomerOrders([]);
          setActiveOrder(undefined);
        }}
      />

      {/* Select Order Modal */}
      <SelectOrderModal
        visible={selectOrderModal}
        setVisible={setSelectOrderModal}
        orders={customerOrders}
        setOrder={setActiveOrder}
      />

      {/* Select Product Modal */}
      <SelectProductModal
        visible={showProductModal}
        setVisible={setShowProductModal}
        products={fetchedProducts}
        selectedProducts={products}
        setSelectedProducts={setProducts}
      />

      {/* Add Product Modal */}
      <NewProductModal
        visible={showAddProduct}
        setVisible={setShowAddProduct}
        setProduct={setProducts}
        products={products}
      />

      {/* Additional Payments Modal */}
      <AdditionalPaymentModal
        show={showAdditionalPayment}
        setShow={setShowAdditionalPayment}
        total={invoiceTotal}
        {...{
          setAddress,
          address,
          setDeposit,
          deposit,
          setDiscount,
          discount,
          dispatchFee,
          setDispatchFee,
          tax,
          setTax,
        }}
      />
    </KeyboardAvoidingView>
  );
};

export default CreateInvoice;

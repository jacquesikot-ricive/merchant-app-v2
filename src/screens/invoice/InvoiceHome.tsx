import React, { useState, useEffect } from 'react';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
  RefreshControl,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import { Feather } from '@expo/vector-icons';
import { useIsFocused } from '@react-navigation/native';

import theme, { Box, Text } from '../../components/Themed';
import { InvoiceNavParamList } from '../../navigation/InvoiceNav';
import SwitchPill from '../../components/SwitchPill';
import { useAppSelector } from '../../redux/hooks';
import InvoiceCard from '../../components/InvoiceCard';
import ScreenLoading from '../../components/ScreenLoading';
import ListEmpty from '../../components/ListEmpty';
import HeaderTextInput from '../../components/HeaderTextInput';
import invoiceApi from '../../api/riciveApi/invoice';
import ScreenError from '../../components/ScreenError';
import metricsApi from '../../api/riciveApi/metrics';
import { useQuery } from 'react-query';
import queryKeys from '../../constants/queryKeys';
import numberWithCommas from '../../utils/numbersWithComma';
import useCurrency from '../../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  switch: {
    height: hp(7),
  },
  list: {
    height: hp(62),
    marginBottom: 50,
  },
  newInvoice: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
    width: wp(90),
  },
  headerFlex: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: wp(90),
    alignItems: 'center',
    marginTop: 50,
  },
  dataBox: {
    width: wp(42),
    height: 68,
    borderRadius: 8,
    backgroundColor: theme.colors.white,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
});

type Props = NativeStackScreenProps<InvoiceNavParamList, 'InvoiceHome'>;

const InvoiceHome = ({ navigation }: Props): JSX.Element => {
  const isFocused = useIsFocused();
  const { returnCurrency } = useCurrency();
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [activeItem, setActiveItem] = useState<any>({
    name: 'Paid',
    value: 'paid',
  });
  const [invoices, setInvoices] = useState<any>([]);
  const [searchResult, setSearchResult] = useState<any[]>([]);
  const [metrics, setMetrics] = useState<any>();
  const [paidSearchResult, setPaidSearchResult] = useState<any>();
  const [unpaidSearchResult, setUnpaidSearchResult] = useState<any>();
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [refreshLoading, setRefreshLoading] = useState<boolean>(false);

  const paidInvoices = invoices.filter((i: any) => i.is_paid === true);
  const UnpaidInvoices = invoices
    .filter((i: any) => i.is_paid === false)
    .sort((a: any, b: any) => a.created_at - b.created_at);

  const [invoiceTotal, setInvoiceTotal] = useState<string>('');

  const calculatePaidTotal = () => {
    const value =
      paidSearchResult &&
      paidSearchResult
        .map((item: any) => Number(item.total))
        .reduce((a: any, b: any) => a + b, 0);
    setInvoiceTotal(value ? value.toString() : '0');
  };

  const calculateUnPaidTotal = () => {
    const value =
      unpaidSearchResult &&
      unpaidSearchResult
        .map((item: any) => Number(item.total))
        .reduce((a: any, b: any) => a + b, 0);
    setInvoiceTotal(value ? value.toString() : '0');
  };

  useEffect(() => {
    if (activeItem.value === 'paid') {
      return calculatePaidTotal();
    } else {
      return calculateUnPaidTotal();
    }
  }, [activeItem, paidSearchResult, unpaidSearchResult]);

  const {
    isLoading: isMetricsLoading,
    isError: isMetricsError,
    refetch: refetchMetrics,
  } = useQuery(
    queryKeys.ANALYTICS_DATA,
    async () => {
      try {
        const getHomeData = await metricsApi.getData(business_id);
        setMetrics(getHomeData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Sorry we cannot fetch metrics now, please try again later...',
        });
        console.log(
          'Error getting metrics:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const {
    isLoading: isInvoiceLoading,
    isError: isInvoiceError,
    refetch: refetchInvoice,
    isFetching: isFetchingInvoice,
  } = useQuery(queryKeys.INVOICE_DATA, async () => {
    try {
      const invoiceData = await invoiceApi.getInvoices({
        order: undefined,
        service: undefined,
      });
      const paidInvoices = invoiceData.data.filter(
        (i: any) => i.is_paid === true
      );
      const UnpaidInvoices = invoiceData.data
        .filter((i: any) => i.is_paid === false)
        .sort((a: any, b: any) => a.created_at - b.created_at);
      setInvoices(invoiceData.data);
      setSearchResult(invoiceData.data);
      setPaidSearchResult(paidInvoices);
      setUnpaidSearchResult(UnpaidInvoices);
    } catch (error: any) {
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Sorry we cannot fetch invoices now, please try again later...',
      });
      console.log(
        'Error getting invoice data:',
        JSON.stringify(error.response.data)
      );
    }
  });

  const handleSearch = async (e: any) => {
    const invoiceToSearch =
      activeItem.value === 'paid' ? paidInvoices : UnpaidInvoices;

    try {
      const firstNameSearch =
        invoiceToSearch &&
        invoiceToSearch.filter(
          (c: any) =>
            c.customer_customerToinvoice.first_name &&
            c.customer_customerToinvoice.first_name
              .trim()
              .toLowerCase()
              .includes(e.trim().toLowerCase())
        );

      const lastNameSearch =
        invoiceToSearch &&
        invoiceToSearch.filter((c: any) =>
          c.customer_customerToinvoice.last_name
            ? c.customer_customerToinvoice.last_name
                .trim()
                .toLowerCase()
                .includes(e.trim().toLowerCase())
            : ''
        );

      const phoneSearch =
        invoiceToSearch &&
        invoiceToSearch.filter((c: any) =>
          c.customer_customerToinvoice.phone
            ? c.customer_customerToinvoice.phone
                .trim()
                .toLowerCase()
                .includes(e.trim().toLowerCase())
            : ''
        );

      const idSearch =
        invoiceToSearch &&
        invoiceToSearch.filter((c: any) =>
          c.id
            ? c.id
                .toString()
                .trim()
                .toLowerCase()
                .includes(e.trim().toLowerCase())
            : ''
        );

      if (e.length !== '') {
        if (activeItem.value === 'paid') {
          setPaidSearchResult([
            ...firstNameSearch,
            ...lastNameSearch,
            ...phoneSearch,
            ...idSearch,
          ]);
        } else {
          setUnpaidSearchResult([
            ...firstNameSearch,
            ...lastNameSearch,
            ...phoneSearch,
            ...idSearch,
          ]);
        }
      } else {
        setPaidSearchResult([...paidInvoices]);
        setUnpaidSearchResult([...UnpaidInvoices]);
      }
    } catch (error) {
      console.log('Error in search:', error);
    }
  };

  const handleRefreshData = async () => {
    await refetchInvoice();
    await refetchMetrics();
  };

  useEffect(() => {
    isFocused && handleRefreshData();
  }, [isFocused]);

  if (isInvoiceError || isMetricsError) {
    return (
      <ScreenError
        onRetry={async () => await handleRefreshData()}
        onBackPress={() => navigation.goBack()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {(isInvoiceLoading || isMetricsLoading) && <ScreenLoading />}
      {/* Header */}
      <Box style={styles.header}>
        <Box style={{ paddingTop: 10 }}>
          <Box style={styles.headerFlex}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{ flexDirection: 'row', alignItems: 'center' }}
            >
              <Feather
                name="chevron-left"
                size={20}
                color={theme.colors.text1}
              />

              <Text variant="Body2M" color="text1">
                Invoices
              </Text>
            </TouchableOpacity>

            <Box flex={1} />

            {userPermissions.create_invoice && (
              <TouchableOpacity
                onPress={() => {
                  if (
                    metrics &&
                    metrics.hasCompleteBusiness &&
                    metrics.hasStorefront
                  ) {
                    navigation.navigate('CreateInvoice', {
                      isInvoiceNav: true,
                    });
                  } else {
                    Alert.alert(
                      'Complete Onboarding',
                      'Please complete your business onboarding to create invoices',
                      [
                        {
                          text: 'Cancel',
                          style: 'destructive',
                        },
                        {
                          text: 'Continue',
                          onPress: () =>
                            navigation.navigate('GettingStartedInvoice'),
                          style: 'default',
                        },
                      ]
                    );
                  }
                }}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <Feather name="plus" size={18} color={theme.colors.primary1} />
                <Text variant={'DetailsM'} color="primary1" ml="m">
                  New Invoice
                </Text>
              </TouchableOpacity>
            )}
          </Box>
        </Box>

        <Box
          style={{
            width: wp(100),
            height: 1,
            backgroundColor: theme.colors.border4,
            marginBottom: 10,
            marginTop: 20,
          }}
        />

        <HeaderTextInput
          type="search"
          width={wp(90)}
          height={40}
          placeholder="Search"
          onChangeText={(e) => handleSearch(e)}
        />

        {/* Main switch for paid and unpaid */}
        <Box mt="m" style={{ width: wp(100), paddingLeft: 20 }}>
          <SwitchPill
            items={[
              {
                name: 'Unpaid Invoices',
                value: 'unpaid',
              },
              {
                name: 'Paid Invoices',
                value: 'paid',
              },
            ]}
            {...{ activeItem, setActiveItem }}
          />
        </Box>
      </Box>

      {/* Invoice List */}
      <Box style={styles.list}>
        <FlatList
          refreshControl={
            <RefreshControl
              refreshing={refreshLoading}
              onRefresh={async () => {
                setRefreshLoading(true);
                await handleRefreshData();
                setRefreshLoading(false);
              }}
            />
          }
          showsVerticalScrollIndicator={false}
          data={
            activeItem.value === 'paid' ? paidSearchResult : unpaidSearchResult
          }
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{
            alignSelf: 'center',
          }}
          ListHeaderComponent={() => (
            // Invoice Data Cards
            <Box
              flexDirection={'row'}
              justifyContent={'space-between'}
              alignItems={'center'}
              style={{
                width: wp(100),
                paddingHorizontal: 20,
                marginVertical: 10,
                backgroundColor: theme.colors.bg12,
                paddingVertical: 15,
              }}
            >
              <Box style={styles.dataBox}>
                <Text variant={'SmallerTextR'} color="text5">
                  {'Total Amount'}
                </Text>
                <Text>
                  <Text variant={'Body1B'} color="text1">
                    {numberWithCommas(invoiceTotal)}
                  </Text>
                  <Text variant={'SmallerTextR'} color="text1">
                    {` (${returnCurrency().code})`}
                  </Text>
                </Text>
              </Box>

              <Box style={styles.dataBox}>
                <Text variant={'SmallerTextR'} color="text5">
                  {`Total ${activeItem.name.toLowerCase()}`}
                </Text>
                <Text variant={'Body1B'} color="text1">
                  {activeItem.value === 'paid'
                    ? paidSearchResult && paidSearchResult.length
                    : unpaidSearchResult && unpaidSearchResult.length}
                </Text>
              </Box>
            </Box>
          )}
          ListEmptyComponent={() => {
            if (isInvoiceLoading) {
              return <ActivityIndicator />;
            }
            return (
              <ListEmpty
                topText=""
                bottomText={`You have no ${activeItem.name.toLowerCase()}`}
                invoicePage
              />
            );
          }}
          renderItem={({ item, index }) => {
            return (
              <Box
                style={{
                  paddingTop: 20,
                  width: wp(100),
                  alignSelf: 'center',
                }}
              >
                <InvoiceCard
                  hasPadding
                  hasBorder
                  fullWidth
                  invoice={item}
                  onPress={() =>
                    navigation.navigate('InvoiceDetail', {
                      items: item.invoice_item,
                      dateCreated: new Date(item.created_at).toDateString(),
                      deliveryFee: item.delivery_fee,
                      id: item.id,
                      isSending: false,
                      serviceType: '',
                      status: item.is_paid ? 'PAID' : 'UNPAID',
                      total: Number(item.total).toFixed(2).toString(),
                      isInvoiceNav: false,
                      customerId: item.customer,
                      deliveryFeeType: item.delivery_type,
                      customerPhone: item.customer_customerToinvoice
                        ? item.customer_customerToinvoice.phone
                        : '',
                    })
                  }
                  customerDetails
                  invoiceId={`${item.id.substring(0, 5).toUpperCase()}`}
                  date={new Date(item.created_at).toDateString()}
                  total={item.total}
                  firstName={
                    item.customer_customerToinvoice
                      ? item.customer_customerToinvoice.first_name
                      : ''
                  }
                  lastName={
                    item.customer_customerToinvoice
                      ? item.customer_customerToinvoice.last_name
                      : ''
                  }
                />
              </Box>
            );
          }}
        />
      </Box>
    </Box>
  );
};

export default InvoiceHome;

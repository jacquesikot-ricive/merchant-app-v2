// import { NativeStackScreenProps } from '@react-navigation/native-stack';
// import React, { useEffect, useState } from 'react';
// import {
//   StyleSheet,
//   SafeAreaView,
//   FlatList,
//   ActivityIndicator,
// } from 'react-native';
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp,
// } from 'react-native-responsive-screen';
// import Toast from 'react-native-toast-message';

// import theme, { Box, Text } from '../components/Themed';
// import { OrderNavParaList } from '../navigation/OrdersNav';
// import StackHeader from '../components/StackHeader';
// import numberWithCommas from '../utils/numbersWithComma';
// import Button from '../components/Button';
// import { useAppSelector } from '../redux/hooks';
// import { Props } from '../navigation';
// import { useRoute } from '@react-navigation/native';
// import PayCashModal from '../components/PayCashModal';
// import supabase from '../../supabase';
// import db from '../api/db';
// import ScreenLoading from '../components/ScreenLoading';

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: theme.colors.white,
//     alignItems: 'center',
//   },
//   head: {
//     width: wp(90),
//     marginTop: 20,
//   },
//   list: {
//     backgroundColor: '#FEF6E9',
//     width: wp(100),
//     alignSelf: 'center',
//     paddingHorizontal: 20,
//     marginTop: 10,
//     paddingTop: 10,
//     maxHeight: hp(40),
//   },
//   rowItem: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center',
//     marginBottom: 15,
//   },
// });

// const InvoiceNavDetail = ({ navigation }: Props): JSX.Element => {
//   const route: any = useRoute();
//   const [payCashModal, setPayCashModal] = useState<boolean>(false);
//   const { invoice, invoiceItems } = route.params;
//   const [loading, setLoading] = useState<boolean>(false);
//   const [fetchLoading, setFetchLoading] = useState<boolean>(false);
//   const [invoiceItemsData, setInvoiceItemsData] = useState<any>();
//   // const merchantId = useAppSelector((state) => state.login.id);
//   const [fetchedIsPaid, setFetchedIsPaid] = useState<boolean>();

//   useEffect(() => {
//     const getInvoiceItemData = async () => {
//       console.log(invoice);
//       try {
//         setFetchLoading(true);
//         // const invoiceData = invoiceApi.getInvoiceById(invoice.id);
//         // setFetchedIsPaid(invoiceData.is_paid);
//         setInvoiceItemsData(invoiceItemData);
//         setFetchLoading(false);
//       } catch (error) {
//         setFetchLoading(false);
//         console.log(error);
//         Toast.show({
//           type: 'error',
//           text1: 'Server Error',
//           text2: 'Error getting invoice data, please try again later..',
//         });
//       }
//     };

//     if (invoiceItems.length < 1) {
//       getInvoiceItemData();
//     }

//     const invoiceSub = supabase
//       .from(db.tables.laundry_invoice)
//       .on('*', () => {
//         getInvoiceItemData();
//       })
//       .subscribe();

//     return () => {
//       supabase.removeSubscription(invoiceSub);
//     };
//   }, []);

//   if (fetchLoading) {
//     return <ScreenLoading />;
//   }

//   return (
//     <Box style={styles.container}>
//       <StackHeader
//         title={`Invoice #${invoice.id}`}
//         onBackPress={() => navigation.goBack()}
//       />

//       <Box style={styles.head}>
//         <Box flexDirection={'row'} justifyContent={'space-between'}>
//           <Text variant={'Body1M'}>{`Invoice #${invoice.id}`}</Text>

//           <Box
//             style={{
//               width: 80,
//               height: 24,
//               borderRadius: 15,
//               justifyContent: 'center',
//               alignItems: 'center',
//               backgroundColor:
//                 invoice && !invoice.is_paid
//                   ? theme.colors.errorLight
//                   : theme.colors.secondary2,
//             }}
//           >
//             <Text
//               variant={'SmallerTextM'}
//               color={invoice && !invoice.is_paid ? 'error' : 'primary1'}
//             >
//               {invoice && !invoice.is_paid ? 'UNPAID' : 'PAID'}
//             </Text>
//           </Box>
//         </Box>

//         <Text variant={'DetailsR'} color="text7" mt="m">
//           {`Date created: ${new Date(invoice.created_at).toDateString()}`}
//         </Text>

//         <Text variant={'Body2M'} color="text1" mt="m">
//           Service Type
//         </Text>

//         <Text variant={'Body2R'} color="text7">
//           {invoice && invoice.order_id && invoice.order_id.service_type.name}
//         </Text>

//         <Box style={styles.list}>
//           <Box style={styles.rowItem}>
//             <Text variant={'DetailsM'} color="text5">
//               Items
//             </Text>
//             <Text variant={'DetailsM'} color="text8">
//               Price
//             </Text>
//           </Box>

//           <FlatList
//             data={invoiceItems.length > 0 ? invoiceItems : invoiceItemsData}
//             keyExtractor={(item: any) => item.id.toString()}
//             renderItem={({ item }) => (
//               <Box style={styles.rowItem}>
//                 <Text variant={'DetailsM'} color="text5">
//                   {`${item.item_id.name} (${item.quantity})`}
//                 </Text>
//                 <Text variant={'DetailsM'} color="text8">
//                   {numberWithCommas(item.total)}
//                 </Text>
//               </Box>
//             )}
//           />

//           {/* {invoice &&
//             invoice.delivery_fee &&
//             invoice.delivery_fee.length > 0 &&
//             invoice.delivery_fee_type.length > 0 && (
//               <Box style={[styles.rowItem, { marginTop: 20 }]}>
//                 <Text variant={'DetailsM'} color="text8">
//                   {`Delivery fee (${''})`}
//                 </Text>
//                 <Text variant={'DetailsM'} color="text8">
//                   {`${numberWithCommas('')}`}
//                 </Text>
//               </Box>
//             )} */}

//           <Box style={[styles.rowItem, { marginTop: 5 }]}>
//             <Text variant={'Body2SB'} color="text8">
//               Total
//             </Text>
//             <Text variant={'Body2SB'} color="text8">
//               {`NGN ${numberWithCommas(invoice.total ? invoice.total : '')}`}
//             </Text>
//           </Box>
//         </Box>

//         {/* {isSending && (
//           <Box
//             style={{
//               marginTop: 40,
//             }}
//           >
//             <Button
//               type="primary"
//               onPress={handleCreateInvoice}
//               label="Send Invoice"
//               loading={loading}
//             />
//           </Box>
//         )} */}

//         {invoice && !invoice.is_paid && (
//           <Box
//             style={{
//               marginTop: 40,
//             }}
//           >
//             <Button
//               type="primary"
//               onPress={() => setPayCashModal(true)}
//               label="Pay Cash"
//               loading={loading}
//             />
//           </Box>
//         )}
//       </Box>

//       <PayCashModal
//         visible={payCashModal}
//         setVisible={setPayCashModal}
//         invoiceId={invoice.id}
//       />
//     </Box>
//   );
// };

// export default InvoiceNavDetail;

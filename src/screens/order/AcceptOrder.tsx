import { useRoute } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from '../../components/Themed';
import Button from '../../components/Button';
import DatePicker from '../../components/DatePicker';
import Picker from '../../components/Picker';
import StackHeader from '../../components/StackHeader';
import orderApi from '../../api/riciveApi/order';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { OrderNavParaList } from '../../navigation/OrdersNav';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
});

const times = [
  {
    label: '',
    value: '',
  },
  {
    label: '7:00am - 8:00am',
    value: '7:00am - 8:00am',
  },
  {
    label: '8:00am - 9:00am',
    value: '8:00am - 9:00am',
  },
  {
    label: '9:00am - 10:00am',
    value: '9:00am - 10:00am',
  },
  {
    label: '10:00am - 11:00am',
    value: '10:00am - 11:00am',
  },
  {
    label: '11:00am - 12:00pm',
    value: '11:00am - 12:00pm',
  },
  {
    label: '12:00pm - 1:00pm',
    value: '12:00pm - 1:00pm',
  },
  {
    label: '1:00pm - 2:00pm',
    value: '1:00pm - 2:00pm',
  },
  {
    label: '2:00pm - 3:00pm',
    value: '2:00pm - 3:00pm',
  },
  {
    label: '3:00pm - 4:00pm',
    value: '3:00pm - 4:00pm',
  },
  {
    label: '4:00pm - 5:00pm',
    value: '5:00pm - 5:00pm',
  },
];

type Props = NativeStackScreenProps<OrderNavParaList, 'AcceptOrder'>;

const AcceptOrder = ({ navigation, route }: Props): JSX.Element => {
  const { order_id, is_pickup, pickup_date, pickup_time } = route.params;
  const [selectedTime, setSelectedTime] = useState<string>(pickup_time);
  const [selectedDate, setSelectedDate] = useState<Date>(new Date(pickup_date));
  const [loading, setLoading] = useState<boolean>(false);

  const handleAcceptOrder = async () => {
    try {
      setLoading(true);
      await orderApi.acceptOrder({
        order_id,
        is_pickup,
        pickup_date: selectedDate.toISOString(),
        pickup_time: selectedTime,
      });
      setLoading(false);
      navigation.goBack();
    } catch (error) {
      console.log('Error accepting delivery order:', error);
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          'Sorry, we cannot process your request now, please try again later...',
      });
    }
  };

  return (
    <Box style={styles.container}>
      <StackHeader
        onBackPress={() => navigation.goBack()}
        title="Confirm  Schedule"
      />

      <Box style={{ height: 20 }} />

      <Picker
        data={times}
        setValue={setSelectedTime}
        value={selectedTime}
        label="Schedule time"
        placeholder=""
      />

      <Box style={{ height: 30 }} />

      <DatePicker
        date={selectedDate}
        setDate={setSelectedDate}
        label="Schedule date"
      />

      {/* Add multiline text input for reason for reschedule if any, 
      this should only be active if date is rescheduled */}

      <Box style={{ flex: 1 }} />

      <Box style={{ marginBottom: 100 }}>
        <Button
          label="Accept Order"
          type="primary"
          onPress={async () => await handleAcceptOrder()}
          loading={loading}
        />
      </Box>
    </Box>
  );
};

export default AcceptOrder;

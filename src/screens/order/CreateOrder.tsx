import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Alert,
} from 'react-native';
import Toast from 'react-native-toast-message';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
import DatePicker from 'react-native-date-picker';
import { useQuery } from 'react-query';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

import theme, { Box, Text } from '../../components/Themed';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import Picker from '../../components/Picker';
import StackHeader from '../../components/StackHeader';
import Checkbox from '../../components/Checkbox';
import AppAccordion from '../../components/AppAccordion';
// import DatePicker from '../../components/DatePicker';
import AddCustomerModal from '../../components/AddCustomerModal';
import TextInput from '../../components/TextInput';
import Button from '../../components/Button';
import serviceApi from '../../api/riciveApi/service';
import customerApi from '../../api/riciveApi/customer';
import ScreenError from '../../components/ScreenError';
import orderApi from '../../api/riciveApi/order';
import productApi from '../../api/riciveApi/product';
import SelectProductModal from '../../components/SelectProductsModal';
import NewProductModal from '../../components/NewProductModal';
import InvoiceItem from '../../components/InvoiceItem';
import numberWithCommas from '../../utils/numbersWithComma';
import AddDeliveryModal from '../../components/AddDeliveryModal';
import { OrderNavParaList } from '../../navigation/OrdersNav';
import DateTimePicker from '../../components/DateTimePicker';
import queryKeys from '../../constants/queryKeys';
import useCurrency from '../../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  form: {
    marginTop: 30,
    marginBottom: 100,
  },
  checkbox: {
    flexDirection: 'row',
    width: wp(90),
    alignItems: 'center',
    marginTop: 30,
  },
  customerCard: {
    marginTop: 10,
    width: wp(90),
    height: 82,
    borderRadius: 8,
    justifyContent: 'center',
    paddingLeft: 20,
    borderWidth: 1,
    marginBottom: 20,
    borderColor: theme.colors.border3,
  },
  removeCustomer: {
    height: 30,
    width: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 60,
    right: 20,
    backgroundColor: theme.colors.errorLight,
    zIndex: 1,
  },
  productButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  deliveryFee: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
  },
  plusContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(145, 206, 51, 0.2)',
    width: 32,
    height: 32,
    borderRadius: 16,
    alignSelf: 'center',
  },
});

// Pick up and Delivery time options TODO: This should come from the server and be related to the merchants open and close time.
const times = [
  {
    id: '1',
    value: '08:00 - 10:00 am',
    label: '08:00 - 10:00 am',
  },
  {
    id: '2',
    value: '10:00 - 12:00 am',
    label: '10:00 - 12:00 am',
  },
  {
    id: '3',
    value: '12:00 - 2:00 pm',
    label: '12:00 - 2:00 pm',
  },
  {
    id: '4',
    value: '2:00 - 04:00 pm',
    label: '2:00 - 04:00 pm',
  },
  {
    id: '5',
    value: '04:00 - 06:00 pm',
    label: '04:00 - 06:00 pm',
  },
];

type Props = NativeStackScreenProps<OrderNavParaList, 'CreateOrder'>;

const CreateOrder = ({ navigation, route }: Props): JSX.Element => {
  const { returnCurrency } = useCurrency();
  const { isHomeNav } = route.params;
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [loading, setLoading] = useState<boolean>(false);
  const [isPickup, setIsPickup] = useState<boolean>(false);
  const [isDelivery, setIsDelivery] = useState<boolean>(false);
  const [selectedTime, setSelectedTime] = useState<string>(times[0].value);
  const [selectedDate, setSelectedDate] = useState<Date>(new Date());
  const [addCustomerModal, setAddCustomerModal] = useState<boolean>(false);
  const [allCustomers, setAllCustomers] = useState<any[]>([]);
  const [activeCustomer, setActiveCustomer] = useState<any>();
  const [finalAddress, setFinalAddress] = useState<string>(''); // Address added by merchant for customer order
  const [finalDeliveryAddress, setFinalDeliveryAddress] = useState<string>(''); // Only useful if we need different pick up and delivery address
  const [createLoading, setCreateLoading] = useState<boolean>(false);
  const [selectedServiceId, setSelectedServiceId] = useState<string>();
  const [sendSms, setSendSms] = useState<boolean>(true);
  const [products, setProducts] = useState<any>([]);
  const [error, setError] = useState<boolean>(false);
  const [showProductModal, setShowProductModal] = useState<boolean>(false);
  const [showDeliveryModal, setShowDeliveryModal] = useState<boolean>(false);
  const [fetchedProducts, setFetchedProducts] = useState<any[]>([]);
  const [showAddProduct, setShowAddProduct] = useState<boolean>(false);
  const [orderPaid, setOrderPaid] = useState<boolean>(false);
  const [total, setTotal] = useState<number>(0);
  const [deliveryFee, setDeliveryFee] = useState<any>({
    amount: '',
    type: '',
  });
  const [openPickupTimePicker, setOpenPickupTimePicker] =
    useState<boolean>(false);
  const [openDeliveryTimePicker, setOpenDeliveryTimePicker] =
    useState<boolean>(false);
  const [pickupDateTime, setPickupDateTime] = useState<boolean>(false);
  const [deliveryDateTime, setDeliveryDateTime] = useState<boolean>(false);

  const {
    isLoading: isCustomersLoading,
    isError: isCustomersError,
    refetch: refetchCustomers,
  } = useQuery(
    queryKeys.CUSTOMERS_DATA,
    async () => {
      try {
        const customersData = await customerApi.getCustomers();
        if (customersData) setAllCustomers(customersData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Error getting customer data, try again later..',
        });
        console.log(
          'Error getting customers data',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const {
    isLoading: isProductsLoading,
    isError: isProductsError,
    refetch: refetchProducts,
  } = useQuery(
    queryKeys.PRODUCTS_DATA,
    async () => {
      try {
        const data = await productApi.getProducts(business_id);
        setFetchedProducts(data.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Error getting products data, try again later..',
        });
        console.log(
          'Error getting create order data',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const refreshData = async () => {
    await refetchCustomers();
    await refetchProducts();
  };

  useEffect(() => {
    refreshData();
  }, [showAddProduct]);

  useEffect(() => {
    const calculateTotal = () => {
      const value = products
        .map((item: any) => Number(item.total))
        .reduce((a: any, b: any) => a + b, 0);
      setTotal(value + Number(deliveryFee.amount));
    };

    calculateTotal();
  }, [products, deliveryFee]);

  if (isProductsError || isCustomersError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => await refreshData()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {(isProductsLoading || isCustomersLoading) && <ScreenLoading />}
      <StackHeader
        title="Create Order"
        onBackPress={() => navigation.goBack()}
        hasBorder
      />

      {/* <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'position' : 'height'}
      > */}
      <ScrollView showsVerticalScrollIndicator={false} style={styles.form}>
        {/* Customer Selector */}
        <Text variant={'SmallerTextR'} color="text1">
          Select Customer
        </Text>

        {/* Remove active customer */}
        {activeCustomer && (
          <TouchableOpacity
            onPress={() => setActiveCustomer(undefined)}
            style={styles.removeCustomer}
          >
            <Icon name="x" color={theme.colors.error} size={15} />
          </TouchableOpacity>
        )}

        <TouchableOpacity
          onPress={() => setAddCustomerModal(true)}
          style={[
            styles.customerCard,
            {
              borderStyle: activeCustomer ? undefined : 'dashed',
            },
          ]}
        >
          {activeCustomer ? (
            <>
              <Text variant={'DetailsM'}>{`${activeCustomer.first_name} ${
                activeCustomer.last_name ? activeCustomer.last_name : ''
              }`}</Text>
              <Text variant={'DetailsR'} color="text5">
                {activeCustomer.phone}
              </Text>
            </>
          ) : (
            <Box style={styles.plusContainer}>
              <Icon
                name="plus"
                color={theme.colors.primary1}
                size={24}
                style={{ alignSelf: 'center' }}
              />
            </Box>
          )}
        </TouchableOpacity>

        {/* Products */}

        <Box
          flexDirection={'row'}
          alignItems="center"
          justifyContent={'space-between'}
        >
          <Text variant={'DetailsR'}>Products</Text>

          <Box flexDirection={'row'}>
            <TouchableOpacity
              style={styles.productButton}
              onPress={() => setShowAddProduct(true)}
            >
              <Box
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 9,
                  borderWidth: 1,
                  borderColor: theme.colors.primary1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Icon name="plus" size={12} color={theme.colors.primary1} />
              </Box>
              <Text variant={'DetailsM'} color="primary1" ml="s" mr="m">
                New Product
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => setShowProductModal(true)}
              style={styles.productButton}
            >
              <Box
                style={{
                  width: 17,
                  height: 17,
                  borderRadius: 9,
                  borderWidth: 1,
                  borderColor: theme.colors.primary1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Icon
                  name="chevron-right"
                  size={13}
                  color={theme.colors.primary1}
                />
              </Box>
              <Text variant={'DetailsM'} color="primary1" ml="s">
                Select Products
              </Text>
            </TouchableOpacity>
          </Box>
        </Box>

        {products.length > 0 &&
          products.map((item: any, index: number) => (
            <InvoiceItem
              image={item.image}
              key={item.id.toString()}
              count={item.quantity}
              name={item.name}
              price={item.price}
              handleMinus={() => {
                if (Number(item.quantity) > 1) {
                  const newQuantity = Number(item.quantity) - 1;

                  const data = {
                    ...item,
                    quantity: newQuantity.toString(),
                    total: Number(item.price) * newQuantity,
                  };

                  const res = products.filter((i: any) => i.id !== item.id);

                  res.splice(index, 0, data);

                  setProducts(res);
                } else {
                  setProducts([
                    ...products.filter((i: any) => i.id !== item.id),
                  ]);
                }
              }}
              handlePlus={() => {
                const newQuantity = Number(item.quantity) + 1;

                const data = {
                  ...item,
                  quantity: newQuantity.toString(),
                  total: Number(item.price) * newQuantity,
                };

                const res = products.filter((i: any) => i.id !== item.id);

                res.splice(index, 0, data);

                setProducts(res);
              }}
              handleRemove={() => {
                const res = products.filter((i: any) => i.id !== item.id);
                setProducts(res);
              }}
            />
          ))}

        {/* Set pick up checkbox */}

        <TouchableOpacity
          style={styles.checkbox}
          onPress={() => {
            if (!activeCustomer)
              return Alert.alert('No Customer', 'Add customer to continue');

            setIsPickup(!isPickup);
          }}
        >
          <Checkbox checked={isPickup} />

          <Text variant={'DetailsR'} color="text1" ml="l">
            Does this order require dispatch?
          </Text>
        </TouchableOpacity>

        {/* Pickup details accordion */}
        {isPickup && (
          <AppAccordion
            height={hp(65)}
            title="Dispatch Details"
            content={
              <Box alignItems={'center'} mt="s">
                <Text
                  style={{
                    fontFamily: 'basier-regular',
                    fontSize: 12,
                    color: '#908F8F',
                    marginBottom: 15,
                    width: wp(80),
                    lineHeight: 20,
                  }}
                >
                  Enter dispatch details and custom fee you would want to charge
                  this customer.
                </Text>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => setOpenPickupTimePicker(!openPickupTimePicker)}
                >
                  <DateTimePicker
                    label="Pickup details"
                    setDate={setPickupDateTime}
                    setShow={setOpenPickupTimePicker}
                    show={openPickupTimePicker}
                    date={pickupDateTime}
                  />
                </TouchableOpacity>
                <Box style={{ height: 20 }} />
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() =>
                    setOpenDeliveryTimePicker(!openDeliveryTimePicker)
                  }
                >
                  <DateTimePicker
                    label="Delivery details"
                    setDate={setDeliveryDateTime}
                    setShow={setOpenDeliveryTimePicker}
                    show={openDeliveryTimePicker}
                    date={deliveryDateTime}
                  />
                </TouchableOpacity>
                <Box style={{ height: 20 }} />

                <Box>
                  <>
                    <Text
                      variant={'DetailsR'}
                      color="text1"
                      mb="m"
                      style={{ textAlign: 'left', width: '100%' }}
                    >
                      Customer's address
                    </Text>

                    <TextInput
                      type="input"
                      autoCompleteType="off"
                      autoCorrect={false}
                      keyboardType="default"
                      returnKeyType="done"
                      autoCapitalize="words"
                      placeholder={'Customer Pickup Address'}
                      onChangeText={(e) => setFinalAddress(e)}
                      value={finalAddress}
                      width={wp(80)}
                    />
                  </>
                </Box>

                <Box
                  style={{
                    width: wp(80),
                    marginTop: 20,
                    alignSelf: 'center',
                  }}
                >
                  <Box
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <Text variant="DetailsM" color="text6" mb="s">
                      Dispatch fee (optional)
                    </Text>

                    {deliveryFee.amount.length > 0 && (
                      <Box flexDirection={'row'}>
                        <TouchableOpacity
                          onPress={() => setShowDeliveryModal(true)}
                        >
                          <Text variant="DetailsM" color="primary4" mb="s">
                            Change
                          </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          onPress={() =>
                            setDeliveryFee({
                              amount: '',
                              type: '',
                            })
                          }
                        >
                          <Text variant="DetailsM" color="error" mb="s" ml="l">
                            Cancel
                          </Text>
                        </TouchableOpacity>
                      </Box>
                    )}
                  </Box>

                  {deliveryFee.amount.length < 1 && (
                    <Text variant="SmallerTextR" color="text7">
                      Enter a custom delivery fee for this order
                    </Text>
                  )}

                  {deliveryFee.amount.length > 0 && (
                    <Box
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}
                    >
                      <Text
                        variant={'SmallerTextM'}
                        color="text7"
                      >{`Fees (${deliveryFee.type})`}</Text>
                      <Text variant={'SmallerTextM'} color="primary3">
                        {`${returnCurrency().code} ${numberWithCommas(
                          deliveryFee.amount
                        )}`}
                      </Text>
                    </Box>
                  )}

                  {deliveryFee.amount.length < 1 && (
                    <TouchableOpacity
                      style={styles.deliveryFee}
                      onPress={() => setShowDeliveryModal(true)}
                    >
                      <Icon
                        name="plus"
                        color={theme.colors.primary4}
                        size={14}
                      />
                      <Text variant="DetailsM" color="primary4" ml="m">
                        Add delivery fee
                      </Text>
                    </TouchableOpacity>
                  )}
                </Box>
              </Box>
            }
          />
        )}

        {/* Set sms checkbox */}

        <TouchableOpacity
          style={styles.checkbox}
          onPress={() => {
            setSendSms(!sendSms);
          }}
        >
          <Checkbox checked={sendSms} />

          <Text variant={'DetailsR'} color="text1" ml="l">
            Send invoice link to customer via SMS
          </Text>
        </TouchableOpacity>

        {/* Set paid checkbox */}
        <TouchableOpacity
          style={[styles.checkbox]}
          onPress={() => {
            setOrderPaid(!orderPaid);
          }}
        >
          <Checkbox checked={orderPaid} />

          <Text variant={'DetailsR'} color="text1" ml="l">
            Has payment been collected?
          </Text>
        </TouchableOpacity>

        <Box style={{ height: 200 }} />
      </ScrollView>
      {/* </KeyboardAvoidingView> */}

      {/* Button */}
      <Box
        style={{
          position: 'absolute',
          zIndex: 1,
          bottom: 0,
          borderTopWidth: 1,
          borderColor: theme.colors.border4,
          paddingTop: 20,
          width: wp(100),
          paddingBottom: 20,
          alignItems: 'center',
          backgroundColor: theme.colors.white,
        }}
      >
        <Button
          type="primary"
          label="Create Order"
          onPress={() => {
            navigation.navigate('OrderCreateDetail', {
              invoice: {
                created_at: new Date().toISOString(),
                is_paid: orderPaid,
                total: total.toString(),
              },
              invoiceItems: products,
              deliveryFee: deliveryFee,
              isDelivery: isDelivery,
              isPickup: isPickup,
              activeCustomer,
              finalAddress,
              selectedDate: selectedDate.toISOString(),
              selectedTime: selectedDate.toTimeString().slice(0, -18),
              sendSms: sendSms,
              isHomeNav,
            });
          }}
          loading={createLoading}
          disabled={!activeCustomer ? true : false}
        />
      </Box>

      {/* Add Customer Modal */}
      <AddCustomerModal
        visible={addCustomerModal}
        setVisible={setAddCustomerModal}
        customers={allCustomers}
        setActiveCustomer={setActiveCustomer}
      />

      {/* Select Product Modal */}
      <SelectProductModal
        visible={showProductModal}
        setVisible={setShowProductModal}
        products={fetchedProducts}
        selectedProducts={products}
        setSelectedProducts={setProducts}
      />

      {/* Add Product Modal */}
      <NewProductModal
        visible={showAddProduct}
        setVisible={setShowAddProduct}
        setProduct={setProducts}
        products={products}
      />

      {/* Add Delivery Fee Modal */}
      <AddDeliveryModal
        visible={showDeliveryModal}
        setVisible={setShowDeliveryModal}
        setFee={setDeliveryFee}
      />
    </Box>
  );
};

export default CreateOrder;

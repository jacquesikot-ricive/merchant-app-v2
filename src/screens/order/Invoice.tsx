import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Feather as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import AddDeliveryModal from '../../components/AddDeliveryModal';
import Button from '../../components/Button';
import FloatingButton from '../../components/FloatingButton';
import StackHeader from '../../components/StackHeader';
import theme, { Box, Text } from '../../components/Themed';
import { OrderNavParaList } from '../../navigation/OrdersNav';
import InvoiceItem from '../../components/InvoiceItem';
import AddInvoiceItemModal from '../../components/AddInvoiceItemModal';
import numberWithCommas from '../../utils/numbersWithComma';
import ListEmpty from '../../components/ListEmpty';
import productApi from '../../api/riciveApi/product';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import SelectProductModal from '../../components/SelectProductsModal';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  list: {
    backgroundColor: theme.colors.light,
    height: hp(41),
  },
  listTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(100),
    paddingHorizontal: 20,
    alignSelf: 'center',
    paddingTop: 10,
    backgroundColor: theme.colors.light,
  },
  total: {
    width: '100%',
    height: 1,
    marginTop: 10,
    backgroundColor: theme.colors.border1,
  },
  plusButton: {
    position: 'absolute',
    right: 5,
    bottom: 260,
  },
  deliveryFee: {
    flexDirection: 'row',
    marginTop: 17,
    alignItems: 'center',
  },
});

type Props = NativeStackScreenProps<OrderNavParaList, 'Invoice'>;

const Invoice = ({ navigation, route }: Props): JSX.Element => {
  const { order, customerId, customerPhone, isInvoiceNav, isHomeNav } =
    route.params;
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openItemModal, setOpenItemModal] = useState<boolean>(false);
  const [items, setItems] = useState<any>([]);
  const [invoiceItems, setInvoiceItems] = useState<any>([]);
  const [deliveryFee, setDeliveryFee] = useState<any>({
    amount: '',
    type: '',
  });
  const [total, setTotal] = useState<number>(0);
  const [itemCount, setItemCount] = useState<string>();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  const getProducts = async () => {
    setError(false);
    try {
      setLoading(true);
      const data = await productApi.getProducts(business_id);
      setItems(data.data);
      setLoading(false);
    } catch (error: any) {
      console.log(
        'Error getting products for invoice:',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'Error getting invoice products',
      });
    }
  };

  useEffect(() => {
    getProducts();

    navigation.addListener('focus', async () => await getProducts());

    return () => {
      navigation.removeListener('focus', async () => await getProducts());
    };
  }, []);

  useEffect(() => {
    const calculateTotal = () => {
      const value = invoiceItems
        .map((item: any) => Number(item.total))
        .reduce((a: any, b: any) => a + b, 0);
      setTotal(value + Number(deliveryFee.amount));
    };

    const calculateCount = () => {
      const value = invoiceItems
        .map((item: any) => Number(item.quantity))
        .reduce((a: any, b: any) => a + b, 0);

      setItemCount(value);
    };

    calculateTotal();
    calculateCount();
  }, [invoiceItems, deliveryFee]);

  if (loading) {
    return <ScreenLoading />;
  }

  if (error) {
    <ScreenError
      onBackPress={() => navigation.goBack()}
      onRetry={getProducts}
    />;
  }

  return (
    <Box style={styles.container}>
      <StackHeader
        title={
          order ? `Order #${order.id.substring(0, 5).toUpperCase()}` : 'Invoice'
        }
        onBackPress={() => navigation.goBack()}
        icon1={
          <Box flexDirection={'row'} alignItems="center">
            <Icon name="plus-circle" size={30} color={theme.colors.primary1} />
          </Box>
        }
        onPressIcon1={() => setOpenItemModal(true)}
      />

      <Box>
        <Box style={styles.listTop}>
          <Text variant="Body2M" color="text8">
            Total
          </Text>
          <Text variant="Body2M" color="text8">
            {`NGN ${numberWithCommas(total.toString())}`}
          </Text>
        </Box>

        <Box style={styles.list}>
          <FlatList
            data={invoiceItems}
            keyExtractor={(item: any) => item.id.toString()}
            renderItem={({ item, index }) => (
              <InvoiceItem
                hPadding
                image={item.image}
                count={item.quantity}
                name={item.name}
                price={item.price}
                handleMinus={() => {
                  if (Number(item.quantity) > 1) {
                    const newQuantity = Number(item.quantity) - 1;

                    const data = {
                      ...item,
                      quantity: newQuantity.toString(),
                      total: Number(item.price) * newQuantity,
                    };

                    const res = invoiceItems.filter(
                      (i: any) => i.id !== item.id
                    );

                    res.splice(index, 0, data);

                    setInvoiceItems(res);
                  } else {
                    setInvoiceItems([
                      ...invoiceItems.filter((i: any) => i.id !== item.id),
                    ]);
                  }
                }}
                handlePlus={() => {
                  const newQuantity = Number(item.quantity) + 1;

                  const data = {
                    ...item,
                    quantity: newQuantity.toString(),
                    total: Number(item.price) * newQuantity,
                  };

                  const res = invoiceItems.filter((i: any) => i.id !== item.id);

                  res.splice(index, 0, data);

                  setInvoiceItems(res);
                }}
                handleRemove={() => {
                  const res = invoiceItems.filter((i: any) => i.id !== item.id);
                  setInvoiceItems(res);
                }}
              />
            )}
          />
        </Box>

        {/* <Box style={styles.plusButton}>
          <FloatingButton onPress={() => setOpenItemModal(true)} />
        </Box> */}

        <Box style={{ width: wp(90), marginTop: 20, alignSelf: 'center' }}>
          <Box
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <Text variant="Body2M" color="text6" mb="s">
              Delivery fees (optional)
            </Text>

            {deliveryFee.amount.length > 0 && (
              <Box flexDirection={'row'}>
                <TouchableOpacity onPress={() => setOpenModal(true)}>
                  <Text variant="DetailsM" color="primary4" mb="s">
                    Change
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() =>
                    setDeliveryFee({
                      amount: '',
                      type: '',
                    })
                  }
                >
                  <Text variant="DetailsM" color="error" mb="s" ml="l">
                    Cancel
                  </Text>
                </TouchableOpacity>
              </Box>
            )}
          </Box>

          {deliveryFee.amount.length < 1 && (
            <Text variant="DetailsR" color="text7">
              Enter a custom delivery fee for this order
            </Text>
          )}

          {deliveryFee.amount.length > 0 && (
            <Box
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <Text
                variant={'SmallerTextM'}
                color="text7"
              >{`Fees (${deliveryFee.type})`}</Text>
              <Text variant={'SmallerTextM'} color="primary3">
                {`NGN ${numberWithCommas(deliveryFee.amount)}`}
              </Text>
            </Box>
          )}

          {deliveryFee.amount.length < 1 && (
            <TouchableOpacity
              style={styles.deliveryFee}
              onPress={() => setOpenModal(true)}
            >
              <Icon name="plus" color={theme.colors.primary4} size={20} />
              <Text variant="DetailsM" color="primary4" ml="m">
                Add delivery fee
              </Text>
            </TouchableOpacity>
          )}
        </Box>
        <Box
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: deliveryFee.amount.length > 0 ? 55 : 15,
          }}
        >
          <Button
            type="primary"
            label="Next"
            onPress={() =>
              navigation.navigate('InvoiceDetail', {
                items: invoiceItems,
                dateCreated: new Date().toDateString(),
                deliveryFee:
                  deliveryFee.amount.length > 0 ? deliveryFee : undefined,
                id: order ? order.id : undefined,
                isSending: true,
                status: 'UNPAID',
                total: total.toString(),
                customerId: order ? order.customer : customerId,
                isInvoiceNav,
                customerPhone: order ? order.phone : customerPhone,
                isHomeNav,
              })
            }
          />
        </Box>

        <AddDeliveryModal
          visible={openModal}
          setVisible={setOpenModal}
          setFee={setDeliveryFee}
        />

        {/* Add Product Modal - Do this in the future, better UX */}
        {/* <NewProductModal
        visible={showAddProduct}
        setVisible={setShowAddProduct}
        setProduct={setProducts}
        products={products}
      /> */}

        {/* <AddInvoiceItemModal
          items={invoiceItems}
          data={items}
          setItem={setInvoiceItems}
          setVisible={setOpenItemModal}
          visible={openItemModal}
        /> */}
        <SelectProductModal
          visible={openItemModal}
          setVisible={setOpenItemModal}
          products={items}
          selectedProducts={invoiceItems}
          setSelectedProducts={setInvoiceItems}
        />
      </Box>
    </Box>
  );
};

export default Invoice;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  FlatList,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import * as Sharing from 'expo-sharing';
import * as FileSystem from 'expo-file-system';
import { Feather as Icon, Ionicons } from '@expo/vector-icons';
import Clipboard from '@react-native-clipboard/clipboard';
import { useQuery } from 'react-query';
import { CommonActions, useIsFocused } from '@react-navigation/native';
import { useAnalytics } from '@segment/analytics-react-native';

import theme, { Box, Text } from '../../components/Themed';
import { OrderNavParaList } from '../../navigation/OrdersNav';
import StackHeader from '../../components/StackHeader';
import numberWithCommas from '../../utils/numbersWithComma';
import Button from '../../components/Button';
import { useAppSelector } from '../../redux/hooks';
import PayCashModal from '../../components/PayCashModal';
import ScreenLoading from '../../components/ScreenLoading';
import invoiceApi from '../../api/riciveApi/invoice';
import { BASE_URL } from '../../api/riciveApi/client';
import ScreenError from '../../components/ScreenError';
import Checkbox from '../../components/Checkbox';
import ShareLinkIcon from '../../svg/ShareLinkIcon';
import storefrontApi from '../../api/riciveApi/storefront';
import queryKeys from '../../constants/queryKeys';
import ItemMenuIcon from '../../svg/ItemMenuIcon';
import useCurrency from '../../hooks/useCurrency';
import InvoiceItem from '../../components/InvoiceItem';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  head: {
    marginTop: 20,
  },
  list: {
    backgroundColor: theme.colors.bg5,
    width: wp(90),
    alignSelf: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginTop: 10,
    paddingTop: 10,
    maxHeight: hp(40),
    borderRadius: 8,
  },
  rowItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 5,
  },
  checkbox: {
    flexDirection: 'row',
    width: wp(90),
    alignItems: 'center',
    marginTop: 30,
  },
  border: {
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 0.6,
    width: wp(100),
    marginTop: 10,
  },
});

type Props = NativeStackScreenProps<OrderNavParaList, 'InvoiceDetail'>;

const InvoiceDetail = ({ navigation, route }: Props): JSX.Element => {
  const { track } = useAnalytics();
  const isFocused = useIsFocused();
  const { returnCurrency } = useCurrency();
  const [payCashModal, setPayCashModal] = useState<boolean>(false);
  const {
    dateCreated,
    deliveryFee,
    id,
    isSending,
    items,
    status,
    total,
    customerId,
    deliveryFeeType,
    isInvoiceNav,
    customerPhone,
    isHomeNav,
  } = route.params;
  const [loading, setLoading] = useState<boolean>(false);
  const [deleteLoading, setDeleteLoading] = useState<boolean>(false);
  const [fetchLoading, setFetchLoading] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [invoice, setInvoice] = useState<any>();
  const [sendSms, setSendSms] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);
  const [reminderLoading, setRemindLoading] = useState<boolean>(false);
  const [shareLoading, setShareLoading] = useState<boolean>(false);
  const [storefront, setStorefront] = useState<any>();
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [subTotal, setSubtotal] = useState<string>('');

  const handleCreateInvoice = async () => {
    try {
      setLoading(true);
      await invoiceApi.createInvoice({
        customer: customerId,
        is_cash: false,
        total,
        delivery_fee:
          deliveryFee && deliveryFee.amount ? deliveryFee.amount : undefined,
        delivery_type:
          deliveryFee && deliveryFee.type ? deliveryFee.type : undefined,
        invoice_items: items,
        order: id ? id : undefined,
        sendSMS: sendSms,
      });
      track('Create Invoice With Products', {
        business: business_id,
        total,
        customer: customerId,
        is_cash: false,
      });
      setLoading(false);
      Toast.show({
        type: 'success',
        text1: 'Invoice Created',
        text2: `Your Invoice has been created successfully`,
      });

      if (isInvoiceNav) {
        return navigation.navigate('InvoiceHome');
      }

      if (isHomeNav) {
        console.log('got here');
        navigation.dispatch(
          CommonActions.navigate({
            name: 'HomeScreen',
          })
        );
      } else {
        return navigation.navigate('OrderDetails', {
          id,
        });
      }
    } catch (error: any) {
      console.log(
        'Error creating invoice:',
        JSON.stringify(error.response.data)
      );
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We cannot process your order now, please try again later..',
      });
    }
  };

  const {
    isLoading: isInvoiceLoading,
    isError: isInvoiceError,
    refetch: refetchInvoice,
    isFetching: isFetchingInvoice,
  } = useQuery(
    queryKeys.INVOICE_DATA + business_id,
    async () => {
      try {
        const res = await invoiceApi.getOneInvoice(id ? id : '');
        setInvoice(res.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Sorry we cannot fetch invoice now, please try again later...',
        });
        console.log(
          'Error getting invoice:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!id,
    }
  );

  const {
    isLoading: isStorefrontLoading,
    isError: isStorefrontError,
    refetch: refetchStorefront,
    isFetching: isFetchingStorefront,
  } = useQuery(
    queryKeys.STOREFRONT_DATA,
    async () => {
      try {
        const storefront = await storefrontApi.getStorefront(business_id);
        setStorefront(storefront.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2:
            'Sorry we cannot fetch storefront now, please try again later...',
        });
        console.log(
          'Error getting storefront:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const handleShare = async () => {
    try {
      setShareLoading(true);

      const download = FileSystem.createDownloadResumable(
        `${BASE_URL}/invoice/${id}/download`,
        FileSystem.documentDirectory + 'invoice.pdf'
      );

      const file = await download.downloadAsync();

      track('Shared Invoice', {
        business: business_id,
      });

      if (file?.uri) {
        await Sharing.shareAsync(file?.uri, {
          UTI: '.pdf',
          mimeType: 'application/pdf',
        });
        setShareLoading(false);
      }
    } catch (error) {
      setShareLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We cannot process your sharing now, please try again later..',
      });
    }
  };

  const handleSendReminder = async () => {
    try {
      setRemindLoading(true);
      await invoiceApi.sendReminder(invoice.id);
      Toast.show({
        type: 'success',
        text1: 'Reminder Sent',
        text2: 'An SMS reminder has been sent to this customer',
      });
      track('Send Invoice Reminder', {
        business: business_id,
      });
      setRemindLoading(false);
    } catch (error) {
      setRemindLoading(false);
      console.log(error);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We cannot process your reminder now, please try again later..',
      });
    }
  };

  const handleCopyInvoiceLink = () => {
    Clipboard.setString(
      `https://${storefront && storefront.domain}.ricive.shop/invoice/${
        invoice.id
      }`
    );
    Toast.show({
      type: 'success',
      text1: 'Copied!',
      text2: 'URL copied successfully!',
    });
  };

  const onRefreshData = async () => {
    await refetchInvoice();
    await refetchStorefront();
  };

  const hasAdditionalInfo =
    (invoice &&
      invoice.delivery_fee &&
      invoice.delivery_fee &&
      invoice.delivery_fee.toString().length > 0) ||
    (invoice && invoice.discount && invoice.discount.length > 0) ||
    (invoice && invoice.deposit && invoice.deposit.toString().length > 0) ||
    (invoice && invoice.tax && invoice.tax.length > 0);

  const finalTotal =
    Number(subTotal) +
    +Number(
      invoice && invoice.tax && !isNaN(invoice.tax) && invoice.tax.length > 0
        ? invoice.tax
        : '0'
    ) +
    Number(
      invoice &&
        invoice.delivery_fee &&
        invoice.delivery_fee.toString().length > 0
        ? invoice.delivery_fee
        : '0'
    ) -
    (Number(
      invoice &&
        invoice.discount &&
        !isNaN(invoice.discount) &&
        invoice.discount.length > 0
        ? invoice.discount
        : '0'
    ) +
      Number(
        invoice && invoice.deposit && invoice.deposit.toString().length > 0
          ? invoice.deposit
          : '0'
      ));

  useEffect(() => {
    isFocused && onRefreshData();
  }, [isFocused]);

  const handleDeleteInvoice = async () => {
    Alert.alert(
      'Delete Invoice',
      'Are you sure you want to delete this invoice',
      [
        {
          text: 'No',
          style: 'cancel',
        },
        {
          text: 'Yes',
          style: 'destructive',
          onPress: async () => {
            setDeleteLoading(true);
            try {
              await invoiceApi.deleteInvoice(invoice && invoice.id);

              navigation.goBack();

              Toast.show({
                type: 'success',
                text1: 'Invoice Deleted',
                text2: 'Invoice Delete Success',
              });

              setDeleteLoading(false);
            } catch (error) {
              setDeleteLoading(false);
              Toast.show({
                type: 'error',
                text1: 'Server Error',
                text2: 'We delete you invoice now..',
              });
            }
          },
        },
      ]
    );
  };

  if (isStorefrontError || isInvoiceError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={onRefreshData}
      />
    );
  }

  useEffect(() => {
    const calculateTotal = () => {
      const value =
        invoice &&
        invoice.invoice_item &&
        invoice.invoice_item
          .map((item: any) => Number(item.total))
          .reduce((a: any, b: any) => a + b, 0);

      setSubtotal((value && value.toString()) || '0');
    };

    calculateTotal();
  }, [invoice]);

  const findPercentage = (amount: string) => {
    const percentage = (100 * Number(amount)) / Number(subTotal);
    return percentage.toFixed().toString();
  };

  const invocieFirstName =
    invoice &&
    invoice.customer_customerToinvoice &&
    invoice.customer_customerToinvoice.first_name
      ? invoice.customer_customerToinvoice.first_name
      : '';

  const invocieLastName =
    invoice &&
    invoice.customer_customerToinvoice &&
    invoice.customer_customerToinvoice.last_name
      ? invoice.customer_customerToinvoice.last_name
      : '';

  return (
    <Box style={styles.container}>
      {(isInvoiceLoading ||
        isFetchingInvoice ||
        isFetchingStorefront ||
        isStorefrontLoading ||
        deleteLoading) && <ScreenLoading />}
      <StackHeader
        title={`Invoice #${id ? id.substring(0, 5).toUpperCase() : ''}`}
        onBackPress={() => navigation.goBack()}
        hasBorder
        icon1={
          <TouchableOpacity
            style={{
              padding: 10,
            }}
            onPress={() => {
              setShowMenu(!showMenu);
            }}
          >
            <ItemMenuIcon />
          </TouchableOpacity>
        }
        onPressIcon1={() => setShowMenu(!showMenu)}
      />

      {showMenu && (
        <Box
          style={{
            paddingVertical: 10,
            paddingHorizontal: 20,
            zIndex: 4,
            position: 'absolute',
            right: 20,
            top: 90,
            borderRadius: 8,
            backgroundColor: theme.colors.white,

            shadowColor: theme.colors.text3,
            shadowOffset: {
              width: 0,
              height: 0,
            },
            shadowOpacity: 0.3,
            shadowRadius: 6,
            elevation: 1,
          }}
        >
          {userPermissions.edit_invoice && !isSending && (
            <TouchableOpacity
              style={{
                padding: 5,
                flexDirection: 'row',
                alignItems: 'center',
              }}
              onPress={() => {
                setShowMenu(false);
                navigation.navigate('CreateInvoice', {
                  isInvoiceNav: true,
                  isEdit: true,
                  customer:
                    invoice && invoice.customer_customerToinvoice
                      ? {
                          id:
                            invoice &&
                            invoice.customer_customerToinvoice &&
                            invoice.customer_customerToinvoice.id,
                          first_name:
                            invoice &&
                            invoice.customer_customerToinvoice &&
                            invoice.customer_customerToinvoice.first_name,
                          last_name:
                            invoice &&
                            invoice.customer_customerToinvoice &&
                            invoice.customer_customerToinvoice.last_name,
                          phone:
                            invoice &&
                            invoice.customer_customerToinvoice &&
                            invoice.customer_customerToinvoice.phone,
                        }
                      : undefined,
                  order:
                    invoice && invoice.order_invoiceToorder
                      ? {
                          id:
                            invoice &&
                            invoice.order_invoiceToorder &&
                            invoice.order_invoiceToorder.id,
                          status:
                            invoice &&
                            invoice.order_invoiceToorder &&
                            invoice.order_invoiceToorder.status,
                          created_at:
                            invoice &&
                            invoice.order_invoiceToorder &&
                            invoice.order_invoiceToorder.created_at,
                        }
                      : undefined,
                  hasProducts:
                    invoice &&
                    invoice.invoice_item &&
                    invoice.invoice_item.length > 0,
                  products:
                    invoice && invoice.invoice_item
                      ? invoice.invoice_item.map((i: any) => {
                          return {
                            id:
                              i &&
                              i.product_invoice_itemToproduct &&
                              i.product_invoice_itemToproduct.id,
                            image:
                              i &&
                              i.product_invoice_itemToproduct &&
                              i.product_invoice_itemToproduct.image,
                            quantity: i && i.quantity && i.quantity,
                            price:
                              i &&
                              i.product_invoice_itemToproduct &&
                              i.product_invoice_itemToproduct.price,
                            name:
                              i &&
                              i.product_invoice_itemToproduct &&
                              i.product_invoice_itemToproduct.name,

                            total: i && i.total && i.total,
                          };
                        })
                      : undefined,
                  dispatch:
                    invoice && invoice.delivery_fee
                      ? invoice.delivery_fee.toString()
                      : undefined,
                  discount:
                    invoice && invoice.discount
                      ? Number(invoice.discount).toFixed(2).toString()
                      : undefined,
                  deposit:
                    invoice && invoice.deposit ? invoice.deposit : undefined,
                  tax:
                    invoice && invoice.tax
                      ? Number(invoice.tax).toString()
                      : undefined,

                  invoice: invoice && invoice,
                });
              }}
            >
              <Icon name="edit" color={theme.colors.text5} size={14} />
              <Text ml="m" variant={'DetailsM'} color="text5">
                Edit Invoice
              </Text>
            </TouchableOpacity>
          )}

          <Box
            style={{
              width: '100%',
              height: 1,
              backgroundColor: theme.colors.border4,
            }}
          />

          {userPermissions.delete_invoice && !isSending && (
            <TouchableOpacity
              style={{
                padding: 5,
                flexDirection: 'row',
                alignItems: 'center',
              }}
              onPress={() => handleDeleteInvoice()}
            >
              <Icon name="trash" color={theme.colors.error} size={14} />
              <Text ml="m" variant={'DetailsM'} color="error">
                Delete
              </Text>
            </TouchableOpacity>
          )}
        </Box>
      )}

      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: 30,
          alignItems: 'center',
        }}
        style={styles.head}
      >
        <Box
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            width: wp(90),
          }}
        >
          <Box flexDirection={'row'} style={{ width: wp(90) }}>
            <Text mr="s" variant={'SmallerTextR'}>
              Issue Date:
            </Text>

            <Text variant={'SmallerTextR'} color="text5">
              {`${dateCreated}`}
            </Text>

            <Box flex={1} />

            {/* Order Status */}
            <Box
              style={{
                width: 80,
                height: 24,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: isSending
                  ? 'rgba(243, 64, 64, 0.05)'
                  : invoice && !invoice.is_paid
                  ? 'rgba(243, 64, 64, 0.05)'
                  : theme.colors.secondary2,
              }}
            >
              <Text
                variant={'SmallerTextR'}
                color={
                  isSending
                    ? 'error'
                    : invoice && !invoice.is_paid
                    ? 'error'
                    : 'primary1'
                }
              >
                {isSending
                  ? 'UNPAID'
                  : invoice && !invoice.is_paid
                  ? 'UNPAID'
                  : 'PAID'}
              </Text>
            </Box>
          </Box>
        </Box>

        {/* Billed To */}
        <Box flexDirection={'row'} width={wp(90)} style={{ marginTop: 10 }}>
          <Text variant={'SmallerTextM'}>Billed To</Text>

          <Text
            variant={'SmallerTextR'}
            color={'text5'}
            style={{ marginLeft: 5 }}
          >
            {invoice && invoice.order_invoiceToorder
              ? invoice.order_invoiceToorder.customer_name.toUpperCase()
              : invocieFirstName + ' ' + invocieLastName}
          </Text>
        </Box>

        {/* Amount Collected */}
        {invoice && invoice.amount_collected && (
          <Box
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              width: wp(90),
              marginTop: 5,
            }}
          >
            <Text variant={'SmallerTextM'} color="text1">
              Amount collected:
            </Text>

            <Text variant={'SmallerTextR'} color="text5" ml="s">
              {returnCurrency().code +
                ' ' +
                numberWithCommas(invoice.amount_collected)}
            </Text>
          </Box>
        )}

        <Text
          variant={'SmallerTextM'}
          color="text1"
          style={{
            width: wp(90),
            marginTop: 20,
          }}
        >
          Order details
        </Text>

        {/* Products */}
        <Box style={styles.list}>
          <Box style={styles.rowItem}>
            <Text variant={'SmallerTextM'} color="text1">
              Items
            </Text>
            <Text variant={'SmallerTextM'} color="text1">
              {`Price (${returnCurrency().code})`}
            </Text>
          </Box>

          <Box
            style={{
              width: wp(90),
              height: 1,
              backgroundColor: theme.colors.border4,
              alignSelf: 'center',
              marginBottom: 5,
            }}
          />

          <FlatList
            data={
              items && items.length > 0
                ? items
                : invoice && invoice.invoice_items && invoice.invoice_items
            }
            keyExtractor={(item: any) => item.id.toString()}
            renderItem={({ item }) => (
              <Box style={styles.rowItem}>
                <Text variant={'SmallerTextR'} color="text5">
                  {`${
                    isSending
                      ? item.name
                      : item.product_invoice_itemToproduct.name
                  } x ${item.quantity}`}
                </Text>
                <Text variant={'SmallerTextR'} color="text1">
                  {numberWithCommas(item.total)}
                </Text>
              </Box>
            )}
          />

          {/* {deliveryFee && (
            <Box style={[styles.rowItem, { marginTop: 20 }]}>
              <Text variant={'DetailsM'} color="text8">
                {`Delivery fee (${
                  isSending && deliveryFee.type.length > 0
                    ? deliveryFee.type
                    : deliveryFeeType
                })`}
              </Text>
              <Text variant={'DetailsM'} color="text8">
                {`${numberWithCommas(
                  isSending ? deliveryFee.amount : deliveryFee
                )}`}
              </Text>
            </Box>
          )} */}
        </Box>

        {/* Set SMS checkbox */}
        {isSending && (
          <TouchableOpacity
            style={styles.checkbox}
            onPress={() => {
              setSendSms(!sendSms);
            }}
          >
            <Checkbox checked={sendSms} />

            <Text variant={'Body2R'} color="text5" ml="l">
              Send invoice SMS to customer?
            </Text>
          </TouchableOpacity>
        )}

        {/* Subtotal */}
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            marginTop: 10,
            width: wp(90),
          }}
        >
          <Text variant={'SmallerTextM'} color="text8">
            Subtotal:
          </Text>
          <Text variant={'SmallerTextM'} color="text8" ml="m">
            {`${returnCurrency().code} ${numberWithCommas(
              subTotal && subTotal
            )}`}
          </Text>
        </Box>

        {/* Additional Payment Details */}
        {hasAdditionalInfo && (
          <Box
            style={{
              marginTop: 10,
              borderWidth: 1,
              borderColor: theme.colors.border4,
              paddingVertical: 7,
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 20,
              borderStyle: 'dashed',
              width: wp(100),
            }}
          >
            <Box
              style={{
                width: wp(90),
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <Box flexDirection={'row'} alignItems={'center'}>
                <Text variant={'SmallerTextM'} color={'text1'}>
                  Additional Payment Information
                </Text>
              </Box>
            </Box>

            {/* Dispatch Fee  */}
            {invoice &&
              invoice.delivery_fee &&
              invoice.delivery_fee.toString().length > 0 && (
                <>
                  <Box
                    style={{
                      flexDirection: 'row',
                      width: wp(90),
                      justifyContent: 'space-between',
                      marginTop: 15,
                    }}
                  >
                    <Text variant={'SmallerTextR'} color="text1">
                      Dispatch Details
                    </Text>

                    <Text variant={'SmallerTextR'} color="text7">
                      {invoice &&
                        invoice.delivery_fee &&
                        '+' + ' ' + invoice.delivery_fee.toString()}
                    </Text>
                  </Box>

                  {invoice &&
                    invoice.order_invoiceToorder &&
                    invoice.order_invoiceToorder.delivery_address && (
                      <Text
                        style={{ width: wp(90) }}
                        variant={'SmallerTextR'}
                        color="text5"
                        numberOfLines={1}
                      >
                        {invoice.order_invoiceToorder.delivery_address}
                      </Text>
                    )}
                </>
              )}

            {/* Discount */}
            {invoice &&
              invoice.discount &&
              !isNaN(invoice.discount) &&
              invoice.discount.length > 0 && (
                <>
                  <Box
                    style={{
                      flexDirection: 'row',
                      width: wp(90),
                      justifyContent: 'space-between',
                      marginTop: 15,
                    }}
                  >
                    <Text variant={'SmallerTextR'} color="text1">
                      {`Discount (${findPercentage(
                        invoice && invoice.discount
                      )}%)`}
                    </Text>

                    <Text variant={'SmallerTextR'} color="text7">
                      {'-' +
                        ' ' +
                        numberWithCommas(
                          Number(invoice && invoice.discount)
                            .toFixed(2)
                            .toString() || '0'
                        )}
                    </Text>
                  </Box>
                </>
              )}

            {/* Deposit */}
            {invoice &&
              invoice.deposit &&
              invoice.deposit.toString().length > 0 && (
                <>
                  <Box
                    style={{
                      flexDirection: 'row',
                      width: wp(90),
                      justifyContent: 'space-between',
                      marginTop: 15,
                    }}
                  >
                    <Text variant={'SmallerTextR'} color="text1">
                      {`Deposit`}
                    </Text>

                    <Text variant={'SmallerTextR'} color="text7">
                      {'-' + ' ' + numberWithCommas(invoice && invoice.deposit)}
                    </Text>
                  </Box>
                </>
              )}

            {/* Tax */}
            {invoice &&
              invoice.tax &&
              !isNaN(invoice.tax) &&
              invoice.tax.length > 0 && (
                <>
                  <Box
                    style={{
                      flexDirection: 'row',
                      width: wp(90),
                      justifyContent: 'space-between',
                      marginTop: 15,
                    }}
                  >
                    <Text variant={'SmallerTextR'} color="text1">
                      {`Tax deducted (${findPercentage(
                        invoice && invoice.tax
                      )}%)`}
                    </Text>

                    <Text variant={'SmallerTextR'} color="text7">
                      {'+' +
                        ' ' +
                        numberWithCommas(
                          Number(invoice && invoice.tax)
                            .toFixed(2)
                            .toString() || '0'
                        )}
                    </Text>
                  </Box>
                </>
              )}
          </Box>
        )}

        {/* Total */}
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            marginTop: 10,
            width: wp(90),
          }}
        >
          <Text variant={'SmallerTextM'} color="text8">
            Total:
          </Text>
          <Text variant={'SmallerTextM'} color="text8" ml="m">
            {`${returnCurrency().code} ${numberWithCommas(
              finalTotal
                ? Number(finalTotal).toFixed(2).toString()
                : invoice && invoice.total && invoice.total
            )}`}
          </Text>
        </Box>

        <Box
          style={{
            width: wp(100),
            alignSelf: 'center',
            height: 1,
            backgroundColor: theme.colors.border4,
            marginTop: 20,
          }}
        />

        {/* Copy & Sharing */}
        {!isSending && (
          <Box
            mt="xl"
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              width: wp(90),
            }}
          >
            <TouchableOpacity
              onPress={handleCopyInvoiceLink}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <ShareLinkIcon />
              <Text variant={'DetailsR'} color="primary1" ml="m">
                Copy shareable link
              </Text>
            </TouchableOpacity>

            {shareLoading ? (
              <ActivityIndicator color={theme.colors.primary1} />
            ) : (
              <TouchableOpacity
                onPress={handleShare}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <Icon name="share" color={theme.colors.primary1} size={18} />
                <Text variant={'DetailsR'} color="primary1" ml="m">
                  Share PDF
                </Text>
              </TouchableOpacity>
            )}
          </Box>
        )}

        {/* Update payment */}
        {invoice && !invoice.is_paid && isSending === false && (
          <Box
            style={{
              marginTop: 40,
            }}
          >
            <Button
              type="transparent"
              onPress={() => setPayCashModal(true)}
              label="Update Payment"
              loading={loading}
            />
          </Box>
        )}

        {/* Send Reminder */}
        {!isSending && (
          <Box mt="l">
            <Button
              type="primary"
              onPress={handleSendReminder}
              label="Send Reminder"
              loading={reminderLoading}
            />
          </Box>
        )}

        {isSending && (
          <Box
            style={{
              marginTop: 40,
            }}
          >
            <Button
              type="primary"
              onPress={handleCreateInvoice}
              label="Create Invoice"
              loading={loading}
            />
          </Box>
        )}
      </ScrollView>

      <PayCashModal
        visible={payCashModal}
        setVisible={setPayCashModal}
        invoice={invoice && invoice}
        navigation={() => {
          if (isSending) {
            return navigation.navigate('OrderDetails', {
              id,
            });
          } else {
            return navigation.goBack();
          }
        }}
      />
    </Box>
  );
};

export default InvoiceDetail;

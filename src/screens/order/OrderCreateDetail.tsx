import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from '../../components/Themed';
import { OrderNavParaList } from '../../navigation/OrdersNav';
import StackHeader from '../../components/StackHeader';
import numberWithCommas from '../../utils/numbersWithComma';
import Button from '../../components/Button';
import { useAppSelector } from '../../redux/hooks';
import PayCashModal from '../../components/PayCashModal';
import ScreenLoading from '../../components/ScreenLoading';
import orderApi from '../../api/riciveApi/order';
import invoiceApi from '../../api/riciveApi/invoice';
import { useAnalytics } from '@segment/analytics-react-native';
import { CommonActions } from '@react-navigation/native';
import useCurrency from '../../hooks/useCurrency';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  head: {
    width: wp(90),
    marginTop: 20,
  },
  list: {
    backgroundColor: '#FEF6E9',
    width: wp(100),
    alignSelf: 'center',
    paddingHorizontal: 20,
    marginTop: 10,
    paddingTop: 10,
    maxHeight: hp(40),
  },
  rowItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
});

type Props = NativeStackScreenProps<OrderNavParaList, 'OrderCreateDetail'>;

const OrderCreateDetail = ({ navigation, route }: Props): JSX.Element => {
  const { returnCurrency } = useCurrency();
  const { track } = useAnalytics();
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [payCashModal, setPayCashModal] = useState<boolean>(false);
  const {
    invoice,
    invoiceItems,
    deliveryFee,
    isDelivery,
    isPickup,
    activeCustomer,
    selectedDate,
    selectedTime,
    finalAddress,
    sendSms,
    isHomeNav,
  } = route.params;
  const [loading, setLoading] = useState<boolean>(false);
  const [fetchLoading, setFetchLoading] = useState<boolean>(false);
  const [invoiceItemsData, setInvoiceItemsData] = useState<any>();
  const [fetchedIsPaid, setFetchedIsPaid] = useState<boolean>();

  const handleCreateOrder = async () => {
    setLoading(true);

    orderApi
      .createOrder({
        is_accepted: true,
        is_delivery: isDelivery,
        is_pickup: isPickup,
        business: business_id,
        customer: activeCustomer.id,
        status: isPickup ? 'SCHEDULED' : 'IN STORE',
        delivery_address:
          finalAddress.length > 0
            ? finalAddress
            : activeCustomer &&
              activeCustomer.address &&
              activeCustomer.address.length > 0
            ? activeCustomer.address
            : undefined,
        pickup_address:
          finalAddress.length > 0
            ? finalAddress
            : activeCustomer &&
              activeCustomer.address &&
              activeCustomer.address.length > 0
            ? activeCustomer.address
            : undefined,
        pickup_date: new Date(selectedDate).toISOString(),
        pickup_time: selectedTime,
        customer_name: `${activeCustomer.first_name} ${
          activeCustomer.last_name ? activeCustomer.last_name : ''
        }`,
        phone: activeCustomer.phone,
      })
      .then(async (order) => {
        await invoiceApi.createInvoice({
          customer: activeCustomer.id,
          is_cash: false,
          total: invoice.total,
          delivery_fee:
            deliveryFee && deliveryFee.amount ? deliveryFee.amount : undefined,
          delivery_type:
            deliveryFee && deliveryFee.type ? deliveryFee.type : undefined,
          invoice_items: invoiceItems,
          order: order.data.order.id,
          sendSMS: sendSms,
          is_paid: invoice.is_paid,
          amount_collected: invoice.total,
        });

        track('Order Created', {
          business_id,
        });

        setLoading(false);
        Toast.show({
          type: 'success',
          text1: 'Order Created',
          text2: 'Your order has been created successfully',
          visibilityTime: 3000,
        });
        if (isHomeNav) {
          navigation.dispatch(
            CommonActions.navigate({
              name: 'HomeScreen',
            })
          );
        } else {
          navigation.navigate('OrdersHome');
        }
      })
      .catch(() => {
        setLoading(false);
        // console.log('Error creating order:', JSON.stringify(error.response.data));
        return Toast.show({
          type: 'error',
          text1: 'Cannot Create Order',
          text2: 'Sorry we cannot create your order, try again later...',
          visibilityTime: 3000,
        });
      });
  };

  return (
    <Box style={styles.container}>
      <StackHeader
        title="Order Summary"
        onBackPress={() => navigation.goBack()}
      />

      <Box style={styles.head}>
        <Box flexDirection={'row'} justifyContent={'space-between'}>
          {/* <Text variant={'Body1M'}>{`Invoice #${invoice.id}`}</Text> */}

          <Box
            style={{
              width: 80,
              height: 24,
              borderRadius: 15,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor:
                invoice && !invoice.is_paid
                  ? theme.colors.errorLight
                  : theme.colors.secondary2,
            }}
          >
            <Text
              variant={'SmallerTextM'}
              color={invoice && !invoice.is_paid ? 'error' : 'primary1'}
            >
              {invoice && !invoice.is_paid ? 'UNPAID' : 'PAID'}
            </Text>
          </Box>
        </Box>

        <Text variant={'DetailsR'} color="text7" mt="m">
          {`Date created: ${new Date(invoice.created_at).toDateString()}`}
        </Text>

        {/* <Text variant={'Body2M'} color="text1" mt="m">
          Service Type
        </Text>

        <Text variant={'Body2R'} color="text7">
          {invoice && invoice.order_id && invoice.order_id.service_type.name}
        </Text> */}

        <Box style={styles.list}>
          <Box style={styles.rowItem}>
            <Text variant={'DetailsM'} color="text5">
              Items
            </Text>
            <Text variant={'DetailsM'} color="text8">
              {`Price (${returnCurrency().code})`}
            </Text>
          </Box>

          <FlatList
            data={
              invoiceItems && invoiceItems.length > 0
                ? invoiceItems
                : invoiceItemsData
            }
            keyExtractor={(item: any) => item.id.toString()}
            renderItem={({ item }) => (
              <Box style={styles.rowItem}>
                <Text variant={'DetailsM'} color="text5">
                  {`${item.name} (${item.quantity})`}
                </Text>
                <Text variant={'DetailsM'} color="text8">
                  {numberWithCommas(item.total)}
                </Text>
              </Box>
            )}
          />

          {deliveryFee && deliveryFee.amount.length > 0 && (
            <Box style={[styles.rowItem, { marginTop: 20 }]}>
              <Text variant={'DetailsM'} color="text8">
                {`Delivery fee (${deliveryFee.type})`}
              </Text>
              <Text variant={'DetailsM'} color="text8">
                {`${numberWithCommas(deliveryFee.amount)}`}
              </Text>
            </Box>
          )}

          <Box style={[styles.rowItem, { marginTop: 5 }]}>
            <Text variant={'Body2SB'} color="text8">
              Total
            </Text>
            <Text variant={'Body2SB'} color="text8">
              {`${returnCurrency().code} ${numberWithCommas(
                invoice.total ? invoice.total : ''
              )}`}
            </Text>
          </Box>
        </Box>

        {/* {isSending && (
          <Box
            style={{
              marginTop: 40,
            }}
          >
            <Button
              type="primary"
              onPress={handleCreateInvoice}
              label="Send Invoice"
              loading={loading}
            />
          </Box>
        )} */}

        <Box
          style={{
            marginTop: 40,
          }}
        >
          <Button
            type="primary"
            onPress={handleCreateOrder}
            label="Create Order"
            loading={loading}
          />
        </Box>
      </Box>

      {/* <PayCashModal
        visible={payCashModal}
        setVisible={setPayCashModal}
        invoiceId={invoice.id}
      /> */}
    </Box>
  );
};

export default OrderCreateDetail;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useEffect, useState } from 'react';
import { StyleSheet, ScrollView, TouchableOpacity } from 'react-native';
import Toast from 'react-native-toast-message';
import { useIsFocused } from '@react-navigation/native';
import { useAnalytics } from '@segment/analytics-react-native';
import { useQuery } from 'react-query';

import StackHeader from '../../components/StackHeader';
import theme, { Box, Text } from '../../components/Themed';
import UpcomingInvoiceCard from '../../components/UpcomigOrderInvoiceCard';
import { OrderNavParaList } from '../../navigation/OrdersNav';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import InvoiceAccordion from '../../components/InvoiceAccordion';
import Accordion from '../../components/Accordion';
import Button from '../../components/Button';
import orderApi, { ORDER_STATUS } from '../../api/riciveApi/order';
import ScreenError from '../../components/ScreenError';
import invoiceApi from '../../api/riciveApi/invoice';
import UpdateStatusModal from '../../components/UpdateStatusModal';
import queryKeys from '../../constants/queryKeys';
import ItemMenuIcon from '../../svg/ItemMenuIcon';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  buttons: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 30,
  },
});

type Props = NativeStackScreenProps<OrderNavParaList, 'OrderDetails'>;

const OrderDetails = ({ navigation, route }: Props): JSX.Element => {
  const { track } = useAnalytics();
  const isFocused = useIsFocused();
  const { id } = route.params;
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const role = useAppSelector((state) => state.login.user.profile.role);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [orderDetails, setOrderDetails] = useState<any>();
  const [deleteLoading, setDeleteLoading] = useState<boolean>(false);
  const [statusLoading, setStatusLoading] = useState<boolean>(false);
  const [invoice, setInvoice] = useState<any>();
  const [invoiceItems, setInvoiceItems] = useState<any[]>();
  const [showUpdateStatus, setShowUpdateStatus] = useState<boolean>(false);
  const [showMenu, setShowMenu] = useState<boolean>(false);

  const {
    isLoading: isOrdersLoading,
    isError: isOrdersError,
    refetch: refetchOrders,
  } = useQuery(
    queryKeys.ORDERS_DATA,
    async () => {
      try {
        const orderData = await orderApi.getOrder(id);
        setOrderDetails(orderData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Error getting products data, try again later..',
        });
        console.log(
          'Error getting create order data',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!id,
    }
  );

  const {
    isLoading: isInvoiceLoading,
    isError: isInvoiceError,
    refetch: refetchInvoice,
  } = useQuery(
    queryKeys.INVOICE_DATA,
    async () => {
      try {
        const invoiceData = await invoiceApi.getInvoices({
          order: id,
        });
        if (invoiceData.data) {
          setInvoice(invoiceData.data);
        }
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Error getting invoice data, try again later..',
        });
        console.log(
          'Error getting create order data',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!id,
    }
  );

  const handleDeleteOrder = async () => {
    try {
      setDeleteLoading(true);
      await orderApi.deleteOrder(id);
      navigation.goBack();

      Toast.show({
        type: 'success',
        text1: 'Order Deleted',
        text2: 'Order Delete Success!',
      });
      setDeleteLoading(false);
    } catch (error) {
      setDeleteLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Error deleting order, trye again..',
      });
    }
  };

  const onRefresh = async () => {
    await refetchInvoice();
    await refetchOrders();
  };

  const handleChangeStatus = async (status: string) => {
    try {
      setStatusLoading(true);
      await orderApi.updateOrderStatus({
        id: orderDetails.id,
        status: status as ORDER_STATUS,
        is_complete: status === 'COMPLETED' ? true : false,
      });
      track('Update Order Status', {
        business_id,
        order: orderDetails.id,
      });
      await refetchOrders();
      setStatusLoading(false);
      setShowUpdateStatus(false);
      Toast.show({
        type: 'success',
        text1: 'Status Updated',
        text2: `Your order ${
          orderDetails && orderDetails.id.substring(0, 5).toUppercase()
        } has been updated successfully`,
      });
    } catch (error: any) {
      setStatusLoading(false);
      console.log(
        'Error updating invoice',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'Error updating order status, please try again later',
      });
    }
  };

  useEffect(() => {
    isFocused && onRefresh();
  }, [isFocused]);

  if (isOrdersError || isInvoiceError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={onRefresh}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {(isInvoiceLoading || isOrdersLoading || deleteLoading) && (
        <ScreenLoading />
      )}
      <StackHeader
        title={`Order #${
          orderDetails &&
          orderDetails.id &&
          orderDetails.id.substring(0, 5).toUpperCase()
        }`}
        icon1={
          <TouchableOpacity
            style={{
              padding: 10,
            }}
            onPress={() => {
              setShowMenu(!showMenu);
            }}
          >
            <ItemMenuIcon />
          </TouchableOpacity>
        }
        onPressIcon1={() => setShowMenu(!showMenu)}
        onBackPress={() => navigation.goBack()}
      />

      {showMenu && (
        <Box
          style={{
            paddingVertical: 10,
            paddingHorizontal: 20,
            zIndex: 4,
            position: 'absolute',
            right: 20,
            top: 90,
            borderRadius: 8,
            backgroundColor: theme.colors.white,

            shadowColor: theme.colors.text3,
            shadowOffset: {
              width: 0,
              height: 0,
            },
            shadowOpacity: 0.3,
            shadowRadius: 6,
            elevation: 1,
          }}
        >
          <TouchableOpacity
            style={{
              padding: 5,
            }}
            onPress={() => handleDeleteOrder()}
          >
            <Text variant={'DetailsM'} color="error">
              Delete Order
            </Text>
          </TouchableOpacity>
        </Box>
      )}

      <Box style={{ marginTop: 15, marginBottom: 15 }}>
        <UpcomingInvoiceCard
          orderStatus={orderDetails && orderDetails.status}
          serviceType={''}
          customerName={orderDetails && orderDetails.customer_name}
          phoneNumber={orderDetails && `${orderDetails.phone}`}
        />
      </Box>
      <ScrollView>
        <Accordion
          isBooking={orderDetails && orderDetails.is_booking}
          address={orderDetails && orderDetails.delivery_address}
          timestamp={
            orderDetails && orderDetails.is_booking
              ? `${new Date(
                  orderDetails && orderDetails.booking_date
                ).toDateString()} | ${
                  orderDetails && orderDetails.booking_time
                }`
              : orderDetails && orderDetails.is_pickup
              ? `${new Date(
                  orderDetails.pickup_date
                ).toDateString()} | ${new Date(orderDetails.pickup_date)
                  .toTimeString()
                  .substring(5, -18)}`
              : '-'
          }
          pickup={orderDetails && orderDetails.is_pickup ? true : false}
        />

        {userPermissions.view_invoice && (
          <Box
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: -5,
            }}
          >
            <InvoiceAccordion
              invoices={invoice}
              navigation={navigation}
              invoiceItems={invoiceItems as any}
              handleCreateInvoice={() => {
                const customerLastName =
                  orderDetails &&
                  orderDetails.customer_name.split(' ').length > 1
                    ? orderDetails.customer_name.split(' ')[1]
                    : '';

                navigation.navigate('CreateInvoice', {
                  isHomeNav: false,
                  isInvoiceNav: false,
                  isOrderNav: true,
                  customer: {
                    id: orderDetails && orderDetails.customer,
                    first_name:
                      orderDetails && orderDetails.customer_name.split(' ')[0],
                    last_name: customerLastName || '',
                    phone: orderDetails && orderDetails.phone,
                  },
                  order: {
                    id: orderDetails && orderDetails.id,
                    status: orderDetails && orderDetails.status,
                    created_at: orderDetails && orderDetails.created_at,
                  },
                });
              }}
            />
          </Box>
        )}
      </ScrollView>

      {userPermissions.update_order_status && (
        <Box style={styles.buttons}>
          <Button
            type="transparent"
            label="Update Order Status"
            onPress={() => setShowUpdateStatus(true)}
            loading={statusLoading}
            disabled={orderDetails && orderDetails.is_accepted ? false : true}
          />
        </Box>
      )}

      <UpdateStatusModal
        status={orderDetails && orderDetails.status}
        handleChangeStatus={handleChangeStatus}
        visible={showUpdateStatus}
        setVisible={setShowUpdateStatus}
        loading={statusLoading}
      />
    </Box>
  );
};

export default OrderDetails;

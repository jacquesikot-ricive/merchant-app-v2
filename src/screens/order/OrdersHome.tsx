import React, { useState, useEffect, useCallback } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Alert,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import { useAnalytics } from '@segment/analytics-react-native';

import HeaderTextInput from '../../components/HeaderTextInput';
import NewOrderCard, { newOrderData } from '../../components/NewOrderCard';
import SwitchPill from '../../components/SwitchPill';
import theme, { Box, Text } from '../../components/Themed';
import { useAppSelector } from '../../redux/hooks';
import ListEmpty from '../../components/ListEmpty';
import UpcomingOrderCard from '../../components/UpcomingOrderCard';
import promptMessage from '../../utils/prompt';
import orderApi from '../../api/riciveApi/order';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { OrderNavParaList } from '../../navigation/OrdersNav';
import ScreenError from '../../components/ScreenError';
import DeclineOrderModal from '../../components/DeclineOrderModal';
import metricsApi from '../../api/riciveApi/metrics';
import { useAppDispatch } from '../../redux/hooks';
import { setNewOrders } from '../../redux/app';
import OrderSwitchPill from '../../components/OrderSwitchPill';
import ProductsCategoryModal from '../../components/ProductsCategoryModal';
import AcceptOrderModal from '../../components/AcceptOrderModal';
import PlusCircleIcon from '../../../svgs/PlusCircleIcon';
import { useQuery } from 'react-query';
import queryKeys from '../../constants/queryKeys';
import { useIsFocused } from '@react-navigation/native';
import { Feather } from '@expo/vector-icons';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  header: {
    backgroundColor: theme.colors.white,
    width: '100%',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingTop: 70,
    paddingBottom: 20,
  },
  list: {
    paddingTop: 10,
    height: hp(75),
    paddingBottom: 80,
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
    width: wp(100),
    paddingBottom: 10,
    alignItems: 'center',
  },
  headerFlex: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    alignItems: 'center',
  },
  headerRight: {
    flexDirection: 'row',
    marginBottom: 24,
    alignItems: 'center',
  },
  headerText: {
    fontSize: 24,
    paddingRight: 10,
  },
  bodyContent: {
    backgroundColor: theme.colors.bg13,
    flex: 1,
    alignItems: 'center',
  },
  switchPill: {
    backgroundColor: theme.colors.white,
    height: 48,
  },
});

type Props = NativeStackScreenProps<OrderNavParaList, 'OrdersHome'>;

const OrdersHome = ({ navigation }: Props) => {
  const dispatch = useAppDispatch();
  const { screen } = useAnalytics();
  const isFocused = useIsFocused();
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [activeItem, setActiveItem] = useState<any>({
    name: 'New',
    value: 'new',
  });
  const [orders, setOrders] = useState<any>();
  const [loading, setLoading] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [refreshing, setRefreshing] = useState<boolean>(false);
  const [acceptLoading, setAcceptLoading] = useState<boolean>(false);
  const [declineLoading, setDeclineLoading] = useState<boolean>(false);
  const [selectedOrder, setSelectedOrder] = useState<string>();
  const [searchResult, setSearchResult] = useState<any[]>([]);
  const [error, setError] = useState<boolean>(false);
  const [showDeclineModal, setShowDeclineModal] = useState<boolean>(false);
  const [metrics, setMetrics] = useState<any>();

  const {
    isLoading: isMetricsLoading,
    isError: isMetricsError,
    refetch: refetchMetrics,
  } = useQuery(
    queryKeys.ANALYTICS_DATA,
    async () => {
      try {
        const metricsData = await metricsApi.getData(business_id);
        setMetrics(metricsData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Sorry we cannot fetch orders now, please try again later...',
        });
        console.log(
          'Error getting merchant orders:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const {
    isLoading: isOrdersLoading,
    isError: isOrdersError,
    refetch: refetchOrders,
    isFetching: isFethchingOrders,
  } = useQuery(queryKeys.ORDERS_DATA, async () => {
    try {
      const ordersData = await orderApi.getOrders();

      setOrders(ordersData.data);
      setSearchResult(ordersData.data);

      const newOrders =
        ordersData &&
        ordersData.data
          .filter((order: any) => order.is_accepted === false)
          .sort((a: any, b: any) => {
            return (
              new Date(b.created_at).getTime() -
              new Date(a.created_at).getTime()
            );
          });

      dispatch(setNewOrders(newOrders.length.toString()));
    } catch (error: any) {
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Sorry we cannot fetch orders now, please try again later...',
      });
      console.log(
        'Error getting merchant orders:',
        JSON.stringify(error.response.data)
      );
    }
  });

  const upcomingOrders =
    searchResult &&
    searchResult
      .filter(
        (order: any) =>
          order.is_accepted === true && order.status === 'SCHEDULED' // this filter
      )
      .sort((a: any, b: any) => {
        return (
          new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );
      });

  const newOrders =
    searchResult &&
    searchResult
      .filter((order: any) => order.is_accepted === false)
      .sort((a: any, b: any) => {
        return (
          new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );
      });

  const ongoingOrders =
    searchResult &&
    searchResult
      .filter(
        (order: any) =>
          order.status === 'IN STORE' ||
          order.status === 'PROCESSING' ||
          order.status === 'DELIVERY'
      )
      .sort((a: any, b: any) => {
        return (
          new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );
      });

  const completedOrders =
    searchResult &&
    searchResult
      .filter((order: any) => order.is_complete === true)
      .sort((a: any, b: any) => {
        return (
          new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );
      });

  const returnOrders = () => {
    if (activeItem.value === 'new') return newOrders;
    if (activeItem.value === 'upcoming') return upcomingOrders;
    if (activeItem.value === 'ongoing') return ongoingOrders;
    if (activeItem.value === 'completed') return completedOrders;
    return orders;
  };

  const onRefresh = useCallback(async () => {
    await refetchMetrics();
    await refetchOrders();
  }, []);

  const handleAccept = async (
    order_id: string,
    is_pickup: boolean,
    pickup_date: string,
    pickup_time: string,
    is_booking: boolean,
    booking_date: string,
    booking_time: string
  ) => {
    if (is_pickup || is_booking) {
      navigation.navigate('AcceptOrder', {
        order_id,
        is_pickup,
        pickup_date: is_booking ? booking_date : pickup_date,
        pickup_time: is_booking ? booking_time : pickup_time,
      });
    } else {
      Alert.alert(
        'Accept Order',
        'Are you sure you want to accept this order?',
        [
          {
            text: 'No',
            style: 'cancel',
          },
          {
            onPress: async () => {
              try {
                setAcceptLoading(true);
                await orderApi.acceptOrder({
                  order_id,
                  is_pickup: false,
                });
                await refetchOrders();
                setAcceptLoading(false);
                Toast.show({
                  type: 'success',
                  text1: 'Order Accepted',
                  text2: 'Your Order has been accepted',
                });
              } catch (error: any) {
                setAcceptLoading(false);
                console.log(JSON.stringify(error.response.data));
              }
            },
            text: 'Yes',
          },
        ]
      );
    }
  };

  // const handleSearch = (e: string) => {
  //   const result = orders.filter(
  //     (c: any) =>
  //       c.customer_name.trim().toLowerCase().includes(e.trim().toLowerCase()) ||
  //       c.phone.trim().toLowerCase().includes(e.trim().toLowerCase()) ||
  // new Date(c.pickup_date)
  //   .toDateString()
  //         .trim()
  //         .toLowerCase()
  //         .includes(e.trim().toLowerCase()) ||
  //       c.id.toString().trim().toLowerCase().includes(e.trim().toLowerCase())
  //   );
  //   setSearchResult([...result]);
  // };

  const handleSearch = async (e: any) => {
    try {
      const nameSearch =
        orders &&
        orders.filter(
          (c: any) =>
            c.customer_name &&
            c.customer_name
              .trim()
              .toLowerCase()
              .includes(e.trim().toLowerCase())
        );

      const phoneSearch =
        orders &&
        orders.filter((c: any) =>
          c.phone
            ? c.phone.trim().toLowerCase().includes(e.trim().toLowerCase())
            : ''
        );

      // const dateSearch =
      //   orders &&
      //   orders.filter((c: any) =>
      //     c.pickup_date
      //       ? new Date(c.pickup_date)
      //           .toDateString()
      //           .trim()
      //           .toLowerCase()
      //           .includes(e.trim().toLowerCase())
      //       : ''
      //   );

      const idSearch =
        orders &&
        orders.filter((c: any) =>
          c.id ? c.id.trim().toLowerCase().includes(e.trim().toLowerCase()) : ''
        );

      if (e.length !== '') {
        setSearchResult([
          ...nameSearch,
          ...phoneSearch,
          // ...dateSearch,
          ...idSearch,
        ]);
      } else {
        setSearchResult(orders);
      }
    } catch (error) {
      console.log('Error in search:', error);
    }
  };

  useEffect(() => {
    isFocused && onRefresh();
  }, [isFocused]);

  if (isOrdersError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={onRefresh}
      />
    );
  }

  return (
    <Box style={styles.container}>
      <Box style={styles.header}>
        <Box style={styles.headerFlex}>
          <Text variant="Body2M" color="text1">
            Orders
          </Text>

          {userPermissions.create_order && (
            <TouchableOpacity
              onPress={() => {
                if (
                  metrics &&
                  metrics.hasCompleteBusiness &&
                  metrics.hasStorefront
                ) {
                  navigation.navigate('CreateOrder', {
                    isHomeNav: false,
                  });
                } else {
                  Alert.alert(
                    'Complete Onboarding',
                    'Please complete your business onboarding to create orders',
                    [
                      {
                        text: 'Cancel',
                        style: 'destructive',
                      },
                      {
                        text: 'Continue',
                        onPress: () =>
                          navigation.navigate('GettingStartedOrder'),
                        style: 'default',
                      },
                    ]
                  );
                }
              }}
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Feather name="plus" size={18} color={theme.colors.primary1} />
              <Text variant={'DetailsM'} color="primary1" ml="m">
                Create new order
              </Text>
            </TouchableOpacity>
          )}
        </Box>

        <Box
          style={{
            width: wp(100),
            height: 1,
            backgroundColor: theme.colors.border4,
            marginTop: 10,
          }}
        />

        <Box style={{ paddingTop: 10 }}>
          <Box
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: wp(90),
            }}
          >
            <HeaderTextInput
              type="search"
              width={wp(90)}
              height={40}
              placeholder="Search Customer Name, Order Id"
              onChangeText={handleSearch}
            />
          </Box>
        </Box>
      </Box>
      <Box style={styles.bodyContent}>
        <Box mt="m" style={styles.switchPill}>
          <OrderSwitchPill
            items={[
              {
                name: 'New',
                value: 'new',
              },
              {
                name: 'Upcoming',
                value: 'upcoming',
              },
              {
                name: 'Ongoing',
                value: 'ongoing',
              },
              {
                name: 'Completed',
                value: 'completed',
              },
            ]}
            {...{ activeItem, setActiveItem }}
          />
        </Box>
        <Box style={styles.list}>
          <FlatList
            data={returnOrders()}
            contentContainerStyle={{
              paddingBottom: 50,
            }}
            keyExtractor={(_: any, index: number) => index.toString()}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => {
              if (isOrdersLoading) {
                return <ActivityIndicator color={theme.colors.primary1} />;
              } else {
                return (
                  <ListEmpty
                    topText=""
                    bottomText={`No ${activeItem.name} Orders`}
                    invoicePage
                  />
                );
              }
            }}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            renderItem={({ item }) => {
              if (activeItem.value === 'new') {
                return (
                  <NewOrderCard
                    serviceType={''}
                    orderId={`${item.id.substring(0, 5).toUpperCase()}`}
                    timestamp={`${new Date(
                      item.created_at
                    ).toDateString()} at ${new Date(item.created_at)
                      .toTimeString()
                      .slice(0, -18)}`}
                    customer={{ name: item.customer_name, phone: item.phone }}
                    name={item.name}
                    isBooking={item.is_booking}
                    onPressOpen={() =>
                      navigation.navigate('OrderDetails', { id: item.id })
                    }
                    onAccept={async () => {
                      setSelectedOrder(item.id);
                      await handleAccept(
                        item.id,
                        item.is_pickup,
                        item.pickup_date,
                        item.pickup_time,
                        item.is_booking,
                        item.booking_date,
                        item.booking_time
                      );
                    }}
                    onDecline={() => {
                      setSelectedOrder(item.id);
                      setShowDeclineModal(true);
                    }}
                    schedule={
                      item.is_pickup
                        ? new Date(item.pickup_date).toDateString()
                        : ''
                    }
                    acceptLoading={item.id === selectedOrder && acceptLoading}
                    declineLoading={item.id === selectedOrder && declineLoading}
                  />
                );
              } else {
                return (
                  <>
                    <UpcomingOrderCard
                      createdAt={new Date(item.created_at).toDateString()}
                      serviceType=""
                      isBooking={item.is_booking}
                      orderId={`${item.id.substring(0, 5).toUpperCase()}`}
                      timestamp={`${new Date(
                        item.pickup_date
                      ).toDateString()} | ${item.pickup_time}`}
                      customer={item.customer_name}
                      onPress={() =>
                        navigation.navigate('OrderDetails', { id: item.id })
                      }
                      status={item.status}
                      isSchedule={item.is_pickup || item.is_booking}
                      scheduleTime={`${new Date(
                        item.pickup_date
                      ).toDateString()} | ${item.pickup_time}`}
                    />
                  </>
                );
              }
            }}
          />
        </Box>
        <AcceptOrderModal visible={openModal} setVisible={setOpenModal} />
        <DeclineOrderModal
          order_id={selectedOrder as string}
          refreshData={async () => {
            await refetchMetrics();
            await refetchOrders();
          }}
          setVisible={setShowDeclineModal}
          visible={showDeclineModal}
        />
      </Box>
    </Box>
  );
};

export default OrdersHome;

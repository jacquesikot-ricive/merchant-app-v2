import { useRoute } from '@react-navigation/native';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import Toast from 'react-native-toast-message';

import theme, { Box, Text } from '../../components/Themed';
import { Props } from '../../navigation';
import Button from '../../components/Button';
import DatePicker from '../../components/DatePicker';
import Picker from '../../components/Picker';
import StackHeader from '../../components/StackHeader';
import orderApi from '../../api/orders/order';
import delivery from '../../api/delivery/delivery';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
});

const times = [
  {
    value: '08:00 - 10:00 am',
    label: '08:00 - 10:00 am',
  },
  {
    value: '10:00 - 12:00 am',
    label: '10:00 - 12:00 am',
  },
  {
    value: '12:00 - 2:00 pm',
    label: '12:00 - 2:00 pm',
  },
  {
    value: '2:00 - 04:00 pm',
    label: '2:00 - 04:00 pm',
  },
  {
    value: '04:00 - 06:00 pm',
    label: '04:00 - 06:00 pm',
  },
];

const ScheduleDelivery = ({ navigation }: Props): JSX.Element => {
  const route: any = useRoute();
  const order = route.params.order;
  const user = route.params.user;
  const merchant = route.params.merchant;
  const [selectedTime, setSelectedTime] = useState<string>(times[0].value);
  const [selectedDate, setSelectedDate] = useState<Date>(new Date());
  const [loading, setLoading] = useState<boolean>(false);

  const handleScheduleDelivery = async () => {
    try {
      setLoading(true);
      await delivery.createMerchantDelivery({
        order,
        user,
        merchant,
        fulfilDate: selectedDate.toISOString(),
        fulfilTime: selectedTime,
        type: 'delivery',
      });
      await orderApi.updateStatus({
        order,
        status: 'DELIVERY',
      });
      setLoading(false);
      navigation.goBack();
    } catch (error) {
      setLoading(false);
      navigation.goBack();
      console.log('Error creating delivery order:', error);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          'Sorry, we cannot create your delivery order now, please try again later...',
      });
    }
  };

  return (
    <Box style={styles.container}>
      <StackHeader
        onBackPress={() => navigation.goBack()}
        title="Create Delivery Order"
      />

      <Box style={{ height: 20 }} />

      <Picker
        data={times}
        setValue={setSelectedTime}
        value={selectedTime}
        label="Schedule pick up time"
        placeholder=""
      />

      <Box style={{ height: 30 }} />

      <DatePicker
        date={selectedDate}
        setDate={setSelectedDate}
        label="Schedule pick up date"
      />

      <Box style={{ flex: 1 }} />

      <Box style={{ marginBottom: 100 }}>
        <Button
          label="Create Delivery"
          type="primary"
          onPress={async () => await handleScheduleDelivery()}
          loading={loading}
        />
      </Box>
    </Box>
  );
};

export default ScheduleDelivery;

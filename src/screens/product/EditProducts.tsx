import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useCallback, useEffect, useState } from 'react';
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Image,
  Alert,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  UIManager,
  LayoutAnimation,
  SwitchBase,
  Touchable,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import ImageResizer from 'react-native-image-resizer';
import * as ImagePicker from 'expo-image-picker';
import { Feather, Feather as Icon } from '@expo/vector-icons';
import { useQuery } from 'react-query';
import { useIsFocused } from '@react-navigation/native';
import { useAnalytics } from '@segment/analytics-react-native';
import { Camera } from 'expo-camera';

import theme, { Box, Text } from '../../components/Themed';
import { ProductsNavParamList } from '../../navigation/ProductsNav';
import { useAppSelector } from '../../redux/hooks';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import productApi from '../../api/riciveApi/product';
import uploadApi from '../../api/riciveApi/upload';
import Checkbox from '../../components/Checkbox';
import Picker from '../../components/Picker';
import PlusIcon from '../../../svgs/PlusIcon';
import ImagePickerModal from '../../components/ImagePickerModal';
import Button from '../../components/Button';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import categoryApi from '../../api/riciveApi/category';
import SelectCategoryModal from '../../components/SelectCategoryModal';
import ProductsPicker from '../../components/ProductsPicker';
import SwitchButton from '../../components/SwitchButton';
import queryKeys from '../../constants/queryKeys';
import useCurrency from '../../hooks/useCurrency';
import CurrencyTextInput from '../../components/CurrencyTextInput';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: theme.colors.white,
    height: 800,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    width: wp(90),
  },
  picker: {
    marginRight: 5,
  },
  button: {
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 100,
  },
  image: {
    width: wp(90),
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 20,
    borderColor: theme.colors.border1,
  },
  bottomSheet: {
    paddingHorizontal: 10,
  },
  bottomSheetCamera: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  bottomSheetBorder: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
  },
  bottomSheetGallery: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 20,
  },
  categoryPicker: {
    width: wp(90),
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: theme.colors.border1,
    marginTop: 10,
  },
  accordion: {
    paddingVertical: 20,
    paddingHorizontal: 10,
    backgroundColor: theme.colors.white,
    // borderWidth: 1.0,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
  },
  priceAccordion: {
    paddingVertical: 20,
    paddingHorizontal: 15,
    backgroundColor: theme.colors.white,
    // borderWidth: 1.0,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
  },
  heading: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  hidden: {
    height: 0,
  },
  list: {
    overflow: 'hidden',
  },
  sectionTitle: {
    fontSize: 16,
    height: 30,
    marginLeft: '5%',
  },
  border: {
    marginTop: 20,
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
  },
});

type Props = NativeStackScreenProps<ProductsNavParamList, 'EditProduct'>;

const priceData = [
  {
    label: '',
    value: '',
  },
  {
    label: 'Fixed',
    value: 'FIXED',
  },
];

const EditProduct = ({ navigation, route }: Props) => {
  const isFocused = useIsFocused();
  const { returnCurrency } = useCurrency();
  const { track } = useAnalytics();
  const { isEdit, product, isGettingStartedNav } = route.params;
  const business_id = useAppSelector((state) => state.login.user.business.id);

  const durationData = [
    {
      label: '',
      value: '',
    },
    {
      label: 'Minutes',
      value: 'minutes',
    },
    {
      label: 'Hours',
      value: 'hours',
    },
    {
      label: 'Days',
      value: 'days',
    },
    {
      label: 'Months',
      value: 'months',
    },
  ];

  //form starts here
  const [name, setName] = useState<string>(
    product && product.name && product.name.length > 0 ? product.name : ''
  );
  const [salePrice, setSalePrice] = useState<string>(
    product && product.price && product.price.length > 0 ? product.price : ''
  );
  const [costPrice, setCostPrice] = useState<string>(
    product && product.cost_price && product.cost_price.length > 0
      ? product.cost_price
      : ''
  );
  const [description, setDescription] = useState<string>(
    product && product.description && product.description.length > 0
      ? product.description
      : ''
  );
  const [stockQuantity, setStockQuantity] = useState<string>(
    product && product.stock_quantity && product.stock_quantity.length > 0
      ? product.stock_quantity
      : ''
  );

  const [lowStockLevel, setLowStockLevel] = useState<string>(
    product && product.low_stock_level && product.low_stock_level.length > 0
      ? product.low_stock_level
      : ''
  );
  const [hasCreationTime, setHasCreationTime] = useState<boolean>(
    product && product.has_creation_time ? product.has_creation_time : false
  );

  const [creationTime, setCreationTime] = useState<string>(
    product && product.creation_time && product.creation_time.length > 0
      ? product.creation_time.replace(/\D/g, '')
      : ''
  );
  const [expiryDate, setExpiryDate] = useState<string>(
    product && product.expiry_date && product.expiry_date.length > 0
      ? product.expiry_date
      : ''
  );
  const [image, setImage] = useState<string>('');
  const [serverImage, setServerImage] = useState<string>(
    product && product.image ? product.image : ''
  );

  // switch button
  const [stockNotification, setStockNotification] = useState<boolean>(
    product && product.low_stock_alert ? product.low_stock_alert : true
  );
  const [preOrderNotification, setPreOrderNotification] = useState<boolean>(
    product && product.allow_preorder ? product.allow_preorder : false
  );
  const [onlineBookings, setOnlineBookings] = useState<boolean>(
    product && product.allow_booking ? product.allow_booking : false
  );

  const [hasProductVariant, setHasProductVariant] = useState<boolean>(
    product && product.product_variant && product.product_variant.length > 0
      ? true
      : false
  );

  // Picker State
  const [durationType, setDurationType] = useState<string>(
    isEdit
      ? product &&
          product.creation_time &&
          product.creation_time.length > 0 &&
          durationData &&
          durationData.filter(
            (d: any) =>
              d.value === product.creation_time.replace(/[^a-zA-Z]/g, '')
          ).length > 0 &&
          durationData.filter(
            (d: any) =>
              d.value === product.creation_time.replace(/[^a-zA-Z]/g, '')
          )[0].label
      : ''
  );

  const [priceType, setPriceType] = useState<string>(
    product && product.price_type && product.price_type.length > 0
      ? product.price_type
      : ''
  );

  const [isPublished, setIsPublished] = useState<boolean>(
    product && product.is_published === true ? true : false
  );

  const [selectedCategories, setSelectedCategires] = useState<any[]>(
    product && product.product_category && product.product_category.length > 0
      ? product.product_category.map((c: any) => {
          return {
            name: c.category_categoryToproduct_category.name,
            id: c.category_categoryToproduct_category.id,
          };
        })
      : []
  );

  const [startCamera, setStartCamera] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [buttonDisabled, setButtonDisabled] = useState<boolean>(true);

  const [screenLoading, setScreenLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [categories, setCategories] = useState<any[]>([]);
  const [variantValues, setVariantValues] = useState<any[]>(
    isEdit
      ? product &&
          product.product_variant &&
          product.product_variant.length > 0 &&
          product.product_variant[0].product_variant_value.map((vv: any) => {
            return {
              id: vv.id,
              value: vv.value,
              price: vv.price ? vv.price : '',
              quantity: vv.quantity ? vv.quantity.toString() : 0,
            };
          })
      : [
          {
            id: 0,
            value: '',
            price: '',
            quantity: 0,
          },
        ]
  );

  const [openCategorySelect, setOpenCategorySelect] = useState<boolean>(false);
  //accordion
  const [duration, setDuration] = useState<string>('');
  const [openPricing, setOpenPricing] = useState<boolean>(false);
  const [stockQuantityAccordion, setOpenStockQuantityAccordion] =
    useState<boolean>(false);
  const [onlineBoookingAccordion, setOpenOnlineBookingAccordion] =
    useState<boolean>(false);
  const [openProductVariantAccordion, setOpenProductVariantAccordion] =
    useState<boolean>(false);
  const [variantName, setVariantName] = useState<string>(
    isEdit
      ? product &&
          product.product_variant &&
          product.product_variant.length > 0 &&
          product.product_variant[0].name
      : ''
  );

  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      quality: 0.8,
      base64: true,
    });

    if (!result.cancelled) {
      const res = await ImageResizer.createResizedImage(
        result.uri,
        800,
        500,
        'JPEG',
        100
      );
      setImage(res.uri);
      setOpenModal(false);
    }
  };

  const handleAddProduct = async () => {
    if (name.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Product Name',
        text2: 'Product name is required!',
      });
    }

    if (salePrice.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Product Price',
        text2: 'Product  sale price is required!',
      });
    }

    try {
      setLoading(true);
      const imageUrl =
        image.length > 0
          ? await uploadApi.upload({
              file: image,
            })
          : undefined;

      await productApi.addProduct({
        business: business_id,
        name,
        price: salePrice.length > 0 ? salePrice : '',
        price_type: priceType.length > 0 ? priceType : 'FIXED',
        low_stock_level: lowStockLevel.length < 1 ? '2' : lowStockLevel,
        low_stock_alert: stockNotification,
        description: description.length > 0 ? description : undefined,
        allow_preorder: preOrderNotification,
        allow_booking: onlineBookings,
        image: image.length > 0 ? imageUrl.data.url : undefined,
        cost_price: costPrice.length > 0 ? costPrice : undefined,
        stock_quantity: hasProductVariant
          ? variantValues
              .map((vv) => vv.quantity)
              .reduce((a, b: any) => Number(a) + Number(b), 0)
              .toString()
          : stockQuantity && stockQuantity.toString().length > 0
          ? stockQuantity.toString()
          : '1',
        creation_time:
          creationTime.length > 0
            ? `${creationTime} ${durationType}`
            : undefined,
        has_creation_time: hasCreationTime,
        is_published: isPublished,
        expiry_date: expiryDate.length > 0 ? `${expiryDate}` : undefined,
        category:
          selectedCategories && selectedCategories.length > 0
            ? selectedCategories.map((p: any) => p.id)
            : undefined,
        variants: [
          {
            name: variantName,
            values: variantValues.map((vv) => {
              return {
                value: vv.value,
                price: vv.price.length > 0 ? vv.price : undefined,
                quantity: vv.quantity.length > 0 ? Number(vv.quantity) : 0,
              };
            }),
          },
        ],
      });

      track('Add Product', {
        business_id,
      });
      setLoading(false);
      if (isGettingStartedNav) {
        navigation.navigate('GettingStarted');
        Toast.show({
          type: 'success',
          text1: 'Product Added',
          text2: `${name} has been added successfully`,
        });
      } else {
        navigation.navigate('Products');
        Toast.show({
          type: 'success',
          text1: 'Product Added',
          text2: `${name} has been added successfully`,
        });
      }
    } catch (error: any) {
      setLoading(false);
      console.log(JSON.stringify(error.response.data));
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'Cannot add new product now, please try again later...',
      });
    }
  };

  const handleUpdateProduct = async () => {
    if (name.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Product Name',
        text2: 'Product name is required!',
      });
    }
    if (salePrice.length < 1) {
      return Toast.show({
        type: 'error',
        text1: 'Product Price',
        text2: 'Product  sale price is required!',
      });
    }
    try {
      setLoading(true);
      const imageUrl =
        image.length > 0 && serverImage.length < 1
          ? await uploadApi.upload({
              file: image,
            })
          : null;

      await productApi.updateProduct({
        id: product.id,
        business: business_id,
        name,
        price: salePrice,
        price_type: priceType,
        low_stock_level: lowStockLevel,
        allow_booking: onlineBookings,
        description: description.length > 0 ? description : undefined,
        low_stock_alert: stockNotification,
        image: imageUrl ? imageUrl.data.url : null,
        cost_price: costPrice.length > 0 ? costPrice : undefined,
        stock_quantity: hasProductVariant
          ? variantValues
              .map((vv) => vv.quantity)
              .reduce((a, b: any) => Number(a) + Number(b), 0)
              .toString()
          : stockQuantity && stockQuantity.toString().length > 0
          ? stockQuantity.toString()
          : '1',
        creation_time:
          creationTime.length > 0
            ? `${creationTime} ${durationType}`
            : undefined,
        has_creation_time: hasCreationTime,
        is_published: isPublished,
        expiry_date: expiryDate.length > 0 ? `${expiryDate}` : undefined,
        category:
          selectedCategories && selectedCategories.length > 0
            ? selectedCategories.map((p: any) => p.id)
            : undefined,
        variants: hasProductVariant
          ? [
              {
                name: variantName,
                values: variantValues.map((vv) => {
                  return {
                    value: vv.value,
                    price: vv.price.length > 0 ? vv.price : undefined,
                    quantity: vv.quantity.length > 0 ? Number(vv.quantity) : 0,
                  };
                }),
              },
            ]
          : [],
      });
      track('Update Product', {
        business_id,
      });
      setLoading(false);
      navigation.navigate('Products');
      Toast.show({
        type: 'success',
        text1: 'Product Updated',
        text2: `${name} has been updated successfully`,
      });
    } catch (error: any) {
      setLoading(false);
      console.log(JSON.stringify(error.response.data));
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'Cannot update product now, please try again later..',
      });
    }
  };

  const {
    isLoading: isCategoryLoading,
    isError: isCategoryError,
    refetch: refetchCategory,
    isFetching: isFetchingCategory,
  } = useQuery(
    queryKeys.CATEGORY_DATA,
    async () => {
      try {
        const categoryData = await categoryApi.getCategories(business_id);
        setCategories(categoryData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2:
            'Sorry we cannot fetch categories now, please try again later...',
        });
        console.log(
          'Error getting categories:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  // Camera
  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      if (status === 'granted') {
        setStartCamera(true);
      } else {
        Alert.alert('Access denied');
      }
    })();
  }, []);

  useEffect(() => {
    isFocused && refetchCategory();
  }, [isFocused]);

  let camera: Camera;
  const takePicture = async () => {
    const photo: any = await camera.takePictureAsync();
    setImage(photo);
  };

  if (isCategoryError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => await refetchCategory()}
      />
    );
  }

  //Accordion for Product titles

  if (
    Platform.OS === 'android' &&
    UIManager.setLayoutAnimationEnabledExperimental
  ) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

  const toggleDetails = () => {
    setIsOpen((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const togglePricing = () => {
    setOpenPricing((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const toggleStockQuantity = () => {
    setOpenStockQuantityAccordion((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const toggleOnlineBooking = () => {
    setOpenOnlineBookingAccordion((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const toggleProductVariant = () => {
    setOpenProductVariantAccordion((value) => !value);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  return (
    <ScrollView bounces={false} style={styles.content}>
      {isCategoryLoading && <ScreenLoading />}
      <StackHeader
        title={isEdit ? 'Update Product' : 'Add New Product'}
        onBackPress={() => navigation.goBack()}
        icon1={
          isCategoryLoading || loading ? (
            <ActivityIndicator color={theme.colors.primary1} />
          ) : (
            <Text color="primary1" variant={'Body2M'}>
              {isEdit ? 'Update' : 'Save'}
            </Text>
          )
        }
        onPressIcon1={isEdit ? handleUpdateProduct : handleAddProduct}
      />
      <Box style={styles.border} />

      {/* Product Details */}
      <Box style={styles.accordion}>
        <TouchableOpacity
          onPress={toggleDetails}
          style={styles.heading}
          activeOpacity={0.6}
        >
          <Text>
            <Text variant="Body2M" color="text6">
              Product Details
            </Text>
            <Text variant="DetailsR" color="error">
              *
            </Text>
          </Text>
          <Feather
            name={isOpen ? 'chevron-up' : 'chevron-down'}
            size={18}
            color={theme.colors.text7}
          />
        </TouchableOpacity>
        <Box style={[styles.list, !isOpen ? styles.hidden : undefined]}>
          <Box
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              width: wp(95),
            }}
          >
            <Box style={{ marginBottom: 10 }}>
              <Text
                mt="xl"
                mb="s"
                variant={'DetailsR'}
                color="text6"
                style={{ width: wp(90) }}
              >
                Add product image (optional)
              </Text>

              <Box style={styles.image}>
                {image.length > 0 || serverImage.length > 0 ? (
                  <>
                    <TouchableOpacity
                      style={{
                        padding: 5,
                      }}
                      onPress={() => {
                        setImage('');
                        setServerImage('');
                      }}
                    >
                      <Icon
                        name="trash"
                        color={theme.colors.border}
                        size={24}
                      />
                    </TouchableOpacity>

                    <Box
                      style={{
                        position: 'absolute',
                        left: 20,
                        top: 15,
                      }}
                    >
                      <Image
                        source={{ uri: image.length > 0 ? image : serverImage }}
                        style={{
                          width: 90,
                          height: 60,
                          borderRadius: 6,
                        }}
                      />
                    </Box>

                    <Text variant="DetailsM" color="text2">
                      Remove image
                    </Text>
                  </>
                ) : (
                  <TouchableOpacity
                    onPress={() => setOpenModal(true)}
                    style={{ justifyContent: 'center', alignItems: 'center' }}
                  >
                    <Box>
                      <PlusIcon />
                    </Box>
                  </TouchableOpacity>
                )}
              </Box>
            </Box>

            <Box>
              <Box flexDirection={'row'} mt="m">
                <Text variant={'DetailsR'} color="text6">
                  Product name
                </Text>
                <Text color="error">*</Text>
              </Box>
              <Box mt="l">
                <TextInput
                  placeholder="Enter name of product"
                  type="input"
                  autoCompleteType="name"
                  autoCorrect={false}
                  autoCapitalize="words"
                  keyboardType="default"
                  returnKeyType="next"
                  onChangeText={(e) => setName(e)}
                  value={name}
                />
              </Box>
            </Box>

            <Box>
              <Text
                mt="l"
                variant={'DetailsR'}
                color="text6"
                style={{ width: wp(90) }}
              >
                Description
              </Text>
              <Box mt="m">
                <TextInput
                  placeholder="Enter Description"
                  type="input"
                  autoCompleteType="off"
                  autoCorrect={false}
                  keyboardType="default"
                  returnKeyType="next"
                  multiline
                  height={80}
                  onChangeText={(e) => setDescription(e)}
                  value={description}
                />
              </Box>
            </Box>

            <Text
              mt="s"
              variant={'DetailsR'}
              color="text6"
              style={{ width: wp(90), textAlign: 'left' }}
            >
              Product categories
            </Text>

            <TouchableOpacity
              style={styles.categoryPicker}
              onPress={() => setOpenCategorySelect(true)}
              activeOpacity={0.6}
            >
              {selectedCategories.length > 0 ? (
                <Box
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}
                >
                  <Box>
                    {selectedCategories.map((s: any, i: number) => (
                      <Text numberOfLines={1}>
                        {`${s.name}${
                          i === selectedCategories.length - 1 ? '' : ', '
                        }`}
                      </Text>
                    ))}
                  </Box>

                  <Box
                    style={{
                      position: 'absolute',
                      right: 0,
                    }}
                  >
                    <Icon
                      name="chevron-right"
                      size={24}
                      color={theme.colors.border}
                    />
                  </Box>
                </Box>
              ) : (
                <Box
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <Text variant={'Body2R'} color="text7">
                    Select Product Categories
                  </Text>
                  <Icon
                    name="chevron-right"
                    size={24}
                    color={theme.colors.border}
                  />
                </Box>
              )}
            </TouchableOpacity>
            {/* <Box>
              <Text
                mt="l"
                variant={'DetailsR'}
                color="text6"
                style={{ width: wp(50) }}
              >
                Expiry date (Optional)
              </Text>
              <Box mt="l">
                <TextInput
                  placeholder="mm/yy"
                  type="input"
                  height={58}
                  autoCorrect={false}
                  keyboardType="default"
                  returnKeyType="next"
                  onChangeText={(e) => setExpiryDate(e)}
                  value={expiryDate}
                />
              </Box>
            </Box> */}
          </Box>
          <Box style={{ width: wp(100), paddingHorizontal: 20 }}>
            {/* {hasCreationTime ? (
              <Box mb="xl">
                <TextInput
                  placeholder="Duration"
                  type="input"
                  autoCorrect={false}
                  keyboardType="default"
                  returnKeyType="done"
                  onChangeText={(e) => setCreationTime(e)}
                  value={creationTime}
                />
              </Box>
            ) : (
              <Box />
            )} */}
            <Box
              style={{
                marginTop: 20,
                marginBottom: 50,
                flexDirection: 'row',
                alignItems: 'center',
                marginLeft: -10,
              }}
            >
              <TouchableOpacity
                onPress={() => setIsPublished(!isPublished)}
                style={{ flexDirection: 'row' }}
              >
                <Checkbox checked={isPublished} />

                <Text variant="DetailsR" ml="l">
                  Publish this product on website?
                </Text>
              </TouchableOpacity>
            </Box>

            {/* <Box marginBottom={'xxxl'} style={{ marginBottom: 170, alignItems: 'center', width: wp(85) }}>
              <Button
                label={isEdit ? 'Update Product' : 'Add Product'}
                onPress={isEdit ? handleUpdateProduct : handleAddProduct}
                type="primary"
                loading={loading}
              />
            </Box> */}
          </Box>
        </Box>
      </Box>

      {/* Pricing */}
      <Box style={styles.priceAccordion}>
        <TouchableOpacity
          onPress={togglePricing}
          style={styles.heading}
          activeOpacity={0.6}
        >
          <Text>
            <Text variant="Body2M" color="text6">
              Pricing
            </Text>
            <Text variant="DetailsR" color="error">
              *
            </Text>
          </Text>
          <Feather
            name={openPricing ? 'chevron-up' : 'chevron-down'}
            size={18}
            color={theme.colors.text7}
          />
        </TouchableOpacity>
        <Box style={[styles.list, !openPricing ? styles.hidden : undefined]}>
          <Box style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Box>
              <Text
                variant={'DetailsR'}
                color="text6"
                style={{ width: wp(50) }}
              >
                Cost price
              </Text>
              <Box
                mt="m"
                style={{
                  alignSelf: 'flex-start',
                }}
              >
                {/* <TextInput
                  placeholder="Enter amount"
                  type="input"
                  width={wp(40)}
                  height={40}
                  autoCorrect={false}
                  keyboardType="number-pad"
                  returnKeyType="next"
                  onChangeText={(e) => setCostPrice(e)}
                  value={costPrice}
                /> */}

                <CurrencyTextInput
                  onChangeValue={(e: any) => setCostPrice(e && e.toString())}
                  value={costPrice}
                  width={wp(40)}
                  height={40}
                  placeholder="Enter amount"
                />
              </Box>
            </Box>
            <Box>
              <Box flexDirection={'row'}>
                <Text variant={'DetailsR'} color="text6">
                  Sale price
                </Text>
                <Text color="error">*</Text>
              </Box>
              <Box mt="m">
                <CurrencyTextInput
                  onChangeValue={(e: any) => setSalePrice(e && e.toString())}
                  value={salePrice}
                  width={wp(40)}
                  height={40}
                  placeholder="Enter amount"
                />
              </Box>
            </Box>
          </Box>
          <Box
            style={{
              marginTop: 20,
              marginBottom: 20,
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: 'row',
              }}
              onPress={() => setHasCreationTime(!hasCreationTime)}
            >
              <Checkbox checked={hasCreationTime} />

              <Text variant="DetailsR" ml="l">
                Does this product require a processing time.
              </Text>
            </TouchableOpacity>
          </Box>
          {hasCreationTime && (
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                width: wp(100),
              }}
            >
              <Box style={{ width: wp(50) }}>
                <ProductsPicker
                  label="Duration type"
                  placeholder={''}
                  value={durationType}
                  setValue={setDurationType}
                  data={durationData}
                />
              </Box>
              <Box>
                <Text
                  style={{
                    marginTop: 5,
                  }}
                  variant={'DetailsR'}
                  color="text6"
                >
                  Duration
                </Text>
                <Box
                  style={{
                    marginTop: 15,
                  }}
                >
                  <TextInput
                    placeholder="e.g 45"
                    type="number"
                    width={wp(40)}
                    height={54}
                    autoCorrect={false}
                    keyboardType="number-pad"
                    returnKeyType="next"
                    onChangeText={(e) => setCreationTime(e)}
                    value={creationTime}
                    editable={
                      durationType && durationType.length > 0 ? true : false
                    }
                    selectTextOnFocus={
                      durationType && durationType.length > 0 ? true : false
                    }
                    onTouchStart={() => {
                      if (durationType && durationType.length > 0) {
                        return;
                      } else {
                        Toast.show({
                          type: 'info',
                          text1: 'Duration Type',
                          text2: 'Select duration time first',
                        });
                      }
                    }}
                  />
                </Box>
              </Box>
            </Box>
          )}
          {/* <Box
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              width: wp(100),
            }}
          >
            <Box style={{}}>
              <Picker
                label="Price type"
                placeholder={''}
                value={priceType}
                setValue={setPriceType}
                data={priceData}
              />
            </Box>
          </Box> */}
        </Box>
      </Box>

      {/* Stock Quantity */}
      <Box style={styles.priceAccordion}>
        <TouchableOpacity
          onPress={toggleStockQuantity}
          style={styles.heading}
          activeOpacity={0.6}
        >
          <Text variant="Body2M" color="text6">
            Stock quantity
          </Text>
          <Feather
            name={stockQuantityAccordion ? 'chevron-up' : 'chevron-down'}
            size={18}
            color={theme.colors.text7}
          />
        </TouchableOpacity>
        <Box
          style={[
            styles.list,
            !stockQuantityAccordion ? styles.hidden : undefined,
          ]}
        >
          <Text variant="SmallerTextR" color="text7" mt="s" mb="l">
            Manage stock levels of this product
          </Text>

          <Box>
            <Box>
              <Box flexDirection={'row'}>
                <Text variant={'DetailsR'} color="text6">
                  Current stock quantity
                </Text>
              </Box>
              <Box mt="l">
                <TextInput
                  placeholder="Qty in stock, defaults to 1"
                  type="number"
                  height={58}
                  editable={!hasProductVariant}
                  autoCorrect={false}
                  keyboardType="number-pad"
                  returnKeyType="next"
                  onPressIn={() => {
                    if (hasProductVariant) {
                      Toast.show({
                        text1: 'Product Variant',
                        text2: 'Update Quantity in Product Variants below',
                        type: 'info',
                      });
                    }
                  }}
                  onChangeText={(e) => setStockQuantity(e)}
                  value={stockQuantity}
                />
              </Box>
            </Box>
            <Box>
              <Text mt="l" variant={'DetailsR'} color="text6">
                Low stock level
              </Text>
              <Box mt="l">
                <TextInput
                  placeholder="Enter quantity"
                  type="number"
                  height={58}
                  autoCorrect={false}
                  keyboardType="number-pad"
                  returnKeyType="next"
                  onChangeText={(e) => setLowStockLevel(e)}
                  value={lowStockLevel}
                />
              </Box>
            </Box>
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 16,
              }}
            >
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => setStockNotification(!stockNotification)}
              >
                <SwitchButton checked={stockNotification} />
              </TouchableOpacity>
              <Text variant="SmallerTextR" color="text1" ml="xl">
                Receive low and out of stock notifications
              </Text>
            </Box>
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 16,
              }}
            >
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => setPreOrderNotification(!preOrderNotification)}
              >
                <SwitchButton checked={preOrderNotification} />
              </TouchableOpacity>
              <Text variant="SmallerTextR" color="text1" ml="xl">
                Allow customers pre-order on website when out of {'\n'} stock?
              </Text>
            </Box>
          </Box>
        </Box>
      </Box>

      {/* Booking */}
      <Box style={styles.priceAccordion}>
        <TouchableOpacity
          onPress={toggleOnlineBooking}
          style={styles.heading}
          activeOpacity={0.6}
        >
          <Text variant="Body2M" color="text6">
            Booking
          </Text>
          <Feather
            name={onlineBoookingAccordion ? 'chevron-up' : 'chevron-down'}
            size={18}
            color={theme.colors.text7}
          />
        </TouchableOpacity>

        <Box
          style={[
            styles.list,
            !onlineBoookingAccordion ? styles.hidden : undefined,
          ]}
        >
          <Box>
            <Box
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 16,
              }}
            >
              <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => setOnlineBookings(!onlineBookings)}
              >
                <SwitchButton checked={onlineBookings} />
              </TouchableOpacity>
              <Text
                variant="SmallerTextR"
                color="text1"
                ml="xl"
                style={{
                  width: wp(70),
                }}
              >
                Enable online bookings and scheduling for this product
              </Text>
            </Box>
          </Box>
        </Box>
      </Box>

      {/* Product Variant */}
      <Box style={styles.priceAccordion}>
        <TouchableOpacity
          onPress={toggleProductVariant}
          style={styles.heading}
          activeOpacity={0.6}
        >
          <Text variant="Body2M" color="text6">
            Product Variant
          </Text>
          <Feather
            name={openProductVariantAccordion ? 'chevron-up' : 'chevron-down'}
            size={18}
            color={theme.colors.text7}
          />
        </TouchableOpacity>

        {hasProductVariant ? (
          <Box
            style={[
              styles.list,
              !openProductVariantAccordion ? styles.hidden : undefined,
            ]}
          >
            <Box mt="l">
              <Box
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  width: wp(90),
                }}
              >
                <Text variant={'SmallerTextR'} mb="s">
                  Variant Name
                </Text>

                <TouchableOpacity
                  onPress={() => {
                    Alert.alert(
                      'Delete Variant',
                      'Are you sure you want to delete this variant?',
                      [
                        {
                          text: 'No',
                          style: 'cancel',
                        },
                        {
                          text: 'Yes',
                          style: 'destructive',
                          onPress: () => {
                            setVariantName('');
                            setVariantValues([
                              {
                                id: 0,
                                value: '',
                                price: '',
                              },
                            ]);
                            setHasProductVariant(false);
                            setOpenProductVariantAccordion(true);
                          },
                        },
                      ]
                    );
                  }}
                >
                  <Text variant={'SmallerTextR'} mb="s" color="error">
                    Delete Variant
                  </Text>
                </TouchableOpacity>
              </Box>
              <TextInput
                placeholder="Variant Name eg Size, Weight, etc"
                type="input"
                height={40}
                autoCapitalize="words"
                value={variantName}
                keyboardType="default"
                onChangeText={(e) => setVariantName(e)}
              />

              <Box style={{ height: 10 }} />

              <Text variant={'SmallerTextR'}>Value</Text>

              {variantValues &&
                variantValues.map((v, index) => {
                  return (
                    <Box
                      key={index.toString()}
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        width: wp(90),
                        marginTop: 10,
                      }}
                    >
                      {index !== 0 && (
                        <TouchableOpacity
                          onPress={() => {
                            const newValues = variantValues.filter(
                              (val) => val.id !== v.id
                            );
                            setVariantValues(newValues);
                          }}
                          style={{
                            position: 'absolute',
                            right: -10,
                            top: -10,
                            zIndex: 1,
                          }}
                        >
                          <Icon
                            name="x-circle"
                            size={20}
                            color={theme.colors.error}
                          />
                        </TouchableOpacity>
                      )}

                      <TextInput
                        placeholder="Value eg L, XL, 1Kg, etc"
                        type="input"
                        height={40}
                        width={wp(43)}
                        keyboardType="default"
                        autoCapitalize="words"
                        value={variantValues[index].value}
                        onChangeText={(e) => {
                          const newValues = variantValues.filter(
                            (val) => val.id !== v.id
                          );

                          const oldValue = variantValues.filter(
                            (val) => val.id === v.id
                          );

                          newValues.splice(index, 0, {
                            id: Math.random(),
                            value: e,
                            price: oldValue[0].price,
                          });

                          setVariantValues(newValues);
                        }}
                      />

                      <Box flex={1} />

                      <Box
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          width: wp(40),
                        }}
                      >
                        <TextInput
                          placeholder="Qty"
                          type="input"
                          height={40}
                          width={wp(15)}
                          value={variantValues[index].quantity}
                          keyboardType="number-pad"
                          onChangeText={(e) => {
                            const newValues = variantValues.filter(
                              (val) => val.id !== v.id
                            );

                            const oldValue = variantValues.filter(
                              (val) => val.id === v.id
                            );

                            newValues.splice(index, 0, {
                              id: Math.random(),
                              value: oldValue[0].value,
                              price: oldValue[0].price,
                              quantity: e,
                            });

                            setVariantValues(newValues);
                          }}
                        />

                        <TextInput
                          placeholder="Price"
                          type="input"
                          height={40}
                          width={wp(22)}
                          value={variantValues[index].price}
                          keyboardType="number-pad"
                          onChangeText={(e) => {
                            const newValues = variantValues.filter(
                              (val) => val.id !== v.id
                            );

                            const oldValue = variantValues.filter(
                              (val) => val.id === v.id
                            );

                            newValues.splice(index, 0, {
                              id: Math.random(),
                              value: oldValue[0].value,
                              quantity: oldValue[0].quantity,
                              price: e,
                            });

                            setVariantValues(newValues);
                          }}
                        />
                      </Box>
                    </Box>
                  );
                })}

              <TouchableOpacity
                onPress={() =>
                  setVariantValues([
                    ...variantValues,
                    {
                      id: Math.random(),
                      value: '',
                      price: '',
                    },
                  ])
                }
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginVertical: 10,
                  marginBottom: 50,
                }}
              >
                <Icon
                  name="plus-circle"
                  size={18}
                  color={theme.colors.primary1}
                />
                <Text variant={'SmallerTextR'} ml="s">
                  Add New Value
                </Text>
              </TouchableOpacity>
            </Box>
          </Box>
        ) : (
          <Box
            mt={!openProductVariantAccordion ? undefined : 'xl'}
            style={[
              styles.list,
              !openProductVariantAccordion ? styles.hidden : undefined,
              {
                alignSelf: 'center',
              },
            ]}
          >
            <Button
              label="Add Variant"
              type="primary"
              onPress={() => {
                setHasProductVariant(true);
                setOpenProductVariantAccordion(true);
                setVariantValues([
                  {
                    id: 0,
                    value: '',
                    price: '',
                    quantity: 0,
                  },
                ]);
              }}
              height={40}
              width={wp(70)}
            />
          </Box>
        )}
      </Box>

      <Box
        style={{
          width: wp(90),
          alignItems: 'center',
          marginTop: 30,
          marginBottom: 100,
          alignSelf: 'center',
        }}
      >
        <Button
          type="primary"
          label={isEdit ? 'Update Product' : 'Add Product'}
          onPress={isEdit ? handleUpdateProduct : handleAddProduct}
          loading={loading}
          disabled={priceData.length < 1 || name.length < 1 ? true : false}
        />
      </Box>

      <ImagePickerModal
        visible={openModal}
        setVisible={setOpenModal}
        onPressImage={pickImage}
        onPressCamera={takePicture}
      />

      <SelectCategoryModal
        categories={categories}
        selectedCategories={selectedCategories}
        setSelectedCategories={setSelectedCategires}
        setVisible={setOpenCategorySelect}
        visible={openCategorySelect}
        getCategories={refetchCategory}
      />
    </ScrollView>
  );
};

export default EditProduct;

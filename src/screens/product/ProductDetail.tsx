import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { Alert, StyleSheet, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import Clipboard from '@react-native-clipboard/clipboard';

import theme, { Box, Text } from '../../components/Themed';
import { ProductsNavParamList } from '../../navigation/ProductsNav';
import StackHeader from '../../components/StackHeader';
import useCurrency from '../../hooks/useCurrency';
import numberWithCommas from '../../utils/numbersWithComma';
import ShareLinkIcon from '../../svg/ShareLinkIcon';
import ItemMenuIcon from '../../svg/ItemMenuIcon';
import BurgerOptions from '../../components/BurgerOptions';
import productApi from '../../api/riciveApi/product';
import ScreenLoading from '../../components/ScreenLoading';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
});

type Props = NativeStackScreenProps<ProductsNavParamList, 'ProductDetail'>;

const ProductDetail = ({ navigation, route }: Props) => {
  const { id, copyLink, name, price, quantity, status, product } = route.params;
  const { returnCurrency } = useCurrency();

  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const handleCopyInvoiceLink = () => {
    Clipboard.setString(copyLink);
    Toast.show({
      type: 'success',
      text1: 'Copied!',
      text2: 'URL copied successfully!',
    });
  };

  const onEdit = () => {
    navigation.navigate('EditProduct', {
      isEdit: true,
      product,
    });
  };

  const onDelete = async () => {
    Alert.alert(
      'Delete Product',
      'Are you sure you want to delete this product?',
      [
        {
          style: 'destructive',
          text: 'Yes',
          onPress: async () => {
            try {
              setLoading(true);
              await productApi.deleteProduct(id);
              navigation.goBack();
              setLoading(false);
              Toast.show({
                type: 'success',
                text1: 'Product Delete',
                text2: 'Product deleted successfully',
              });
            } catch (error: any) {
              setLoading(false);
              //   console.log(
              //     'Error fetching products or service:',
              //     JSON.stringify(error.response.data)
              //   );
              Toast.show({
                type: 'error',
                text1: 'Server Error',
                text2: 'Cannot delete product, please try again later...',
              });
            }
          },
        },
        {
          style: 'default',
          text: 'No',
        },
      ]
    );
  };

  const ProductDetailRow = ({ title, data }: any) => (
    <Box flexDirection={'row'} width={wp(90)} justifyContent={'space-between'}>
      <Text variant={'SmallerTextR'} color="text7" marginVertical={'s'}>
        {title}
      </Text>
      <Text variant={'SmallerTextR'} color="text1">
        {data}
      </Text>
    </Box>
  );

  return (
    <Box style={styles.container}>
      {loading && <ScreenLoading />}
      <StackHeader
        onBackPress={() => navigation.goBack()}
        title="Product Details"
        hasBorder
        icon1={
          <TouchableOpacity
            style={{
              padding: 10,
            }}
            onPress={() => {
              setShowMenu(!showMenu);
            }}
          >
            <ItemMenuIcon />
          </TouchableOpacity>
        }
        onPressIcon1={() => setShowMenu(!showMenu)}
      />

      {showMenu && (
        <BurgerOptions
          onEdit={onEdit}
          onDelete={onDelete}
          deleteLabel="Delete"
          editLabel="Edit"
        />
      )}

      <Box
        style={{
          backgroundColor: 'rgba(243, 250, 242, 0.5)',
          padding: 16,
          marginTop: 10,
        }}
      >
        <ProductDetailRow title={'Product Name'} data={name} />
        <ProductDetailRow
          title={'Price'}
          data={returnCurrency().code + ' ' + numberWithCommas(price)}
        />
        <ProductDetailRow title={'Stock Quantity'} data={quantity} />
        <ProductDetailRow
          title={'Status'}
          data={status ? 'Published' : 'Unpublished'}
        />
      </Box>

      <Box width={wp(90)} alignSelf={'center'} mt="l">
        <TouchableOpacity
          onPress={handleCopyInvoiceLink}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          <ShareLinkIcon />
          <Text variant={'DetailsM'} color="primary1" ml="m">
            Copy link
          </Text>
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default ProductDetail;

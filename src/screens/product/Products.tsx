import React, { useEffect, useState, useRef } from 'react';
import {
  Alert,
  Dimensions,
  FlatList,
  Modal,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import theme, { Box } from '../../components/Themed';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import Clipboard from '@react-native-clipboard/clipboard';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useIsFocused } from '@react-navigation/native';
import { Ionicons as Icon } from '@expo/vector-icons';
import { useQuery } from 'react-query';

import ProductsList from '../../components/ProductList';
import { useAppSelector } from '../../redux/hooks';
import ListEmpty from '../../components/ListEmpty';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import productApi from '../../api/riciveApi/product';
import { ProductsNavParamList } from '../../navigation/ProductsNav';
import HeaderTextInput from '../../components/HeaderTextInput';
import ProductsSwitchPill from '../../components/ProductsSwitchPill';
import PlusCircleIcon from '../../../svgs/PlusCircleIcon';
import ProductsCategoryModal from '../../components/ProductsCategoryModal';
import CategoryCard from '../../components/CategoryCard';
import categoryApi from '../../api/riciveApi/category';
import storefrontApi from '../../api/riciveApi/storefront';
import queryKeys from '../../constants/queryKeys';
import ProductCard from '../../components/ProductCard';
import AppModal from '../../components/AppModal';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
    paddingTop: 30,
  },
  layout: {
    marginTop: 8,
    marginBottom: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  rightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    paddingRight: 5,
  },
  productsBorder: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.border1,
    paddingVertical: 13,
    width: wp(100),
    alignItems: 'center',
  },
});

type Props = NativeStackScreenProps<ProductsNavParamList, 'Products'>;

const { width, height } = Dimensions.get('window');
const Products = ({ navigation }: Props) => {
  const [addProductModalVisibility, setAddProductModalVisibility] =
    useState<boolean>(false);
  const [card1Active, setCard1Active] = useState<boolean>(false);
  const [card2Active, setCard2Active] = useState<boolean>(false);
  const isFocused = useIsFocused();
  const [products, setProducts] = useState<any[]>([]);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [error, setError] = useState<boolean>(false);
  const [searchResult, setSearchResult] = useState<any[]>();
  const [activeItem, setActiveItem] = useState<'product' | 'category'>(
    'product'
  );
  const [categories, setCategories] = useState<any[]>([]);
  const [categoriesSearch, setCategoriesSearch] = useState<any[]>([]);
  const [categoryLoading, setCategoryLoading] = useState<boolean>(false);
  const [editCategory, setEditCategory] = useState<boolean>(false);
  const [activeCategory, setActiveCategory] = useState<any>();
  const [domain, setDomain] = useState<string>('');
  const [activeProduct, setActiveProduct] = useState<any>();
  const [refreshing, setIsRefreshing] = useState<boolean>(false);

  const {
    isLoading: isProductsLoading,
    isError: isProductsError,
    refetch: refetchProducts,
    isFetching: isFetchingProducts,
  } = useQuery(
    queryKeys.PRODUCTS_DATA,
    async () => {
      try {
        const productsData = await productApi.getProducts(business_id);
        setProducts(productsData.data);
        setSearchResult(productsData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2:
            'Sorry we cannot fetch products now, please try again later...',
        });
        console.log(
          'Error getting products:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const {
    isLoading: isCategoryLoading,
    isError: isCategoryError,
    refetch: refetchCategory,
    isFetching: isFetchingCategory,
  } = useQuery(
    queryKeys.CATEGORY_DATA,
    async () => {
      try {
        const categoryData = await categoryApi.getCategories(business_id);
        setCategories(categoryData.data);
        setCategoriesSearch(categoryData.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2:
            'Sorry we cannot fetch categories now, please try again later...',
        });
        console.log(
          'Error getting categories:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const {
    isLoading: isStorefrontLoading,
    isError: isStorefrontError,
    refetch: refetchStorefront,
    isFetching: isFetchingStorefront,
  } = useQuery(
    queryKeys.STOREFRONT_DATA,
    async () => {
      try {
        const res = await storefrontApi.getStorefront(business_id);
        setDomain(res && res.data.domain);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2:
            'Sorry we cannot fetch storefront now, please try again later...',
        });
        console.log(
          'Error getting storefront:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  const onRefreshData = async () => {
    await refetchProducts();
    await refetchCategory();
    await refetchStorefront();
  };

  const handleDelete = async (id: string) => {
    Alert.alert(
      'Delete Product',
      'Are you sure you want to delete this product?',
      [
        {
          style: 'destructive',
          text: 'Yes',
          onPress: async () => {
            try {
              setLoading(true);
              await productApi.deleteProduct(id);
              await onRefreshData();
              setLoading(false);
              Toast.show({
                type: 'success',
                text1: 'Product Delete',
                text2: 'Product deleted successfully',
              });
            } catch (error: any) {
              setLoading(false);
              console.log(
                'Error fetching products or service:',
                JSON.stringify(error.response.data)
              );
              Toast.show({
                type: 'error',
                text1: 'Server Error',
                text2: 'Cannot delete product, please try again later...',
              });
            }
          },
        },
        {
          style: 'default',
          text: 'No',
        },
      ]
    );
  };

  const handleNewCategory = async (name: string) => {
    try {
      setCategoryLoading(true);

      await categoryApi.createCategory({
        name,
      });
      await onRefreshData();
      setCategoryLoading(false);
    } catch (error) {
      setCategoryLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot get data, please try again later...',
      });
    }
  };

  useEffect(() => {
    isFocused && onRefreshData();
  }, [isFocused]);

  const handleSearch = async (e: any) => {
    try {
      const nameSearch =
        products &&
        products.filter(
          (c: any) =>
            c.name &&
            c.name.trim().toLowerCase().includes(e.trim().toLowerCase())
        );

      const priceSearch =
        products &&
        products.filter((c: any) =>
          c.price
            ? c.price.trim().toLowerCase().includes(e.trim().toLowerCase())
            : ''
        );

      if (e.length !== '') {
        setSearchResult([...nameSearch, ...priceSearch]);
      } else {
        setSearchResult(products);
      }
    } catch (error) {
      console.log('Error in search:', error);
    }
  };

  const handleSearchCategories = (e: string) => {
    const result = categories.filter(
      (c: any) => c.name && c.name.trim().includes(e.trim())
    );

    setCategoriesSearch([...result]);
  };

  const handleEditCategory = (item: any) => {
    navigation.navigate('EditCategory', {
      name: item.name,
      id: item.id,
    });
  };

  const handleRefresh = async () => {
    setIsRefreshing(true);

    await onRefreshData();

    setIsRefreshing(false);
  };

  const searchInputRef = useRef<any>();

  if (isProductsError || isCategoryError) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => await onRefreshData()}
      />
    );
  }

  const handleSimpleAdd = () => {
    navigation.navigate('SimpleAdd');
    setAddProductModalVisibility(false);
  };
  const handleFullAdd = () => {
    navigation.navigate('EditProduct', {
      isEdit: false,
    });
    setAddProductModalVisibility(false);
  };
  return (
    <Box style={styles.container}>
      {(isProductsLoading || isCategoryLoading) && <ScreenLoading />}
      <Box mt="xxl" style={styles.productsBorder}>
        <ProductsSwitchPill
          active={activeItem}
          handleSwitch={() => {
            setSearchResult(products);
            setCategoriesSearch(categories);
            searchInputRef?.current?.clear();
            setActiveItem(activeItem === 'category' ? 'product' : 'category');
          }}
        />
      </Box>
      <Box style={styles.layout}>
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: wp(90),
          }}
        >
          <HeaderTextInput
            inputRef={searchInputRef}
            type="search"
            width={userPermissions.create_product ? wp(75) : wp(90)}
            height={40}
            placeholder="Search"
            onChangeText={
              activeItem === 'product' ? handleSearch : handleSearchCategories
            }
          />
          {userPermissions.create_product && (
            <TouchableOpacity
              onPress={() =>
                activeItem === 'product'
                  ? setAddProductModalVisibility(true)
                  : setOpenModal(true)
              }
            >
              <PlusCircleIcon />
            </TouchableOpacity>
          )}
        </Box>
      </Box>
      {activeItem === 'product' ? (
        <Box
          style={{
            height: hp(75),
            paddingTop: 10,
            backgroundColor: theme.colors.bg13,
            width: wp(100),
            alignItems: 'center',
          }}
        >
          <FlatList
            contentContainerStyle={{ paddingBottom: 50 }}
            data={searchResult}
            refreshing={refreshing}
            onRefresh={() => (
              <RefreshControl
                refreshing={refreshing}
                onRefresh={handleRefresh}
              />
            )}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => {
              if (isProductsLoading || isCategoryLoading) {
                return null;
              } else {
                return (
                  // handle search empty UI
                  <Box style={{ marginTop: 30 }}>
                    <ListEmpty
                      topText=""
                      bottomText="You haven't added any product yet"
                      buttonText="Add a product"
                      invoicePage
                      button={userPermissions.create_product ? true : false}
                      onPressButton={() => {
                        return navigation.navigate('EditProduct', {
                          isEdit: false,
                        });
                      }}
                    />
                  </Box>
                );
              }
            }}
            keyExtractor={(item: any, index: number) => index.toString()}
            renderItem={({ item }) => (
              <Box
                style={{
                  marginBottom: 10,
                }}
              >
                <ProductsList
                  quantity={item.stock_quantity}
                  isPublished={item.is_published}
                  price={item.price}
                  image={item.image}
                  onPress={() =>
                    navigation.navigate('ProductDetail', {
                      copyLink: `https://${domain}.ricive.shop/products/${item.id}`,
                      id: item.id,
                      name: item.name,
                      price: item.price,
                      quantity: item.stock_quantity,
                      status: item.is_published,
                      product: item,
                    })
                  }
                  productPrice={item.price && item.price.toString()}
                  name={item.name}
                  handleDelete={async () => await handleDelete(item.id)}
                  handleCopy={() => {
                    Clipboard.setString(
                      `https://${domain}.ricive.shop/products/${item.id}`
                    );
                    Toast.show({
                      type: 'success',
                      text1: 'Copied!',
                      text2: 'URL copied successfully!',
                    });
                  }}
                  handleEdit={() =>
                    navigation.navigate('EditProduct', {
                      isEdit: true,
                      product: item,
                    })
                  }
                />
              </Box>
            )}
          />
        </Box>
      ) : (
        <Box
          style={{
            height: hp(75),
            backgroundColor: theme.colors.bg13,
            width: wp(100),
            alignItems: 'center',
          }}
        >
          <FlatList
            contentContainerStyle={{ paddingBottom: 50 }}
            data={categoriesSearch}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => (
              <Box style={{ paddingTop: 30 }}>
                <ListEmpty
                  topText=""
                  bottomText="You haven't added any category yet"
                  buttonText="Add a product category"
                  invoicePage
                  button
                  onPressButton={() => {
                    return setOpenModal(true);
                  }}
                />
              </Box>
            )}
            keyExtractor={(item: any) => item.id.toString()}
            renderItem={({ item }) => (
              <Box>
                <CategoryCard
                  categoryName={item.name}
                  productCount={item.product_category.length}
                  handleEdit={() => handleEditCategory(item)}
                />
              </Box>
            )}
          />
        </Box>
      )}
      <ProductsCategoryModal
        visible={openModal}
        setVisible={setOpenModal}
        isEdit={editCategory}
        activeCategory={activeCategory}
        handleNewCategory={handleNewCategory}
        loading={categoryLoading}
      />
      <AppModal
        visible={addProductModalVisibility}
        setVisible={setAddProductModalVisibility}
        heightValue={hp(35)}
        top={100}
        content={
          <Box
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginTop: hp(10),
              paddingHorizontal: 30,
            }}
          >
            <ProductCard
              onPress={handleSimpleAdd}
              width={wp(35)}
              height={hp(17)}
              title={'Simple'}
              paragraph={
                'Add the basic requirements and create a simple product'
              }
              active={card1Active}
            />
            <ProductCard
              onPress={handleFullAdd}
              active={card2Active}
              width={wp(35)}
              height={hp(17)}
              title={'Advanced'}
              paragraph={
                'Add the basics and extra options for an advanced product'
              }
            />
          </Box>
        }
      />
      {/* <Modal
        animationType="fade"
        visible={addProductModalVisibility}
        onRequestClose={() => {
          setAddProductModalVisibility(false);
        }}
      >
        <Box flex={1} backgroundColor="bg1" justifyContent="center">
          <Box position="absolute" top={40} right={10} padding="m">
            <TouchableOpacity
              onPress={() => setAddProductModalVisibility(false)}
            >
              <Icon name="close" size={30} color={theme.colors.primary1} />
            </TouchableOpacity>
          </Box>
          <Box
            flexDirection="row"
            justifyContent="space-around"
            marginHorizontal="l"
          >
            <ProductCard
              onPress={handleSimpleAdd}
              iconName={'paper-plane'}
              width={width / 3}
              height={height / 4}
              title={'Simple Add'}
              paragraph={
                'click here to quickly add products by providing only required details'
              }
              active={card1Active}
            />
            <ProductCard
              onPress={handleFullAdd}
              active={card2Active}
              iconName={'ios-add-circle'}
              width={width / 3}
              height={height / 4}
              title={'Add with Full Description'}
              paragraph={
                'click here to add products with full details and description'
              }
            />
          </Box>
        </Box>
      </Modal> */}
    </Box>
  );
};

export default Products;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { KeyboardAvoidingView, Platform, StyleSheet } from 'react-native';

import Button from '../../components/Button';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import theme, { Box, Text } from '../../components/Themed';
import { ProfileNavParamList } from '../../navigation/ProfileNav';
import profile from '../../api/riciveApi/profile';
import Toast from 'react-native-toast-message';
import { useAppSelector } from '../../redux/hooks';

type Props = NativeStackScreenProps<ProfileNavParamList, 'ChangePassword'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerEdit: {
    position: 'absolute',
    marginLeft: 330,
  },
  body: {
    alignItems: 'center',
    marginTop: 30,
  },
  image: {
    height: 122,
    width: 122,
  },
  inputColumn: {
    marginTop: 20,
  },
  inputField: {
    marginTop: 15,
  },
  profileButton: {
    marginTop: 20,
    marginBottom: 30,
  },
  button: {
    alignItems: 'center',
    marginTop: 40,
  },
  bottomSheet: {
    paddingHorizontal: 30,
  },
  bottomSheetCamera: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
  },
  bottomSheetBorder: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.text3,
  },
  bottomSheetGallery: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
  },
});

const ChangePassword = ({ navigation, route }: Props): JSX.Element => {
  const [oldPassword, setOldPassword] = useState<string>('');
  const [newPassword, setNewPassword] = useState<string>('');
  const [confirmNewPassword, setConfirmNewPassword] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  //Change password handler
  const user = useAppSelector((state) => state.login.user.auth.id);
  const handleUpdate = async () => {
    if (oldPassword == newPassword) {
      return Toast.show({
        type: 'error',
        text1: 'New and old password should not match',
        text2: 'Please try again',
      });
    }

    if (newPassword !== confirmNewPassword) {
      return Toast.show({
        type: 'error',
        text1: 'Passwords do not match',
        text2: 'Please try again',
      });
    }

    if (newPassword.length < 6) {
      return Toast.show({
        type: 'error',
        text1: 'Passwords must be more than 6 characters',
        text2: 'Please try again',
      });
    }
    setLoading(true);
    try {
      const updateResponse = await profile.updatePassword({
        oldPassword,
        newPassword,
        id: user,
      });

      Toast.show({
        type: 'success',
        text1: 'Password Update Success',
        text2: 'You have successfully updated your password',
      });

      if (updateResponse.status === 'failed') {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Error Updating Password',
          text2: updateResponse.message,
        });
      }
      setLoading(false);
      return navigation.navigate('ProfileHome');
    } catch (error: any) {
      setLoading(false);
      console.log(
        'Error updating user password:',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error!',
        text2: 'We cannot process your request now...',
      });
    }
  };

  return (
    <Box style={styles.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'position' : 'height'}
      >
        <Box style={styles.header}>
          <StackHeader
            backTitle=""
            title="Change Password"
            onBackPress={() => navigation.goBack()}
          />
        </Box>
        <Box>
          <Box style={styles.body}>
            <Box style={styles.inputColumn}>
              <Text>Existing password</Text>
              <Box style={styles.inputField}>
                <TextInput
                  type="input"
                  secured
                  placeholder="Existing password"
                  autoCompleteType="password"
                  autoCapitalize="none"
                  keyboardType="default"
                  autoCorrect={false}
                  onChangeText={(e) => setOldPassword(e)}
                />
              </Box>
            </Box>
            <Box style={styles.inputColumn}>
              <Text>New Password</Text>
              <Box style={styles.inputField}>
                <TextInput
                  type="input"
                  secured
                  placeholder="New password"
                  autoCompleteType="password"
                  autoCapitalize="none"
                  keyboardType="default"
                  autoCorrect={false}
                  onChangeText={(e) => setNewPassword(e)}
                />
              </Box>
            </Box>
            <Box style={styles.inputColumn}>
              <Text>Confirm new password</Text>
              <Box style={styles.inputField}>
                <TextInput
                  type="input"
                  secured
                  placeholder="Confirm new password"
                  autoCompleteType="password"
                  autoCapitalize="none"
                  keyboardType="default"
                  autoCorrect={false}
                  onChangeText={(e) => setConfirmNewPassword(e)}
                />
              </Box>
            </Box>
          </Box>
        </Box>
        <Box style={styles.button}>
          <Button
            label="Change password"
            onPress={handleUpdate}
            type="primary"
            loading={loading}
            disabled={
              oldPassword.length < 1 ||
              newPassword.length < 1 ||
              confirmNewPassword.length < 1
                ? true
                : false
            }
          />
        </Box>
        <Box style={{ padding: 40 }} />
      </KeyboardAvoidingView>
    </Box>
  );
};

export default ChangePassword;

import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import theme, { Box, Text } from '../../components/Themed';
import { Props } from '../../navigation';
import DeliveryCard from '../../components/DeliveryCard';
import StackHeader from '../../components/StackHeader';
import SwitchPill from '../../components/SwitchPill';
import delivery from '../../api/delivery/delivery';
import { useAppSelector } from '../../redux/hooks';
import supabase from '../../../supabase';
import db from '../../api/db';
import order from '../../api/orders/order';
import { DrawerActions } from '@react-navigation/native';
import ListEmpty from '../../components/ListEmpty';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.light3,
    alignItems: 'center',
  },
  list: {
    marginTop: 20,
    height: heightPercentageToDP(70),
    paddingBottom: 60,
  },
  switch: {
    height: 50,
    width: wp(90),
    alignItems: 'center',
    marginTop: 10,
  },
});

const Deliveries = ({ navigation }: Props): JSX.Element => {
  const [activeItem, setActiveItem] = useState<any>({
    name: 'New',
    value: 'new',
  });
  const [deliveries, setDeliveries] = useState<any>();
  const merchantId = useAppSelector((state) => state.login.id);
  const [loading, setLoading] = useState<boolean>(false);
  const role = useAppSelector((state) => state.login.role);

  useEffect(() => {
    const getDeliveries = async () => {
      setLoading(true);
      try {
        const data = await delivery.getMerchantDeliveries(merchantId);
        setDeliveries(data);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        console.log('Error getting deliveries', error);
      }
    };

    getDeliveries();

    const deliverySub = supabase
      .from(db.tables.merchant_delivery)
      .on('*', () => {
        getDeliveries();
      })
      .subscribe();

    return () => {
      supabase.removeSubscription(deliverySub);
    };
  }, []);

  if (loading) {
    return (
      <Box
        style={{
          flex: 1,
          backgroundColor: 'rgba(255, 255, 255)',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <ActivityIndicator />
        <Text variant={'DetailsM'} color="text2" mt="l">
          loading...
        </Text>
      </Box>
    );
  }

  return (
    <Box style={styles.container}>
      <StackHeader
        title="Deliveries"
        onBackPress={role !== 'Rider' ? () => navigation.goBack() : undefined}
        burger={
          role === 'Rider'
            ? () => navigation.dispatch(DrawerActions.openDrawer())
            : undefined
        }
      />

      <Text mt="s" variant={'Body2M'}>
        {`Orders for ${new Date().toDateString()}`}
      </Text>

      <Box style={styles.switch}>
        <SwitchPill
          items={[
            {
              name: 'New',
              value: 'new',
            },
            {
              name: 'Completed',
              value: 'completed',
            },
          ]}
          {...{ activeItem, setActiveItem }}
        />
      </Box>

      <Box style={styles.list}>
        <FlatList
          data={deliveries}
          keyExtractor={(item: any) => item.id.toString()}
          ListEmptyComponent={() => {
            if (loading) {
              return <ActivityIndicator />;
            }
            return (
              <ListEmpty
                topText="Deliveries"
                bottomText={`No ${activeItem.name} Deliveries`}
              />
            );
          }}
          renderItem={({ item }) => (
            <DeliveryCard
              serviceType={item.type}
              orderId={item.order.id}
              address={item.order.pick_up_address.address_name}
              onPress={() =>
                navigation.navigate('DeliveryDetail', {
                  id: item.order.id,
                })
              }
              time={item.fulfil_time}
            />
          )}
        />
      </Box>
    </Box>
  );
};

export default Deliveries;

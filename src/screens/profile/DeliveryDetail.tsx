import React from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';

import theme, { Box, Text } from '../../components/Themed';

const styles = StyleSheet.create({
  container: {},
});

const DeliveryDetail = (): JSX.Element => {
  return (
    <Box style={styles.container}>
      <Text>DeliveryDetail</Text>
    </Box>
  );
};

export default DeliveryDetail;

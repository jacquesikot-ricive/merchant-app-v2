import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import BottomSheet, { BottomSheetView } from '@gorhom/bottom-sheet';
import * as ImagePicker from 'expo-image-picker';
import { Camera } from 'expo-camera';

import Button from '../../components/Button';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import theme, { Box, Text } from '../../components/Themed';
import { ProfileNavParamList } from '../../navigation/ProfileNav';
import CameraIcon from '../../svg/CameraIcon';
import GalleryIcon from '../../svg/GalleryIcon';
import { useAppSelector } from '../../redux/hooks';
import Toast from 'react-native-toast-message';
import profile from '../../api/riciveApi/profile';
import storage from '../../utils/storage';
import storageKeys from '../../constants/storageKeys';

type Props = NativeStackScreenProps<ProfileNavParamList, 'EditProfile'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerEdit: {
    position: 'absolute',
    marginLeft: 330,
  },
  body: {
    alignItems: 'center',
    marginTop: 30,
  },
  image: {
    height: 122,
    width: 122,
  },
  inputColumn: {
    marginTop: 20,
  },
  inputField: {
    marginTop: 15,
  },
  profileButton: {
    marginTop: 20,
    marginBottom: 30,
  },
  button: {
    alignItems: 'center',
    marginTop: 40,
  },
  bottomSheet: {
    paddingHorizontal: 30,
  },
  bottomSheetCamera: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
  },
  bottomSheetBorder: {
    borderBottomWidth: 1,
    borderBottomColor: theme.colors.text3,
  },
  bottomSheetGallery: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
  },
});

const EditProfile = ({ navigation, route }: Props): JSX.Element => {
  const user = useAppSelector((state) => state.login.user);
  const bottomSheetRef = useRef<BottomSheet>(null);
  // const { expand } = useBottomSheet();
  // const handleClosePress = () => bottomSheetRef.current.close();
  const [open, setOpen] = useState<boolean>(false);
  const [image, setImage] = useState<any>();
  const [startCamera, setStartCamera] = useState<boolean>(false);
  const [first_name, setFirstName] = useState<string>('');
  const [last_name, setLastName] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);

  // variables
  const snapPoints = useMemo(() => ['25%', '50%'], []);

  // callbacks
  const handleSheetChanges = useCallback((index: number) => {
    bottomSheetRef.current?.snapToIndex(index);
    setOpen(true);
  }, []);

  //image from gallery
  const selectImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  //camera
  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      if (status === 'granted') {
        setStartCamera(true);
      } else {
        Alert.alert('Access denied');
      }
    })();
  }, []);

  let camera: Camera;
  const takePicture = async () => {
    const photo: any = await camera.takePictureAsync();
    setImage(photo);
  };

  //Edit user profile handler

  const handleUpdate = async () => {
    try {
      const updateResponse = await profile.updateProfile({
        first_name:
          first_name.length > 0 ? first_name : user.profile.first_name,
        last_name: last_name.length > 0 ? last_name : user.profile.last_name,
        id: user.profile.id,
      });

      await storage.storeData(storageKeys.USER, {});
      console.log(JSON.stringify(updateResponse.message), 'witwicki');
      setLoading(false);
      return navigation.navigate('PersonalInformation');
    } catch (error: any) {
      console.log('Error updating user', JSON.stringify(error.response.data));
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: error || 'Server Error!',
        text2: 'We cannot process your request now...',
      });
    }
  };

  return (
    <Box
      style={[
        styles.container,
        { backgroundColor: open ? '#00000010' : '#fff' },
      ]}
    >
      <Box style={styles.header}>
        <StackHeader
          title="Edit Profile"
          icon1="Done"
          onPressIcon1={handleUpdate}
          onBackPress={() => navigation.goBack()}
        />
      </Box>
      <Box>
        <ScrollView bounces={false}>
          <Box style={styles.body}>
            <Box>
              {image ? (
                <Image
                  style={styles.image}
                  source={{ uri: `${user.profile.image}` }}
                />
              ) : (
                <Image
                  style={styles.image}
                  source={{ uri: `${user.profile.image}` }}
                />
              )}
            </Box>
            <TouchableOpacity
              onPress={() => handleSheetChanges(0)}
              style={[styles.profileButton]}
            >
              <Text variant="DetailsM" color="primary1">
                Change profile photo
              </Text>
            </TouchableOpacity>
            <Box style={styles.inputColumn}>
              <Text>First Name</Text>
              <Box style={styles.inputField}>
                <TextInput
                  type="input"
                  placeholder={user.profile.first_name}
                  onChangeText={(e) => setFirstName(e)}
                />
              </Box>
            </Box>
            <Box style={styles.inputColumn}>
              <Text>Last Name</Text>
              <Box style={styles.inputField}>
                <TextInput
                  type="input"
                  placeholder={user.profile.last_name}
                  onChangeText={(e) => setLastName(e)}
                />
              </Box>
            </Box>
            <Box style={styles.inputColumn}>
              <Text>Email address</Text>
              <Box style={styles.inputField}>
                <TextInput type="input" placeholder={user.profile.email} />
              </Box>
            </Box>
          </Box>
        </ScrollView>
      </Box>
      <Box style={styles.button}>
        {/* <Button
                    label="Update"
                    onPress={handleUpdate}
                    type="primary"
                    loading={loading}
                /> */}
      </Box>
      <BottomSheet
        index={-1}
        ref={bottomSheetRef}
        snapPoints={snapPoints}
        enablePanDownToClose={true}
        onClose={() => setOpen(false)}
      >
        <BottomSheetView style={styles.bottomSheet}>
          <TouchableOpacity
            style={styles.bottomSheetCamera}
            onPress={takePicture}
          >
            <CameraIcon />
            <Text ml="m" variant="DetailsR" color="text1">
              Take photo
            </Text>
          </TouchableOpacity>
          <Box style={styles.bottomSheetBorder} />
          <TouchableOpacity
            style={styles.bottomSheetGallery}
            onPress={selectImage}
          >
            <GalleryIcon />
            <Text ml="m" variant="DetailsR" color="text1">
              Pick from Gallery
            </Text>
          </TouchableOpacity>
        </BottomSheetView>
      </BottomSheet>
    </Box>
  );
};

export default EditProfile;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { useAnalytics } from '@segment/analytics-react-native';
import React, { useCallback, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Alert,
  Keyboard,
  KeyboardAvoidingView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';

import shipping from '../../api/riciveApi/shipping';
import Button from '../../components/Button';
import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import theme, { Box, Text } from '../../components/Themed';
import { ShippingNavParamList } from '../../navigation/ShippingNav';
import { useAppSelector } from '../../redux/hooks';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  content: {
    paddingHorizontal: 15,
    width: wp(100),
    alignItems: 'center',
    marginTop: 20,
  },
  checkbox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  picker: {
    // width: 10,
    marginRight: 5,
  },
  button: {
    marginTop: 52,
    justifyContent: 'flex-end',
    flexDirection: 'row',
    alignSelf: 'center',
  },
});

type Props = NativeStackScreenProps<ShippingNavParamList, 'EditShipping'>;

const EditShipping = ({ navigation, route }: Props) => {
  const { id, name, cost, message } = route.params;
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const { screen } = useAnalytics();
  const [loading, setLoading] = useState<boolean>(false);
  const [deleteLoading, setDeleteLoading] = useState<boolean>(false);
  const [newName, setNewName] = useState<string>(name ? name : '');
  const [newCost, setNewCost] = useState<string>(cost ? cost : '');
  const [newMessage, setNewMessage] = useState<string>(message ? message : '');

  const handleUpdateShipping = async () => {
    try {
      setLoading(true);
      await shipping.updateShipping({
        id,
        name: newName ? newName : '',
        cost: newCost ? newCost : '',
        message: newMessage ? newMessage : undefined,
      });
      setLoading(false);
      navigation.navigate('Shipping', {
        reload: true,
      });

      Toast.show({
        type: 'success',
        text1: 'Shipping Updated',
        text2: 'Shipping has been changed!',
      });
    } catch (error: any) {
      console.log(JSON.stringify(error.response.data));
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot update data, please try again later...',
      });
    }
  };

  const handleDeleteShipping = () => {
    Alert.alert(
      'Delete Shipping',
      'Are you sure you want to delete the shipping?',
      [
        {
          style: 'default',
          text: 'No',
        },
        {
          style: 'destructive',
          text: 'Yes',
          onPress: async () => {
            try {
              setDeleteLoading(true);
              await shipping.deleteShipping(id);
              setDeleteLoading(false);
              navigation.navigate('Shipping', {
                reload: true,
              });
              Toast.show({
                type: 'success',
                text1: 'Shipping Deleted',
                text2: 'Shipping has been deleted!',
              });
            } catch (error: any) {
              console.log(JSON.stringify(error.response.data));
              setDeleteLoading(false);
              Toast.show({
                type: 'error',
                text1: 'Shipping Deleted',
                text2: 'Shipping has been deleted!',
              });
            }
          },
        },
      ]
    );
  };

  return (
    <Box style={styles.container}>
      <StackHeader
        title="Edit Shipping"
        onBackPress={() => navigation.goBack()}
      />
      <Box style={styles.content}>
        <KeyboardAvoidingView behavior="padding">
          <Box>
            <Text variant="SmallerTextR" color="text1" mb="l">
              Name
            </Text>
            <TextInput
              type="input"
              placeholder={'Enter Name'}
              width={wp(90)}
              autoCompleteType="name"
              autoCorrect={false}
              autoCapitalize="words"
              returnKeyType="next"
              value={newName}
              onChangeText={(e) => setNewName(e)}
            />
          </Box>
          <Box>
            <Text variant="SmallerTextR" color="text1" mb="l" mt="m">
              Shipping cost
            </Text>
            <TextInput
              type="input"
              placeholder={'Enter Cost'}
              width={wp(90)}
              keyboardType="number-pad"
              autoCorrect={false}
              autoCapitalize="none"
              returnKeyType="next"
              value={newCost}
              onChangeText={(e) => setNewCost(e)}
            />
          </Box>
          <Box>
            <Text variant="SmallerTextR" color="text1" mb="l" mt="m">
              Message (optional)
            </Text>
            <TextInput
              type="input"
              placeholder={'Enter Message'}
              width={wp(90)}
              autoCompleteType="off"
              autoCorrect={false}
              autoCapitalize="none"
              returnKeyType="next"
              onChangeText={(e) => setNewMessage(e)}
              value={newMessage}
            />
          </Box>
          <Box style={{ paddingBottom: 10 }} />
        </KeyboardAvoidingView>
      </Box>

      <Box style={styles.button}>
        <Button
          disabled={name.length < 1 || cost.length < 1 ? true : false}
          loading={loading}
          type="primary"
          label="Update Shipping"
          onPress={handleUpdateShipping}
          width={wp(90)}
        />
      </Box>

      <Box style={{ alignItems: 'center' }}>
        <TouchableOpacity
          onPress={handleDeleteShipping}
          activeOpacity={0.6}
          style={{
            marginTop: 20,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 8,
            backgroundColor: theme.colors.error,
            height: 56,
            width: wp(90),
          }}
        >
          {deleteLoading ? (
            <ActivityIndicator color={theme.colors.white} />
          ) : (
            <Text variant={'DetailsR'} color="white">
              Delete Shipping
            </Text>
          )}
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default EditShipping;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useLayoutEffect, useState } from 'react';
import {
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  Image,
  TouchableOpacity,
} from 'react-native';
import Toast from 'react-native-toast-message';
import profile from '../../api/riciveApi/profile';
import useAuth from '../../auth/useAuth';
import AppModal from '../../components/AppModal';
import Button from '../../components/Button';
import PersonalInfoCard from '../../components/PersonalInfoCard';
import ProfileActions from '../../components/ProfileActions';

import StackHeader from '../../components/StackHeader';
import TextInput from '../../components/TextInput';
import theme, { Box, Text } from '../../components/Themed';
import storageKeys from '../../constants/storageKeys';
import { ProfileNavParamList } from '../../navigation/ProfileNav';
import { useAppSelector } from '../../redux/hooks';
import storage from '../../utils/storage';
import profileApi from '../../api/riciveApi/profile';

type Props = NativeStackScreenProps<ProfileNavParamList, 'PersonalInformation'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.white,
    flex: 1,
  },
  layout: {
    width: '100%',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerEdit: {
    position: 'absolute',
    marginLeft: 330,
  },
  body: {
    marginTop: 35,
    alignItems: 'center',
  },
  avatar: {
    height: 122,
    width: 122,
    backgroundColor: theme.colors.primary4,
    borderRadius: 100,
    marginBottom: 21,
    alignItems: 'center',
    paddingVertical: 40,
  },
  profile: {
    marginTop: 15,
  },
  profileBorder: {
    borderWidth: 1,
    borderColor: theme.colors.border1,
    width: 350,
    height: 58,
    borderRadius: 6,
    marginTop: 15,
  },
  profileText: {
    paddingTop: 0,
    paddingHorizontal: 15,
    marginTop: 15,
  },
  button: {
    alignItems: 'center',
    marginTop: 40,
  },
  image: {
    height: 122,
    width: 122,
  },
  inputColumn: {
    marginTop: 20,
  },
  inputField: {
    marginTop: 15,
  },
  profileButton: {
    marginBottom: 30,
  },
});

const PersonalInformation = ({ navigation }: Props): JSX.Element => {
  const user = useAppSelector((state) => state.login.user);

  const [first_name, setFirstName] = useState<string>('');
  const [last_name, setLastName] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [image, setImage] = useState<any>();

  const returnInitials = () => {
    return `${user.profile.first_name.charAt(
      0
    )} ${user.profile.last_name?.charAt(0)}`;
  };

  const handleUpdate = async () => {
    try {
      await profile.updateProfile({
        first_name:
          first_name.length > 0 ? first_name : user.profile.first_name,
        last_name: last_name.length > 0 ? last_name : user.profile.last_name,
        id: user.profile.id,
      });

      await storage.storeData(storageKeys.USER, {});
      Toast.show({
        type: 'success',
        text1: 'Profile Update Succesful',
        text2: 'You have successfully updated your profile',
      });
      setLoading(false);
    } catch (error: any) {
      console.log('Error updating user', JSON.stringify(error.response.data));
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: JSON.stringify(error.response.data.message) || 'Server Error!',
        text2: 'We cannot process your request now..',
      });
    }
  };

  return (
    <Box style={styles.container}>
      <Box style={styles.layout}>
        <Box style={styles.header}>
          <StackHeader
            title="Profile"
            // icon1="Edit"
            onPressIcon1={handleUpdate}
            onBackPress={() => navigation.goBack()}
          />
        </Box>

        <KeyboardAvoidingView>
          <Box style={styles.body}>
            <Box>
              {image ? (
                <>
                  <Image
                    style={styles.image}
                    source={{ uri: `${user.profile.image}` }}
                  />
                  <TouchableOpacity
                    // onPress={() => handleSheetChanges(0)}
                    style={[styles.profileButton]}
                  >
                    <Text variant="DetailsM" color="primary1">
                      Change profile photo
                    </Text>
                  </TouchableOpacity>
                </>
              ) : (
                <Box style={styles.avatar}>
                  <Text variant="MainTitleM">{returnInitials()}</Text>
                </Box>
              )}
            </Box>
            {/* <Box style={styles.inputColumn}>
              <Text>First Name</Text>
              <Box style={styles.inputField}>
                <TextInput
                  type="input"
                  placeholder={user.profile.first_name}
                  placeholderTextColor={theme.colors.text3}
                  onChangeText={(e) => setFirstName(e)}
                  autoCapitalize="none"
                  keyboardType="default"
                  returnKeyType="next"
                />
              </Box>
            </Box> */}

            <Box style={styles.profile}>
              <Box>
                <Text variant="DetailsR" color="text1">
                  First Name
                </Text>
              </Box>
              <Box style={styles.profileBorder}>
                <Box style={styles.profileText}>
                  <Text variant="DetailsR" color="text3">
                    {user.profile.first_name}
                  </Text>
                </Box>
              </Box>
            </Box>

            <Box style={styles.profile}>
              <Box>
                <Text variant="DetailsR" color="text1">
                  Last Name
                </Text>
              </Box>
              <Box style={styles.profileBorder}>
                <Box style={styles.profileText}>
                  <Text variant="DetailsR" color="text3">
                    {user.profile.last_name}
                  </Text>
                </Box>
              </Box>
            </Box>

            {/* <Box style={styles.inputColumn}>
              <Text>Last Name</Text>
              <Box style={styles.inputField}>
                <TextInput
                  type="input"
                  placeholder={user.profile.last_name}
                  placeholderTextColor={theme.colors.text3}
                  onChangeText={(e) => setLastName(e)}
                  autoCapitalize="none"
                  keyboardType="default"
                  returnKeyType="next"
                />
              </Box>
            </Box> */}

            {/* <Box style={{ marginTop: 50 }}>
              <Button
                onPress={handleUpdate}
                label="Update"
                type="primary"
                loading={loading}
                disabled={
                  first_name.length < 1 || last_name.length < 1 ? true : false
                }
              />
            </Box> */}
            <Box style={styles.profile}>
              <Box>
                <Text variant="DetailsR" color="text1">
                  Email address
                </Text>
              </Box>
              <Box style={styles.profileBorder}>
                <Box style={styles.profileText}>
                  <Text variant="DetailsR" color="text3">
                    {user.profile.email}
                  </Text>
                </Box>
              </Box>
            </Box>
          </Box>
        </KeyboardAvoidingView>
      </Box>
    </Box>
  );
};

export default PersonalInformation;

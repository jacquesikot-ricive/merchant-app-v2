import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useState } from 'react';
import { ActivityIndicator, StyleSheet, TouchableOpacity } from 'react-native';
import Toast from 'react-native-toast-message';
import { Feather as Icon } from '@expo/vector-icons';

import StackHeader from '../../components/StackHeader';
import theme, { Box, Text } from '../../components/Themed';
import { ProfileNavParamList } from '../../navigation/ProfileNav';
import { useAppSelector } from '../../redux/hooks';
import auth from '../../api/riciveApi/auth';
import useAuth from '../../auth/useAuth';
import AppModal from '../../components/AppModal';
import ProfileActions from '../../components/ProfileActions';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

type Props = NativeStackScreenProps<ProfileNavParamList, 'ProfileHome'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.bg1,
    flex: 1,
    alignItems: 'center',
    width: wp(100),
  },
  layout: {},
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    backgroundColor: theme.colors.bg1,
  },
  body: {
    marginTop: 0,
  },
  profile: {
    marginBottom: 30,
  },
  profileTitle: {
    paddingHorizontal: 20,
    marginBottom: 10,
  },
  modal: {
    marginTop: 60,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  modalDescription: {
    alignItems: 'center',
    textAlign: 'center',
    width: '85%',
    marginTop: 25,
  },
  modalText: {
    textAlign: 'center',
  },
  modalButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    marginTop: 25,
    paddingHorizontal: 30,
  },
  cancelLogoutButton: {
    backgroundColor: theme.colors.bg11,
    borderRadius: 4,
    height: 36,
    width: wp(35),
    paddingVertical: 4,
    alignItems: 'center',
  },
  logoutButton: {
    backgroundColor: theme.colors.errorLight,
    borderRadius: 4,
    height: 36,
    width: wp(35),
    paddingVertical: 4,
    alignItems: 'center',
  },
  border: {
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 0.5,
    width: wp(100),
  },
  subtitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 0.9,
    paddingVertical: 18,
    width: wp(100),
    paddingHorizontal: 20,
  },
  lastTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 14,
    width: wp(100),
    paddingHorizontal: 20,
  },
  footer: {
    paddingHorizontal: 20,
    marginTop: 48,
  },
});
const ProfileHome = ({ navigation }: Props): JSX.Element => {
  const [open, setOpen] = useState<boolean>(false);
  const user = useAppSelector((state) => state.login.user);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [loading, setLoading] = useState<boolean>(false);
  const role = user.profile.role;
  const { logout } = useAuth();

  const handleAccountDelete = async () => {
    try {
      setLoading(true);
      await auth.deleteAccount();
      await logout();
      setLoading(false);
    } catch (error: any) {
      // console.log(JSON.stringify(error.response));
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Error deleting your account',
      });
    }
  };

  return (
    <Box style={styles.container}>
      <Box style={styles.layout}>
        <Box style={styles.header}>
          <StackHeader
            title="Account"
            onBackPress={() => navigation.goBack()}
          />
        </Box>
        <Box style={styles.border} />
        <Box style={styles.body}>
          <Box style={styles.profile}>
            <ProfileActions
              title={'Account'}
              toggleType="account"
              content={
                <Box
                  style={{
                    width: wp(100),
                    marginTop: 20,
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('PersonalInformation');
                    }}
                    style={styles.subtitle}
                  >
                    <Text variant="SmallerTextR" color="text1">
                      Personal Information
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      size={18}
                      color={theme.colors.text7}
                    />
                  </TouchableOpacity>
                  {userPermissions.business_info && (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('AboutBusiness', {
                          isProfileNav: true,
                        });
                      }}
                      style={styles.subtitle}
                    >
                      <Text variant="SmallerTextR" color="text1">
                        Business details
                      </Text>
                      <Icon
                        name={'chevron-right'}
                        size={18}
                        color={theme.colors.text7}
                      />
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('ChangePassword');
                    }}
                    style={styles.subtitle}
                  >
                    <Text variant="SmallerTextR" color="text1">
                      Change Password
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      size={18}
                      color={theme.colors.text7}
                    />
                  </TouchableOpacity>
                  {role === 'SUPERADMIN' && (
                    <TouchableOpacity
                      onPress={() => setOpen(true)}
                      style={styles.subtitle}
                    >
                      <Text variant="SmallerTextR" color="text1">
                        Delete Account
                      </Text>
                      <Icon
                        name={'chevron-right'}
                        size={18}
                        color={theme.colors.text7}
                      />
                    </TouchableOpacity>
                  )}
                  <TouchableOpacity
                    onPress={() => setOpen(true)}
                    style={styles.lastTitle}
                  >
                    <Text variant="SmallerTextR" color="text1">
                      Logout
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      size={18}
                      color={theme.colors.text7}
                    />
                  </TouchableOpacity>
                </Box>
              }
            />
            <ProfileActions
              title={'Store Settings'}
              toggleType="account"
              content={
                <Box
                  style={{
                    width: wp(100),
                    marginTop: 20,
                  }}
                >
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('ShippingNav');
                    }}
                    style={styles.lastTitle}
                  >
                    <Text variant="SmallerTextR" color="text1">
                      Setup Shipping
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      size={18}
                      color={theme.colors.text7}
                    />
                  </TouchableOpacity>

                  {/* <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('ShippingNav');
                    }}
                    style={styles.lastTitle}
                  >
                    <Text variant="SmallerTextR" color="text1">
                      Working Hours
                    </Text>
                    <Icon
                      name={'chevron-right'}
                      size={18}
                      color={theme.colors.text7}
                    />
                  </TouchableOpacity> */}

                  {/* <TouchableOpacity
                    onPress={() => {
                      // navigation.navigate("WorkingHoursNav");
                    }}
                    style={styles.lastTitle}
                  >
                    <Box flexDirection="row" alignItems="center">
                      <Icon
                        name={'plus-circle'}
                        size={18}
                        color={theme.colors.primary2}
                      />
                      <Text variant="DetailsR" color="primary2" marginLeft="s">
                        setup working hours
                      </Text>
                    </Box>
                    <Icon
                      name={'chevron-right'}
                      size={18}
                      color={theme.colors.text7}
                    />
                  </TouchableOpacity> */}
                </Box>
              }
            />
            {/* <ProfileActions
              title={"General"}
              toggleType="account"
              content={undefined}
            /> */}

            <Box style={styles.footer}>
              <Text variant="SmallerTextR" color="text7">
                Version 3.0
              </Text>
            </Box>

            {/* {userPermissions.business_info && (
              <ProfileActions
                onPress={() => {
                  navigation.navigate('AboutBusiness', {
                    isProfileNav: true,
                  });
                }}
                label="Business Information"
                labelBottom="Business, location, money"
                iconType="business"
              />
            )} */}

            {/* <ProfileActions
              onPress={() => {
                navigation.navigate('PersonalInformation');
              }}
              label="Personal Information"
              labelBottom="Firstname, Last name, Username"
              iconType="personalInfo"
            /> */}

            {/* <ProfileActions
                onPress={() => {
                  navigation.navigate('AboutBusiness', {
                    isProfileNav: true,
                  });
                }}
                label="Business Information"
                labelBottom="Business, location, money"
                iconType="business"
              /> */}

            {/* {role === 'SUPERADMIN' && (
              <ProfileActions
                onPress={() => setOpen(true)}
                label="Delete Account"
                labelBottom="Delete and Erase all your information"
                iconType="delete"
              />
            )} */}
          </Box>
          {/* <Box style={styles.profile}>
            <Box style={styles.profileTitle}>
              <Text variant="DetailsR" color="text2">
                PASSWORD SETUP
              </Text>
            </Box>
            <ProfileActions
              onPress={() => {
                navigation.navigate('ChangePassword');
              }}
              label="Change Password"
              labelBottom="Reset password"
              iconType="password"
            />
          </Box> */}
          {/* <Box style={styles.profile}>
            <ProfileActions
              onPress={() => setOpen(true)}
              label="LOG OUT"
              labelColor="logout"
            />
          </Box> */}
        </Box>
        <AppModal
          visible={open}
          setVisible={setOpen}
          heightValue={hp(35)}
          content={
            <Box style={styles.modal}>
              <Text variant="Body2M" color="text1">
                Are you sure?
              </Text>
              <Box style={styles.modalDescription}>
                <Text style={styles.modalText} variant="DetailsR" color="text2">
                  You are about to delete your entire Ricive account, team
                  members, business data, website and analytics
                </Text>
              </Box>
              <Box style={styles.modalButton}>
                <TouchableOpacity
                  style={styles.cancelLogoutButton}
                  onPress={() => setOpen(false)}
                >
                  <Text variant="SmallerTextR" color="text7">
                    Go back
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.logoutButton}
                  onPress={handleAccountDelete}
                >
                  {loading ? (
                    <ActivityIndicator
                      color={theme.colors.white}
                      style={{ marginTop: 5 }}
                    />
                  ) : (
                    <Text variant="SmallerTextR" color="error">
                      Yes, Delete Account
                    </Text>
                  )}
                </TouchableOpacity>
              </Box>
            </Box>
          }
        />
      </Box>
    </Box>
  );
};

export default ProfileHome;

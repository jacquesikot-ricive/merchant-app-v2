import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useCallback, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import StackHeader from '../../components/StackHeader';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { AntDesign } from '@expo/vector-icons';

import { ShippingNavParamList } from '../../navigation/ShippingNav';
import theme, { Box, Text } from '../../components/Themed';
import { FlatList } from 'react-native-gesture-handler';
import ShippingModal, { ShippingProps } from '../../components/ShippingModal';
import { data } from '../../components/DashboardList';
import shipping from '../../api/riciveApi/shipping';
import { useAppSelector } from '../../redux/hooks';
import { useAnalytics } from '@segment/analytics-react-native';
import Toast from 'react-native-toast-message';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import ListEmpty from '../../components/ListEmpty';
import numberWithCommas from '../../utils/numbersWithComma';

type Props = NativeStackScreenProps<ShippingNavParamList, 'Shipping'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.bg1,
    flex: 1,
    alignItems: 'center',
    width: wp(100),
  },
  layout: {},
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    backgroundColor: theme.colors.bg1,
  },
  body: {
    marginTop: 0,
  },
  border: {
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 0.5,
    width: wp(100),
  },
  content: {
    paddingHorizontal: 20,
    marginTop: 28,
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 18,
  },
});

export const shippingData = [
  {
    id: '1',
    name: 'Lagos Island',
    shippingCost: '2000',
    message: 'excluding Ikeja and Ikorodu Environs',
  },
  {
    id: '2',
    name: 'Lagos Island',
    shippingCost: '2000',
    message: 'excluding Ikeja and Environs',
  },
  {
    id: '3',
    name: 'Lagos Island',
    shippingCost: '2000',
    message: 'excluding Ikeja and Environs',
  },
];
const Shipping = ({ navigation, route }: Props): JSX.Element => {
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const { track } = useAnalytics();
  const [pushData, setPushData] = useState<any>();
  const [loading, setLoading] = useState<boolean>(false);
  const [openShippingModal, setOpenShippingModal] = useState<boolean>(false);
  const [nameInput, setNameInput] = useState<string>(
    pushData && pushData.name && pushData.name?.length > 0 && pushData
  );
  const [shippingCostInput, setShippingCostInput] = useState<string>('');
  const [messageInput, setMessageInput] = useState<string>('');
  const [shippingData, setShippingData] = useState<any[]>([]);
  const [error, setError] = useState<boolean>(false);

  const getBusinessShipping = useCallback(async () => {
    setError(false);
    try {
      setLoading(true);

      const shippingDataRes = await shipping.getShipping(business_id);

      setShippingData(shippingDataRes.data);

      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      setError(true);
      console.log(
        'Error from getBusienssShipping:',
        JSON.stringify(error.response)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot fetch shipping data..',
      });
    }
  }, [business_id]);

  useEffect(() => {
    getBusinessShipping();

    navigation.addListener('focus', () => getBusinessShipping());

    return () => {
      navigation.removeListener('focus', () => getBusinessShipping());
    };
  }, []);

  if (loading) {
    return <ScreenLoading />;
  }

  if (error) {
    return (
      <ScreenError
        onRetry={async () => await getBusinessShipping()}
        onBackPress={() => navigation.goBack()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      <Box style={styles.layout}>
        <Box style={styles.header}>
          <StackHeader
            title="Shipping"
            onBackPress={() => navigation.goBack()}
            icon1={
              <TouchableOpacity onPress={() => setOpenShippingModal(true)}>
                <AntDesign
                  name="pluscircleo"
                  size={20}
                  color={theme.colors.primary1}
                />
              </TouchableOpacity>
            }
          />
        </Box>
        <Box style={styles.border} />
        <Box style={styles.content}>
          <Box>
            <FlatList
              data={shippingData}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item: any) => item.id.toString()}
              style={{ height: hp(70) }}
              ListHeaderComponent={
                <Box>
                  <Text variant="DetailsM" color="text1">
                    Shipping charges
                  </Text>
                  <Text variant="SmallerTextR" color="text7">
                    Set-up and manage how much you charge your customers for
                    shipping at checkout.
                  </Text>
                </Box>
              }
              ListEmptyComponent={
                <Box style={{ marginTop: -50 }}>
                  <ListEmpty
                    topText=""
                    bottomText="No Shipping Available"
                    invoicePage
                    button
                    buttonText="Add Shipping"
                    onPressButton={() => setOpenShippingModal(true)}
                  />
                </Box>
              }
              renderItem={({ item }) => (
                <Box
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    borderBottomColor: theme.colors.border1,
                    borderBottomWidth: 0.6,
                    paddingVertical: 16,
                  }}
                >
                  <Box style={{ width: '80%' }}>
                    <Text variant="SmallerTextM" color="text1">
                      {item.name}
                    </Text>
                    <Text
                      variant="SmallerTextM"
                      color="text1"
                      numberOfLines={1}
                    >
                      {item.message ? item.message : '-'}
                    </Text>
                    <Text variant="SmallerTextM" color="text1">
                      {'NGN' + ' ' + numberWithCommas(item.cost)}
                    </Text>
                  </Box>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('EditShipping', {
                        id: item.id,
                        name: item.name,
                        message: item.message,
                        cost: item.cost,
                      });
                    }}
                  >
                    <Text variant="DetailsM" color="primary1">
                      Edit
                    </Text>
                  </TouchableOpacity>
                </Box>
              )}
            />
            {/* <TouchableOpacity
              onPress={() => setOpenShippingModal(true)}
              style={styles.button}
            >
              <AntDesign
                name="pluscircleo"
                size={14}
                color={theme.colors.primary1}
              />
              <Text ml="l" color="primary1">
                {pushData ? 'Add New' : 'Setup shipping'}
              </Text>
            </TouchableOpacity> */}
          </Box>
        </Box>
      </Box>
      <ShippingModal
        visible={openShippingModal}
        setVisible={setOpenShippingModal}
        onPress={getBusinessShipping}
      />
    </Box>
  );
};

export default Shipping;

import { NativeStackScreenProps } from '@react-navigation/native-stack';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Pressable,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import StackHeader from '../../components/StackHeader';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { AntDesign } from '@expo/vector-icons';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from '../../components/Themed';

import { data } from '../../components/DashboardList';
import { useAppSelector } from '../../redux/hooks';
import { useAnalytics } from '@segment/analytics-react-native';
import Toast from 'react-native-toast-message';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import { WorkingHoursNavParamList } from '../../navigation/WorkingHoursNav';
import SwitchButton from '../../components/SwitchButton';
import DatePicker from '../../components/DatePicker';

import { BottomSheetModal } from '@gorhom/bottom-sheet';
import AppBottomSheet from '../../components/AppBottomSheetModal';
import TextInput from '../../components/TextInput';
import TimePicker from '../../components/TimePicker';

type Props = NativeStackScreenProps<WorkingHoursNavParamList, 'WorkingHours'>;

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.colors.bg1,
    flex: 1,
    alignItems: 'center',
    width: wp(100),
  },
  layout: {},
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    backgroundColor: theme.colors.bg1,
  },

  border: {
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 0.5,
    width: wp(100),
  },
  content: {
    paddingHorizontal: 20,
    marginTop: 28,
  },
  switchTouchable: {
    alignItems: 'center',
    marginRight: '43%',
    flexDirection: 'row',
  },
});
const { width, height } = Dimensions.get('window');

const WorkingHours = ({ navigation, route }: Props): JSX.Element => {
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const { track } = useAnalytics();
  const [description, setDescription] = useState<string>('closed description');
  const [startdate, setStartDate] = useState(new Date());
  const [enddate, setEndDate] = useState(new Date());
  const [checked, setChecked] = useState<boolean>(false);
  const [clickedIndex, setClickedIndex] = useState<number>();
  const [editClosedDate, setEditClosedDate] = useState<boolean>(false);
  const [mondayChecked, setMondayChecked] = useState<boolean>(false);
  const [tuesdayChecked, setTuesdayChecked] = useState<boolean>(false);
  const [wednesdayChecked, setWednesdayChecked] = useState<boolean>(false);
  const [thursdayChecked, setThursdayChecked] = useState<boolean>(false);
  const [fridayChecked, setFridayChecked] = useState<boolean>(false);
  const [saturdayChecked, setSaturdayChecked] = useState<boolean>(false);
  const [sundayChecked, setSundayChecked] = useState<boolean>(false);
  const [mondayStartTime, setMondayStartTime] = useState(new Date());
  const [tuesdayStartTime, setTuesdayStartTime] = useState(new Date());
  const [wednesdayStartTime, setWednesdayStartTime] = useState(new Date());
  const [thursdayStartTime, setThursdayStartTime] = useState(new Date());
  const [fridayStartTime, setFridayStartTime] = useState(new Date());
  const [saturdayStartTime, setSaturdayStartTime] = useState(new Date());
  const [sundayStartTime, setSundayStartTime] = useState(new Date());
  const [mondayEndTime, setMondayEndTime] = useState(new Date());
  const [tuesdayEndTime, setTuesdayEndTime] = useState(new Date());
  const [wednesdayEndTime, setWednesdayEndTime] = useState(new Date());
  const [thursdayEndTime, setThursdayEndTime] = useState(new Date());
  const [fridayEndTime, setFridayEndTime] = useState(new Date());
  const [saturdayEndTime, setSaturdayEndTime] = useState(new Date());
  const [sundayEndTime, setSundayEndTime] = useState(new Date());
  const [loading, setLoading] = useState<boolean>(false);

  const AppBotomModal = useRef<BottomSheetModal>(null);

  //   if (loading) {
  //     return <ScreenLoading />;
  //   }
  const onOpenModal = () => AppBotomModal.current?.present();
  const onCloseModal = () => {
    AppBotomModal.current?.close();
    setEditClosedDate(false);
  };
  const handleEditClosedDate = () => {
    setEditClosedDate(true);
    onOpenModal();
  };
  return (
    <Box style={styles.container}>
      <Box style={styles.layout}>
        <Box style={styles.header}>
          <StackHeader
            title="Working hours"
            onBackPress={() => navigation.goBack()}
            icon1={
              <TouchableOpacity onPress={() => {}}>
                <Text variant="Body2M" color="primary1">
                  Save
                </Text>
              </TouchableOpacity>
            }
          />
        </Box>
        <Box style={styles.border} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Box style={styles.content}>
            <Box>
              <Text variant="Body2B" marginBottom="m">
                Set working hours
              </Text>
              <Text variant="DetailsR" marginBottom="l" color="text2">
                Set-up your business working hours. Customers will be able to
                see your active period for each day of the week.
              </Text>
            </Box>
            <Box>
              <Box
                flexDirection="row"
                justifyContent="space-between"
                marginBottom="s"
                marginTop="xl"
              >
                <Text variant="DetailsR">Monday</Text>
                <Pressable onPress={() => setMondayChecked(!mondayChecked)}>
                  <Box style={styles.switchTouchable}>
                    <SwitchButton checked={mondayChecked} />
                    <Text marginLeft="m" variant="DetailsR">
                      {mondayChecked ? 'Open' : 'Closed'}
                    </Text>
                  </Box>
                  {mondayChecked ? (
                    <Box
                      flexDirection="row"
                      alignItems="center"
                      marginTop="m"
                      marginBottom="s"
                    >
                      <TimePicker
                        time={mondayStartTime}
                        setTime={setMondayStartTime}
                      />
                      <Text marginHorizontal="m">TO</Text>
                      <TimePicker
                        time={mondayEndTime}
                        setTime={setMondayEndTime}
                      />
                    </Box>
                  ) : null}
                </Pressable>
              </Box>

              <Box style={styles.border} />
              <Box
                flexDirection="row"
                justifyContent="space-between"
                marginBottom="s"
                marginTop="xl"
              >
                <Text variant="DetailsR">Tuesday</Text>
                <Pressable onPress={() => setTuesdayChecked(!tuesdayChecked)}>
                  <Box style={styles.switchTouchable}>
                    <SwitchButton checked={tuesdayChecked} />
                    <Text marginLeft="m" variant="DetailsR">
                      {tuesdayChecked ? 'Open' : 'Closed'}
                    </Text>
                  </Box>
                  {tuesdayChecked ? (
                    <Box
                      flexDirection="row"
                      alignItems="center"
                      marginTop="m"
                      marginBottom="s"
                    >
                      <TimePicker
                        time={tuesdayStartTime}
                        setTime={setTuesdayStartTime}
                      />
                      <Text marginHorizontal="m">TO</Text>
                      <TimePicker
                        time={tuesdayEndTime}
                        setTime={setTuesdayEndTime}
                      />
                    </Box>
                  ) : null}
                </Pressable>
              </Box>

              <Box style={styles.border} />
              <Box
                flexDirection="row"
                justifyContent="space-between"
                marginBottom="s"
                marginTop="xl"
              >
                <Text variant="DetailsR">Wednesday</Text>
                <Pressable
                  onPress={() => setWednesdayChecked(!wednesdayChecked)}
                >
                  <Box style={styles.switchTouchable}>
                    <SwitchButton checked={wednesdayChecked} />
                    <Text marginLeft="m" variant="DetailsR">
                      {wednesdayChecked ? 'Open' : 'Closed'}
                    </Text>
                  </Box>
                  {wednesdayChecked ? (
                    <Box
                      flexDirection="row"
                      alignItems="center"
                      marginTop="m"
                      marginBottom="s"
                    >
                      <TimePicker
                        time={wednesdayStartTime}
                        setTime={setWednesdayStartTime}
                      />
                      <Text marginHorizontal="m">TO</Text>
                      <TimePicker
                        time={wednesdayEndTime}
                        setTime={setWednesdayEndTime}
                      />
                    </Box>
                  ) : null}
                </Pressable>
              </Box>
              <Box style={styles.border} />
              <Box
                flexDirection="row"
                justifyContent="space-between"
                marginBottom="s"
                marginTop="xl"
              >
                <Text variant="DetailsR">Thursday</Text>
                <Pressable onPress={() => setThursdayChecked(!thursdayChecked)}>
                  <Box style={styles.switchTouchable}>
                    <SwitchButton checked={thursdayChecked} />
                    <Text marginLeft="m" variant="DetailsR">
                      {thursdayChecked ? 'Open' : 'Closed'}
                    </Text>
                  </Box>
                  {thursdayChecked ? (
                    <Box
                      flexDirection="row"
                      alignItems="center"
                      marginTop="m"
                      marginBottom="s"
                    >
                      <TimePicker
                        time={thursdayStartTime}
                        setTime={setThursdayStartTime}
                      />
                      <Text marginHorizontal="m">TO</Text>
                      <TimePicker
                        time={thursdayEndTime}
                        setTime={setThursdayEndTime}
                      />
                    </Box>
                  ) : null}
                </Pressable>
              </Box>
              <Box style={styles.border} />
              <Box
                flexDirection="row"
                justifyContent="space-between"
                marginBottom="s"
                marginTop="xl"
              >
                <Text variant="DetailsR">Friday</Text>
                <Pressable onPress={() => setFridayChecked(!fridayChecked)}>
                  <Box style={styles.switchTouchable}>
                    <SwitchButton checked={fridayChecked} />
                    <Text marginLeft="m" variant="DetailsR">
                      {fridayChecked ? 'Open' : 'Closed'}
                    </Text>
                  </Box>
                  {fridayChecked ? (
                    <Box
                      flexDirection="row"
                      alignItems="center"
                      marginTop="m"
                      marginBottom="s"
                    >
                      <TimePicker
                        time={fridayStartTime}
                        setTime={setFridayStartTime}
                      />
                      <Text marginHorizontal="m">TO</Text>
                      <TimePicker
                        time={fridayEndTime}
                        setTime={setFridayEndTime}
                      />
                    </Box>
                  ) : null}
                </Pressable>
              </Box>
              <Box style={styles.border} />
              <Box
                flexDirection="row"
                justifyContent="space-between"
                marginBottom="s"
                marginTop="xl"
              >
                <Text variant="DetailsR">Saturday</Text>
                <Pressable onPress={() => setSaturdayChecked(!saturdayChecked)}>
                  <Box style={styles.switchTouchable}>
                    <SwitchButton checked={saturdayChecked} />
                    <Text marginLeft="m" variant="DetailsR">
                      {saturdayChecked ? 'Open' : 'Closed'}
                    </Text>
                  </Box>
                  {saturdayChecked ? (
                    <Box
                      flexDirection="row"
                      alignItems="center"
                      marginTop="m"
                      marginBottom="s"
                    >
                      <TimePicker
                        time={saturdayStartTime}
                        setTime={setSaturdayStartTime}
                      />
                      <Text marginHorizontal="m">TO</Text>
                      <TimePicker
                        time={saturdayEndTime}
                        setTime={setSaturdayEndTime}
                      />
                    </Box>
                  ) : null}
                </Pressable>
              </Box>
              <Box style={styles.border} />
              <Box
                flexDirection="row"
                justifyContent="space-between"
                marginBottom="s"
                marginTop="xl"
              >
                <Text variant="DetailsR">Sunday</Text>
                <Pressable onPress={() => setSundayChecked(!sundayChecked)}>
                  <Box style={styles.switchTouchable}>
                    <SwitchButton checked={sundayChecked} />
                    <Text marginLeft="m" variant="DetailsR">
                      {sundayChecked ? 'Open' : 'Closed'}
                    </Text>
                  </Box>
                  {sundayChecked ? (
                    <Box
                      flexDirection="row"
                      alignItems="center"
                      marginTop="m"
                      marginBottom="s"
                    >
                      <TimePicker
                        time={sundayStartTime}
                        setTime={setSundayStartTime}
                      />
                      <Text marginHorizontal="m">TO</Text>
                      <TimePicker
                        time={sundayEndTime}
                        setTime={setSundayEndTime}
                      />
                    </Box>
                  ) : null}
                </Pressable>
              </Box>
              <Box style={styles.border} />
            </Box>
            <Box marginTop="xxl">
              <Box flexDirection="row" alignItems="center">
                <Text variant="Body2B" marginRight="s">
                  Closed dates
                </Text>
                <TouchableOpacity>
                  <AntDesign
                    name={'exclamationcircle'}
                    size={13}
                    color={theme.colors.text7}
                  />
                </TouchableOpacity>
              </Box>
              <Box marginTop="l" backgroundColor="primary1">
                <Box
                  flexDirection="row"
                  justifyContent="space-between"
                  backgroundColor="error"
                >
                  <Box flexDirection="row">
                    <Text>{startdate.toDateString()} - </Text>
                    <Text>{enddate.toDateString()}</Text>
                  </Box>
                  <Box flexDirection="row">
                    <TouchableOpacity onPress={handleEditClosedDate}>
                      <AntDesign
                        name={'edit'}
                        size={18}
                        color={theme.colors.primary1}
                        style={{ marginRight: 30 }}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity>
                      <AntDesign
                        name={'delete'}
                        size={18}
                        color={theme.colors.primary1}
                      />
                    </TouchableOpacity>
                  </Box>
                </Box>
                <Text
                  // marginLeft="s"
                  color="text2"
                  variant="DetailsR"
                  marginTop="m"
                >
                  Closed for {description}
                </Text>
              </Box>
              <TouchableOpacity
                onPress={onOpenModal}
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 20,
                }}
              >
                <Icon
                  name={'plus-circle'}
                  size={18}
                  color={theme.colors.primary1}
                />
                <Text marginLeft="m" color="primary1" variant="DetailsM">
                  Add Closed Date
                </Text>
              </TouchableOpacity>
            </Box>

            <AppBottomSheet
              hasBottomButton
              bottomSheetModalRef={AppBotomModal}
              onProceed={() => {}}
              handleClose={onCloseModal}
              children={
                <Box marginTop="xxl">
                  <Text
                    marginTop="l"
                    marginBottom="m"
                    variant="Body2B"
                    marginHorizontal="l"
                  >
                    {editClosedDate ? 'Edit Closed Date' : 'Add Closed Date'}
                  </Text>
                  <Box
                    flexDirection="row"
                    backgroundColor="bg2"
                    paddingVertical="m"
                    paddingHorizontal="m"
                    borderRadius="m"
                    marginHorizontal="l"
                  >
                    <AntDesign
                      name={'exclamationcircle'}
                      size={15}
                      color={theme.colors.text7}
                    />
                    <Text marginLeft="m">
                      No orders will be allowed on selected dates
                    </Text>
                  </Box>
                  <Box paddingHorizontal="m" alignSelf="center" paddingTop="l">
                    <DatePicker
                      label={'Start date'}
                      placeholder="Select start date"
                      date={startdate}
                      setDate={setStartDate}
                    />
                  </Box>
                  <Box paddingHorizontal="m" alignSelf="center" paddingTop="l">
                    <DatePicker
                      label={'End date'}
                      placeholder="Select start date"
                      date={enddate}
                      setDate={setEndDate}
                    />
                  </Box>
                  <Box
                    paddingHorizontal="m"
                    alignSelf="center"
                    paddingTop="l"
                    marginTop="m"
                  >
                    <Text marginBottom="l">Description (optional)</Text>
                    <TextInput
                      value={description}
                      type="input"
                      placeholder={'e.g Public Holiday'}
                    />
                  </Box>
                </Box>
              }
              label={editClosedDate ? 'Save Changes' : 'Save'}
            />
          </Box>
        </ScrollView>
      </Box>
    </Box>
  );
};

export default WorkingHours;

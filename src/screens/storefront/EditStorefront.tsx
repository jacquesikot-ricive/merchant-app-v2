import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import * as ImagePicker from 'expo-image-picker';
import Toast from 'react-native-toast-message';
import { Feather as Icon } from '@expo/vector-icons';
import { MaterialCommunityIcons as Icon2 } from '@expo/vector-icons';
import ImageResizer from 'react-native-image-resizer';
import { useAnalytics } from '@segment/analytics-react-native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

import theme, { Box, Text } from '../../components/Themed';
import { StoreFrontNavParamList } from '../../navigation/StoreFrontNav';
import storefrontApi from '../../api/riciveApi/storefront';
import TextInput from '../../components/TextInput';
import Button from '../../components/Button';
import StackHeader from '../../components/StackHeader';
import ImageIcon from '../../../svgs/ImageIcon';
import ColorPicker from '../../components/ColorPicker';
import SocialmediaModal from '../../components/SocialMediaModal';
import uploadApi from '../../api/riciveApi/upload';
import { useAppSelector } from '../../redux/hooks';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: theme.colors.white,
    flex: 1,
  },
  url: {
    marginTop: 19,
  },
  urlText: {
    marginBottom: 10,
  },
  colorBrand: {
    marginTop: 24,
    marginBottom: 19,
  },
  colorContainer: {
    flexDirection: 'row',
    marginBottom: 10,
    width: '40%',
  },
  image: {
    alignItems: 'center',
    marginTop: 16,
    borderStyle: 'dotted',
    borderRadius: 8,
    borderWidth: 2,
    paddingVertical: 20,
    borderColor: theme.colors.border1,
  },
  firstColor: {
    width: 100,
    height: 50,
    borderWidth: 1,
    borderColor: theme.colors.border,
    borderRadius: 25,
  },
  button: {
    marginTop: 10,
  },
});

type Props = NativeStackScreenProps<StoreFrontNavParamList, 'EditStorefront'>;

const EditStorefront = ({ navigation, route }: Props) => {
  const { track } = useAnalytics();
  const {
    id,
    domain: editDomain,
    tagline: editTagline,
    isEdit,
    logo: editLogo,
    facebook_url,
    twitter_url,
    instagram_url,
    color: editColor,
  } = route.params;

  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [domain, setDomain] = useState<string>('');
  const [logo, setLogo] = useState<string>(editLogo ? editLogo : '');
  const [tagline, setTagline] = useState<string>('');
  const [color, setColor] = useState<string | null>(
    editColor ? editColor : theme.colors.primary1
  );
  const [facebookUrl, setFacebookUrl] = useState<string>(
    facebook_url ? facebook_url : ''
  );
  const [twitterUrl, setTwitterUrl] = useState<string>(
    twitter_url ? twitter_url : ''
  );
  const [instagramUrl, setInstagramUrl] = useState<string>(
    instagram_url ? instagram_url : ''
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [showColorPicker, setShowColorPicker] = useState<boolean>(false);
  const [showSocialModal, setShowSocialModal] = useState<boolean>(false);
  const [isEditSocialMedia, setIsEditSocialMedia] = useState<boolean>(false);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      quality: 0.2,
      base64: true,
    });

    if (!result.cancelled) {
      const res = await ImageResizer.createResizedImage(
        result.uri,
        200,
        100,
        'JPEG',
        50
      );
      setLogo(res.uri);
    }
  };

  const handleSetupStorefront = async () => {
    try {
      setLoading(true);

      if (
        domain.trim().toLowerCase() === 'admin' ||
        domain.trim().toLowerCase() === 'web' ||
        domain.trim().toLowerCase() === 'domain' ||
        domain.trim().toLowerCase() === 'user' ||
        domain.trim().toLowerCase() === 'app'
      ) {
        return Toast.show({
          type: 'error',
          text1: `Invalid Domain`,
          text2: `${domain} is not allowed!, try another..`,
        });
      }

      if (domain.trim().toLowerCase().includes('.')) {
        return Toast.show({
          type: 'error',
          text1: `Invalid Domain`,
          text2: `${domain} is not allowed!, try another..`,
        });
      }

      const fullDomain = `${domain}.ricive.shop`;

      if (!fullDomain.match(`^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$`)) {
        return Toast.show({
          type: 'error',
          text1: `Invalid Domain`,
          text2: `${domain} is not allowed!, try another..`,
        });
      }

      const logoUrl =
        logo && logo.length > 0
          ? await uploadApi.upload({
              file: logo,
            })
          : undefined;

      await storefrontApi.createStorefront({
        domain,
        tagline,
        logo:
          logoUrl && logoUrl.data && logoUrl.data.url
            ? logoUrl.data.url
            : undefined,
        color: color as string,
        facebook_url:
          facebookUrl.length > 0
            ? `https://web.facebook.com/${facebookUrl}/`
            : undefined,
        twitter_url:
          twitterUrl.length > 0
            ? `https://twitter.com/${twitterUrl}`
            : undefined,
        instagram_url:
          instagramUrl.length > 0
            ? `https://www.instagram.com/${instagramUrl}/`
            : undefined,
      });
      setLoading(false);
      track('Create Website', {
        business_id,
      });
      navigation.navigate('StoreFront');
      Toast.show({
        type: 'success',
        text1: `Website ${isEdit ? 'Update' : 'Set up'} Done`,
        text2: `Your website has been setup at www.${domain}.ricive.shop`,
      });
    } catch (error: any) {
      setLoading(false);
      console.log(
        'Error posting to storefront:',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We cannot setup website now, please try again later..',
      });
    }
  };

  const handleEditStorefront = async () => {
    try {
      setLoading(true);

      const editedDomain = domain.length > 0 ? domain : (editDomain as string);

      if (
        editedDomain.trim().toLowerCase() === 'admin' ||
        editedDomain.trim().toLowerCase() === 'web' ||
        editedDomain.trim().toLowerCase() === 'domain' ||
        editedDomain.trim().toLowerCase() === 'user' ||
        editedDomain.trim().toLowerCase() === 'app'
      ) {
        return Toast.show({
          type: 'error',
          text1: `Invalid Domain`,
          text2: `${editedDomain} is not allowed!, try another..`,
        });
      }

      if (editedDomain.trim().toLowerCase().includes('.')) {
        return Toast.show({
          type: 'error',
          text1: `Invalid Domain`,
          text2: `${editedDomain} is not allowed!, try another..`,
        });
      }

      const fullDomain = `${editedDomain}.ricive.shop`;

      if (!fullDomain.match(`^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$`)) {
        return Toast.show({
          type: 'error',
          text1: `Invalid Domain`,
          text2: `${editedDomain} is not allowed!, try another..`,
        });
      }

      const logoUrl =
        logo.length > 0 && !logo.includes('https')
          ? await uploadApi.upload({
              file: logo,
            })
          : undefined;

      console.log('DATA:', {
        id: id as string,
        domain: editedDomain,
        tagline: tagline.length > 0 ? tagline : (editTagline as string),
        logo:
          logoUrl && logoUrl.data && logoUrl.data.url ? logoUrl.data.url : null,
        color: color && color.length > 0 ? color : null,
        facebook_url:
          facebookUrl.length > 0
            ? `https://web.facebook.com/${facebookUrl}/`
            : null,
        twitter_url:
          twitterUrl.length > 0 ? `https://twitter.com/${twitterUrl}` : null,
        instagram_url:
          instagramUrl.length > 0
            ? `https://www.instagram.com/${instagramUrl}/`
            : null,
      });

      await storefrontApi.updateStorefront({
        id: id as string,
        domain: editedDomain,
        tagline: tagline.length > 0 ? tagline : (editTagline as string),
        logo:
          logoUrl && logoUrl.data && logoUrl.data.url ? logoUrl.data.url : null,
        color: color && color.length > 0 ? color : null,
        facebook_url:
          facebookUrl.length > 0
            ? `https://web.facebook.com/${facebookUrl}/`
            : null,
        twitter_url:
          twitterUrl.length > 0 ? `https://twitter.com/${twitterUrl}` : null,
        instagram_url:
          instagramUrl.length > 0
            ? `https://www.instagram.com/${instagramUrl}/`
            : null,
      });
      track('Edit Website', {
        business_id,
      });
      setLoading(false);
      navigation.navigate('StoreFront');
      Toast.show({
        type: 'success',
        text1: `Website ${isEdit ? 'Update' : 'Set up'} Done`,
        text2: `Your website has been setup at www.${domain}.ricive.shop`,
      });
    } catch (error: any) {
      setLoading(false);
      console.log(
        'Error posting to storefront:',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We cannot setup website now, please try again later..',
      });
    }
  };

  return (
    <Box style={styles.container}>
      <StackHeader
        onBackPress={() => navigation.goBack()}
        title={`${isEdit ? 'Update Storefront' : 'Create Storefront'}`}
      />
      <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
        <Box style={styles.url}>
          <Box style={styles.urlText}>
            <Text variant="DetailsR" color="text6">
              Store domain URL
            </Text>
          </Box>
          <Box
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <Box
              style={{
                height: 56,
                width: wp(15),
                borderTopLeftRadius: 6,
                borderBottomLeftRadius: 6,
                justifyContent: 'center',
                alignItems: 'flex-end',
                backgroundColor: theme.colors.bg5,
                borderLeftWidth: 1,
                borderTopWidth: 1,
                borderBottomWidth: 1,
                borderColor: theme.colors.border1,
              }}
            >
              <Text variant={'DetailsR'} color="text5" mr="s">
                www.
              </Text>
            </Box>
            <TextInput
              placeholder={isEdit ? (editDomain as string) : 'domain'}
              type="input"
              width={wp(45)}
              noBorderRadius
              autoCapitalize="none"
              autoCompleteType="off"
              keyboardType="url"
              autoCorrect={false}
              onChangeText={(e) => setDomain(e)}
            />
            <Box
              style={{
                height: 56,
                width: wp(31),
                borderTopRightRadius: 6,
                borderBottomRightRadius: 6,
                justifyContent: 'center',
                backgroundColor: theme.colors.bg5,
                borderRightWidth: 1,
                borderTopWidth: 1,
                borderBottomWidth: 1,
                borderColor: theme.colors.border1,
              }}
            >
              <Text variant={'DetailsR'} color="text5" ml="s">
                .ricive.shop
              </Text>
            </Box>
          </Box>
        </Box>

        <Text
          variant="DetailsR"
          color="text6"
          marginVertical={'l'}
          style={{ width: '100%' }}
        >
          Tagline
        </Text>
        <TextInput
          placeholder={
            isEdit ? (editTagline as string) : 'Tagline for your website'
          }
          type="input"
          width={wp(90)}
          autoCapitalize="words"
          autoCompleteType="off"
          keyboardType="default"
          onChangeText={(e) => setTagline(e)}
        />

        <Box style={{ width: wp(90) }}>
          {/* <Box style={styles.colorBrand}>
            <Box>
              <Text variant="DetailsR" color="text6" mb="m">
                {isEdit ? 'Update brand color' : 'Set brand color'}
              </Text>
            </Box>
            <TouchableOpacity
              onPress={() => setShowColorPicker(true)}
              style={styles.colorContainer}
            >
              {color && color.length > 0 ? (
                <Box
                  style={[
                    styles.firstColor,
                    { backgroundColor: color as string },
                  ]}
                />
              ) : (
                <Box
                  style={[
                    styles.firstColor,
                    {
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                  ]}
                >
                  <Icon2
                    name="format-color-marker-cancel"
                    size={34}
                    color={theme.colors.border}
                  />
                </Box>
              )}
            </TouchableOpacity>
            <Text variant="SmallerTextR" color="text9">
              The brand colors are a reflection of your business and this will
              be the colors used on your website.
            </Text>
          </Box> */}

          {facebookUrl.length < 1 &&
          twitterUrl.length < 1 &&
          instagramUrl.length < 1 ? (
            <Box
              flexDirection={'row'}
              justifyContent={'space-between'}
              alignItems={'center'}
              marginVertical="l"
            >
              <Text variant={'DetailsR'}>Social Media</Text>

              <TouchableOpacity
                onPress={() => setShowSocialModal(true)}
                style={{
                  width: 95,
                  height: 36,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: theme.colors.light3,
                  borderRadius: 6,
                }}
              >
                <Text variant={'DetailsR'} color={'text9'}>
                  Add
                </Text>
              </TouchableOpacity>
            </Box>
          ) : (
            <Box marginBottom={'xxl'}>
              <Box
                flexDirection={'row'}
                justifyContent={'space-between'}
                alignItems={'center'}
                marginVertical="l"
              >
                <Text variant={'DetailsR'}>Social Media</Text>

                <TouchableOpacity
                  onPress={() => {
                    setIsEditSocialMedia(true);
                    setShowSocialModal(true);
                  }}
                  style={{
                    width: 95,
                    height: 36,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: theme.colors.light3,
                    borderRadius: 6,
                  }}
                >
                  <Text variant={'DetailsR'} color={'text9'}>
                    Edit
                  </Text>
                </TouchableOpacity>
              </Box>

              {instagramUrl.length > 0 && (
                <Box
                  flexDirection={'row'}
                  justifyContent={'space-between'}
                  mb="s"
                >
                  <Text variant={'DetailsR'} color="text5">
                    Instagram:
                  </Text>
                  <Text
                    variant={'DetailsR'}
                    color="text5"
                    numberOfLines={1}
                    style={{
                      width: '60%',
                      textAlign: 'center',
                    }}
                  >
                    {instagramUrl.length > 0 ? `${instagramUrl}` : ''}
                  </Text>

                  <TouchableOpacity
                    onPress={() => setInstagramUrl('')}
                    activeOpacity={0.4}
                  >
                    <Text variant={'DetailsR'} color="error">
                      Delete
                    </Text>
                  </TouchableOpacity>
                </Box>
              )}

              {facebookUrl.length > 0 && (
                <Box
                  flexDirection={'row'}
                  justifyContent={'space-between'}
                  mb="s"
                >
                  <Text variant={'DetailsR'} color="text5">
                    Facebook:
                  </Text>
                  <Text
                    variant={'DetailsR'}
                    color="text5"
                    numberOfLines={1}
                    style={{
                      width: '60%',
                      textAlign: 'center',
                    }}
                  >
                    {facebookUrl.length > 0 ? `${facebookUrl}` : ''}
                  </Text>

                  <TouchableOpacity
                    onPress={() => setFacebookUrl('')}
                    activeOpacity={0.4}
                  >
                    <Text variant={'DetailsR'} color="error">
                      Delete
                    </Text>
                  </TouchableOpacity>
                </Box>
              )}

              {twitterUrl.length > 0 && (
                <Box
                  flexDirection={'row'}
                  justifyContent={'space-between'}
                  mb="s"
                >
                  <Text variant={'DetailsR'} color="text5">
                    Twitter:
                  </Text>
                  <Text
                    variant={'DetailsR'}
                    color="text5"
                    numberOfLines={1}
                    style={{
                      width: '60%',
                      textAlign: 'center',
                    }}
                  >
                    {twitterUrl.length > 0 ? `${twitterUrl}` : ''}
                  </Text>

                  <TouchableOpacity
                    onPress={() => setTwitterUrl('')}
                    activeOpacity={0.4}
                  >
                    <Text variant={'DetailsR'} color="error">
                      Delete
                    </Text>
                  </TouchableOpacity>
                </Box>
              )}
            </Box>
          )}

          <Box mb="xl">
            <Box>
              <Text variant="DetailsR" color="text6">
                Add logo
              </Text>
            </Box>
            <TouchableOpacity
              style={styles.image}
              onPress={logo.length > 0 ? () => setLogo('') : pickImage}
            >
              {logo.length > 0 ? (
                <>
                  <Icon name="trash" color={theme.colors.border} size={24} />

                  <Box
                    style={{
                      position: 'absolute',
                      left: 20,
                      top: 15,
                    }}
                  >
                    <Image
                      source={{ uri: logo }}
                      style={{
                        width: 90,
                        height: 60,
                      }}
                    />
                  </Box>

                  <Text variant="DetailsM" color="text2">
                    Remove logo
                  </Text>
                </>
              ) : (
                <>
                  <Box>
                    <ImageIcon />
                  </Box>
                  <Box>
                    <Text variant="DetailsM" color="text2">
                      Upload logo
                    </Text>
                  </Box>
                </>
              )}
            </TouchableOpacity>
          </Box>
        </Box>

        <Box style={styles.button}>
          <Button
            type="primary"
            label={isEdit ? 'Update Website' : 'Create Website'}
            onPress={isEdit ? handleEditStorefront : handleSetupStorefront}
            width={wp(90)}
            loading={loading}
            disabled={
              !isEdit
                ? domain.length > 0 && tagline.length > 0
                  ? false
                  : true
                : false
            }
          />
        </Box>

        <Box style={{ height: 100 }} />

        <ColorPicker
          visible={showColorPicker}
          setVisible={setShowColorPicker}
          setColor={setColor}
        />

        <SocialmediaModal
          isEdit={isEdit || isEditSocialMedia}
          visible={showSocialModal}
          setVisible={setShowSocialModal}
          setFacebook={setFacebookUrl}
          setTwitter={setTwitterUrl}
          setInstagram={setInstagramUrl}
          twitter={twitterUrl}
          facebook={facebookUrl}
          instagram={instagramUrl}
          isUpdate={false}
        />
      </ScrollView>
    </Box>
  );
};

export default EditStorefront;

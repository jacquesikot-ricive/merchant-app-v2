import React, { useEffect, useState } from 'react';
import {
  Alert,
  StyleSheet,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import theme, { Box, Text } from '../../components/Themed';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { DrawerActions } from '@react-navigation/native';
import Toast from 'react-native-toast-message';
import * as Browser from 'expo-web-browser';
import Clipboard from '@react-native-clipboard/clipboard';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

import StackHeader from '../../components/StackHeader';
import Button from '../../components/Button';
import { useAppSelector } from '../../redux/hooks';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import storefrontApi from '../../api/riciveApi/storefront';
import { StoreFrontNavParamList } from '../../navigation/StoreFrontNav';
import metricsApi from '../../api/riciveApi/metrics';
import CopyText from '../../svg/CopyText';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  icon: {
    marginTop: 48,
    paddingRight: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: wp(85),
    marginBottom: 15,
  },
  content: {
    flex: 1,
    textAlign: 'center',
    width: wp(90),
  },
  description: {
    marginTop: 21,
    alignItems: 'center',
    textAlign: 'center',
    width: '90%',
  },
  button: {
    marginTop: 64,
  },
  url: {
    marginTop: 19,
  },
  urlText: {
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  domainContainer: {
    width: wp(90),
    borderRadius: 6,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: theme.colors.border1,
    paddingHorizontal: 15,
    height: 56,
  },
  firstColor: {
    width: 100,
    height: 50,
    borderWidth: 1,
    borderColor: theme.colors.border,
    borderRadius: 25,
  },
  colorBrand: {
    marginTop: 24,
    marginBottom: 19,
  },
});

type Props = NativeStackScreenProps<StoreFrontNavParamList, 'StoreFront'>;

const Services = ({ navigation }: Props) => {
  const [addStoreModal, setAddStoreModal] = useState<boolean>(false);
  const [storeFront, setStoreFront] = useState<any>();
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [domain, setDomain] = useState<string>('');
  const [color, setColor] = useState<string>('');
  const [logo, setLogo] = useState<string>('');
  const [tagline, setTagline] = useState<string>('');
  const [updateLoading, setUpdateLoading] = useState<boolean>(false);
  const [metrics, setMetrics] = useState<any>();
  const [socialMedia, setSocialMedia] = useState<any>({
    instagramUrl: '',
    facebookUrl: '',
    twitterUrl: '',
  });

  const getStore = async () => {
    try {
      setError(false);
      setLoading(true);
      const res = await storefrontApi.getStorefront(business_id);
      console.log(res.data);
      const getHomeData = await metricsApi.getData(business_id);
      setMetrics(getHomeData.data);
      setStoreFront(res.data);
      setDomain(res && res.data.domain);
      setTagline(res && res.data.tagline);
      setColor(res && res.data.color);
      setLogo(res && res.data.logo);
      setSocialMedia({
        instagramUrl: res && res.data.instagram_url,
        twitterUrl: res && res.data.twitter_url,
        facebookUrl: res && res.data.facebook_url,
      });
      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      console.log('Error getting storefront', JSON.stringify(error));
      setError(true);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          'Sorry, we cannot get your store front now, please try again later',
      });
    }
  };

  const handleUpdateStorefront = async () => {
    navigation.navigate('EditStorefront', {
      isEdit: true,
      color,
      domain,
      facebook_url: socialMedia.facebookUrl,
      twitter_url: socialMedia.twitterUrl,
      instagram_url: socialMedia.instagramUrl,
      tagline,
      id: storeFront.id,
      logo,
    });
  };

  const handleCopyWebsiteLink = () => {
    Clipboard.setString(`https://${domain}.ricive.shop`);
    Toast.show({
      type: 'success',
      text1: 'Copied!',
      text2: 'URL copied successfully!',
    });
  };

  useEffect(() => {
    getStore();

    navigation.addListener('focus', async () => await getStore());

    return () => {
      navigation.removeListener('focus', async () => await getStore());
    };
  }, [updateLoading]);

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={() => getStore()}
      />
    );
  }

  if (loading) {
    return <ScreenLoading />;
  }

  return (
    <Box style={styles.container}>
      <Box style={styles.header}>
        <StackHeader
          onBackPress={() => navigation.goBack()}
          title="Storefront"
        />
      </Box>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.content}
        contentContainerStyle={{ alignItems: 'center' }}
        bounces={false}
      >
        {storeFront && !storeFront.id && (
          <>
            <Box style={{ marginTop: hp(20) }}>
              <Text variant="Body2SB" color="text6">
                Welcome to Storefronts
              </Text>
            </Box>
            <Box style={styles.description}>
              <Text
                variant="DetailsR"
                color="text5"
                style={{ textAlign: 'center' }}
              >
                Increase your reach to more customers by hosting your products
                and services on your online website.
              </Text>
            </Box>
            <Box style={styles.button}>
              <Button
                type="primary"
                label="Create storefront"
                onPress={() => {
                  if (
                    metrics &&
                    metrics.hasCompleteBusiness &&
                    metrics.hasProducts
                  ) {
                    navigation.navigate('EditStorefront', {
                      isEdit: false,
                    });
                  } else {
                    Alert.alert(
                      'Complete Onboarding',
                      'Please complete your business onboarding to create orders',
                      [
                        {
                          text: 'Cancel',
                          style: 'destructive',
                        },
                        {
                          text: 'Continue',
                          onPress: () =>
                            navigation.navigate('GettingStartedStorefront'),
                          style: 'default',
                        },
                      ]
                    );
                  }
                }}
              />
            </Box>
          </>
        )}

        {storeFront && storeFront.id && (
          <>
            {/* Content */}
            <Text variant={'Body1SB'} color="text1" style={{ width: '100%' }}>
              Storefront
            </Text>

            <Box>
              <Box style={styles.url}>
                <Box style={styles.urlText}>
                  <Text variant="DetailsR" color="text6">
                    Store domain URL
                  </Text>

                  <TouchableOpacity onPress={handleCopyWebsiteLink}>
                    <Text variant="DetailsR" color="text6">
                      Copy link
                    </Text>
                  </TouchableOpacity>
                </Box>
                <Box style={styles.domainContainer}>
                  <Text
                    style={{
                      fontFamily: 'basier-regular',
                      fontSize: 14,
                      color: theme.colors.text5,
                    }}
                  >{`https://${domain}.ricive.shop`}</Text>

                  <TouchableOpacity onPress={handleCopyWebsiteLink}>
                    <CopyText />
                  </TouchableOpacity>
                </Box>
              </Box>
              <Text variant="DetailsR" color="text6" marginTop="xl">
                Tagline
              </Text>
              <Text variant={'DetailsR'} color="text5">
                {tagline}
              </Text>

              {/* <Box style={styles.colorBrand}>
                <Text variant="DetailsR" color="text6" mb="m">
                  Your brand color
                </Text>

                {color && color.length > 0 ? (
                  <Box
                    style={[styles.firstColor, { backgroundColor: color }]}
                  />
                ) : (
                  <Box
                    style={[
                      styles.firstColor,
                      {
                        justifyContent: 'center',
                        alignItems: 'center',
                      },
                    ]}
                  >
                    <Icon
                      name="format-color-marker-cancel"
                      size={34}
                      color={theme.colors.border}
                    />
                  </Box>
                )}
              </Box> */}

              {(socialMedia.facebookUrl &&
                socialMedia.facebookUrl.length > 0) ||
                (socialMedia.twitterUrl && socialMedia.twitterUrl.length > 0) ||
                (socialMedia.instagramUrl &&
                  socialMedia.instagramUrl.length > 0 && (
                    <Text variant={'DetailsR'} mb="m">
                      Social Media
                    </Text>
                  ))}

              {socialMedia.instagramUrl && socialMedia.instagramUrl.length > 0 && (
                <Box mt="xxl">
                  <Box
                    flexDirection={'row'}
                    justifyContent={'space-between'}
                    mb="s"
                  >
                    <Text variant={'DetailsR'} color="text5">
                      Instagram:
                    </Text>
                    <Text
                      variant={'DetailsR'}
                      color="text5"
                      numberOfLines={1}
                      style={{
                        width: '60%',
                        textAlign: 'center',
                      }}
                    >
                      {socialMedia.instagramUrl.substring(8)}
                    </Text>
                  </Box>
                </Box>
              )}

              {socialMedia.facebookUrl && socialMedia.facebookUrl.length > 0 && (
                <Box>
                  <Box
                    flexDirection={'row'}
                    justifyContent={'space-between'}
                    mb="s"
                  >
                    <Text variant={'DetailsR'} color="text5">
                      Facebook:
                    </Text>
                    <Text
                      variant={'DetailsR'}
                      color="text5"
                      numberOfLines={1}
                      style={{
                        width: '60%',
                        textAlign: 'center',
                      }}
                    >
                      {socialMedia.facebookUrl.substring(8)}
                    </Text>
                  </Box>
                </Box>
              )}

              {socialMedia.twitterUrl && socialMedia.twitterUrl.length > 0 && (
                <Box>
                  <Box
                    flexDirection={'row'}
                    justifyContent={'space-between'}
                    mb="s"
                  >
                    <Text variant={'DetailsR'} color="text5">
                      Twitter:
                    </Text>
                    <Text
                      variant={'DetailsR'}
                      color="text5"
                      numberOfLines={1}
                      style={{
                        width: '60%',
                        textAlign: 'center',
                      }}
                    >
                      {socialMedia.twitterUrl.substring(8)}
                    </Text>
                  </Box>
                </Box>
              )}

              <Box mt="xl">
                <Text variant="DetailsR" color="text6" mb="m">
                  Logo
                </Text>

                {logo && logo.length > 0 ? (
                  <Image
                    source={{ uri: logo }}
                    style={{
                      width: 100,
                      height: 80,
                      borderRadius: 6,
                    }}
                  />
                ) : (
                  <Box
                    style={{
                      width: 100,
                      height: 80,
                      borderRadius: 6,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 1,
                      borderColor: theme.colors.border,
                    }}
                  >
                    <Icon
                      name="image-off"
                      size={34}
                      color={theme.colors.border}
                    />
                  </Box>
                )}
              </Box>

              <Box style={styles.button}>
                <Button
                  type="transparent"
                  label="Open Website"
                  onPress={async () =>
                    await Browser.openBrowserAsync(
                      `https://${domain}.ricive.shop`
                    )
                  }
                  width={'100%'}
                />

                <Box style={{ height: 20 }} />

                <Button
                  type="primary"
                  label="Edit Website"
                  onPress={handleUpdateStorefront}
                  width={'100%'}
                  loading={updateLoading}
                />
              </Box>
            </Box>
          </>
        )}
        <Box style={{ height: 100 }} />
      </ScrollView>
    </Box>
  );
};

export default Services;

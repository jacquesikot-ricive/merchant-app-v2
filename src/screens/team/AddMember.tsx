import React, { useState, useRef, useEffect } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import NigerianPhone from 'validate_nigerian_phone';
import { Feather as Icon } from '@expo/vector-icons';

import theme, { Box, Text } from '../../components/Themed';
import TextInput from '../../components/TextInput';
import Button from '../../components/Button';
import Picker from '../../components/Picker';
import validateEmail from '../../utils/validateEmail';
import team from '../../api/merchant/team';
import { useAppSelector } from '../../redux/hooks';
import teamApi from '../../api/riciveApi/team';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { TeamsNavParamList } from '../../navigation/TeamsNav';
import StackHeader from '../../components/StackHeader';
import { useAnalytics } from '@segment/analytics-react-native';
import SetPermissionsModal from '../../components/SetPermissionsModal';
import capitalizeAllWords from '../../utils/capitalizeAllWords';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  textHeader: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  form: {
    alignItems: 'center',
    marginTop: 30,
  },
  input: {
    marginBottom: 24,
  },
  innerText: {
    marginBottom: 10,
    justifyContent: 'flex-start',
    width: wp(90),
  },
});

const roles = [
  {
    label: '',
    value: '',
  },
  {
    label: 'Admin',
    value: 'ADMIN',
  },
  {
    label: 'Team',
    value: 'TEAM',
  },
  {
    label: 'Delivery',
    value: 'DELIVERY',
  },
];

type Props = NativeStackScreenProps<TeamsNavParamList, 'AddMember'>;

const AddMember = ({ navigation, route }: Props) => {
  const { track } = useAnalytics();
  const {
    isEdit,
    first_name,
    last_name,
    username: editUsername,
    role,
    id,
    permissions: userPermissions,
  } = route.params;

  console.log('PERM:', userPermissions);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [selectedRole, setSelectedRole] = useState<string>('ADMIN');
  const [firstName, setFirstName] = useState<string>('');
  const [lastName, setLastName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [username, setUsername] = useState<string>('');
  const [openPermissions, setOpenPermissions] = useState<boolean>(false);
  const [permissions, setPermissions] = useState<any[]>(
    isEdit && userPermissions
      ? userPermissions.map((p: any, index: number) => {
          return {
            id: index,
            name: p,
            value: true,
          };
        })
      : []
  );

  console.log(permissions);

  const handleAddMember = async () => {
    try {
      setLoading(true);
      if (firstName.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'First name cannot be empty',
          visibilityTime: 3000,
        });
      }

      if (lastName.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Last name cannot be empty',
          visibilityTime: 3000,
        });
      }

      if (!validateEmail(email)) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Email not correct',
          visibilityTime: 3000,
        });
      }

      await teamApi.addTeamMember({
        first_name: firstName,
        last_name: lastName,
        role: selectedRole as 'ADMIN' | 'TEAM' | 'DELIVERY',
        email,
        business: business_id,
        permissions: permissions.reduce(
          (obj, item) => Object.assign(obj, { [item.name]: item.value }),
          {}
        ),
      });

      track('Add Team', {
        business_id,
      });

      setLoading(false);
      Toast.show({
        type: 'success',
        text1: 'New Team Mate Added',
        text2: `${firstName} has been added successfully`,
      });
      navigation.navigate('Teams');
    } catch (error: any) {
      console.log(
        'Error creating team member:',
        JSON.stringify(error.response.data)
      );
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          JSON.stringify(error.response.data.message) ||
          'We cannot process your request now, please try again later..',
      });
    }
  };

  const handleUpdateMember = async () => {
    try {
      setLoading(true);
      await teamApi.updateTeam({
        profile_id: id as string,
        first_name: firstName.length > 0 ? firstName : (first_name as string),
        last_name: lastName.length > 0 ? lastName : (last_name as string),
        role: selectedRole,
        permissions: permissions.reduce(
          (obj, item) => Object.assign(obj, { [item.name]: item.value }),
          {}
        ),
      });

      track('Update Team', {
        business_id,
      });

      setLoading(false);
      navigation.navigate('Teams');
      return Toast.show({
        type: 'success',
        text1: 'Update Success',
        text2: 'Team mate updated successfully',
      });
    } catch (error: any) {
      console.log(
        'Error updating team member:',
        JSON.stringify(error.response.data)
      );
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: JSON.stringify(error.response.data.message) || 'Server Error',
        text2: 'We cannot process your request now, please try again later..',
      });
    }
  };

  useEffect(() => {
    if (isEdit) {
      setSelectedRole(role as string);
    }
  }, []);
  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{ justifyContent: 'center' }}
    >
      <StackHeader
        title={isEdit ? 'Edit Team Member' : 'Add Team Member'}
        onBackPress={() => navigation.goBack()}
      />
      <Box style={styles.form}>
        <Box style={styles.input}>
          <TextInput
            type="input"
            placeholder={isEdit ? (first_name as string) : 'First Name'}
            autoCompleteType="name"
            autoCorrect={false}
            autoCapitalize="words"
            keyboardType="default"
            returnKeyType="next"
            onChangeText={(e) => setFirstName(e)}
          />
        </Box>
        <Box style={styles.input}>
          <TextInput
            type="input"
            placeholder={isEdit ? (last_name as string) : 'Last Name'}
            autoCompleteType="name"
            autoCorrect={false}
            autoCapitalize="words"
            keyboardType="default"
            returnKeyType="next"
            onChangeText={(e) => setLastName(e)}
          />
        </Box>
        {!isEdit && (
          <Box style={styles.input}>
            <TextInput
              type="input"
              placeholder="Email Address"
              autoCompleteType="email"
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              returnKeyType="next"
              onChangeText={(e) => setEmail(e)}
            />
          </Box>
        )}

        <Text
          variant={'DetailsM'}
          style={{
            width: wp(90),
            marginBottom: 5,
            color: theme.colors.text5,
          }}
        >
          {isEdit ? 'Update Permissions' : 'Add Permissions'}
        </Text>

        <TouchableOpacity
          onPress={() => setOpenPermissions(true)}
          activeOpacity={0.6}
          style={{
            width: wp(90),
            // height: 58,
            minHeight: 58,
            paddingVertical: 10,
            borderRadius: 8,
            borderWidth: 1,
            borderColor: theme.colors.border1,
            justifyContent: 'space-between',
            alignItems: 'center',
            paddingHorizontal: 20,
            flexDirection: 'row',
          }}
        >
          {/* <Text
            style={{
              fontFamily: 'basier-regular',
              fontSize: 14,
              lineHeight: 20,
              color: theme.colors.text5,
              paddingRight: 5,
              width: wp(75),
            }}
          >
            {isEdit
              ? `${permissions.map((p, index) => {
                  return `${index === 0 ? '' : ' '} ${capitalizeAllWords(
                    p.name.replaceAll('_', ' ')
                  )}`;
                })}`
              : 'Set Permissions'}
          </Text> */}

          <Box flexDirection={'row'} flexWrap={'wrap'}>
            {permissions && permissions.length > 0 ? (
              permissions.map((p, index) => {
                return (
                  <Box
                    key={index.toString()}
                    style={{
                      borderWidth: 1,
                      borderColor: theme.colors.primary1,
                      marginRight: 10,
                      borderRadius: 4,
                      paddingHorizontal: 5,
                      marginBottom: 10,
                    }}
                  >
                    <Text variant={'SmallerTextR'}>{p.name}</Text>
                  </Box>
                );
              })
            ) : (
              <Text variant={'DetailsR'} color="text5">
                No Permissions
              </Text>
            )}
          </Box>

          <Box style={{ position: 'absolute', right: 10 }}>
            <Icon name="chevron-down" size={20} color={theme.colors.border} />
          </Box>
        </TouchableOpacity>

        {/* <Picker
          placeholder=""
          label="Select role"
          data={roles}
          setValue={setSelectedRole}
          value={selectedRole}
        />
        <Box style={{ marginTop: 126 }}>
          <Button
            type="primary"
            label={isEdit ? `Update User` : 'Add User'}
            onPress={isEdit ? handleUpdateMember : handleAddMember}
            disabled={
              isEdit
                ? false
                : firstName.length > 0 &&
                  lastName.length > 0 &&
                  email.length > 0 &&
                  selectedRole.length > 0
                ? false
                : true
            }
            loading={loading}
          />
        </Box> */}
      </Box>

      <SetPermissionsModal
        permissions={[
          'create_order',
          'view_order',
          'update_order_status',
          'create_invoice',
          'view_analytics',
          'view_product',
          'create_product',
          'edit_product',
          'delete_product',
          'view_category',
          'create_category',
          'edit_category',
          'delete_category',
          'view_customer',
          'edit_customer',
          'create_customer',
          'delete_customer',
          'view_team',
          'create_team',
          'edit_team',
          'delete_team',
          'view_wallet',
          'withdraw',
          'storefront',
          'business_info',
          'view_invoice',
          'edit_invoice',
          'delete_invoice',
          'created_at',
          'updated_at',
        ].map((p: string, index: number) => {
          return {
            id: index,
            name: p,
          };
        })}
        setSelectedPermissions={setPermissions}
        selectedPermissions={permissions}
        visible={openPermissions}
        setVisible={setOpenPermissions}
      />

      <Box style={{ marginTop: hp(20), alignSelf: 'center' }}>
        <Button
          type="primary"
          label={isEdit ? `Update User` : 'Add User'}
          onPress={isEdit ? handleUpdateMember : handleAddMember}
          disabled={
            isEdit
              ? false
              : firstName.length > 0 &&
                lastName.length > 0 &&
                email.length > 0 &&
                selectedRole.length > 0
              ? false
              : true
          }
          loading={loading}
        />
      </Box>
    </ScrollView>
  );
};

export default AddMember;

import React, { useCallback, useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import theme, { Box, Text } from '../../components/Themed';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { DrawerActions } from '@react-navigation/native';
import { Ionicons as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import HeaderTextInput from '../../components/HeaderTextInput';
import TeamList from '../../components/TeamList.tsx';
import StackHeader from '../../components/StackHeader';
import { useAppSelector } from '../../redux/hooks';
import ListEmpty from '../../components/ListEmpty';
import teamApi from '../../api/riciveApi/team';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { TeamsNavParamList } from '../../navigation/TeamsNav';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import PlusCircleIcon from '../../../svgs/PlusCircleIcon';
import TeamDetailsModal from '../../components/TeamDetailsModal';
import EditTeam from '../../components/EditTeam';
import { useQuery } from 'react-query';
import queryKeys from '../../constants/queryKeys';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  layout: {
    width: wp(90),
    marginTop: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  rightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    paddingRight: 5,
  },
  border: {
    width: wp(100),
    alignItems: 'center',
    borderBottomColor: theme.colors.border1,
    borderBottomWidth: 0.6,
    marginTop: 10,
  },
});

type Props = NativeStackScreenProps<TeamsNavParamList, 'Teams'>;

const Teams = ({ navigation }: Props) => {
  const [teamMembers, setTeamMembers] = useState<any[]>([]);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [searchResult, setSearchResult] = useState<any[]>([]);
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openEditModal, setOpenEditModal] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [teamLoading, setTeamLoading] = useState<boolean>(false);
  const [activeTeam, setActiveTeam] = useState<any>();
  const [roleList, setRoleList] = useState<string>('');

  const { isLoading, isError, refetch, isFetching } = useQuery(
    queryKeys.TEAM_DATA,
    async () => {
      try {
        const data = await teamApi.getTeamsMembers(business_id);
        setTeamMembers(data.data);
        setSearchResult(data.data);
        setRoleList(data.data);
      } catch (error: any) {
        Toast.show({
          type: 'error',
          text1: 'Server Error',
          text2: 'Cannot get data, please try again later...',
        });
        console.log(
          'Error getting invoice:',
          JSON.stringify(error.response.data)
        );
      }
    },
    {
      enabled: !!business_id,
    }
  );

  // const getMembers = useCallback(async () => {
  //   setError(false);
  //   try {
  //     setLoading(true);
  //     const data = await teamApi.getTeamsMembers(business_id);
  //     setTeamMembers(data.data);
  //     setSearchResult(data.data);
  //     setRoleList(data.data);
  //     setLoading(false);
  //   } catch (error: any) {
  //     setError(true);
  //     setLoading(false);
  //     console.log(
  //       'Error fetching team members:',
  //       JSON.stringify(error.response.data)
  //     );
  //     Toast.show({
  //       type: 'error',
  //       text1: 'Server Error',
  //       text2: 'Cannot get data, please try again later...',
  //     });
  //   }
  // }, []);

  const handleSearch = (e: string) => {
    const result = teamMembers.filter(
      (t) =>
        t.first_name.trim().toLowerCase().includes(e.trim().toLowerCase()) ||
        t.last_name.trim().toLowerCase().includes(e.trim().toLowerCase()) ||
        t.role.trim().toLowerCase().includes(e.trim().toLowerCase())
    );

    setSearchResult([...result]);
  };

  const handleDelete = (auth_id: string) => {
    try {
      Alert.alert(
        'Delete Team',
        'Are you sure you want to delete this team mate?',
        [
          {
            style: 'destructive',
            text: 'yes',
            onPress: async () => {
              try {
                setLoading(true);
                await teamApi.deleteTeam(auth_id);

                refetch();
                setLoading(false);
                return Toast.show({
                  type: 'success',
                  text1: 'Success',
                  text2: 'Team mate deleted successfully!',
                });
              } catch (error) {
                setLoading(false);
                Toast.show({
                  type: 'error',
                  text1: 'Server Error',
                  text2: 'Cannot delete team now, please try again later...',
                });
                // console.log('Error deleting service:', error);
              }
            },
          },
          {
            style: 'cancel',
            text: 'No',
          },
        ]
      );
    } catch (error) {
      setLoading(false);
      setError(true);
      // console.log('Error deletingteam:', error);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot delete team, please try again later...',
      });
    }
  };

  if (isError || error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => await refetch()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {isLoading && <ScreenLoading />}
      <StackHeader onBackPress={() => navigation.goBack()} title="Teams" />
      <Box style={styles.border} />
      <Box style={styles.layout}>
        <Box
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            width: wp(90),
          }}
        >
          <HeaderTextInput
            type="search"
            width={userPermissions.create_team ? wp(75) : wp(90)}
            height={40}
            placeholder="Search"
            onChangeText={handleSearch}
          />
          {userPermissions.create_team && (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('AddMember', {
                  isEdit: false,
                })
              }
            >
              <PlusCircleIcon />
            </TouchableOpacity>
          )}
        </Box>
        <FlatList
          data={searchResult}
          bounces={false}
          ListEmptyComponent={() => {
            if (loading) {
              return <ActivityIndicator />;
            } else {
              return (
                <ListEmpty
                  topText=""
                  invoicePage
                  bottomText="You haven't added any Team members yet."
                  buttonText="Add Team member"
                  button
                  onPressButton={() =>
                    navigation.navigate('AddMember', {
                      isEdit: false,
                    })
                  }
                />
              );
            }
          }}
          keyExtractor={(item: any) => item.id.toString()}
          renderItem={({ item }) => (
            <Box>
              <TeamList
                colorNum={Number((Math.random() * 10).toFixed())}
                role={item.role}
                name={`${item.first_name} ${item.last_name}`}
                email={item.email}
                image={item.image}
                handleDelete={() => handleDelete(item.user_auth.id)}
                onTeamSelect={() => {
                  setActiveTeam(item);
                  setOpenModal(true);
                }}
                handleEdit={() => {
                  navigation.navigate('AddMember', {
                    isEdit: true,
                    first_name: item.first_name,
                    last_name: item.last_name,
                    username: item.username,
                    role: item.role,
                    id: item.id,
                    permissions: item.user_auth.user_permission,
                  });
                }}
              />
            </Box>
          )}
        />
      </Box>
      <TeamDetailsModal
        visible={openModal}
        setVisible={setOpenModal}
        loading={teamLoading}
        isEdit={false}
        data={activeTeam}
        EditPress={() => {
          setOpenModal(false);
          navigation.navigate('AddMember', {
            isEdit: true,
            first_name: activeTeam.first_name,
            last_name: activeTeam.last_name,
            username: activeTeam.username,
            role: activeTeam.role,
            id: activeTeam.id,
            permissions: activeTeam.user_auth.user_permission
              ? Object.entries(activeTeam.user_auth.user_permission[0])
                  .map((e) => {
                    if (e[1] === true) {
                      return e[0];
                    }
                  })
                  .filter((v) => v !== undefined)
              : undefined,
          });
        }}
        DeletePress={() => {
          setOpenModal(false);
          handleDelete(activeTeam.user_auth.id);
        }}
      />
      <EditTeam
        visible={openEditModal}
        setVisible={setOpenEditModal}
        loading={teamLoading}
        isEdit={true}
        data={activeTeam}
        EditPress={activeTeam}
        value={roleList}
        setValue={setRoleList}
        roleData={searchResult}
      />
    </Box>
  );
};

export default Teams;

import React, { useEffect, useState } from 'react';
import { Alert, FlatList, StyleSheet } from 'react-native';
import theme, { Box, Text } from '../../components/Themed';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { DrawerActions } from '@react-navigation/native';
import { Feather as Icon } from '@expo/vector-icons';
import Toast from 'react-native-toast-message';

import ProductsList from '../../components/ProductList';
import StackHeader from '../../components/StackHeader';
import { useAppSelector } from '../../redux/hooks';
import ListEmpty from '../../components/ListEmpty';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import productApi from '../../api/riciveApi/product';
import serviceApi from '../../api/riciveApi/service';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ProductsNavParamList } from '../../navigation/ProductsNav';
import HeaderTextInput from '../../components/HeaderTextInput';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.white,
  },
  layout: {
    width: wp(90),
    marginTop: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  rightHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    paddingRight: 5,
  },
});

type Props = NativeStackScreenProps<ProductsNavParamList, 'Products'>;

const Products = ({ navigation }: Props) => {
  const [products, setProducts] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const [error, setError] = useState<boolean>(false);
    const [searchResult, setSearchResult] = useState<any[]>();
    const [activeItem, setActiveItem] = useState<any>({
        name: 'Product',
        value: 'Product',
      });

  const getData = async () => {
    setError(false);
    try {
      setLoading(true);
      const productsData = await productApi.getProducts(business_id);
      setProducts(productsData.data);
      setSearchResult(productsData.data);
      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      setError(true);
      console.log(
        'Error fetching products or service:',
        JSON.stringify(error.response.data)
      );
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Cannot get data, please try again later...',
      });
    }
  };

  const handleDelete = async (id: string) => {
    Alert.alert(
      'Delete Product',
      'Are you sure you want to delete this product?',
      [
        {
          style: 'destructive',
          text: 'Yes',
          onPress: async () => {
            try {
              setLoading(true);
              await productApi.deleteProduct(id);
              await getData();
              setLoading(false);
              Toast.show({
                type: 'success',
                text1: 'Product Delete',
                text2: 'Product deleted successfully',
              });
            } catch (error: any) {
              setLoading(false);
              console.log(
                'Error fetching products or service:',
                JSON.stringify(error.response.data)
              );
              Toast.show({
                type: 'error',
                text1: 'Server Error',
                text2: 'Cannot delete product, please try again later...',
              });
            }
          },
        },
        {
          style: 'default',
          text: 'No',
        },
      ]
    );
  };

  useEffect(() => {
    getData();

    navigation.addListener('focus', async () => await getData());

    return () => {
      navigation.removeListener('focus', async () => await getData());
    };
  }, []);

  const handleSearch = (e: string) => {
    const result = products.filter(
      (c: any) =>
        (c.name && c.name.trim().includes(e.trim())) ||
        (c.price && c.price.trim().includes(e.trim()))
    );

    setSearchResult([...result]);
  };

  if (loading) {
    return <ScreenLoading />;
  }

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={async () => await getData()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {/* <StackHeader
        burger={() => navigation.dispatch(DrawerActions.openDrawer())}
        title="Products"
        icon1={
          <Box flexDirection={'row'} alignItems={'center'}>
            <Box style={styles.icon}>
              <Icon name="plus" color={theme.colors.primary1} size={14} />
            </Box>
            <Text variant="Body1M" color="primary1">
              Add
            </Text>
          </Box>
        }
        onPressIcon1={() => {
          return navigation.navigate('EditProduct', {
            isEdit: false,
          });
        }}
      /> */}
      <Box style={styles.layout}>
        <Box>
          <HeaderTextInput
            type="search"
            placeholder="Search products"
            onChangeText={handleSearch}
          />
        </Box>

        <Box style={{ height: hp(75), paddingTop: 30 }}>
          <FlatList
            contentContainerStyle={{ paddingBottom: 50 }}
            data={searchResult}
            showsVerticalScrollIndicator={false}
            ListEmptyComponent={() => (
              <ListEmpty
                topText="Get your business up and running by"
                bottomText="adding your products"
                buttonText="Add products"
                button
                onPressButton={() => {
                  return navigation.navigate('EditProduct', {
                    isEdit: false,
                  });
                }}
              />
            )}
            keyExtractor={(item: any) => item.id.toString()}
            renderItem={({ item }) => (
              <Box style={{ marginBottom: 3 }}>
                <ProductsList
                  isPublished={item.is_published}
                  price={item.price}
                  image={item.image}
                  productPrice={item.product_price}
                  name={item.name}
                  handleDelete={async () => await handleDelete(item.id)}
                  handleEdit={() =>
                    navigation.navigate('EditProduct', {
                      isEdit: true,
                      product: item,
                    })
                  }
                />
              </Box>
            )}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default Products;

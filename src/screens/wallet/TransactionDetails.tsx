import { StyleSheet, Text, View } from "react-native";
import React, { useEffect } from "react";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { WalletNavParamList } from "../../navigation/WalletNav";

type Props = NativeStackScreenProps<WalletNavParamList, "TransactionDetails">;

const TransactionDetails = ({ navigation, route }: Props): JSX.Element => {
  const { item } = route.params;
  useEffect(() => {
    console.log(item);
  }, []);
  return (
    <View>
      <Text>TransactionDetails</Text>
    </View>
  );
};

export default TransactionDetails;

const styles = StyleSheet.create({});

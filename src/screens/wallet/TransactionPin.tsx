import React, { useRef, useState } from "react";
import {
  NativeSyntheticEvent,
  ScrollView,
  StyleSheet,
  TextInputKeyPressEventData,
  TouchableOpacity,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

import theme, { Box, Text } from "../../components/Themed";
import Button from "../../components/Button";
import Toast from "react-native-toast-message";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { WalletNavParamList } from "../../navigation/WalletNav";
import OtpInput from "../../components/OtpInput";
import transactionApi from "../../api/riciveApi/transaction";
import StackHeader from "../../components/StackHeader";
import LoadingBackdrop from "../../components/Loadinbackdrop";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
  },
  layout: {
    justifyContent: "flex-start",
    width: wp(90),
  },
  title: {
    marginTop: 47,
  },
  description: {
    marginTop: 24,
  },
  inputContainer: {},
  input: {
    marginTop: 35,
    marginBottom: 10,
  },
  resend: {
    flexDirection: "row",
    marginTop: hp(2),
    alignItems: "center",
  },
  button: {
    marginTop: 40,
  },
});

type Props = NativeStackScreenProps<WalletNavParamList, "TransactionPin">;

const TransactionPin = ({ navigation, route }: Props): JSX.Element => {
  const { isCreate } = route.params;
  const [loading, setLoading] = useState<boolean>(false);
  const [otpArray, setOtpArray] = useState(["", "", "", ""]);

  const firstTextInputRef = useRef<TextInput>();
  const secondTextInputRef = useRef<TextInput>();
  const thirdTextInputRef = useRef<TextInput>();
  const fourthTextInputRef = useRef<TextInput>();

  const resetWaitTime = 120;

  const refArray = [
    firstTextInputRef,
    secondTextInputRef,
    thirdTextInputRef,
    fourthTextInputRef,
  ];

  const onChangeOtp = (value: string, index: number) => {
    const otpArrayCopy = otpArray.concat();
    otpArrayCopy[index] = value;
    setOtpArray(otpArrayCopy);

    if (index < refArray.length - 1 && value) {
      refArray[index + 1].current?.focus();
    }
    if (index === refArray.length - 1) {
      refArray[index].current?.blur();
    }
  };

  const onKeyPressOtp = (
    e: NativeSyntheticEvent<TextInputKeyPressEventData>,
    index: number
  ) => {
    if (e.nativeEvent.key === "Backspace" && otpArray[index] === "") {
      if (index === 1) {
        firstTextInputRef.current?.focus();
      } else if (index === 2) {
        secondTextInputRef.current?.focus();
      } else if (index === 3) {
        thirdTextInputRef.current?.focus();
      }

      if (index > 0) {
        // Check behavior on android
        const otpArrayCopy = otpArray.concat();
        otpArrayCopy[index - 1] = "";
        setOtpArray(otpArrayCopy);
      }
    }
  };

  const handleCreatePin = async () => {
    setLoading(true);

    function escapeRegExp(string: any) {
      return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
    }
    function replaceAll(str: any, match: any, replacement: any) {
      return str.replace(
        new RegExp(escapeRegExp(match), "g"),
        () => replacement
      );
    }

    try {
      const transactionPin = replaceAll(otpArray.join(), ",", "").toString();
      if (transactionPin.length < 1) {
        setLoading(false);
        return Toast.show({
          type: "error",
          text1: "Input Error",
          text2: "Enter transaction pin",
          visibilityTime: 3000,
        });
      }

      await transactionApi.updateTransactionPin(transactionPin);
      setLoading(false);
      Toast.show({
        type: "success",
        text1: "Pin Created",
        text2: "You transaction pin has been successfully created",
      });
      return navigation.navigate("TransactionSuccess");
    } catch (error: any) {
      console.log(JSON.stringify(error.response.data));
      setLoading(false);
      Toast.show({
        type: "error",
        text1: "Server Error",
        text2: "We cannot process your request now, please try again later..",
      });
    }
  };

  return (
    <Box style={styles.container}>
      <StackHeader onBackPress={() => navigation.goBack()} title="" />
      <ScrollView contentContainerStyle={{ alignItems: "center" }}>
        <Box style={styles.layout}>
          <Box style={styles.title}>
            <Text variant="Body1B" color="text6">
              Create your transaction pin
            </Text>
          </Box>
          <Box style={styles.description}>
            <Text variant="DetailsR" color="text5">
              Please create a 4-digit PIN that would be used to complete your
              transactions.
            </Text>
          </Box>
          <Box style={styles.inputContainer}>
            <Box style={styles.input}>
              <Text>Enter Transaction Pin</Text>
            </Box>
          </Box>
        </Box>
        <Box style={{ flexDirection: "row", alignItems: "center" }}>
          {refArray.map((textInputRef, index) => (
            <OtpInput
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              refCallback={textInputRef}
              onChangeText={(value) => onChangeOtp(value, index)}
              onKeyPress={(e) => onKeyPressOtp(e, index)}
            />
          ))}
        </Box>

        <Box style={styles.button}>
          <Button
            type="primary"
            label="Create Pin"
            onPress={handleCreatePin}
            loading={loading}
          />
        </Box>
      </ScrollView>
      {loading ? <LoadingBackdrop /> : null}
    </Box>
  );
};

export default TransactionPin;

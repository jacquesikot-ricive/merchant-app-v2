import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useState } from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

import TransactionSuccessIcon from "../../../svgs/TransactionSuccessIcon";
import Button from "../../components/Button";
import StackHeader from "../../components/StackHeader";
import theme, { Box, Text } from "../../components/Themed";
import { WalletNavParamList } from "../../navigation/WalletNav";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: "center",
  },
  layout: {
    width: wp(90),
    marginTop: 57,
  },
  button: {
    marginTop: 50,
  },
  successIcon: {
    marginTop: 109,
  },
  description: {
    textAlign: "center",
    marginTop: 50,
  },
});

type Props = NativeStackScreenProps<WalletNavParamList, "TransactionSuccess">;

const TransactionSuccess = ({ navigation }: Props): JSX.Element => {
  const [loading, setLoading] = useState<boolean>(false);

  const handleProceed = () => {
    navigation.navigate("Withdraw");
  };

  return (
    <Box style={styles.container}>
      <StackHeader onBackPress={() => navigation.goBack()} title="" />
      <Box style={styles.successIcon}>
        <TransactionSuccessIcon />
      </Box>
      <Box style={styles.description}>
        <Text variant="Body2M" color="text6">
          Your 4-digit transaction pin has been created successfully
        </Text>
      </Box>
      <Box style={styles.button}>
        <Button
          type="primary"
          onPress={handleProceed}
          label="Proceed to withdraw"
        />
      </Box>
    </Box>
  );
};

export default TransactionSuccess;

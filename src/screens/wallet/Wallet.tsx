import React, { useState, useEffect } from 'react';
import { StyleSheet, FlatList, TouchableOpacity, Alert } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

import theme, { Box, Text } from '../../components/Themed';
import StackHeader from '../../components/StackHeader';
import { DrawerActions } from '@react-navigation/native';
import { useAppSelector } from '../../redux/hooks';
import TransactionList, { data } from '../../components/TransactionList';
import Button from '../../components/Button';
import ListEmpty from '../../components/ListEmpty';
import ScreenLoading from '../../components/ScreenLoading';
import ScreenError from '../../components/ScreenError';
import RocketIcon from '../../svg/RocketIcon';
import { WalletNavParamList } from '../../navigation/WalletNav';
import numberWithCommas from '../../utils/numbersWithComma';
import walletApi from '../../api/riciveApi/wallet';
import transactionApi from '../../api/riciveApi/transaction';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: wp(85),
  },
  subHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 42,
    marginBottom: 26,
    width: wp(90),
  },
  amountHeader: {
    alignItems: 'center',
  },
  amountFlex: {
    flexDirection: 'row',
  },
  icon: {
    marginTop: 48,
    paddingRight: 10,
  },
  button: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(90),
    marginTop: 42,
  },
});

type Props = NativeStackScreenProps<WalletNavParamList, 'Wallet'>;

const Wallet = ({ navigation }: Props): JSX.Element => {
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const role = useAppSelector((state) => state.login.user.profile.role);
  const isSuperAdmin = role === 'SUPERADMIN';
  const userPermissions = useAppSelector(
    (state) => state.login.user.permissions
  );
  const [balance, setBalance] = useState<any>();
  const [transactions, setTransactions] = useState<any>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  const getData = async () => {
    try {
      setError(false);
      setLoading(true);
      const balanceData = await walletApi.getWallet();
      const transactionData = await transactionApi.getTransaction();
      console.log(transactionData.data[1]);
      setBalance(balanceData.data);
      setTransactions(transactionData.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
      setError(true);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2:
          'Sorry, we cannot get your wallet data now, please try again later',
      });
    }
  };

  useEffect(() => {
    getData();

    navigation.addListener('focus', async () => await getData());

    return () => {
      navigation.removeListener('focus', async () => await getData());
    };
  }, []);

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.goBack()}
        onRetry={() => getData()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {loading && <ScreenLoading />}
      <StackHeader
        onBackPress={() => navigation.goBack()}
        title="Wallet"
        hasBorder
      />

      <Box mt="xl" />

      {!balance && !loading && (
        <>
          <Box
            style={{
              flexDirection: 'row',
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: hp(25),
            }}
          >
            <Text variant={'Body1SB'} color="text1" mr="l">
              Welcome
            </Text>
            <RocketIcon />
          </Box>

          <Text
            variant={'DetailsR'}
            color="text1"
            mt="l"
            style={{ textAlign: 'center', width: '70%' }}
          >
            To activate your wallet, first set up your transaction pin
          </Text>

          <Box mt="xxl">
            <Button
              type={'primary'}
              label={'Set up Transaction Pin'}
              onPress={() =>
                navigation.navigate('TransactionPin', {
                  isCreate: true,
                })
              }
              width={wp(80)}
            />
          </Box>
        </>
      )}

      {balance && (
        <>
          <Box style={styles.amountHeader}>
            <Text variant="SmallerTextM" color="text1">
              Current Balance
            </Text>
            <Box style={styles.amountFlex}>
              <Text variant="MainTitleB" color="text6">
                {balance && balance.balance > 1
                  ? numberWithCommas((balance.balance / 100).toString())
                  : '0.00'}
              </Text>
              <Box style={{ marginLeft: 7, marginTop: 10 }}>
                <Text variant="SmallerTextR" color="text6">
                  (NGN)
                </Text>
              </Box>
            </Box>
          </Box>
          {userPermissions.withdraw && (
            <Box style={styles.button}>
              <Button
                type="secondary"
                label="Deposit"
                width={wp(40)}
                height={46}
                onPress={() => Alert.alert('Coming Soon!')}
              />
              <Button
                type="primary"
                label="Withdraw"
                height={46}
                width={wp(40)}
                onPress={() => {
                  if (balance && balance.pin)
                    return navigation.navigate('Withdraw');
                  return navigation.navigate('TransactionPin', {
                    isCreate: true,
                  });
                }}
              />
            </Box>
          )}
          <Box style={styles.subHeader}>
            <Text variant="SmallerTextM" color="text1">
              Recent Transactions
            </Text>
            {/* <TouchableOpacity>
              <Text variant="SmallerTextM" color="primary1">
                View History
              </Text>
            </TouchableOpacity> */}
          </Box>
          <Box style={{ height: 500 }}>
            <FlatList
              data={transactions && transactions}
              showsVerticalScrollIndicator={false}
              ListEmptyComponent={() => (
                <ListEmpty
                  noImage
                  topText="No Transactions yet"
                  bottomText="Start collecting payments to see transaction data"
                />
              )}
              keyExtractor={(item: any) => item.id.toString()}
              renderItem={({ item }) => (
                // <TouchableOpacity
                //   onPress={() => {
                //     navigation.navigate('TransactionDetails', {
                //       item,
                //     });
                //   }}
                // >
                <TransactionList
                  transactionStatus={`${
                    item.event === 'charge.success' ? 'CR' : 'WDL'
                  }${
                    item.user && item.user.name ? item.user.name.trim() : ''
                  }/${item.channel}`}
                  fees={item.fees}
                  transactionType={item.event}
                  timestamp={new Date(item.created_at).toDateString()}
                  amount={`${numberWithCommas(
                    (Number(item.amount) / 100).toString()
                  )}`}
                  balance={item.balance}
                  reason={item.reason}
                />
                // </TouchableOpacity>
              )}
            />
          </Box>
        </>
      )}
    </Box>
  );
};

export default Wallet;

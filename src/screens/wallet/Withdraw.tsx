import React, { useEffect, useRef, useState } from 'react';
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import { Ionicons } from '@expo/vector-icons';
import theme, { Box, Text } from '../../components/Themed';
import { useAppSelector } from '../../redux/hooks';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import Toast from 'react-native-toast-message';
import SecureBalance from '../../components/SecureBalance';
import Picker from '../../components/Picker';
import StackHeader from '../../components/StackHeader';
import ScreenLoading from '../../components/ScreenLoading';
import balanceApi from '../../api/merchant/balance';
import axios from 'axios';
import ScreenError from '../../components/ScreenError';
import numberWithCommas from '../../utils/numbersWithComma';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { WalletNavParamList } from '../../navigation/WalletNav';
import walletApi from '../../api/riciveApi/wallet';
import SelectBankModal from '../../components/SelectBankModal';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  layout: {
    width: wp(90),
  },
  description: {},
  inputContainer: {
    marginTop: 14,
    marginBottom: 14,
  },
  input: {
    marginBottom: 10,
  },
  button: {
    marginTop: 64,
  },
  balance: {
    marginTop: 10,
  },
});

type Props = NativeStackScreenProps<WalletNavParamList, 'Withdraw'>;
type Reasons = 'Rent/Shop' | 'Repairs' | 'Salary' | 'Pay Bills' | 'Others';

const TransactionPin = ({ navigation }: Props): JSX.Element => {
  const merchant_id = useAppSelector((state) => state.login.user.business.id);
  const [amount, setAmount] = useState<string>('');
  const [accountNumber, setAccountNumber] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [balance, setBalance] = useState<any>();
  const [banks, setBanks] = useState<any>([]);
  const [selectedBank, setSelectedBank] = useState<string>('');
  const [Reasons, setReasons] = useState<string[]>([
    'Rent/Shop',
    'Repairs',
    'Salary',
    'Pay Bills',
    'Others',
  ]);
  const [selectedReason, setSelectedReason] = useState<string>('');
  const [otherReason, setOtherReason] = useState<string>('');
  const [showReasons, setShowReasons] = useState<boolean>(false);
  const [selectedBankCode, setSelectedBankCode] = useState<string>('');
  const [accountHolderName, setAccountHolderName] = useState<string>('');
  const [bankNameLoading, setBankNameLoading] = useState<boolean>(false);
  const [onReady, setOnReady] = useState<boolean>(false);
  const [openSelectBank, setOpenSelectBank] = useState<boolean>(false);

  const amountRef = useRef<any>();
  const accountNumberRef = useRef<any>();

  const handleConfirm = () => {
    try {
      setLoading(true);
      if (amount.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Amount cannot be empty',
          visibilityTime: 5000,
        });
      }

      if (accountNumber.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Account number cannot be empty',
          visibilityTime: 5000,
        });
      }

      if (selectedBank.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Please select a bank first',
          visibilityTime: 5000,
        });
      }
      if (selectedReason.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Please give reason for withdrawal',
          visibilityTime: 5000,
        });
      }
      if (selectedReason == 'Others' && otherReason.length < 1) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Input Error',
          text2: 'Please give reason for withdrawal',
          visibilityTime: 5000,
        });
      }

      setLoading(false);

      navigation.navigate('WithdrawFunds', {
        accountName: accountHolderName,
        accountNumber,
        amount,
        bankCode: selectedBankCode,
        selectedBank,
        pin: balance && balance.pin,
        reason: selectedReason == 'Others' ? otherReason : selectedReason,
      });
    } catch (error) {
      console.log('Error:', error);
      setLoading(false);
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We cannot process your request now, please try again later..',
      });
    }
  };

  const getData = async () => {
    try {
      setLoading(true);
      setError(false);
      const balanceData = await walletApi.getWallet();
      setBalance(balanceData.data);
      const banksData = await axios.get(
        'https://api.paystack.co/bank?currency=NGN',
        {
          headers: {
            Authorization:
              'Bearer sk_live_593598ab4f197196c659fa21a438088b6699c4a6',
          },
        }
      );

      setBanks(banksData.data.data);
      setLoading(false);
    } catch (error: any) {
      setLoading(false);
      setError(true);
      console.log(JSON.stringify(error.response.data));
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'We cannot process your request now, please try again later..',
      });
    }
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    const getAccountHolder = async () => {
      setBankNameLoading(true);
      try {
        const accountHolder = await axios.get(
          `https://api.paystack.co/bank/resolve?account_number=${accountNumber}&bank_code=${selectedBankCode}`,
          {
            headers: {
              Authorization:
                'Bearer sk_live_593598ab4f197196c659fa21a438088b6699c4a6',
            },
          }
        );
        setAccountHolderName(accountHolder.data.data.account_name);
        // console.log(accountHolder.data.data);
        setBankNameLoading(false);
      } catch (error: any) {
        console.log(JSON.stringify(error.response.data));
        setBankNameLoading(false);
        // Toast.show({
        //   type: 'error',
        // text1: 'Account Holder',
        // text2: 'We cannot process your request now, please try again later..',
        // })
        setAccountHolderName('User not found!');
      }
    };

    console.log({ accountNumber, selectedBankCode, onReady });

    if (accountNumber.length === 10 && selectedBankCode.length > 0) {
      getAccountHolder();
    }
  }, [accountNumber, selectedBankCode, onReady]);

  if (error) {
    return (
      <ScreenError
        onBackPress={() => navigation.navigate('Wallet')}
        onRetry={async () => await getData()}
      />
    );
  }

  return (
    <Box style={styles.container}>
      {loading && <ScreenLoading />}
      <StackHeader
        onBackPress={() => navigation.navigate('Wallet')}
        title="Withdraw Funds"
      />
      <ScrollView style={styles.layout} showsVerticalScrollIndicator={false}>
        <Box style={styles.balance}>
          <SecureBalance
            amount={numberWithCommas(
              balance ? (balance.balance / 100).toString() : ''
            )}
            secured
          />
        </Box>

        <Box style={styles.inputContainer}>
          <Box style={styles.input}>
            <Text>Amount</Text>
          </Box>
          <TextInput
            inputRef={amountRef}
            type={'number'}
            placeholder={'NGN Enter Amount'}
            autoCorrect={false}
            keyboardType="number-pad"
            returnKeyType="next"
            onChangeText={(e) => setAmount(e)}
          />
        </Box>

        <Box style={styles.inputContainer}>
          <Box style={styles.input}>
            <Text>Account number</Text>
          </Box>
          <TextInput
            inputRef={accountNumberRef}
            type={'number'}
            placeholder={'A/c no'}
            autoCorrect={false}
            keyboardType="number-pad"
            returnKeyType="next"
            onChangeText={(e) => setAccountNumber(e)}
          />
        </Box>

        {/* <Picker
          placeholder="Select Bank"
          // setOnReady={setOnReady}
          // autoClose
          label="Select bank"
          data={
            banks &&
            banks.map((d: any) => {
              return {
                id: d.code,
                label: d.name,
                value: d.name,
              };
            })
          }
          setValue={setSelectedBank}
          value={selectedBank}
          setSelectedId={setSelectedBankCode}
        /> */}

        <Box style={styles.input} mt="l">
          <Text>Select bank</Text>
        </Box>

        <TouchableOpacity
          onPress={() => setOpenSelectBank(true)}
          activeOpacity={0.6}
          style={{
            width: wp(90),
            height: 58,
            borderRadius: 8,
            borderWidth: 1,
            borderColor: theme.colors.border1,
            justifyContent: 'space-between',
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              fontFamily: 'basier-regular',
              fontSize: 14,
              color: theme.colors.text5,
            }}
          >
            {selectedBank ? selectedBank : 'Select Bank'}
          </Text>
          <Ionicons name="chevron-down-outline" />
        </TouchableOpacity>

        {!accountHolderName && !bankNameLoading ? null : (
          <Box>
            {bankNameLoading ? (
              <ActivityIndicator color={theme.colors.primary1} />
            ) : (
              <Text variant="DetailsR" color="text11" mt="s" mb="m">
                {accountHolderName}
              </Text>
            )}
          </Box>
        )}

        <Box style={styles.input} mt="l">
          <Text>Reason for withdrawing</Text>
        </Box>
        {/* <TouchableOpacity onPress={() => setShowReasons(!showReasons)}>
          <TextInput
            rightIcon={
              showReasons ? 'chevron-up-outline' : 'chevron-down-outline'
            }
            type="input"
            placeholder={selectedReason ? selectedReason : 'Select option'}
            editable={false}
            returnKeyType="next"
            onChangeText={(e) => setSelectedReason(e)}
            rightIconPress={() => setShowReasons(!showReasons)}
          />
        </TouchableOpacity> */}
        <TouchableOpacity
          onPress={() => setShowReasons(!showReasons)}
          activeOpacity={0.6}
          style={{
            width: wp(90),
            height: 58,
            borderRadius: 8,
            borderWidth: 1,
            borderColor: theme.colors.border1,
            justifyContent: 'space-between',
            paddingHorizontal: 20,
            flexDirection: 'row',
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              fontFamily: 'basier-regular',
              fontSize: 14,
              color: theme.colors.text5,
            }}
          >
            {selectedReason ? selectedReason : 'Select option'}
          </Text>
          <Ionicons name="chevron-down-outline" />
        </TouchableOpacity>
        {showReasons ? (
          <Box mt="l">
            {Reasons.map((e, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  setSelectedReason(e);
                  setShowReasons(false);
                }}
              >
                <Box
                  width={'100%'}
                  backgroundColor="bg4"
                  borderWidth={0.2}
                  paddingVertical="l"
                  paddingHorizontal="m"
                  borderColor="text1"
                >
                  <Text>{e}</Text>
                </Box>
              </TouchableOpacity>
            ))}
          </Box>
        ) : null}
        {selectedReason == 'Others' ? (
          <>
            <Box style={styles.input} mt="l">
              <Text>Enter reason</Text>
            </Box>
            <Box>
              <TextInput
                type={'number'}
                placeholder={otherReason}
                autoCorrect={false}
                keyboardType="number-pad"
                returnKeyType="next"
                onChangeText={(e) => setOtherReason(e)}
              />
            </Box>
          </>
        ) : null}

        <Box style={styles.button}>
          <Button
            type="primary"
            label="Confirm"
            onPress={handleConfirm}
            disabled={
              amount.length > 0 &&
              accountNumber.length > 9 &&
              selectedBank.length > 0 &&
              accountHolderName.length > 0
                ? false
                : true
            }
            loading={loading}
          />
        </Box>
      </ScrollView>

      <SelectBankModal
        setSelectedBankId={setSelectedBankCode}
        banks={
          banks &&
          banks.map((d: any) => {
            return {
              id: d.code,
              label: d.name,
              value: d.name,
            };
          })
        }
        selectedBank={selectedBank}
        setSelectedBank={setSelectedBank}
        setVisible={setOpenSelectBank}
        visible={openSelectBank}
      />
    </Box>
  );
};

export default TransactionPin;

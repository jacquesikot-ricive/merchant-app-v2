import React, { useRef, useState } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import Toast from 'react-native-toast-message';
import { useAnalytics } from '@segment/analytics-react-native';

import theme, { Box, Text } from '../../components/Themed';
import { useAppSelector } from '../../redux/hooks';
import Button from '../../components/Button';
import TextInput from '../../components/TextInput';
import SecureBalance from '../../components/SecureBalance';
import Picker from '../../components/Picker';
import StackHeader from '../../components/StackHeader';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { WalletNavParamList } from '../../navigation/WalletNav';
import numberWithCommas from '../../utils/numbersWithComma';
import walletApi from '../../api/riciveApi/wallet';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.colors.white,
    alignItems: 'center',
  },
  layout: {
    width: wp(90),
  },
  title: {
    marginTop: 47,
  },
  description: {
    marginTop: 43,
  },
  inputContainer: {
    marginTop: 14,
    marginBottom: 14,
  },
  input: {
    marginTop: 28,
    marginBottom: 10,
  },
  button: {
    marginTop: 64,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(60),
  },
  balance: {
    marginTop: 43,
  },
});

type Props = NativeStackScreenProps<WalletNavParamList, 'WithdrawFunds'>;

const WithdrawFunds = ({ navigation, route }: Props): JSX.Element => {
  const business_id = useAppSelector((state) => state.login.user.business.id);
  const {
    accountName,
    accountNumber,
    amount,
    selectedBank,
    pin,
    bankCode,
    reason,
  } = route.params;
  const [transactionPin, setTransactionPin] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const { track } = useAnalytics();

  const transactionPinRef = useRef<any>();

  const handleWithdraw = async () => {
    setLoading(true);
    try {
      if (transactionPin !== pin) {
        setLoading(false);
        return Toast.show({
          type: 'error',
          text1: 'Transaction PIN',
          text2: 'Your PIN is not correct, try again..',
        });
      }

      await walletApi.withdraw({
        account_number: accountNumber,
        amount: (Number(amount) * 100).toString(),
        bank_code: bankCode,
        account_name: accountName,
        pin: transactionPin,
        reason: reason,
      });
      track('Withdraw Funds', {
        business: business_id,
        account_number: accountNumber,
        amount: (Number(amount) * 100).toString(),
        bank_code: bankCode,
        account_name: accountName,
      });

      Toast.show({
        type: 'success',
        text1: 'Transfer Complete',
        text2: 'Your transfer has been completed',
      });

      setLoading(false);
      navigation.navigate('Wallet');
    } catch (error: any) {
      setLoading(false);
      console.log(JSON.stringify(error.response.data));
      Toast.show({
        type: 'error',
        text1: 'Server Error',
        text2: 'Sorry we cannot process your transfer now, try again later',
      });
    }
  };

  return (
    <Box style={styles.container}>
      <StackHeader
        onBackPress={() => navigation.goBack()}
        title="Withdraw Funds"
      />
      <ScrollView style={styles.layout}>
        <Box style={styles.description}>
          <Text variant="DetailsR" color="text11">
            {`You are about to send the sum of NGN${numberWithCommas(
              amount
            )} to ${accountName}'s ${selectedBank} account ${accountNumber}`}
          </Text>
        </Box>

        <Box style={styles.inputContainer}>
          <Box style={styles.input}>
            <Text>Enter Transaction Pin</Text>
          </Box>
          <TextInput
            inputRef={transactionPinRef}
            secured
            type={'number'}
            placeholder={''}
            autoCorrect={false}
            keyboardType="numeric"
            onChangeText={(e) => setTransactionPin(e)}
          />
        </Box>
        <Box>
          <Text variant="DetailsR" color="text9">
            Any issues with fund withdrawal? please contact support on
            support@ricive.com
          </Text>
        </Box>

        <Box style={styles.button}>
          <Button
            type="primary"
            label="Withdraw"
            onPress={handleWithdraw}
            disabled={transactionPin.length > 0 ? false : true}
            loading={loading}
          />
        </Box>
      </ScrollView>
    </Box>
  );
};

export default WithdrawFunds;

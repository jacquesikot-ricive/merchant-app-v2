import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const AddCustomerIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M10 11c2.206 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4 1.794 4 4 4Zm11-5h-1V5c0-.55-.45-1-1-1s-1 .45-1 1v1h-1c-.55 0-1 .45-1 1s.45 1 1 1h1v1c0 .55.45 1 1 1s1-.45 1-1V8h1c.55 0 1-.45 1-1s-.45-1-1-1Zm-4 14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1c0-3.86 3.141-7 7-7s7 3.14 7 7Z"
      fill="#4E903C"
    />
  </Svg>
);

export default AddCustomerIcon;

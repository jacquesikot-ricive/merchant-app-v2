import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const CallIcon = (props: SvgProps) => (
  <Svg {...props} width={40} height={40} fill="none">
    <Circle cx={20} cy={20} r={20} fill="#F3FAF2" />  
    <Path
       fillRule="evenodd"
       clipRule="evenodd"
       d="M24.5 28.333c-7.076 0-12.833-5.756-12.833-12.833a3.838 3.838 0 0 1 5.076-3.627.833.833 0 0 1 .539.602l1.142 4.97a.836.836 0 0 1-.211.763c-.114.119-.117.121-1.15.662a8.29 8.29 0 0 0 4.067 4.069c.54-1.034.544-1.038.662-1.15a.843.843 0 0 1 .764-.212l4.97 1.141c.28.064.507.268.601.538.068.197.12.4.153.61a3.837 3.837 0 0 1-3.78 4.468"
       fill="#4E903C"
    />
  </Svg>
);

export default CallIcon;

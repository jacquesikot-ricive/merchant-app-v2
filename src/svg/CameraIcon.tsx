import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const ImageIcon = (props: SvgProps) => (
  <Svg {...props} width={36} height={36} fill="none">
    <Circle cx={18} cy={18} r={18} fill="#4E903C" fillOpacity={0.2} />  
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M18 18.458c-.758 0-1.375.617-1.375 1.375s.617 1.375 1.375 1.375 1.375-.617 1.375-1.375-.617-1.375-1.375-1.375Zm0 4.584a3.212 3.212 0 0 1-3.208-3.209c0-1.769 1.439-3.208 3.208-3.208 1.77 0 3.208 1.44 3.208 3.208 0 1.77-1.439 3.209-3.208 3.209Zm-1.833-11a.46.46 0 0 1 .458-.459h2.75a.46.46 0 0 1 .458.459v1.375h-3.666v-1.375Zm8.25 1.375h-2.75v-1.375a2.294 2.294 0 0 0-2.292-2.292h-2.75a2.294 2.294 0 0 0-2.292 2.292v1.375h-2.75a2.753 2.753 0 0 0-2.75 2.75V23.5a2.753 2.753 0 0 0 2.75 2.75h12.834a2.753 2.753 0 0 0 2.75-2.75v-7.333a2.753 2.753 0 0 0-2.75-2.75Z"
      fill="#4E903C"
    />
  </Svg>
);

export default ImageIcon;

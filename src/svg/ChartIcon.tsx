import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const ChartIcon = (props: SvgProps) => (
  <Svg {...props} width={40} height={40} fill="none">
    <Circle cx={20} cy={20} r={20} fill="#F3FAF2" />
    <Path
      d="M29.81 13.715 27.177 10l-2.617 3.715h2.216v15.46H10.602a.422.422 0 0 0-.412.413c0 .22.191.412.412.412h16.587c.22 0 .412-.192.412-.412V13.715h2.209Z"
      fill="#4E903C"
    />
    <Path
      d="M14.729 20.178h-4.127v7.524h4.127v-7.524ZM20.252 18.381h-4.127v9.3h4.127v-9.3ZM25.792 16.193h-4.127v11.509h4.127V16.193Z"
      fill="#4E903C"
    />
  </Svg>
);

export default ChartIcon;

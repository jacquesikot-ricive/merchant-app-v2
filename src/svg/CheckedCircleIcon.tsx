import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';
import theme from '../components/Themed';

const CheckedCircleIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="m16.296 9.605-4.569 6a1 1 0 0 1-.789.395h-.007c-.306 0-.596-.142-.786-.383l-2.433-3.108a1 1 0 1 1 1.575-1.232l1.633 2.087 3.784-4.97a1 1 0 0 1 1.591 1.211ZM12 2C6.477 2 2 6.477 2 12s4.476 10 10 10c5.522 0 10-4.477 10-10S17.523 2 12 2Z"
      fill={theme.colors.primary3}
    />
  </Svg>
);

export default CheckedCircleIcon;

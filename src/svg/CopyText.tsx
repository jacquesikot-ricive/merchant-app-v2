import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const CopyText = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9 12v1H5.667A.667.667 0 0 1 5 12.333V5.667C5 5.299 5.299 5 5.667 5h6.666c.368 0 .667.299.667.667V9h-1c-1.654 0-3 1.346-3 3Zm9-3h-3V5.667A2.67 2.67 0 0 0 12.333 3H5.667A2.67 2.67 0 0 0 3 5.667v6.666A2.67 2.67 0 0 0 5.667 15H9v3c0 1.654 1.346 3 3 3h6c1.654 0 3-1.346 3-3v-6c0-1.654-1.346-3-3-3Z"
      fill="#636366"
    />
  </Svg>
);

export default CopyText;

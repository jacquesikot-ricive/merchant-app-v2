import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const EditIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M11.983 7.323 6.366 12.94l-.264 2.956 2.977-.271 5.6-5.606-2.696-2.696Zm5.983-.594-2.695-2.694-1.948 1.948 2.695 2.696 1.948-1.95ZM5.091 17.996a.997.997 0 0 1-.798-.289.999.999 0 0 1-.289-.797l.38-4.171c.041-.457.243-.888.568-1.213l8.996-8.997c.702-.704 1.976-.67 2.716.07l2.738 2.738h.001c.766.767.796 1.984.068 2.714l-8.997 8.997a1.967 1.967 0 0 1-1.213.569l-4.17.379ZM5 20h14c.55 0 1 .45 1 1s-.45 1-1 1H5c-.55 0-1-.45-1-1s.45-1 1-1Z"
      fill="#636366"
    />
  </Svg>
);

export default EditIcon;

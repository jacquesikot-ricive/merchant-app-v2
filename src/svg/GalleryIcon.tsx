import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const GalleryIcon = (props: SvgProps) => (
  <Svg {...props} width={36} height={36} fill="none">
    <Circle cx={18} cy={18} r={18} fill="#4E903C" fillOpacity={0.2} />  
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M14.333 16.167a1.376 1.376 0 1 0 0-2.751 1.376 1.376 0 0 0 0 2.75ZM12.5 11.583h11c.506 0 .917.411.917.917v7.667l-2.936-2.504c-.908-.772-2.328-.772-3.227-.006l-6.67 5.566V12.5c0-.506.41-.917.916-.917Zm11-1.833h-11a2.753 2.753 0 0 0-2.75 2.75v11a2.753 2.753 0 0 0 2.75 2.75h11a2.753 2.753 0 0 0 2.75-2.75v-11a2.753 2.753 0 0 0-2.75-2.75Z"
      fill="#4E903C"
    />
  </Svg>
);

export default GalleryIcon;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const HomeNavIcon = ({ color }: SvgProps) => (
  <Svg width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="m12.715 2.3 7.709 7.885c.366.376.576.9.576 1.439V20c0 1.103-.847 2-1.888 2H16v-9a1 1 0 0 0-1-1H9a1 1 0 0 0-1 1v9H4.889c-1.041 0-1.89-.897-1.89-2v-8.376c0-.54.21-1.063.576-1.438l7.71-7.885c.377-.385 1.053-.385 1.43 0ZM14 21h-4v-7h4v7Z"
      fill={color ? color : '#4E903C'}
    />
  </Svg>
);

export default HomeNavIcon;

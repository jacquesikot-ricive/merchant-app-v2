import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const ProductsHomeIcon = ({ color }: SvgProps) => (
  <Svg width={25} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12.06 11.56a1.5 1.5 0 1 1-2.12-2.119 1.5 1.5 0 0 1 2.12 2.12Zm9.907.021L15.55 5.165a.995.995 0 0 0-.614-.288l-9.344-.872a1.002 1.002 0 0 0-1.089 1.088l.872 9.345c.023.232.124.45.29.614l6.416 6.417a1.817 1.817 0 0 0 2.57 0l7.316-7.316c.343-.342.532-.8.532-1.285 0-.487-.189-.943-.532-1.286Z"
      fill={color ? color : '#908F8F'}
    />
  </Svg>
);

export default ProductsHomeIcon;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const ItemMenuIcon = (props: SvgProps) => (
  <Svg {...props} width={18} height={4} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0 2a2 2 0 1 1 4.001.001A2 2 0 0 1 0 2Zm9-2a2 2 0 1 0 .001 4.001A2 2 0 0 0 9 0Zm7 0a2 2 0 1 0 .001 4.001A2 2 0 0 0 16 0Z"
      fill="#636366"
    />
  </Svg>
);

export default ItemMenuIcon;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const LogoutIcon = ({color}: SvgProps) => (
  <Svg width={18} height={18} fill="none">
    <Path
      d="m14 5-1.41 1.41L14.17 8H6v2h8.17l-1.58 1.58L14 13l4-4-4-4ZM2 2h7V0H2C.9 0 0 .9 0 2v14c0 1.1.9 2 2 2h7v-2H2V2Z"
      fill={color}
    />
  </Svg>
);

export default LogoutIcon;


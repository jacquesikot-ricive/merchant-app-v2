import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const MailIcon = (props: SvgProps) => (
  <Svg {...props} width={40} height={40} fill="none">
    <Circle cx={20} cy={20} r={20} fill="#F3FAF2" />  
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M24.116 12.5a4.214 4.214 0 0 1 4.218 4.208v6.584a4.215 4.215 0 0 1-4.218 4.208h-8.232a4.214 4.214 0 0 1-4.217-4.208v-6.584a4.209 4.209 0 0 1 4.217-4.208h8.232Zm1.326 5.45.067-.067c.199-.241.199-.591-.01-.833a.7.7 0 0 0-.44-.217.633.633 0 0 0-.468.167l-3.757 3c-.484.4-1.176.4-1.667 0l-3.75-3a.634.634 0 0 0-.833.058.637.637 0 0 0-.06.834l.11.108 3.791 2.958a2.62 2.62 0 0 0 3.258 0l3.759-3.008Z"
      fill="#4E903C"
    />
  </Svg>
);

export default MailIcon;

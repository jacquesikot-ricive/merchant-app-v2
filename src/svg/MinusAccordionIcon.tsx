import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const MinusAccordionIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={26} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M15 13H9c-.55 0-1 .45-1 1s.45 1 1 1h6c.55 0 1-.45 1-1s-.45-1-1-1Zm-3 9c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8Zm0-18C6.486 4 2 8.486 2 14s4.486 10 10 10 10-4.486 10-10S17.514 4 12 4Z"
      fill="#4E903C"
    />
  </Svg>
);

export default MinusAccordionIcon;

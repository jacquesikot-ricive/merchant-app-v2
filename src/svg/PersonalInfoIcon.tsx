import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const PersonalInfoIcon = (props: SvgProps) => (
  <Svg {...props} width={40} height={40} fill="none">
    <Circle cx={20} cy={20} r={20} fill="#F3FAF2"  />  
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M23.334 15.833A3.337 3.337 0 0 1 20 19.167a3.337 3.337 0 0 1-3.333-3.334A3.337 3.337 0 0 1 20 12.5a3.337 3.337 0 0 1 3.334 3.333Zm2.5 10.834c0 .46-.373.833-.834.833H15a.833.833 0 0 1-.833-.833A5.84 5.84 0 0 1 20 20.833a5.84 5.84 0 0 1 5.834 5.834Z"
      fill="#4E903C"
    />
  </Svg>
);

export default PersonalInfoIcon;

import * as React from 'react';
import Svg, { SvgProps, Mask, Path, G } from 'react-native-svg';

const ShareIcon = (props: SvgProps) => (
  <Svg {...props} width={19} height={19} fill="none">
    <Mask id="a" x={2} y={1} width={15} height={17}>
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.25 12.718v-7.94l2.47 2.47 1.06-1.06-4.28-4.28-4.28 4.28 1.06 1.06 2.47-2.47v7.94h1.5Zm6 3v-6.75h-1.5v6.75H4.25v-6.75h-1.5v6.75a1.5 1.5 0 0 0 1.5 1.5h10.5a1.5 1.5 0 0 0 1.5-1.5Z"
        fill="#000"
      />
    </Mask>
    <G mask="url(#a)">
      <Path fill="#4E903C" d="M.5.718h18v18H.5z" />
    </G>
  </Svg>
);

export default ShareIcon;

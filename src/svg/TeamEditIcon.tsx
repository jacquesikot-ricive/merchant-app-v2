import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const TeamEditIcon = (props: SvgProps) => (
  <Svg {...props} width={13} height={14} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M6.487 4.492 2.275 8.705l-.198 2.217 2.232-.203 4.2-4.205-2.022-2.022Zm4.488-.445-2.022-2.02-1.46 1.46 2.02 2.022 1.462-1.462Zm-9.657 8.45a.747.747 0 0 1-.815-.815l.284-3.128c.032-.343.183-.666.427-.91L7.961.897C8.488.369 9.443.395 9.998.949l2.054 2.054c.575.575.597 1.488.051 2.035l-6.747 6.748a1.475 1.475 0 0 1-.91.427l-3.128.284ZM1.25 14h10.5c.412 0 .75.338.75.75s-.338.75-.75.75H1.25a.752.752 0 0 1-.75-.75c0-.412.338-.75.75-.75Z"
      fill="#4E903C"
    />
  </Svg>
);

export default TeamEditIcon;

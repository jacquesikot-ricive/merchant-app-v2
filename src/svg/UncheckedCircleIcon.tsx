import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const UncheckedCircleIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Circle cx={12} cy={12} r={9} stroke="#D9DBE1" strokeWidth={2} />
  </Svg>
);

export default UncheckedCircleIcon;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const ProductsNavIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      d="M10.56 8.44a1.5 1.5 0 1 1-2.119 2.12 1.5 1.5 0 0 1 2.12-2.12Zm-4.726 4.452 6.032 6.032 7.058-7.057-6.032-6.034-7.785-.726.727 7.785Zm6.032 8.107c-.465 0-.929-.177-1.284-.53L4.165 14.05a1 1 0 0 1-.289-.614l-.872-9.344a1.002 1.002 0 0 1 1.09-1.088l9.343.872a.995.995 0 0 1 .614.288l6.417 6.418c.343.342.532.798.532 1.284s-.189.943-.532 1.286l-7.317 7.316a1.81 1.81 0 0 1-1.285.53Z"
      fill={props.color || '#636366'}
    />
  </Svg>
);

export default ProductsNavIcon;

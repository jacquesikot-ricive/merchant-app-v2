import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const WalletNavIcon = (props: SvgProps) => (
  <Svg {...props} width={19} height={18} fill="none">
    <Path
      d="M18 4.28V2c0-1.1-.9-2-2-2H2a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14c1.1 0 2-.9 2-2v-2.28A2 2 0 0 0 19 12V6a2 2 0 0 0-1-1.72ZM17 6v6h-7V6h7ZM2 16V2h14v2h-6c-1.1 0-2 .9-2 2v6c0 1.1.9 2 2 2h6v2H2Z"
      fill={props.color}
    />
  </Svg>
);

export default WalletNavIcon;

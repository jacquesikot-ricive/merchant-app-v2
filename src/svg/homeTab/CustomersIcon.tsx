import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const CustomerIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M9 11c2.206 0 4-1.794 4-4s-1.794-4-4-4-4 1.794-4 4 1.794 4 4 4Zm8 2c1.654 0 3-1.346 3-3s-1.346-3-3-3-3 1.346-3 3 1.346 3 3 3Zm5 6a1 1 0 0 1-1 1h-5a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1c0-3.86 3.141-7 7-7 1.927 0 3.673.783 4.94 2.046A4.994 4.994 0 0 1 17 14c2.757 0 5 2.243 5 5Z"
      fill={props.color}
    />
  </Svg>
);

export default CustomerIcon;

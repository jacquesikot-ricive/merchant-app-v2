import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const HomeIcon = (props: SvgProps) => (
  <Svg width={18} height={18} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M2 0h4c1.103 0 2 .897 2 2v4c0 1.103-.897 2-2 2H2C.897 8 0 7.103 0 6V2C0 .897.897 0 2 0zm10 0h4c1.103 0 2 .897 2 2v4c0 1.103-.897 2-2 2h-4c-1.103 0-2-.897-2-2V2c0-1.103.897-2 2-2zM6 10H2c-1.103 0-2 .897-2 2v4c0 1.103.897 2 2 2h4c1.103 0 2-.897 2-2v-4c0-1.103-.897-2-2-2zm6 0h4c1.103 0 2 .897 2 2v4c0 1.103-.897 2-2 2h-4c-1.103 0-2-.897-2-2v-4c0-1.103.897-2 2-2z"
      fill={props.color}
    />
  </Svg>
);

export default HomeIcon;

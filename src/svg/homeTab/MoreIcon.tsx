import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const MoreIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12 7a2 2 0 1 0-.001-4.001A2 2 0 0 0 12 7Zm0 3a2 2 0 1 0 .001 4.001A2 2 0 0 0 12 10Zm-2 9a2 2 0 1 1 4.001.001A2 2 0 0 1 10 19Z"
      fill={props.color}
    />
  </Svg>
);

export default MoreIcon;

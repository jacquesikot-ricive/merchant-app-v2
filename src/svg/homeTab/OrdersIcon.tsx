import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const OrdersIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="m6.414 7 1.707-1.707A1.01 1.01 0 0 1 8.828 5h6.344c.263 0 .52.107.707.293L17.586 7H6.414ZM12 16c-2.206 0-4-1.794-4-4a1 1 0 1 1 2 0c0 1.103.897 2 2 2 1.102 0 2-.897 2-2a1 1 0 1 1 2 0c0 2.206-1.794 4-4 4Zm8.121-9.293-2.828-2.828A2.978 2.978 0 0 0 15.172 3H8.828c-.801 0-1.555.312-2.121.879L3.879 6.707A2.978 2.978 0 0 0 3 8.829V18c0 1.654 1.346 3 3 3h12c1.654 0 3-1.346 3-3V8.829c0-.802-.313-1.556-.879-2.122Z"
      fill={props.color}
    />
  </Svg>
);

export default OrdersIcon;

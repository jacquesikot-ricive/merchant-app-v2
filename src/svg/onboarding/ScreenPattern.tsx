import * as React from 'react';
import Svg, { SvgProps, Rect, G, Defs } from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: filter */

const ScreenPattern = (props: SvgProps) => (
  <Svg {...props} width={89} height={137} fill="none">
    <Rect
      opacity={0.4}
      y={102.146}
      width={48.291}
      height={48.291}
      rx={4.829}
      transform="rotate(-45 0 102.146)"
      fill="#E3F3CC"
    />
    <G opacity={0.3}>
      <Rect
        x={23.988}
        y={34.147}
        width={44.065}
        height={44.065}
        rx={2.716}
        transform="rotate(-45 23.988 34.147)"
        stroke="#91CE33"
        strokeWidth={4.225}
        // shapeRendering="crispEdges"
      />
    </G>
    <Defs></Defs>
  </Svg>
);

export default ScreenPattern;

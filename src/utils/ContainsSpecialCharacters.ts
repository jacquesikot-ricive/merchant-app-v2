function ContainsSpecialChars(s: string) {
    const specialChars = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return specialChars.test(s);
}

export default ContainsSpecialChars;
interface ApiCallParams {
  data?: any;
  method: 'POST' | 'GET' | 'PATCH';
  headers: any;
  url: string;
}

const apiCall = async (props: ApiCallParams) => {
  const res = await fetch(props.url, {
    method: props.method,
    headers: props.headers,
    body: props.data ? JSON.stringify(props.data) : undefined,
  });

  const response = await res.json();

  return response;
};

export default apiCall;

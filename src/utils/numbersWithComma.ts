const numberWithCommas = (x: string): string => {
  return (x && x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')) || '0';
};

export default numberWithCommas;

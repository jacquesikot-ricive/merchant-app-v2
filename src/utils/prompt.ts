import { Alert } from 'react-native';

interface PromptProps {
  title: string;
  body: string;
  func: any;
  styleForAction: 'destructive' | 'cancel' | 'default';
}

const promptMessage = ({ title, body, func, styleForAction }: PromptProps) => {
  Alert.alert(title, body, [
    {
      text: 'Yes',
      onPress: func,
      style: styleForAction,
    },
    {
      text: 'No',
    },
  ]);
};

export default promptMessage;

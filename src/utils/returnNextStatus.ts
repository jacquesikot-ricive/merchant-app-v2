const returnNextStatus = (status: string) => {
  if (status === 'CREATED')
    return {
      name: 'Accepted',
      value: 'ACCEPTED',
    };

  if (status === 'ACCEPTED')
    return {
      name: 'Scheduled',
      value: 'SCHEDULED',
    };

  if (status === 'SCHEDULED')
    return {
      name: 'In Store',
      value: 'IN STORE',
    };

  if (status === 'IN STORE')
    return {
      name: 'Processing',
      value: 'PROCESING',
    };

  if (status === 'PROCESSING')
    return {
      name: 'Ready for Delivery',
      value: 'DELIVERY',
    };

  if (status === 'DELIVERY')
    return {
      name: 'Completed',
      value: 'COMPLETED',
    };

  return {
    name: '',
    value: '',
  };
};

export default returnNextStatus;

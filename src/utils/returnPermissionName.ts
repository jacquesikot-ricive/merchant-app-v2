const returnPermissionName = (name: string) => {
  if (name === 'view_order') return 'View Orders';
  if (name === 'update_order_status') return 'Update Order Status';
  if (name === 'create_order') return 'Create Orders';
  if (name === 'view_invoice') return 'View Invoices';
  if (name === 'edit_invoice') return 'Edit Invoices';
  if (name === 'delete_invoice') return 'Delete Invoices';
  if (name === 'create_invoice') return 'Create Invoices';

  if (name === 'view_product') return 'View Products';
  if (name === 'edit_product') return 'Edit Products';
  if (name === 'delete_product') return 'Delete Products';
  if (name === 'create_product') return 'Create Products';

  if (name === 'view_category') return 'View Categories';
  if (name === 'edit_category') return 'Edit Categories';
  if (name === 'delete_category') return 'Delete Categories';
  if (name === 'create_category') return 'Create Categories';

  if (name === 'view_team') return 'View Team Members';
  if (name === 'edit_team') return 'Edit Team Mamber';
  if (name === 'delete_team') return 'Delete Team Member';
  if (name === 'create_team') return 'Create Team member';

  if (name === 'view_wallet') return 'View Wallet';
  if (name === 'withdraw') return 'Withdraw Funds';

  if (name === 'view_analytics') return 'View Analytics';

  if (name === 'business_info') return 'Edit Business Information';

  return name;
};

export default returnPermissionName;

import AsyncStorage from '@react-native-async-storage/async-storage';
import { createClient } from '@supabase/supabase-js';

const URL = 'https://modcpyizgieywmgwvprt.supabase.co';
const PUBLIC_ANON_KEY =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im1vZGNweWl6Z2lleXdtZ3d2cHJ0Iiwicm9sZSI6InNlcnZpY2Vfcm9sZSIsImlhdCI6MTY0NjU3MjgxMywiZXhwIjoxOTYyMTQ4ODEzfQ.mFU91BhXEszzb-BZrRHq2J-Hi2umAuhSNnjtid9OfUg';
const supabase = createClient(URL, PUBLIC_ANON_KEY, {
  localStorage: AsyncStorage,
});

export default supabase;

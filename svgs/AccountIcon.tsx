import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const AccountIcon = (props: SvgProps) => (
  <Svg {...props} width={40} height={40} fill="none">
    <Circle cx={20} cy={20} r={20} fill="#F3FAF2" />
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M24 15c0 2.206-1.794 4-4 4s-4-1.794-4-4 1.794-4 4-4 4 1.794 4 4Zm3 13a1 1 0 0 1-1 1H14a1 1 0 0 1-1-1c0-3.86 3.141-7 7-7s7 3.14 7 7Z"
      fill="#4E903C"
    />
  </Svg>
);

export default AccountIcon;

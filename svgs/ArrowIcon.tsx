import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const ArrowIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M17.993 7.049A1 1 0 0 0 17 6.05L9.006 6H9a1 1 0 0 0-.006 2l5.558.035-8.259 8.258a.999.999 0 1 0 1.414 1.414l8.288-8.288.005 5.582A1 1 0 0 0 17 16h.001a1 1 0 0 0 1-1.001l-.008-7.95Z"
      fill="#636366"
    />
  </Svg>
);

export default ArrowIcon;

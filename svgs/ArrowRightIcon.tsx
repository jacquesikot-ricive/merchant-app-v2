import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const ArrowRightIcon = (props: SvgProps) => (
  <Svg {...props} width={18} height={18} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M3.75 9.75h8.899l-2.725 3.27a.75.75 0 1 0 1.153.96l3.75-4.5c.029-.035.043-.077.065-.116.018-.031.04-.058.053-.093A.738.738 0 0 0 15 9.003L15 9v-.003a.738.738 0 0 0-.055-.268c-.013-.035-.035-.062-.053-.093-.022-.04-.036-.08-.065-.116l-3.75-4.5a.752.752 0 0 0-1.057-.096.75.75 0 0 0-.096 1.056l2.725 3.27H3.75a.75.75 0 0 0 0 1.5Z"
      fill="#636366"
    />
  </Svg>
);

export default ArrowRightIcon;

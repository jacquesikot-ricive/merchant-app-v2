import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function ChevronRight(): JSX.Element {
  return (
    <Svg width={6} height={11} fill="none">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M1.42 10.95a.75.75 0 0 1-.576-1.23l3.358-4.028L.965 1.67a.75.75 0 0 1 1.17-.94l3.62 4.5a.75.75 0 0 1-.008.95l-3.75 4.5a.75.75 0 0 1-.576.27Z"
        fill="#636366"
      />
    </Svg>
  );
}

export default ChevronRight;

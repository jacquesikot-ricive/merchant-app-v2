import * as React from 'react';
import Svg, { Circle } from 'react-native-svg';

function CircleCheckbox(): JSX.Element {
  return (
    <Svg width={20} height={20} fill="none">
      <Circle
        cx={10} cy={10} r={9} stroke="#D9DBE1" strokeWidth={2}
      />
    </Svg>
  );
}

export default CircleCheckbox;

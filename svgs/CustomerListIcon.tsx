import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function CustomerListIcon(): JSX.Element {
  return (
    <Svg width={18} height={4} fill="none">
      <Path
      d="M0 2a2 2 0 1 1 4.001.001A2 2 0 0 1 0 2Zm9-2a2 2 0 1 0 .001 4.001A2 2 0 0 0 9 0Zm7 0a2 2 0 1 0 .001 4.001A2 2 0 0 0 16 0Z"
      fill="#636366"
      />
    </Svg>
  );
}

export default CustomerListIcon;

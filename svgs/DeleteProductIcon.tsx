import * as React from 'react';
import Svg, { SvgProps, G, Path, Defs, ClipPath } from 'react-native-svg';

const DeleteProductIcon = (props: SvgProps) => (
  <Svg {...props} width={14} height={15} fill="none">
    <G clipPath="url(#a)">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.666 10.167c0 .367-.3.667-.666.667a.669.669 0 0 1-.667-.667V7.501c0-.367.3-.667.667-.667.366 0 .666.3.666.667v2.666Zm4 0c0 .367-.3.667-.666.667a.669.669 0 0 1-.667-.667V7.501c0-.367.3-.667.667-.667.366 0 .666.3.666.667v2.666Zm1.334 2a.667.667 0 0 1-.667.668H3.666A.667.667 0 0 1 3 12.168V4.834h8v7.334ZM5.667 2.387c0-.104.142-.219.333-.219h2c.19 0 .333.115.333.219V3.5H5.666V2.386ZM13 3.5H9.666V2.386C9.666 1.53 8.92.834 8 .834H6c-.92 0-1.667.696-1.667 1.552v1.115H1c-.367 0-.667.3-.667.666 0 .367.3.667.667.667h.666v7.333c0 1.103.898 2 2 2h6.667c1.103 0 2-.897 2-2V4.834H13c.366 0 .666-.3.666-.667 0-.366-.3-.666-.666-.666Z"
        fill="#fff"
      />
    </G>
    <Defs>
      <ClipPath id="a">
        <Path fill="#fff" d="M0 0h14v15H0z" />
      </ClipPath>
    </Defs>
  </Svg>
);

export default DeleteProductIcon;

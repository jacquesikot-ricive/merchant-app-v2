import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const DownArrowIcon = (props: SvgProps) => (
  <Svg {...props} width={24} height={24} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M11 2a1 1 0 0 0-1 1v5.586L1.707.293A.999.999 0 1 0 .293 1.707L8.586 10H3a1 1 0 1 0 0 2h8a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1Z"
      fill="#636366"
    />
  </Svg>
);

export default DownArrowIcon;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const EditIconProduct = (props: SvgProps) => (
  <Svg {...props} width={11} height={15} fill="none">
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="m.833 11.5.06-.003 2.78-.253c.306-.028.592-.162.81-.38l5.997-5.997c.485-.487.465-1.298-.045-1.81L8.608 1.233C8.115.74 7.266.716 6.799 1.186L.8 7.184a1.317 1.317 0 0 0-.38.808l-.252 2.78a.666.666 0 0 0 .664.728ZM10.166 12.832H.833c-.367 0-.667.3-.667.667 0 .366.3.666.667.666h9.333c.367 0 .667-.3.667-.666 0-.367-.3-.667-.667-.667"
      fill="#fff"
    />
  </Svg>
);

export default EditIconProduct;

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function FloatingPlus(): JSX.Element {
  return (
    <Svg width={33} height={33} fill="none">
      <Path
        d="M24.032 14.69H17.36V8.02h-2.67v6.672H8.02v2.668h6.672v6.672h2.669V17.36h6.672v-2.668Z"
        fill="#fff"
      />
    </Svg>
  );
}

export default FloatingPlus;

import * as React from 'react';
import Svg, { SvgProps, Path } from 'react-native-svg';

const HeaderTextSearchIcon = (props: SvgProps) => (
  <Svg {...props} width={13} height={13} fill="none">
    <Path
      d="M5.666.667a5 5 0 0 1 3.976 8.032l3.163 3.163a.666.666 0 0 1-.88.998l-.063-.055-3.164-3.162A5 5 0 1 1 5.666.667Zm0 1.333a3.667 3.667 0 1 0 0 7.334 3.667 3.667 0 0 0 0-7.334Z"
      fill="#636366"
    />
  </Svg>
);

export default HeaderTextSearchIcon;

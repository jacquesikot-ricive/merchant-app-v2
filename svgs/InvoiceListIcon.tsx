import * as React from 'react';
import Svg, { SvgProps, Path, Circle } from 'react-native-svg';

const InvoiceListIcon = (props: SvgProps) => (
  <Svg {...props} width={61} height={60} fill="none">
    <Circle cx={30.5} cy={30} r={30} fill="#38CB89" fillOpacity={0.05} />
    <Path
      d="M28 27.333h8M28 32h8m4.667 10V20.667A2.667 2.667 0 0 0 38 18H22a2.667 2.667 0 0 0-2.667 2.667V42l5.334-2.667L30 42l5.333-2.667L40.667 42Z"
      stroke="#38CB89"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <Circle cx={23.333} cy={27.333} r={1.333} fill="#38CB89" />
    <Circle cx={23.333} cy={32.667} r={1.333} fill="#38CB89" />
  </Svg>
);

export default InvoiceListIcon;

import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function MenuIcon(): JSX.Element {
  return (
    <Svg width={24} height={24} fill="none">
      <Path
      d="M2 4a2 2 0 1 0-.001-4.001A2 2 0 0 0 2 4Zm0 3a2 2 0 1 0 .001 4.001A2 2 0 0 0 2 7Zm-2 9a2 2 0 1 1 4.001.001A2 2 0 0 1 0 16Z"
      fill="#636366"
      />
    </Svg>
  );
}

export default MenuIcon;

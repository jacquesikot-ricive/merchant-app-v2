import * as React from 'react';
import Svg, { Path } from 'react-native-svg';

function NotificationIcon(): JSX.Element {
  return (
    <Svg width={18} height={20} fill="none">
      <Path
      d="M11 16.341c0 .9-.916 1.66-2 1.66s-2-.76-2-1.66v-.34h4v.34Zm6.521-3.134-1.8-1.803V6.936C15.72 3.456 13.217.5 9.898.06a6.724 6.724 0 0 0-5.316 1.607A6.731 6.731 0 0 0 2.28 6.727v4.677l-1.8 1.804a1.63 1.63 0 0 0-.355 1.782C.38 15.603.973 16 1.637 16H5v.341c0 2.018 1.794 3.66 4 3.66s4-1.642 4-3.66v-.34h3.362a1.63 1.63 0 0 0 1.511-1.01 1.632 1.632 0 0 0-.352-1.784Z"
      fill="#908F8F"
      />
    </Svg>
  );
}

export default NotificationIcon;

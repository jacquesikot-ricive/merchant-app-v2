import * as React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';

function PlusCircleIcon(): JSX.Element {
  return (
    <Svg width={32} height={33} fill="none">
        <Path
            d="M20 17.833h-2.667V20.5c0 .733-.6 1.333-1.333 1.333s-1.333-.6-1.333-1.333v-2.667H12c-.733 0-1.333-.6-1.333-1.333s.6-1.334 1.333-1.334h2.667V12.5c0-.733.6-1.334 1.333-1.334s1.333.6 1.333 1.334v2.666H20c.733 0 1.333.6 1.333 1.334 0 .733-.6 1.333-1.333 1.333M16 3.167c-7.352 0-13.333 5.98-13.333 13.333 0 7.352 5.981 13.333 13.333 13.333S29.333 23.852 29.333 16.5 23.352 3.166 16 3.166"
            fill="#4E903C"
        />
    </Svg>
  );
}

export default PlusCircleIcon;

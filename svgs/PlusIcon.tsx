import * as React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';

function PlusIcon(): JSX.Element {
  return (
    <Svg width={32} height={32} fill="none">
        <Circle cx={16} cy={16} r={16} fill="#91CE33" fillOpacity={0.2} />
        <Path
        d="M22.322 15.095h-5.42V9.677a.903.903 0 1 0-1.806 0v5.42h-5.42a.903.903 0 1 0 0 1.806h5.42v5.42a.903.903 0 1 0 1.806 0v-5.42h5.42a.903.903 0 1 0 0-1.807"
        fill="#4E903C"
        />
    </Svg>
  );
}

export default PlusIcon;

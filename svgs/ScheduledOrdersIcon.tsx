import * as React from 'react';
import Svg, { Path, Circle, SvgProps, Defs, ClipPath, G } from 'react-native-svg';

function ScheduledOrdersIcon({ color }: SvgProps): JSX.Element {
  return (
    <Svg width={24} height={24} fill="none">
      <G clipPath="url(#a)" fill="#F9A31B">
        <Path d="M6.822 7.095 4.93 1.496H.031V0h5.973l2.235 6.616-1.417.48Z" />
        <Path
          opacity={0.4}
          d="M3.193 5.839h20.776l-3.96 11.11H6.653L3.193 5.84Z"
        />
        <Path d="M17.678 24a2.881 2.881 0 1 0 0-5.762 2.881 2.881 0 0 0 0 5.762ZM8.988 24a2.881 2.881 0 1 0 0-5.762 2.881 2.881 0 0 0 0 5.762Z" />
      </G>
      <Defs>
        <ClipPath id="a">
          <Path fill="#fff" d="M0 0h24v24H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );
}

export default ScheduledOrdersIcon;

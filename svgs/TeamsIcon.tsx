import * as React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';

function TeamsIcon(): JSX.Element {
  return (
    <Svg width={40} height={40} fill="none">
        <Circle cx={20} cy={20} r={20} fill="#F3FAF2" />
        <Path
            d="M17 21.75c-2.34 0-7 1.17-7 3.5V27h14v-1.75c0-2.33-4.66-3.5-7-3.5ZM12.34 25c.84-.58 2.87-1.25 4.66-1.25s3.82.67 4.66 1.25h-9.32ZM17 20c1.93 0 3.5-1.57 3.5-3.5S18.93 13 17 13s-3.5 1.57-3.5 3.5S15.07 20 17 20Zm0-5c.83 0 1.5.67 1.5 1.5S17.83 18 17 18s-1.5-.67-1.5-1.5.67-1.5 1.5-1.5Zm7.04 6.81c1.16.84 1.96 1.96 1.96 3.44V27h4v-1.75c0-2.02-3.5-3.17-5.96-3.44ZM23 20c1.93 0 3.5-1.57 3.5-3.5S24.93 13 23 13c-.54 0-1.04.13-1.5.35.63.89 1 1.98 1 3.15s-.37 2.26-1 3.15c.46.22.96.35 1.5.35Z"
            fill="#4E903C"
        />
    </Svg>
  );
}

export default TeamsIcon;
